package shengdong.data.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import shengdong.data.entity.Bt13Role;

import java.util.List;

/**
 * 角色表Mapper映射方法接口
 */
@Mapper
@Component
public interface Bt13RoleMapper {

    /**
     * 根据id删除角色表数据
     *
     * @param id 自增主键
     * @return 删除行数
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 新增角色表数据
     *
     * @param record 角色实体类
     * @return 新增行数
     */
    int insert(Bt13Role record);

    /**
     * 新增角色表数据(根据一定规则)
     *
     * @param record 角色实体类
     * @return 新增行数
     */
    int insertSelective(Bt13Role record);

    /**
     * 根据id查询角色表数据
     *
     * @param id 角色实体类
     * @return 角色实体类
     */
    Bt13Role selectByPrimaryKey(Long id);

    /**
     * 更新角色表数据(根据一定规则)
     *
     * @param record 角色实体类
     * @return 更新行数
     */
    int updateByPrimaryKeySelective(Bt13Role record);

    /**
     * 更新角色表数据
     *
     * @param record 角色实体类
     * @return 更新行数
     */
    int updateByPrimaryKey(Bt13Role record);

    Bt13Role getRoleByUid(String uid);

    void truncateTable();

    List<Bt13Role> selectAll();
}