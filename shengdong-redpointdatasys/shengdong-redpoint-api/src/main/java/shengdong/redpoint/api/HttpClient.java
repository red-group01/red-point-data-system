package shengdong.redpoint.api;

import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import shengdong.common.error.impl.BaseRuntimeException;
import shengdong.redpoint.httpentity.response.ResponsePositionHistory;
import shengdong.redpoint.model.query.HttpMethod;
import shengdong.redpoint.model.query.HttpParam;

import javax.net.ssl.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Slf4j
class HttpClient {
    public static String ExecApi(HttpParam param) throws Exception {
        OkHttpClient client = getHttpClient();
        Request.Builder builder = new Request.Builder().addHeader("content-type", "application/json");

        //追加固定头部
        addfixedheader(builder, param);
        if (param.getMethod() == HttpMethod.GET) {
            String params = "?";
            //检查get参数并拼接
            if (param.getOtherHeaderParam() != null) {
                for (String k : param.getGetParam().keySet()) {
                    String v = param.getGetParam().get(k);
                    if (params.length() > 2) {
                        params += String.format("&{0}={1}", k, v);
                    } else {
                        params += String.format("{0}={1}", k, v);
                    }
                }
            }
            //如果没有参数则清空
            if (params.length() == 1) {
                params = "";
            }
            builder.url(param.getUrl() + params).get();
        } else if (param.getMethod() == HttpMethod.POST) {
            builder.url(param.getUrl()).post(param.getPostBody());
        } else if (param.getMethod() == HttpMethod.PUT) {
            builder.url(param.getUrl()).put(param.getPostBody());
        } else if (param.getMethod() == HttpMethod.DELETE) {
            builder.url(param.getUrl()).delete(param.getPostBody());
        } else if (param.getMethod() == HttpMethod.PATCH) {
            builder.url(param.getUrl()).patch(param.getPostBody());
        } else {
            throw new UnsupportedOperationException();
        }

        Request request = builder.build();
        Response response = client.newCall(request).execute();
        String res = response.body().string();
        response.body().close();
        response.close();
        if (response.code() != 200) {
            //throw new BaseRuntimeException(String.valueOf(response.code()), res);
            log.warn(res);
            res = "{}";
        }
        return res;
    }

    /**
     * 下载文件
     *
     * @param savePath 存储路径
     * @param param
     * @return
     * @throws IOException
     */
    public static ResponsePositionHistory downLoadFile(String savePath, HttpParam param) throws IOException {
        OkHttpClient client = getHttpClient();
        Request.Builder builder = new Request.Builder();

        //追加固定头部
        addfixedheader(builder, param);
        File file = null;
        Path p = Path.of(savePath);
        if (!Files.exists(p)) {
            Files.createDirectory(p);
        }
        //发起请求
        builder.url(param.getUrl()).get();
        Response response = client.newCall(builder.build()).execute();
        String page = response.header("content-next-positions-page");
        if (response.code() != 200) {
            String res = response.body().string();
            throw new BaseRuntimeException(String.valueOf(response.code()), res);
        }
        ResponseBody result = response.body();
        ResponsePositionHistory rph = null;
        //如果响应正常下载，则page不为null 为null则表示请求下载失败
        if (page != null) {
            rph = new ResponsePositionHistory();
            //获取待下载文件总长度 只有服务器发送了header才能取到，不发送则取不到
            long total = result.contentLength();
            //准备下载
            String filename = getHeaderFileName(response);
            //文件名前增加uuid防止文件重名
            file = new File(savePath, UUID.randomUUID().toString() + filename);
            InputStream is = result.byteStream();

            //下载缓冲区
            byte[] buf = new byte[2048];
            //实际读取数据库长度
            int len = 0;
            //执行文件下载
            FileOutputStream fos = new FileOutputStream(file);
            while ((len = is.read(buf)) != -1) {
                fos.write(buf, 0, len);
                //sum += len;
                //下载进度百分比计算
                //int progress = (int)(sum * 1.0f / total * 100);
            }
            fos.close();
            rph.setFile(file);
            rph.setPage(Integer.valueOf(page));
        }
        response.body().close();
        response.close();
        return rph;
    }

    /**
     * 获取忽略可信验证的httpclient
     *
     * @return
     */
    private static OkHttpClient getHttpClient() {
//        OkHttpClient client = new OkHttpClient().newBuilder().hostnameVerifier(new HostnameVerifier() {
//            @Override
//            public boolean verify(String hostname, SSLSession session) {
//                //强行返回true 即验证成功
//                return true;
//            }
//        }).readTimeout(60, TimeUnit.SECONDS).build();//getSSLClient();// new OkHttpClient();
//        return client;
        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .sslSocketFactory(SSLSocketClient.getSSLSocketFactory(), SSLSocketClient.getX509TrustManager())
                .hostnameVerifier(SSLSocketClient.getHostnameVerifier())
                .build();
        return client;
    }

    /**
     * 固定头部添加
     *
     * @param builder
     * @param param
     * @return
     */
    static Request.Builder addfixedheader(Request.Builder builder, HttpParam param) {
        builder.addHeader("X-User-Email", param.getHeader().getEmail())
                .addHeader("X-User-Token", param.getHeader().getToken())
                .addHeader("X-User-Project", param.getHeader().getProject());
        return builder;
    }

    /**
     * 忽略证书 忽略域名验证
     *
     * @return
     * @throws NoSuchAlgorithmException
     * @throws KeyManagementException
     * @throws NoSuchProviderException
     */
    static OkHttpClient getSSLClient()
            throws NoSuchAlgorithmException, KeyManagementException, NoSuchProviderException {
        Provider[] ps = Security.getProviders();
        //        for (Provider i : ps) {
        //            try {
        //
        //                SSLContext.getInstance("SSL", i.getName());
        //                System.out.println("client = " + i.getName());
        //            } catch (Exception e) {
        //
        //            }
        //
        //        }
        final SSLContext sslContext = SSLContext.getInstance("SSL", "SunJSSE");
        final TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            @Override
            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            }

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }

            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            }
        }};

        sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
        final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

        return new OkHttpClient().newBuilder().sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0])
                .hostnameVerifier(new HostnameVerifier() {

                    @Override
                    public boolean verify(String hostname, SSLSession session) {
                        //强行返回true 即验证成功
                        return true;
                    }
                }).build();
    }

    /**
     * 解析文件头 Content-Disposition:attachment;filename=FileName.txt Content-Disposition: attachment;*
     * filename*="UTF-8''%E6%9B%BF%E6%8D%A2%E5%AE%9E%E9%AA%8C%E6%8A%A5%E5%91%8A.pdf"
     */
    private static String getHeaderFileName(Response response) {
        String dispositionHeader = response.header("Content-Disposition");
        if (!Strings.isNullOrEmpty(dispositionHeader)) {
            dispositionHeader.replace("attachment;filename=", "");
            dispositionHeader.replace("filename*=utf-8", "");
            String[] strings = dispositionHeader.split("; ");
            if (strings.length > 1) {
                dispositionHeader = strings[1].replace("filename=", "");
                dispositionHeader = dispositionHeader.replace("\"", "");
                return dispositionHeader;
            }
            return "";
        }
        return "";
    }
}
