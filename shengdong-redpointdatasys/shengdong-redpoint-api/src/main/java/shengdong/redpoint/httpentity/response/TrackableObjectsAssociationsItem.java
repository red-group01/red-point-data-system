package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class TrackableObjectsAssociationsItem {
    private String uid;
    @JsonProperty("removed_at")
    private String removedAt;
    @JsonProperty("mac_address")
    private String macAddress;
    @JsonProperty("trob_uid")
    private String trobUid;
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("company_uid")
    private String companyUid;
    @JsonProperty("obj_uid")
    private String objUid;
    @JsonProperty("project_uid")
    private String projectUid;
}