package shengdong.redpoint.httpentity.response;

import lombok.Data;

import java.io.File;

@Data
public class ResponsePositionHistory {
    /**
     * 下载的文件对象 没有下载到文件则为null
     */
    File file;
    /**
     * 后续数据分页
     */
    Integer page;
}
