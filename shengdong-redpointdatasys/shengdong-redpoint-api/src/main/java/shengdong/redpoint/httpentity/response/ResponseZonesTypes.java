package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class ResponseZonesTypes {
    @JsonProperty("zone_types")
    private List<ZoneTypesItem> zoneTypes;
}