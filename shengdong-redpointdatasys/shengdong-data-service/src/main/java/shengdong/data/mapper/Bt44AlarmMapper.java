package shengdong.data.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import shengdong.data.entity.Bt44Alarm;

import java.util.Date;
import java.util.List;

/**
 * 设备告警表Mapper映射方法接口
 */
@Mapper
@Component
public interface Bt44AlarmMapper {

    /**
     * 根据id删除设备告警表数据
     *
     * @param id 自增主键
     * @return 删除行数
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 新增设备告警表数据
     *
     * @param record 设备告警实体类
     * @return 新增行数
     */
    int insert(Bt44Alarm record);

    /**
     * 新增设备告警表数据(根据一定规则)
     *
     * @param record 设备告警实体类
     * @return 新增行数
     */
    int insertSelective(Bt44Alarm record);

    /**
     * 根据id查询设备告警表数据
     *
     * @param id 设备告警id
     * @return 设备告警实体类
     */
    Bt44Alarm selectByPrimaryKey(Long id);

    /**
     * 更新设备告警表数据(根据一定规则)
     *
     * @param record 设备告警实体类
     * @return 更新行数
     */
    int updateByPrimaryKeySelective(Bt44Alarm record);

    /**
     * 更新设备告警表数据
     *
     * @param record 设备告警实体类
     * @return 更新行数
     */
    int updateByPrimaryKey(Bt44Alarm record);

    void truncateTable();

    List<Bt44Alarm> selectAllAlarm(String puid);

    /**
     * 根据地点子区域uid查询设备告警表数据
     *
     * @param sublocationUid 地点子区域uid
     * @return 设备告警实体类列表
     */
    List<Bt44Alarm> selectBySublocationUid(String sublocationUid, String createdAtBegin, String createdAtEnd);

    /**
     * 根据标签uid和报警时间查询设备告警表数据
     *
     * @param bt44Alarm
     * @return 设备告警实体类列表
     */
    List<Bt44Alarm> selectByObjAndTime(Bt44Alarm bt44Alarm);
}