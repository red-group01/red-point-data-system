package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class TrackablesItem {
    private String uid;
    @JsonProperty("driver_uid")
    private Object driverUid;
    private int x;
    private int y;
    @JsonProperty("drived_uid")
    private Object drivedUid;
}