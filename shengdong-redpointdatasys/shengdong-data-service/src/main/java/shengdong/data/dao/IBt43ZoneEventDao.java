package shengdong.data.dao;

import shengdong.data.entity.Bt43ZoneEvent;

import java.util.Date;
import java.util.List;

/**
 * 区域事件Dao接口
 */
public interface IBt43ZoneEventDao {
    /**
     * 插入区域事件数据
     *
     * @param record 区域事件实体类
     * @return 插入的行数
     */
    public int insert(Bt43ZoneEvent record);

    Date getProjectLastTime(String project);

    void truncateTable();

    List<Bt43ZoneEvent> getAllZoneEvent(String puid);
}
