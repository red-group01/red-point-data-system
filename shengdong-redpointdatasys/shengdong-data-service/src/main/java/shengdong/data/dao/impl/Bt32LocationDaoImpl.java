package shengdong.data.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import shengdong.data.dao.IBt32LocationDao;
import shengdong.data.entity.Bt32Location;
import shengdong.data.mapper.Bt32LocationMapper;

import java.util.List;

/**
 * 项目地点Dao实现
 */
@Repository
public class Bt32LocationDaoImpl implements IBt32LocationDao {
    // 注入Mapper接口
    @Autowired
    private Bt32LocationMapper bt32LocationMapper;

    /**
     * 插入项目地点数据
     *
     * @param record 项目地点实体类
     * @return 插入的行数
     */
    @Override
    public int insert(Bt32Location record) {
        return bt32LocationMapper.insert(record);
    }

    @Override
    public List<Bt32Location> getLocationByUserId(Long id) {
        return bt32LocationMapper.selectByUserId(id);
    }

    @Override
    public int updateByid(Bt32Location local) {
        return bt32LocationMapper.updateByPrimaryKey(local);
    }

    @Override
    public List<Bt32Location> getAllLocation() {
        return bt32LocationMapper.selectAllLocation();
    }

    @Override
    public void truncateTable() {
        bt32LocationMapper.truncateTable();
    }
}
