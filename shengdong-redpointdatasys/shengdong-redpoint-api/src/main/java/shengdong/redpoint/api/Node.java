package shengdong.redpoint.api;

import lombok.Data;

@Data
public class Node{
	private String uid;
	private String macAddress;
}