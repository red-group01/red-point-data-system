package shengdong.redpoint.websockentity;

import lombok.Data;

@Data
public class CreatedBy{
	private String uid;
	private String email;
}