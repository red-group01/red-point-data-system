package shengdong.redpoint.httpentity.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class RequestTrackable {
    @JsonProperty("default_height")
    private int defaultHeight;
    @JsonProperty("position_update_interval")
    private int positionUpdateInterval;
    @JsonProperty("bt_config")
    private Object btConfig;
    private List<Object> groups;
    @JsonProperty("project_uid")
    private String projectUid;
    @JsonProperty("display_name")
    private Object displayName;
    private String type;
    @JsonProperty("acc_config")
    private Object accConfig;
    @JsonProperty("sensor_config")
    private SensorConfig sensorConfig;
    private Node node;
    @JsonProperty("pos_report_frequency")
    private Object posReportFrequency;
    @JsonProperty("custom_info")
    private String customInfo;
    private String name;
    @JsonProperty("company_uid")
    private String companyUid;
    @JsonProperty("self_zone_uid")
    private Object selfZoneUid;
    @JsonProperty("tag_config")
    private TagConfig tagConfig;
    private Position position;
    private String category;
    private Buffer buffer;

}