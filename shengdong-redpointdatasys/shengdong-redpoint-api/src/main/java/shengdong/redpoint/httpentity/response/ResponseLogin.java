package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 登录接口响应结构
 */
@Data
public class ResponseLogin implements Serializable {
    /**
     * 接口响应数据
     */
    @JsonProperty("data")
    private LoginData data;
    /**
     * 接口调用状态 true 成功 false 失败
     */
    private boolean success;
}
