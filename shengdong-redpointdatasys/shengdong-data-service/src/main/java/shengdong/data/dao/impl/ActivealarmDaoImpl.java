package shengdong.data.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import shengdong.data.dao.IActivealarmDao;
import shengdong.data.entity.Activealarm;
import shengdong.data.mapper.ActivealarmMapper;

import java.util.List;

@Repository
public class ActivealarmDaoImpl implements IActivealarmDao {
    @Autowired
    private ActivealarmMapper activealarmMapper;

    @Override
    public int insertSelective(Activealarm record) {
        return activealarmMapper.insertSelective(record);
    }

    @Override
    public int deleteByUserId(Long userId) {
        return activealarmMapper.deleteByUserId(userId);
    }

    @Override
    public List<Activealarm> selectBySubUid(String sublocationUid) {
        return activealarmMapper.selectBySubUid(sublocationUid);
    }
}
