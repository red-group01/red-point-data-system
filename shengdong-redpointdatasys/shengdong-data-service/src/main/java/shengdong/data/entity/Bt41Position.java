package shengdong.data.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 位置历史信息实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Bt41Position {

    //The identifier of the position report in the server
    private Long id;

    //公司唯一标识
    private String companyUid;

    //项目唯一标识
    private String projectUid;

    //项目地点唯一标识
    private String locationUid;

    //地点子区域唯一标识
    private String sublocationUid;

    //区域围栏唯一标识
    private String zoneUid;

    //The MAC address of the node
    private String macAddress;

    //The unique identifier of the trackable
    private String objUid;

    //The type of position report(UWB ，HB)
    private String type;

    //The time stamp of the position report in seconds
    private Date ts;

    //The milliseconds part of the time stamp
    private Long ms;

    //The x coordinate of the position
    private Long x;

    //The y coordinate of the position
    private Long y;

    //The z coordinate of the position
    private Long z;

    //位置历史信息的服务器标识id
    private Long positionId;

    //创建时间(UTC)
    private Date createdAt;

    //更新时间(UTC)
    private Date updatedAt;

    //数据最后更新者
    private String creatorAt;

    //数据信息备注
    private String remark;

}