package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class Conditions {
    @JsonProperty("alarm_type_list")
    private List<String> alarmTypeList;
    @JsonProperty("zone_severity_level")
    private int zoneSeverityLevel;
}