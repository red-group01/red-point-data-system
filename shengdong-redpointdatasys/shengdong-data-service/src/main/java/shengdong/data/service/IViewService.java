package shengdong.data.service;

import shengdong.data.entity.Activealarm;
import shengdong.data.entity.Bt44Alarm;
import shengdong.data.entity.view.*;

import java.util.List;

public interface IViewService {
    List<OutAlarmListView> getAlarmListView(String sublocationuid);

    List<OutNodeListView> getNodeListView(String sublocationuid);

    List<OutZoneListView> getZoneListView(String sublocationuid);

    List<OutNodeStateListView> getNodeStateListView(String sublocationuid);

    OutNodeCountView getNodeCountListView(String sublocationuid);

    OutHotImageView getHotImgView(String sublocationuid);

    List<OutBaseInfoView> getBaseInfoView();

    OutHisDataView getHisDataView(String sublocationuid, String begintime, String endtime, List<String> objUids);

    OutHisDataView getHisDataView2(String begintime, String endtime, List<String> objUids);

    List<OutCameraConfigView> getCameraConfigView();

    List<Activealarm> getAlarmActiveView(String sublocationuid);
}
