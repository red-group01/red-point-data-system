package shengdong.redpoint.httpentity.response;

import lombok.Data;

@Data
public class Battery {
    private String pr;
    private String mv;
}