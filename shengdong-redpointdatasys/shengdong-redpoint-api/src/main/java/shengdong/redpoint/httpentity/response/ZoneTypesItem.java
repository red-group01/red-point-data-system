package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ZoneTypesItem {
    @JsonProperty("updated_at")
    private String updatedAt;
    private String name;
    @JsonProperty("created_at")
    private String createdAt;
    private int id;
    @JsonProperty("display_name")
    private String displayName;
}