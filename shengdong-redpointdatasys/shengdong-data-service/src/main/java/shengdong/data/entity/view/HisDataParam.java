package shengdong.data.entity.view;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HisDataParam {
    List<String> uid;
    String sublocationuid;
    String begintime;
    String endtime;
}
