package shengdong.data.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import shengdong.data.dao.IBt34ZoneDao;
import shengdong.data.entity.Bt34Zone;
import shengdong.data.mapper.Bt34ZoneMapper;

import java.util.List;

/**
 * 区域围栏Dao实现
 */
@Repository
public class Bt34ZoneDaoImpl implements IBt34ZoneDao {
    // 注入Mapper接口
    @Autowired
    private Bt34ZoneMapper bt34ZoneMapper;

    /**
     * 插入区域围栏数据
     *
     * @param record 区域围栏实体类
     * @return 插入的行数
     */

    @Override
    public int insert(Bt34Zone record) {
        return bt34ZoneMapper.insert(record);
    }

    @Override
    public List<Bt34Zone> getUserZone(Long id) {
        return bt34ZoneMapper.selectByUserId(id);
    }

    @Override
    public int updateByid(Bt34Zone local) {
        return bt34ZoneMapper.updateByPrimaryKeySelective(local);
    }

    @Override
    public List<Bt34Zone> getAllZone() {
        return bt34ZoneMapper.selectAllZone();
    }

    @Override
    public void truncateTable() {
        bt34ZoneMapper.truncateTable();
    }

    @Override
    public List<Bt34Zone> selectBySublocationUid(String sublocationUid) {
        return bt34ZoneMapper.selectBySublocationUid(sublocationUid);
    }

    @Override
    public List<Bt34Zone> selectInTrackableUid(List<String> trackableUids) {
        return bt34ZoneMapper.selectInTrackableUid(trackableUids);
    }
}
