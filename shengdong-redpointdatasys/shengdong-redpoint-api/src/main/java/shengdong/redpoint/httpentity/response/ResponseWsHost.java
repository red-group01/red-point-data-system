package shengdong.redpoint.httpentity.response;

import lombok.Data;

@Data
public class ResponseWsHost{
	private String wsHost;
}