package shengdong.redpoint.httpentity.response;

import lombok.Data;

import java.io.Serializable;

@Data
public class LoginData implements Serializable {
    private boolean isCompanyAdmin;
    private boolean isSystemUser;
    private Company company;
    private boolean isSuperAdmin;
    private int signInCount;
    private String email;
    private String token;
}
