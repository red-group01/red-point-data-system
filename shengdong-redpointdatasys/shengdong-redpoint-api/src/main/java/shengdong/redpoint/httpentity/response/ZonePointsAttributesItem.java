package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ZonePointsAttributesItem {
    @JsonProperty("zone_id")
    private int zoneId;
    @JsonProperty("position_x")
    private int positionX;
    @JsonProperty("position_y")
    private int positionY;
    @JsonProperty("position_z")
    private Object positionZ;
    private int id;
}