package shengdong.redpoint.websockentity;

import lombok.Data;

@Data
public class RequestGet {
    private String method;
    private int id;
    private String jsonrpc;
    private RequestGetParams params;
}