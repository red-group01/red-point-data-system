package shengdong.data.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import shengdong.data.entity.BaseNodeType;
import shengdong.data.entity.Bt22Anchor;
import shengdong.data.entity.Bt31Project;

import java.util.List;

/**
 * 锚节点表Mapper映射方法接口
 */
@Mapper
@Component
public interface Bt22AnchorMapper {

    /**
     * 根据id删除锚节点表数据
     *
     * @param id 自增主键
     * @return 删除行数
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 新增锚节点表数据
     *
     * @param record 锚节点类
     * @return 新增行数
     */
    int insert(Bt22Anchor record);

    /**
     * 新增锚节点表数据
     *
     * @param record 锚节点类
     * @return 新增行数
     */
    int insert(Bt31Project record);

    /**
     * 新增锚节点表数据(根据一定规则)
     *
     * @param record 锚节点实体类
     * @return 新增行数
     */
    int insertSelective(Bt22Anchor record);

    /**
     * 根据id查询锚节点表数据
     *
     * @param id 锚节点实体类
     * @return 锚节点实体类
     */
    Bt22Anchor selectByPrimaryKey(Long id);

    /**
     * 更新锚节点表数据(根据一定规则)
     *
     * @param record 锚节点实体类
     * @return 更新行数
     */
    int updateByPrimaryKeySelective(Bt22Anchor record);

    /**
     * 更新锚节点表数据
     *
     * @param record 锚节点实体类
     * @return 更新行数
     */
    int updateByPrimaryKey(Bt22Anchor record);

    List<Bt22Anchor> selectByUserId(Long id);

    List<Bt22Anchor> selectAllAnchor();

    void truncateTable();

    /**
     * 根据地点子区域uid查询锚节点表数据
     *
     * @param sublocationUid 地点子区域uid
     * @return 锚节点实体类
     */
    List<Bt22Anchor> selectBySublocationUid(String sublocationUid);

    /**
     * 根据地点子区域uid查询锚节点表数据的类别分组
     *
     * @param sublocationUid 地点子区域uid
     * @return 标签类别实体类
     */
    List<BaseNodeType> selectBySublocationUidGroupByType(String sublocationUid);
}