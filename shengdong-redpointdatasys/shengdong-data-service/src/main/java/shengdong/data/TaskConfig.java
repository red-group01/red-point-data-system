package shengdong.data;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.scheduling.config.Task;

@Configuration
@EnableScheduling
@ComponentScan(basePackages = {"shengdong.data.scheduledtask"})
public class TaskConfig implements SchedulingConfigurer {
    /**
     * Callback allowing a {@link TaskScheduler TaskScheduler} and specific {@link Task Task} instances to be registered
     * against the given the {@link ScheduledTaskRegistrar}.
     *
     * @param taskRegistrar the registrar to be configured.
     */
    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {

    }
}
