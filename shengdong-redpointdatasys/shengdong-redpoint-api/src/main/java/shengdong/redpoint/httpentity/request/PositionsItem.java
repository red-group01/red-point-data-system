package shengdong.redpoint.httpentity.request;

import lombok.Data;

@Data
public class PositionsItem{
	private String x;
	private String y;
}