package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class LocationServiceCoverageItem {
    private String uid;
    @JsonProperty("sublocation_uid")
    private String sublocationUid;
    private boolean exclusion;
    private Geometry geometry;
    private String type;
}