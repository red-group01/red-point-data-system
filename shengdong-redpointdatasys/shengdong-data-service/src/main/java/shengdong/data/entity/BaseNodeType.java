package shengdong.data.entity;

import lombok.Data;

/**
 * 标签类别实体类
 */
@Data
public class BaseNodeType {
    String nodeType;
}
