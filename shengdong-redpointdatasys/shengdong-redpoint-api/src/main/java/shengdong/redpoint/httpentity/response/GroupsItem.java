package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class GroupsItem {
    private String uid;
    @JsonProperty("sensor_config")
    private SensorConfig sensorConfig;
    @JsonProperty("removed_at")
    private String removedAt;
    @JsonProperty("updated_at")
    private String updatedAt;
    @JsonProperty("custom_info")
    private String customInfo;
    private String name;
    private String color;
    @JsonProperty("company_uid")
    private String companyUid;
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("project_uid")
    private String projectUid;
    @JsonProperty("tag_config")
    private TagConfig tagConfig;
    @JsonProperty("user_params")
    private UserParams userParams;
}