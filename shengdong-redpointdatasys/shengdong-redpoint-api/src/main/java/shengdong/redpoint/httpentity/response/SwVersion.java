package shengdong.redpoint.httpentity.response;

import lombok.Data;

@Data
public class SwVersion{
	private Object swStr;
	private String git;
}