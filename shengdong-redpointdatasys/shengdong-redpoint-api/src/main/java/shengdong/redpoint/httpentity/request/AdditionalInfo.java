package shengdong.redpoint.httpentity.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class AdditionalInfo {
    @JsonProperty("zone_uid")
    private String zoneUid;
    @JsonProperty("zone_alarm_level")
    private int zoneAlarmLevel;
}