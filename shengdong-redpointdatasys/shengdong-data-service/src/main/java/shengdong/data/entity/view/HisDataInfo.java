package shengdong.data.entity.view;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HisDataInfo {
    private String name;
    private String uid;
    private List<HisData> hisDataList = new ArrayList<>();
}
