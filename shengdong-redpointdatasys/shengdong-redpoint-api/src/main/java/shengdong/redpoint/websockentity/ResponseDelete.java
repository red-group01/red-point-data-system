package shengdong.redpoint.websockentity;

import java.util.List;
import lombok.Data;

@Data
public class ResponseDelete{
	private List<String> result;
	private int id;
	private String jsonrpc;
}