package shengdong.redpoint.httpentity.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class RequestFloorplan {
    private Image image;
    @JsonProperty("sublocation_uid")
    private String sublocationUid;
    @JsonProperty("image_offset_x")
    private int imageOffsetX;
    @JsonProperty("image_offset_y")
    private int imageOffsetY;
    @JsonProperty("image_scale")
    private int imageScale;
    @JsonProperty("display_name")
    private String displayName;
    @JsonProperty("image_rotation")
    private int imageRotation;
}