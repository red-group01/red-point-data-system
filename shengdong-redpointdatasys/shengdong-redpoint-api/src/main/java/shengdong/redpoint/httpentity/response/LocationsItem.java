package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class LocationsItem {
    private int elevation;
    private String zip;
    @JsonProperty("street_address")
    private String streetAddress;
    private String country;
    private long orientation;
    private String city;
    private String latitude;
    private List<ShiftItem> shift;
    private String description;
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("project_uid")
    private String projectUid;
    private String zip4;
    private String uid;
    @JsonProperty("updated_at")
    private String updatedAt;
    private String name;
    @JsonProperty("street_address2")
    private String streetAddress2;
    private String state;
    @JsonProperty("user_params")
    private UserParams userParams;
    private String longitude;
}