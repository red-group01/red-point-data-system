package shengdong.data.iotcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import shengdong.common.error.impl.BaseResponse;
import shengdong.data.service.IBaseInfoService;
import shengdong.data.service.IDataService;

@RestController
@RequestMapping("/iot/syninfo")
public class SynController {

    @Autowired
    private IBaseInfoService bt14UserService;
    @Autowired
    private IDataService dataService;
    @Autowired
    private IBaseInfoService infoService;

    /**
     * 同步用户数据
     *
     * @return
     */
    @GetMapping("/syn")
    public BaseResponse<String> syn() throws Exception {
        //         bt14UserService.synAll();
        dataService.synAll();
        //        return BaseResponse.success("1");
        return BaseResponse.success("同步成功");
    }

    /**
     * 读取与用户有关的全部基础数据
     *
     * @param uid
     * @return
     */
    @GetMapping("/getAllBaseInfo")
    public BaseResponse<String> getAllBasInfo(String uid) {
        //        boolean result = bt14UserService.synAll();
        //        return BaseResponse.success("同步" + (result ? "成功" : "失败"));
        return BaseResponse.success("ok");
    }

    @GetMapping("/getdata")
    public BaseResponse<String> getData() throws Exception {
        dataService.synAll();
        return BaseResponse.success("ok");
    }
}
