package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class SublocationsItem {
    private String uid;
    @JsonProperty("updated_at")
    private String updatedAt;
    private String name;
    @JsonProperty("description")
    private String description;
    @JsonProperty("created_at")
    private String createdAt;
    private long id;
    @JsonProperty("offset_x")
    private long offsetX;
    @JsonProperty("offset_y")
    private long offsetY;
    @JsonProperty("location_uid")
    private String locationUid;
    @JsonProperty("offset_z")
    private long offsetZ;
    @JsonProperty("project_name")

    private String projectName;
    @JsonProperty("project_uid")
    private String projectUid;
    @JsonProperty("company_uid")
    private String companyUid;
}