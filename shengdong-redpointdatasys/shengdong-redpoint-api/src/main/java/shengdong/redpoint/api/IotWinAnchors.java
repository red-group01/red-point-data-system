package shengdong.redpoint.api;

import com.google.common.base.Strings;
import shengdong.common.util.JSONUtil;
import shengdong.redpoint.httpentity.request.RequestAnchorBridge;
import shengdong.redpoint.httpentity.request.RequestWinAnchor;
import shengdong.redpoint.httpentity.response.ResponseWinAnchor;
import shengdong.redpoint.httpentity.response.ResponseWinAnchorAssociations;
import shengdong.redpoint.httpentity.response.WinAnchorAssociationsItem;
import shengdong.redpoint.httpentity.response.WinAnchorsItem;
import shengdong.redpoint.model.query.Header;
import shengdong.redpoint.model.query.HttpMethod;
import shengdong.redpoint.model.query.HttpParam;

public class IotWinAnchors extends IotApiBase {

    public IotWinAnchors(IotApiConfig cnf, Header httpheader) {
        super(cnf, httpheader);
    }

    /**
     * 获取全部锚点
     *
     * @return
     * @throws Exception
     */
    public ResponseWinAnchor getWinAnchors() throws Exception {
        return getSingleWinAnchor("");
    }

    /**
     * 获取指定锚点
     *
     * @param uid
     * @return
     * @throws Exception
     */
    public ResponseWinAnchor getSingleWinAnchor(String uid) throws Exception {
        HttpParam hp = new HttpParam();
        String url = config.getDomen();
        if (!Strings.isNullOrEmpty(uid)) {
            url += config.getWinAnchorsApiGetSingleWinAnchor() + uid;
        } else {
            url += config.getWinAnchorsApiGetWinAnchors();
        }
        hp.setUrl(url);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.GET);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseWinAnchor.class);
    }

    /**
     * 创建锚点
     *
     * @param req
     * @return
     * @throws Exception
     */
    public WinAnchorsItem createWinAnchors(RequestWinAnchor req) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getWinAnchorsApiCreateWinAnchors());
        hp.setHeader(header);
        hp.setPostParam(JSONUtil.getObjectToString(req));
        hp.setMethod(HttpMethod.POST);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), WinAnchorsItem.class);
    }

    /**
     * 修改锚点
     *
     * @param req
     * @param uid
     * @return
     * @throws Exception
     */
    public WinAnchorsItem editWinAnchors(RequestWinAnchor req, String uid) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getWinAnchorsApiEditWinAnchors() + uid);
        hp.setHeader(header);
        hp.setPostParam(JSONUtil.getObjectToString(req));
        hp.setMethod(HttpMethod.PUT);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), WinAnchorsItem.class);
    }

    /**
     * 删除锚点
     *
     * @param uid
     * @return
     * @throws Exception
     */
    public String deleteAnchor(String uid) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getWinAnchorsApiDeleteAnchor() + uid);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.DELETE);
        String msg = HttpClient.ExecApi(hp);
        return msg;
    }

    /**
     * 只获取当前的关联，而不获取任何已关闭的过去关联
     *
     * @return
     * @throws Exception
     */
    public ResponseWinAnchorAssociations getActiveAssociation() throws Exception {
        HttpParam hp = new HttpParam();
        String url = config.getDomen() + config.getWinAnchorsApiGetActiveAssociation();
        hp.setUrl(url);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.GET);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseWinAnchorAssociations.class);
    }

    /**
     * 获得所有活跃和非活跃的关联
     *
     * @return
     * @throws Exception
     */
    public ResponseWinAnchorAssociations getAllAssociations() throws Exception {
        HttpParam hp = new HttpParam();
        String url = config.getDomen() + config.getWinAnchorsApiGetAllAssociations();
        hp.setUrl(url);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.GET);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseWinAnchorAssociations.class);
    }

    /**
     * 为锚或桥创建一个关联
     *
     * @param req
     * @return
     * @throws Exception
     */
    public WinAnchorAssociationsItem createAssociation(RequestAnchorBridge req) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getWinAnchorsApiCreateAssociation());
        hp.setHeader(header);
        hp.setPostParam(JSONUtil.getObjectToString(req));
        hp.setMethod(HttpMethod.POST);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), WinAnchorAssociationsItem.class);
    }

    /**
     * 使用指定的锚关联UID编辑锚关联
     *
     * @param req
     * @param uid
     * @return
     * @throws Exception
     */
    public WinAnchorAssociationsItem editAssociation(RequestAnchorBridge req, String uid) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getWinAnchorsApiEditWinAnchors() + uid);
        hp.setHeader(header);
        hp.setPostParam(JSONUtil.getObjectToString(req));
        hp.setMethod(HttpMethod.PUT);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), WinAnchorAssociationsItem.class);
    }

    /**
     * 通过win_uid或mac_address关闭关联。成功的请求返回状态200
     *
     * @param win_uid
     * @return
     * @throws Exception
     */
    public WinAnchorAssociationsItem closeAssociation(String win_uid) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getWinAnchorsApiCloseAssociation() + win_uid);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.PUT);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), WinAnchorAssociationsItem.class);
    }

    /**
     * 删除具有指定锚点关联UID的锚点关联。检索WIN锚关联UID
     *
     * @param uid
     * @return
     * @throws Exception
     */
    public String deleteAssociation(String uid) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getWinAnchorsApiDeleteAnchor() + uid);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.DELETE);
        String msg = HttpClient.ExecApi(hp);
        return msg;
    }
}
