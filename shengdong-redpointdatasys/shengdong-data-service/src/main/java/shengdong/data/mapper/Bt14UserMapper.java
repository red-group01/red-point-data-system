package shengdong.data.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import shengdong.data.entity.Bt14User;

import java.util.List;

/**
 * 用户表Mapper映射方法接口
 */
@Mapper
@Component
public interface Bt14UserMapper {

    /**
     * 根据id删除用户表数据
     *
     * @param id 自增主键
     * @return 删除行数
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 新增用户表数据
     *
     * @param record 用户实体类
     * @return 新增行数
     */
    int insert(Bt14User record);

    /**
     * 新增用户表数据(根据一定规则)
     *
     * @param record 用户实体类
     * @return 新增行数
     */
    int insertSelective(Bt14User record);

    /**
     * 根据id查询用户表数据
     *
     * @param id 用户实体类
     * @return 用户实体类
     */
    Bt14User selectByPrimaryKey(Long id);

    /**
     * 查询用户表所有数据
     *
     * @return 用户实体类
     */
    List<Bt14User> selectAllUser();

    /**
     * 更新用户表数据(根据一定规则)
     *
     * @param record 用户实体类
     * @return 更新行数
     */
    int updateByPrimaryKeySelective(Bt14User record);

    /**
     * 更新用户表数据
     *
     * @param record 用户实体类
     * @return 更新行数
     */
    int updateByPrimaryKey(Bt14User record);

    /**
     * 根据邮箱地址获取用户对象
     *
     * @return
     */
    Bt14User selectByEmail(String email);

    void truncateTable();
}