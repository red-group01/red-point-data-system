package shengdong.common;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import shengdong.common.util.CommonUtil;

import java.time.temporal.ChronoUnit;
import java.util.Date;

@SpringBootTest
class ShengdongCommonApplicationTests {

    @Test
    void contextLoads() {
        System.out.println(CommonUtil.msFormat(817852));
    }

    @Test
    void t1() {
        Date start = CommonUtil.fromUtcDate("2021-02-26T08:18:44.000Z");
        Date start1 = CommonUtil.fromUtcDate("2023-03-01T23:18:44.000Z");
        System.out.println(start.getTime());
        System.out.println(start1.getTime());
        long l = ChronoUnit.DAYS.between(start.toInstant(), new Date().toInstant());
        System.out.println(l);
    }
}
