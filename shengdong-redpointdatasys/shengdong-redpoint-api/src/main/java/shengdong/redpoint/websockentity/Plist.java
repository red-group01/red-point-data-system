package shengdong.redpoint.websockentity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class Plist {
    @JsonProperty("zone_mode")
    private String zoneMode;
    @JsonProperty("sublocation_uid")
    private String sublocationUid;
    private String shape;
    @JsonProperty("zone_geometry")
    private ZoneGeometry zoneGeometry;
    @JsonProperty("zone_points_attributes")
    private List<ZonePointsAttributesItem> zonePointsAttributes;
    private int priority;
    @JsonProperty("zone_alarm_level")
    private int zoneAlarmLevel;
    @JsonProperty("trackable_object_uid")
    private Object trackableObjectUid;
    @JsonProperty("default_allowed")
    private boolean defaultAllowed;
    @JsonProperty("zone_exceptions")
    private List<Object> zoneExceptions;
    private Schedule schedule;
    @JsonProperty("sublocation_id")
    private Object sublocationId;
    private String name;
    @JsonProperty("zone_type_name")
    private String zoneTypeName;
    @JsonProperty("zone_type_id")
    private Object zoneTypeId;
    private String dynamic;
    @JsonProperty("created_at")
    private String createdAt;
    private boolean active;
    @JsonProperty("project_uid")
    private String projectUid;
    @JsonProperty("location_uid")
    private String locationUid;
    private String uid;
    private Object depth;
    @JsonProperty("updated_at")
    private String updatedAt;
    @JsonProperty("zone_type")
    private ZoneType zoneType;
    @JsonProperty("zone_exceptions_groups")
    private List<Object> zoneExceptionsGroups;
}