package shengdong.data.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import shengdong.data.entity.BaseNodeType;
import shengdong.data.entity.Bt23Trackable;

import java.util.List;

/**
 * 可追踪对象节点表Mapper映射方法接口
 */
@Mapper
@Component
public interface Bt23TrackableMapper {

    /**
     * 根据id删除可追踪对象节点表数据
     *
     * @param id 自增主键
     * @return 删除行数
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 新增可追踪对象节点表数据
     *
     * @param record 可追踪对象节点实体类
     * @return 新增行数
     */
    int insert(Bt23Trackable record);

    /**
     * 新增可追踪对象节点表数据(根据一定规则)
     *
     * @param record 可追踪对象节点实体类
     * @return 新增行数
     */
    int insertSelective(Bt23Trackable record);

    /**
     * 根据id查询可追踪对象节点表数据
     *
     * @param id 可追踪对象节点实体类
     * @return 可追踪对象节点实体类
     */
    Bt23Trackable selectByPrimaryKey(Long id);

    /**
     * 更新可追踪对象节点表数据(根据一定规则)
     *
     * @param record 可追踪对象节点实体类
     * @return 更新行数
     */
    int updateByPrimaryKeySelective(Bt23Trackable record);

    /**
     * 更新可追踪对象节点数据
     *
     * @param record 可追踪对象节点实体类
     * @return 更新行数
     */
    int updateByPrimaryKey(Bt23Trackable record);

    List<Bt23Trackable> selectByUserId(Long id);

    void truncateTable();

    List<Bt23Trackable> selectAllTrackable();

    /**
     * 根据可追踪对象节点uid查询可追踪对象节点表数据
     *
     * @param trackableUid 可追踪对象节点uid
     * @return 可追踪对象节点实体类
     */
    List<Bt23Trackable> selectByTrackableUid(String trackableUid);

    /**
     * 根据地点子区域uid查询可追踪对象节点表数据
     *
     * @param sublocationUid 地点子区域uid
     * @return 可追踪对象节点实体类
     */
    List<Bt23Trackable> selectBySublocationUid(String sublocationUid);

    /**
     * 根据地点子区域uid查询可追踪对象节点表数据的类别分组
     *
     * @param sublocationUid 地点子区域uid
     * @return 可追踪对象节点实体类
     */
    List<BaseNodeType> selectBySublocationUidGroupByCategory(String sublocationUid);
}