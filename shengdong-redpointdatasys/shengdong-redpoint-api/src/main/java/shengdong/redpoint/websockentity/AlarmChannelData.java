package shengdong.redpoint.websockentity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class AlarmChannelData {
    @JsonProperty("alarm_name")
    private String alarmName;
    private int acknowledged;
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("zone_uid")
    private String zoneUid;
    @JsonProperty("project_uid")
    private String projectUid;
    @JsonProperty("hysteresis_count")
    private int hysteresisCount;
    private String uid;
    @JsonProperty("updated_at")
    private String updatedAt;
    @JsonProperty("additional_info")
    private AdditionalInfo additionalInfo;
    @JsonProperty("mac_address")
    private String macAddress;
    @JsonProperty("obj_uid")
    private String objUid;
    @JsonProperty("acknowledged_by")
    private Object acknowledgedBy;
    private String state;
    @JsonProperty("alarm_type_id")
    private int alarmTypeId;
    private int timestamp;
}