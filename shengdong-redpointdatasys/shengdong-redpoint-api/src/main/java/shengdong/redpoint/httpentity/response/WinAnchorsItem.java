package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class WinAnchorsItem {
    @JsonProperty("sublocation_uid")
    private String sublocationUid;
    @JsonProperty("bt_config")
    private Object btConfig;
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("project_uid")
    private String projectUid;
    private String type;
    @JsonProperty("acc_config")
    private Object accConfig;
    private String uid;
    @JsonProperty("shm_status")
    private ShmStatus shmStatus;
    private Node node;
    @JsonProperty("updated_at")
    private String updatedAt;
    private String name;
    @JsonProperty("company_uid")
    private String companyUid;
    private Object state;
    private Position position;
    private boolean locked;
}