package shengdong.data.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import shengdong.data.dao.IBt12OrganizationDao;
import shengdong.data.entity.Bt12Organization;
import shengdong.data.mapper.Bt12OrganizationMapper;

/**
 * 人员组织结构Dao实现
 */
@Repository
public class Bt12OrganizationDaoImpl implements IBt12OrganizationDao {

    // 注入Mapper接口
    @Autowired
    private Bt12OrganizationMapper bt12OrganizationMapper;

    /**
     * 插人员组织结构司数据
     *
     * @param record 人员组织结构司实体类
     * @return 插入的行数
     */
    @Override
    public int insert(Bt12Organization record) {
        return bt12OrganizationMapper.insert(record);
    }
}
