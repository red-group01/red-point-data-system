package shengdong.redpoint.httpentity.response;

import lombok.Data;

import java.io.Serializable;

@Data
public class Role implements Serializable {
    private String uid;
    private String name;
    private int id;
    private String displayName;
}
