package shengdong.redpoint.websockentity;

import lombok.Data;

@Data
public class RequestDelete {
    private String method;
    private int id;
    private String jsonrpc;
    private RequestDeleteParams params;
}