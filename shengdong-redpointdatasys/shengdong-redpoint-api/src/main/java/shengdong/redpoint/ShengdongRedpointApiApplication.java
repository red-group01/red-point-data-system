package shengdong.redpoint;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShengdongRedpointApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShengdongRedpointApiApplication.class, args);
    }

}
