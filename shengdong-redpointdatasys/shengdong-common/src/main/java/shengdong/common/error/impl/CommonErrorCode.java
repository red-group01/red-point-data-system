package shengdong.common.error.impl;

import shengdong.common.error.IErrorCode;

public enum CommonErrorCode implements IErrorCode {
    // 长度超过限制
    LENGTH_EXCEEDS_THE_LIMIT("100010", "长度超过限制"), // 请求参数错误
    REQUEST_PARAM_ERROR("100011", "请求参数错误"), // 用户id为空
    USER_ID_IS_EMPTY("100012", "用户id为空"), // 无该用户
    USER_IS_NOT_FOUND("100013", "无该用户"), // 发送消息失败
    SEND_MSG_ERROR("100014", "发送消息失败"), // SQL执行异常
    SQL_EXCEPTION("100015", "SQL执行异常"), // 数据库连接异常
    DATASOURCE_CONNECTION_ERROR("100015", "数据库连接异常"), // 文件获取错误
    ERR_GET_FILE_ADDR_ERROR("100017", "文件获取错误"), // 文件上传错误
    ERR_UPLOAD_FILE_ERROR("100018", "文件上传错误");//

    private String code;
    private String message;

    CommonErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
