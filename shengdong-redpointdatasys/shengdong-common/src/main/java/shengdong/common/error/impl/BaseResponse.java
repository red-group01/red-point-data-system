package shengdong.common.error.impl;

import shengdong.common.error.IErrorCode;

public class BaseResponse<T> {
    private String code = "200";
    private String msg = "success";
    private T data;

    public String getCode() {
        return code;
    }
    public String getMsg() {
        return msg;
    }
    public T getData() {
        return data;
    }

    private BaseResponse(String code, String msg, T data){
        this.code = code;
        this.msg = msg;
        this.data =data;
    }

    public static <T>BaseResponse success(T data){
        BaseResponse resp = new BaseResponse("200", "success", data);
        return  resp;
    }

    public static <T>BaseResponse success(String msg, T data){
        BaseResponse resp = new BaseResponse("200", msg, data);
        return  resp;
    }

    public static <T>BaseResponse error(IErrorCode commonErrorCode){
        BaseResponse resp = new BaseResponse(commonErrorCode.getCode(), commonErrorCode.getMessage(), null);
        return  resp;
    }

    public static <T>BaseResponse error(String code, String msg){
        BaseResponse resp = new BaseResponse(code, msg, null);
        return  resp;
    }

}
