package shengdong.redpoint.websockentity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ChannelZoneData {
    private Plist plist;
    private int ms;
    private String action;
    @JsonProperty("project_uid")
    private String projectUid;
    @JsonProperty("created_by")
    private CreatedBy createdBy;
    private int ts;
    private String object;
}