package shengdong.data.scheduledtask;

import lombok.extern.slf4j.Slf4j;
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import shengdong.data.service.IBaseInfoService;
import shengdong.data.service.IDataService;

import java.util.Date;

@Component
@Slf4j
class IotSynchronization {

    @Autowired
    private IBaseInfoService baseService;
    @Autowired
    IDataService dataService;

    /**
     * 定时同步基础数据 服务器启动1分钟后，每隔1分钟执行一次 ；1分钟内不能重复启动，超过2分钟则释放锁定，允许重复运行
     */
    @Scheduled(fixedRate = 1000 * 60 * 20, initialDelay = 1000 * 60 * 1)
    @SchedulerLock(lockAtMostFor = "PT2M", lockAtLeastFor = "PT1M")
    public void synIotBaseInfo() throws Exception {
        log.info("synchronization basedata begin:{}", new Date());
        baseService.synAll();
        log.info("synchronization basedata end:{}", new Date());
    }

    /**
     * 定时同步运行数据 服务器启动5分钟后，每隔16分钟执行一次 ；15分钟内不能重复启动，超过20分钟则释放锁定，允许重复运行
     */
    @Scheduled(fixedRate = 1000 * 60 * 16, initialDelay = 1000 * 60 * 5)
    @SchedulerLock(lockAtMostFor = "PT20M", lockAtLeastFor = "PT15M")
    public void synIotDataInfo() throws Exception {
        log.info("synchronization hisdata begin:{}", new Date());
        dataService.synAll();
        log.info("synchronization hisdata end:{}", new Date());
    }

    /**
     * 定时同步实时告警数据 服务器启动2分钟后，每隔3秒执行一次
     */
    @Scheduled(fixedRate = 3000, initialDelay = 1000 * 60 * 2)
    public void synActiveAlarmInfo() throws Exception {
        //log.info("synactivealarm begin:{}", new Date());
        dataService.synAlarmsActive();
        //log.info("synactivealarm end:{}", new Date());
    }
}
