package shengdong.data.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import shengdong.data.entity.Bt41Position;

import java.util.List;

/**
 * 位置历史信息表Mapper映射方法接口
 */
@Mapper
@Component
public interface Bt41PositionMapper {

    /**
     * 根据id删除位置历史信息表数据
     *
     * @param id 自增主键
     * @return 删除行数
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 新增位置历史信息表数据
     *
     * @param record 位置历史信息实体类
     * @return 新增行数
     */
    int insert(Bt41Position record);

    /**
     * 新增位置历史信息表数据(根据一定规则)
     *
     * @param record 位置历史信息实体类
     * @return 新增行数
     */
    int insertSelective(Bt41Position record);

    /**
     * 根据id查询位置历史信息表数据
     *
     * @param id 位置历史信息实体类
     * @return 位置历史信息实体类
     */
    Bt41Position selectByPrimaryKey(Long id);

    /**
     * 更新位置历史信息表数据(根据一定规则)
     *
     * @param record 位置历史信息实体类
     * @return 更新行数
     */
    int updateByPrimaryKeySelective(Bt41Position record);

    /**
     * 更新位置历史信息表数据
     *
     * @param record 位置历史信息实体类
     * @return 更新行数
     */
    int updateByPrimaryKey(Bt41Position record);

    void truncateTable();

    List<Bt41Position> selectAllPostion(String puid);

    /**
     * 根据objUid查询位置历史信息表数据
     *
     * @param objUid 位置历史信息实体类
     * @return 位置历史信息实体类
     */
    Bt41Position selectByObjUid(String objUid);

    /**
     * 根据objUid查询区间内的位置历史信息表数据
     *
     * @param objUid 位置历史信息实体类
     * @return 位置历史信息实体类
     */
    List<Bt41Position> selectByObjUidInTime(String objUid, String tsBegin, String tsEnd);

    /**
     * 根据objUid集合查询区间内的位置历史信息表数据
     *
     * @param objUids 可追踪标签uid集合
     * @return 位置历史信息实体类
     */
    List<Bt41Position> selectInObjUidInTime(String tsBegin, String tsEnd, List<String> objUids);

    /**
     * 根据sublocationUid查询区间内的位置历史信息表数据
     *
     * @param sublocationUid 位置历史信息实体类
     * @return 位置历史信息实体类
     */
    List<Bt41Position> selectSublocationUidInTime(String sublocationUid, String tsBegin, String tsEnd);

    /**
     * 根据sublocationUid查询区间内的位置历史信息表数据
     *
     * @param sublocationUid 位置历史信息实体类
     * @return 位置历史信息实体类
     */
    List<Bt41Position> selectSublocationUidInTime2(String sublocationUid, String tsBegin, String tsEnd);

    Bt41Position selectNewDataTime();

}