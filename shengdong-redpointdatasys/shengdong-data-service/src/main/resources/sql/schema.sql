/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306_Shengdong@1809
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : localhost:3306
 Source Schema         : sd_redpoint_data01

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 06/03/2023 10:25:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for activealarm
-- ----------------------------
DROP TABLE IF EXISTS `activealarm`;
CREATE TABLE `activealarm` (
                               `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
                               `alarm_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'alarm_uid',
                               `sublocation_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'sublocation_uid',
                               `zone_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'zone_uid',
                               `mac_address` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'mac_address',
                               `obj_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'obj_uid',
                               `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'type',
                               `additional_info` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'additional_info',
                               `hysteresis_count` int DEFAULT NULL COMMENT 'hysteresis_count',
                               `state` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'state',
                               `ts` datetime(3) DEFAULT NULL COMMENT 'ts',
                               `x` bigint DEFAULT NULL COMMENT 'x',
                               `y` bigint DEFAULT NULL COMMENT 'y',
                               `z` bigint DEFAULT NULL COMMENT 'z',
                               `created_at` datetime(3) DEFAULT NULL COMMENT 'created_at',
                               `updated_at` datetime(3) DEFAULT NULL COMMENT 'updated_at',
                               `creator_at` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'creator_at',
                               `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'remark',
                               `project_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'project_uid',
                               `user_id` bigint DEFAULT NULL,
                               PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=67309 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='设备实时告警';

-- ----------------------------
-- Table structure for bm01dictionary
-- ----------------------------
DROP TABLE IF EXISTS `bm01dictionary`;
CREATE TABLE `bm01dictionary` (
                                  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
                                  `dictionary_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'dictionary_uid',
                                  `code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'code',
                                  `value` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'value',
                                  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'name',
                                  `created_at` datetime DEFAULT NULL COMMENT 'created_at',
                                  `updated_at` datetime DEFAULT NULL COMMENT 'updated_at',
                                  `creator_at` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'creator_at',
                                  `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'remark',
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='数据字典';

-- ----------------------------
-- Table structure for bm01webserver
-- ----------------------------
DROP TABLE IF EXISTS `bm01webserver`;
CREATE TABLE `bm01webserver` (
                                 `id` int NOT NULL AUTO_INCREMENT,
                                 `server_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
                                 PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for bm02pagelayout
-- ----------------------------
DROP TABLE IF EXISTS `bm02pagelayout`;
CREATE TABLE `bm02pagelayout` (
                                  `id` bigint NOT NULL AUTO_INCREMENT,
                                  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                  `pageid` int DEFAULT NULL,
                                  `typeid` int DEFAULT NULL,
                                  `min_dimension` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                  `pagemode` int DEFAULT NULL,
                                  `sizetype` int DEFAULT NULL,
                                  `created_at` datetime DEFAULT NULL,
                                  `creator_at` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                  `remark` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                  `updated_at` datetime DEFAULT NULL,
                                  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for bm03materialstype
-- ----------------------------
DROP TABLE IF EXISTS `bm03materialstype`;
CREATE TABLE `bm03materialstype` (
                                     `id` int NOT NULL AUTO_INCREMENT,
                                     `name` varchar(100) DEFAULT NULL,
                                     `created_at` datetime DEFAULT NULL,
                                     `creator_at` varchar(20) DEFAULT NULL,
                                     `remark` varchar(200) DEFAULT NULL,
                                     `updated_at` datetime DEFAULT NULL,
                                     PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Table structure for bm04stayalarmcon
-- ----------------------------
DROP TABLE IF EXISTS `bm04stayalarmcon`;
CREATE TABLE `bm04stayalarmcon` (
                                    `id` int NOT NULL AUTO_INCREMENT,
                                    `zone_uid` varchar(22) DEFAULT NULL,
                                    `name` varchar(100) DEFAULT NULL,
                                    `additional_info` varchar(500) DEFAULT NULL,
                                    `residence_time` int DEFAULT NULL,
                                    `created_at` datetime(3) DEFAULT NULL,
                                    `creator_at` varchar(20) DEFAULT NULL,
                                    `remark` varchar(200) DEFAULT NULL,
                                    `updated_at` datetime(3) DEFAULT NULL,
                                    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Table structure for bm05zonetype
-- ----------------------------
DROP TABLE IF EXISTS `bm05zonetype`;
CREATE TABLE `bm05zonetype` (
                                `id` int NOT NULL AUTO_INCREMENT,
                                `name` varchar(100) DEFAULT NULL,
                                `remark` varchar(200) DEFAULT NULL,
                                PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Table structure for bm06borderline
-- ----------------------------
DROP TABLE IF EXISTS `bm06borderline`;
CREATE TABLE `bm06borderline` (
                                  `id` int NOT NULL AUTO_INCREMENT,
                                  `name` varchar(200) DEFAULT NULL,
                                  `coordinate` text,
                                  `sublocation_uid` varchar(22) DEFAULT NULL,
                                  `remark` varchar(200) DEFAULT NULL,
                                  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Table structure for bt11company
-- ----------------------------
DROP TABLE IF EXISTS `bt11company`;
CREATE TABLE `bt11company` (
                               `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
                               `company_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'company_uid',
                               `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'name',
                               `company_id` bigint DEFAULT NULL COMMENT 'company_id',
                               `created_at` datetime(3) DEFAULT NULL COMMENT 'created_at',
                               `updated_at` datetime(3) DEFAULT NULL COMMENT 'updated_at',
                               `creator_at` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'creator_at',
                               `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'remark',
                               PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=142 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='公司';

-- ----------------------------
-- Table structure for bt12organization
-- ----------------------------
DROP TABLE IF EXISTS `bt12organization`;
CREATE TABLE `bt12organization` (
                                    `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键id',
                                    `organization_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'organization_uid',
                                    `company_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'company_uid',
                                    `organization_code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'organization_code',
                                    `level` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'level',
                                    `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'name',
                                    `created_at` datetime(3) DEFAULT NULL COMMENT 'created_at',
                                    `updated_at` datetime(3) DEFAULT NULL COMMENT 'updated_at',
                                    `creator_at` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'creator_at',
                                    `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'remark',
                                    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='人员组织结构';

-- ----------------------------
-- Table structure for bt13role
-- ----------------------------
DROP TABLE IF EXISTS `bt13role`;
CREATE TABLE `bt13role` (
                            `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
                            `role_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'role_uid',
                            `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'name',
                            `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'remark',
                            `created_at` datetime(3) DEFAULT NULL COMMENT 'created_at',
                            `updated_at` datetime(3) DEFAULT NULL COMMENT 'updated_at',
                            `creator_at` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'creator_at',
                            PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='角色';

-- ----------------------------
-- Table structure for bt14user
-- ----------------------------
DROP TABLE IF EXISTS `bt14user`;
CREATE TABLE `bt14user` (
                            `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
                            `user_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'user_uid',
                            `company_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'company_uid',
                            `project_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'project_uid',
                            `role_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'role_uid',
                            `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'name',
                            `email` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'email',
                            `password` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'password',
                            `organization_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'organization_uid',
                            `created_at` datetime(3) DEFAULT NULL COMMENT 'created_at',
                            `updated_at` datetime(3) DEFAULT NULL COMMENT 'updated_at',
                            `creator_at` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'creator_at',
                            `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'remark',
                            `token` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci COMMENT 'token',
                            `serverid` int DEFAULT NULL COMMENT 'serverid',
                            PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='用户';

-- ----------------------------
-- Table structure for bt21node
-- ----------------------------
DROP TABLE IF EXISTS `bt21node`;
CREATE TABLE `bt21node` (
                            `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
                            `node_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'node_uid',
                            `company_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'company_uid',
                            `project_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'project_uid',
                            `mac_address` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'mac_address',
                            `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'name',
                            `parent_mac_address` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'parent_mac_address',
                            `bridge_mac_address` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'bridge_mac_address',
                            `sw_version` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'sw_version',
                            `hw_info` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'hw_info',
                            `last_heard` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'last_heard',
                            `announce_flags` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'announce_flags',
                            `announce_ts` bigint DEFAULT NULL COMMENT 'announce_ts',
                            `announce_last_ts` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'announce_last_ts',
                            `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'type',
                            `uwb_trx_config` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'uwb_trx_config',
                            `current_system_timestamp` datetime(3) DEFAULT NULL COMMENT 'current_system_timestamp',
                            `status_ext_pow` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'status_ext_pow',
                            `status_low_bat` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'status_low_bat',
                            `status_sync` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'status_sync',
                            `status_tdoa` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'status_tdoa',
                            `battery_mv` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'battery_mv',
                            `battery_pr` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'battery_pr',
                            `node_id` bigint NOT NULL COMMENT 'node_id',
                            `created_at` datetime(3) DEFAULT NULL COMMENT 'created_at',
                            `updated_at` datetime(3) DEFAULT NULL COMMENT 'updated_at',
                            `creator_at` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'creator_at',
                            `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'remark',
                            `user_id` bigint DEFAULT NULL COMMENT 'user_id',
                            `delete_mark` bit(1) DEFAULT NULL COMMENT 'delete_mark',
                            PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='硬件设备节点';

-- ----------------------------
-- Table structure for bt22anchor
-- ----------------------------
DROP TABLE IF EXISTS `bt22anchor`;
CREATE TABLE `bt22anchor` (
                              `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
                              `anchor_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'anchor_uid',
                              `company_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'company_uid',
                              `project_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'project_uid',
                              `sublocation_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'sublocation_uid',
                              `mac_address` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'mac_address',
                              `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'name',
                              `locked` bit(1) DEFAULT NULL COMMENT 'locked',
                              `position_x` bigint DEFAULT NULL COMMENT 'position_x',
                              `position_y` bigint DEFAULT NULL COMMENT 'position_y',
                              `position_z` bigint DEFAULT NULL COMMENT 'position_z',
                              `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'type',
                              `node_id` bigint DEFAULT NULL COMMENT 'node_id',
                              `created_at` datetime(3) DEFAULT NULL COMMENT 'created_at',
                              `updated_at` datetime(3) DEFAULT NULL COMMENT 'updated_at',
                              `creator_at` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'creator_at',
                              `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'remark',
                              `user_id` bigint DEFAULT NULL COMMENT 'user_id',
                              `delete_mark` bit(1) DEFAULT NULL COMMENT 'delete_mark',
                              PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='锚节点';

-- ----------------------------
-- Table structure for bt23trackable
-- ----------------------------
DROP TABLE IF EXISTS `bt23trackable`;
CREATE TABLE `bt23trackable` (
                                 `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
                                 `trackable_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'trackable_uid',
                                 `company_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'company_uid',
                                 `project_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'project_uid',
                                 `mac_address` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'mac_address',
                                 `self_zone_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'self_zone_uid',
                                 `grouplist` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'grouplist',
                                 `sublocation_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'sublocation_uid',
                                 `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'name',
                                 `default_height` bigint DEFAULT NULL COMMENT 'default_height',
                                 `display_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'display_name',
                                 `bt_config` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'bt_config',
                                 `acc_config` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'acc_config',
                                 `removed_at` datetime(3) DEFAULT NULL COMMENT 'removed_at',
                                 `sensor_config` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'sensor_config',
                                 `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'type',
                                 `responsive` bit(1) DEFAULT NULL COMMENT 'responsive',
                                 `idle` bit(1) DEFAULT NULL COMMENT 'idle',
                                 `position_update_interval` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'position_update_interval',
                                 `tag_config` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'tag_config',
                                 `category` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'category',
                                 `x` bigint DEFAULT NULL COMMENT 'x',
                                 `y` bigint DEFAULT NULL COMMENT 'y',
                                 `z` bigint DEFAULT NULL COMMENT 'z',
                                 `node_id` bigint DEFAULT NULL COMMENT 'node_id',
                                 `created_at` datetime(3) DEFAULT NULL COMMENT 'created_at',
                                 `updated_at` datetime(3) DEFAULT NULL COMMENT 'updated_at',
                                 `creator_at` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'creator_at',
                                 `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'remark',
                                 `user_id` bigint DEFAULT NULL COMMENT 'user_id',
                                 `delete_mark` bit(1) DEFAULT NULL COMMENT 'delete_mark',
                                 `set_name` varchar(200) DEFAULT NULL,
                                 `set_typeid` int DEFAULT NULL,
                                 `set_imageurl` varchar(200) DEFAULT NULL,
                                 PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='可追踪对象节点';

-- ----------------------------
-- Table structure for bt31project
-- ----------------------------
DROP TABLE IF EXISTS `bt31project`;
CREATE TABLE `bt31project` (
                               `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
                               `project_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'project_uid',
                               `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'name',
                               `zone_vertices_limit` bigint DEFAULT NULL COMMENT 'zone_vertices_limit',
                               `address` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'address',
                               `manager` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'manager',
                               `timezone` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'timezone',
                               `project_id` bigint DEFAULT NULL COMMENT 'project_id',
                               `created_at` datetime(3) DEFAULT NULL COMMENT 'created_at',
                               `updated_at` datetime(3) DEFAULT NULL COMMENT 'updated_at',
                               `creator_at` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'creator_at',
                               `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'remark',
                               `user_id` bigint DEFAULT NULL COMMENT 'user_id',
                               `delete_mark` bit(1) DEFAULT NULL COMMENT 'delete_mark',
                               PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='项目';

-- ----------------------------
-- Table structure for bt32location
-- ----------------------------
DROP TABLE IF EXISTS `bt32location`;
CREATE TABLE `bt32location` (
                                `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
                                `location_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'location_uid',
                                `project_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'project_uid',
                                `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'name',
                                `description` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'description',
                                `latitude` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'latitude',
                                `longitude` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'longitude',
                                `elevation` int DEFAULT NULL COMMENT 'elevation',
                                `address` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'address',
                                `address2` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'address2',
                                `city` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'city',
                                `state` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'state',
                                `zip` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'zip',
                                `zip4` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'zip4',
                                `orientation` bigint DEFAULT NULL COMMENT 'orientation',
                                `country` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'country',
                                `company_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'company_uid',
                                `created_at` datetime(3) DEFAULT NULL COMMENT 'created_at',
                                `updated_at` datetime(3) DEFAULT NULL COMMENT 'updated_at',
                                `creator_at` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'creator_at',
                                `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'remark',
                                `user_id` bigint DEFAULT NULL COMMENT 'user_id',
                                `delete_mark` bit(1) DEFAULT NULL COMMENT 'delete_mark',
                                PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='项目地点';

-- ----------------------------
-- Table structure for bt33sublocation
-- ----------------------------
DROP TABLE IF EXISTS `bt33sublocation`;
CREATE TABLE `bt33sublocation` (
                                   `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
                                   `sublocation_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'sublocation_uid',
                                   `location_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'location_uid',
                                   `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'name',
                                   `description` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'description',
                                   `offset_x` bigint DEFAULT NULL COMMENT 'offset_x',
                                   `offset_y` bigint DEFAULT NULL COMMENT 'offset_y',
                                   `offset_z` bigint DEFAULT NULL COMMENT 'offset_z',
                                   `company_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'company_uid',
                                   `project_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'project_uid',
                                   `sublocation_id` bigint DEFAULT NULL COMMENT 'sublocation_id',
                                   `created_at` datetime(3) DEFAULT NULL COMMENT 'created_at',
                                   `updated_at` datetime(3) DEFAULT NULL COMMENT 'updated_at',
                                   `creator_at` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'creator_at',
                                   `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'remark',
                                   `user_id` bigint DEFAULT NULL COMMENT 'user_id',
                                   `delete_mark` bit(1) DEFAULT NULL COMMENT 'delete_mark',
                                   `lastpointsyn_at` datetime(3) DEFAULT NULL COMMENT 'lastpointsyn_at',
                                   `lastalarmsyn_at` datetime(3) DEFAULT NULL COMMENT 'lastalarmsyn_at',
                                   `lastalzonesyn_at` datetime(3) DEFAULT NULL COMMENT 'lastalzonesyn_at',
                                   PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='地点子区域';

-- ----------------------------
-- Table structure for bt34zone
-- ----------------------------
DROP TABLE IF EXISTS `bt34zone`;
CREATE TABLE `bt34zone` (
                            `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
                            `zone_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'zone_uid',
                            `sublocation_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'sublocation_uid',
                            `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'name',
                            `removed_at` datetime(3) DEFAULT NULL COMMENT 'removed_at',
                            `active` bit(1) DEFAULT NULL COMMENT 'active',
                            `default_allowed` bit(1) DEFAULT NULL COMMENT 'default_allowed',
                            `zone_alarm_level` int DEFAULT NULL COMMENT 'zone_alarm_level',
                            `zone_type_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'zone_type_name',
                            `dynamic` bit(1) DEFAULT NULL COMMENT 'dynamic',
                            `shape` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'shape',
                            `radius` bigint DEFAULT NULL,
                            `positions` varchar(255) DEFAULT NULL,
                            `zone_exceptions` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'zone_exceptions',
                            `zone_exceptions_groups` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'zone_exceptions_groups',
                            `zone_mode` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'zone_mode',
                            `start_time` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'start_time',
                            `stop_time` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'stop_time',
                            `zone_type_id` bigint DEFAULT NULL COMMENT 'zone_type_id',
                            `company_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'company_uid',
                            `project_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'project_uid',
                            `location_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'location_uid',
                            `sublocation_id` bigint DEFAULT NULL COMMENT 'sublocation_id',
                            `trackable_object_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'trackable_object_uid',
                            `zone_id` bigint NOT NULL COMMENT 'zone_id',
                            `set_typeid` int DEFAULT NULL,
                            `set_name` varchar(200) DEFAULT NULL,
                            `is_stay` bit(1) DEFAULT NULL,
                            `created_at` datetime(3) DEFAULT NULL COMMENT 'created_at',
                            `updated_at` datetime(3) DEFAULT NULL COMMENT 'updated_at',
                            `creator_at` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'creator_at',
                            `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'remark',
                            `user_id` bigint DEFAULT NULL COMMENT 'user_id',
                            `delete_mark` bit(1) DEFAULT NULL COMMENT 'delete_mark',
                            PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='区域围栏';

-- ----------------------------
-- Table structure for bt41position
-- ----------------------------
DROP TABLE IF EXISTS `bt41position`;
CREATE TABLE `bt41position` (
                                `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键id',
                                `company_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'company_uid',
                                `project_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'project_uid',
                                `location_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'location_uid',
                                `sublocation_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'sublocation_uid',
                                `zone_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'zone_uid',
                                `mac_address` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'mac_address',
                                `obj_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'obj_uid',
                                `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'type',
                                `ts` datetime(3) DEFAULT NULL COMMENT 'ts',
                                `ms` bigint DEFAULT NULL COMMENT 'ms',
                                `x` bigint DEFAULT NULL COMMENT 'x',
                                `y` bigint DEFAULT NULL COMMENT 'y',
                                `z` bigint DEFAULT NULL COMMENT 'z',
                                `position_id` bigint DEFAULT NULL COMMENT 'position_id',
                                `tmp_length` double(22,2) DEFAULT '0.00',
  `tmp_time` bigint DEFAULT '0',
  `flag_track` int DEFAULT '0',
  `flag_hot` varchar(255) DEFAULT '-1',
  `created_at` datetime(3) DEFAULT NULL COMMENT 'created_at',
  `updated_at` datetime(3) DEFAULT NULL COMMENT 'updated_at',
  `creator_at` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'creator_at',
  `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'remark',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `sublocation_uid` (`sublocation_uid`,`ts`) USING BTREE,
  KEY `obj_uid` (`obj_uid`,`ts`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=3830385 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='位置历史信息';

-- ----------------------------
-- Table structure for bt42notifications
-- ----------------------------
DROP TABLE IF EXISTS `bt42notifications`;
CREATE TABLE `bt42notifications` (
                                     `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
                                     `notifications_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'notifications_uid',
                                     `project_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'project_uid',
                                     `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'name',
                                     `zone_severity_level` int DEFAULT NULL COMMENT 'zone_severity_level',
                                     `alarm_type_list` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'alarm_type_list',
                                     `active` bit(1) DEFAULT NULL COMMENT 'active',
                                     `type_email` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'type_email',
                                     `type_phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'type_phone',
                                     `removed_at` datetime DEFAULT NULL COMMENT 'removed_at',
                                     `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'type',
                                     `notifications_id` bigint NOT NULL COMMENT 'notifications_id',
                                     `created_at` datetime(3) DEFAULT NULL COMMENT 'created_at',
                                     `updated_at` datetime(3) DEFAULT NULL COMMENT 'updated_at',
                                     `creator_at` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'creator_at',
                                     `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'remark',
                                     `user_id` bigint DEFAULT NULL COMMENT 'user_id',
                                     `delete_mark` bit(1) DEFAULT NULL COMMENT 'delete_mark',
                                     PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='系统通知';

-- ----------------------------
-- Table structure for bt43zoneevent
-- ----------------------------
DROP TABLE IF EXISTS `bt43zoneevent`;
CREATE TABLE `bt43zoneevent` (
                                 `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
                                 `zone_event_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'zone_event_uid',
                                 `zone_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'zone_uid',
                                 `obj_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'obj_uid',
                                 `zone_type_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'zone_type_name',
                                 `action` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'action',
                                 `x` bigint DEFAULT NULL COMMENT 'x',
                                 `y` bigint DEFAULT NULL COMMENT 'y',
                                 `z` bigint DEFAULT NULL COMMENT 'z',
                                 `duration` int DEFAULT NULL COMMENT 'duration',
                                 `company_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'company_uid',
                                 `project_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'project_uid',
                                 `location_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'location_uid',
                                 `sublocation_id` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'sublocation_id',
                                 `created_at` datetime(3) DEFAULT NULL COMMENT 'created_at',
                                 `updated_at` datetime(3) DEFAULT NULL COMMENT 'updated_at',
                                 `creator_at` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'creator_at',
                                 `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'remark',
                                 PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=360 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='区域事件';

-- ----------------------------
-- Table structure for bt44alarm
-- ----------------------------
DROP TABLE IF EXISTS `bt44alarm`;
CREATE TABLE `bt44alarm` (
                             `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
                             `alarm_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'alarm_uid',
                             `sublocation_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'sublocation_uid',
                             `zone_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'zone_uid',
                             `mac_address` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'mac_address',
                             `obj_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'obj_uid',
                             `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'type',
                             `additional_info` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'additional_info',
                             `hysteresis_count` int DEFAULT NULL COMMENT 'hysteresis_count',
                             `state` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'state',
                             `ts` datetime(3) DEFAULT NULL COMMENT 'ts',
                             `x` bigint DEFAULT NULL COMMENT 'x',
                             `y` bigint DEFAULT NULL COMMENT 'y',
                             `z` bigint DEFAULT NULL COMMENT 'z',
                             `created_at` datetime(3) DEFAULT NULL COMMENT 'created_at',
                             `updated_at` datetime(3) DEFAULT NULL COMMENT 'updated_at',
                             `creator_at` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'creator_at',
                             `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'remark',
                             `project_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'project_uid',
                             PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=607 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='设备告警';

-- ----------------------------
-- Table structure for bt45stayalarm
-- ----------------------------
DROP TABLE IF EXISTS `bt45stayalarm`;
CREATE TABLE `bt45stayalarm` (
                                 `id` int NOT NULL AUTO_INCREMENT,
                                 `trackable_uid` varchar(22) DEFAULT NULL,
                                 `zone_uid` varchar(22) DEFAULT NULL,
                                 `stay_begin` datetime(3) DEFAULT NULL,
                                 `stay_end` datetime(3) DEFAULT NULL,
                                 `stay_time` int DEFAULT NULL,
                                 `sublocation_uid` varchar(22) DEFAULT NULL,
                                 `additional_info` varchar(500) DEFAULT NULL,
                                 `created_at` datetime(3) DEFAULT NULL,
                                 `creator_at` varchar(20) DEFAULT NULL,
                                 `remark` varchar(200) DEFAULT NULL,
                                 `updated_at` datetime(3) DEFAULT NULL,
                                 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1150 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Table structure for bt51motionanalysis
-- ----------------------------
DROP TABLE IF EXISTS `bt51motionanalysis`;
CREATE TABLE `bt51motionanalysis` (
                                      `id` int NOT NULL AUTO_INCREMENT,
                                      `typeid` int DEFAULT NULL,
                                      `trackable_uid` varchar(22) DEFAULT NULL,
                                      `sublocation_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
                                      `movement_distance` double(22,2) DEFAULT NULL,
  `count_time` datetime(3) DEFAULT NULL,
  `dispersion` double(22,2) DEFAULT NULL,
  `data_marker` int DEFAULT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `creator_at` varchar(20) DEFAULT NULL,
  `remark` varchar(200) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=661 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Table structure for bt52attendance
-- ----------------------------
DROP TABLE IF EXISTS `bt52attendance`;
CREATE TABLE `bt52attendance` (
                                  `id` int NOT NULL AUTO_INCREMENT,
                                  `typeid` int DEFAULT NULL,
                                  `trackable_uid` varchar(22) DEFAULT NULL,
                                  `sublocation_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
                                  `count_time` datetime(3) DEFAULT NULL,
                                  `work_begin` datetime(3) DEFAULT NULL,
                                  `work_end` datetime(3) DEFAULT NULL,
                                  `created_at` datetime(3) DEFAULT NULL,
                                  `creator_at` varchar(20) DEFAULT NULL,
                                  `remark` varchar(200) DEFAULT NULL,
                                  `updated_at` datetime(3) DEFAULT NULL,
                                  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Table structure for bt53manhour
-- ----------------------------
DROP TABLE IF EXISTS `bt53manhour`;
CREATE TABLE `bt53manhour` (
                               `id` int NOT NULL AUTO_INCREMENT,
                               `typeid` int DEFAULT NULL,
                               `trackable_uid` varchar(22) DEFAULT NULL,
                               `sublocation_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
                               `count_time` datetime(3) DEFAULT NULL,
                               `man_hour` double(22,2) DEFAULT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `creator_at` varchar(20) DEFAULT NULL,
  `remark` varchar(200) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Table structure for bt54operationdaily
-- ----------------------------
DROP TABLE IF EXISTS `bt54operationdaily`;
CREATE TABLE `bt54operationdaily` (
                                      `id` int NOT NULL AUTO_INCREMENT,
                                      `typeid` int DEFAULT NULL,
                                      `trackable_uid` varchar(22) DEFAULT NULL,
                                      `sublocation_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
                                      `count_time` datetime(3) DEFAULT NULL,
                                      `work_time` double(22,2) DEFAULT NULL,
  `rest_time` double(22,2) DEFAULT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `creator_at` varchar(20) DEFAULT NULL,
  `remark` varchar(200) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Table structure for bt91lifecycle
-- ----------------------------
DROP TABLE IF EXISTS `bt91lifecycle`;
CREATE TABLE `bt91lifecycle` (
                                 `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
                                 `lifecycle_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'lifecycle_uid',
                                 `company_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'company_uid',
                                 `project_uid` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'project_uid',
                                 `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'type',
                                 `cycle` int DEFAULT NULL COMMENT 'cycle',
                                 `unit` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'unit',
                                 `created_at` datetime DEFAULT NULL COMMENT 'created_at',
                                 `updated_at` datetime DEFAULT NULL COMMENT 'updated_at',
                                 `creator_at` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'creator_at',
                                 `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'remark',
                                 PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='数据生命周期';

-- ----------------------------
-- Table structure for position_history
-- ----------------------------
DROP TABLE IF EXISTS `position_history`;
CREATE TABLE `position_history` (
                                    `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
                                    `ts` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
                                    `ms` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
                                    `mac_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
                                    `x` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
                                    `y` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
                                    `z` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
                                    `obj_uid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
                                    `sublocation_uid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
                                    `project_uid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
                                    `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for test_table
-- ----------------------------
DROP TABLE IF EXISTS `test_table`;
CREATE TABLE `test_table` (
                              `id` int NOT NULL AUTO_INCREMENT,
                              `name` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
                              PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for tm01stayalarm
-- ----------------------------
DROP TABLE IF EXISTS `tm01stayalarm`;
CREATE TABLE `tm01stayalarm` (
                                 `id` int NOT NULL AUTO_INCREMENT,
                                 `trackable_uid` varchar(22) DEFAULT NULL,
                                 `zone_uid` varchar(22) DEFAULT NULL,
                                 `stay_begin` datetime(3) DEFAULT NULL,
                                 `stay_end` datetime(3) DEFAULT NULL,
                                 `created_at` datetime(3) DEFAULT NULL,
                                 `creator_at` varchar(20) DEFAULT NULL,
                                 `remark` varchar(200) DEFAULT NULL,
                                 `updated_at` datetime(3) DEFAULT NULL,
                                 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4339 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;



truncate table bm01dictionary;
truncate table bm01webserver;
truncate table bt11company;
truncate table bt12organization;
truncate table bt13role;
truncate table bt14user;
truncate table bt21node;
truncate table bt22anchor;
truncate table bt23trackable;
truncate table bt31project;
truncate table bt32location;
truncate table bt33sublocation;
truncate table bt34zone;
truncate table bt41position;
truncate table bt42notifications;
truncate table bt43zoneevent;
truncate table bt44alarm;
truncate table bt91lifecycle;
truncate table position_history;
truncate table test_table;
truncate table activealarm;
truncate table bm02pagelayout;
truncate table bm03materialstype;
truncate table bm04stayalarmcon;
truncate table bm05zonetype;
truncate table bm06borderline;
truncate table bt45stayalarm;
truncate table tm01stayalarm;
truncate table bt51motionanalysis;
truncate table bt52attendance;
truncate table bt53manhour;
truncate table bt54operationdaily;

INSERT bm01webserver(server_ip) VALUES('jp1.rpplabs.com');
INSERT bt14user(email,`password`,serverid) VALUES('yao.jianwen@as-bang.com','Redp0int2',1);

SET sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));