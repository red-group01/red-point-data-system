package shengdong.redpoint.api;

import com.google.common.base.Strings;
import shengdong.common.util.JSONUtil;
import shengdong.redpoint.httpentity.request.RequestNode;
import shengdong.redpoint.httpentity.response.NodesItem;
import shengdong.redpoint.httpentity.response.ResponseNode;
import shengdong.redpoint.model.query.Header;
import shengdong.redpoint.model.query.HttpMethod;
import shengdong.redpoint.model.query.HttpParam;

public class IotNode extends IotApiBase {

    public IotNode(IotApiConfig cnf, Header httpheader) {
        super(cnf, httpheader);
    }

    /**
     * 获取全部节点
     *
     * @return
     * @throws Exception
     */
    public ResponseNode getNodes() throws Exception {
        return getSingleNode("");
    }

    /**
     * 获取指定节点
     *
     * @param uid
     * @return
     * @throws Exception
     */
    public ResponseNode getSingleNode(String uid) throws Exception {
        HttpParam hp = new HttpParam();
        String url = config.getDomen();
        if (!Strings.isNullOrEmpty(uid)) {
            url += config.getNodesApiGetSingleNode() + uid;
        } else {
            url += config.getNodesApiGetNodes();
        }
        hp.setUrl(url);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.GET);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseNode.class);
    }

    /**
     * 通过在REST调用中输入筛选器来筛选节点列表
     *
     * @param mac      节点的MAC地址
     * @param uid      节点硬件的唯一标识符
     * @param fields   响应中显示的字段。其他字段将被过滤掉
     * @param interval 返回节点的时间间隔。间隔是自节点报告的最后一次活动以来经过的秒数。
     * @return
     * @throws Exception
     */
    public ResponseNode getFilteredNodes(String[] mac, String[] uid, String[] fields, String[] interval)
        throws Exception {
        HttpParam hp = new HttpParam();
        String url = config.getDomen() + config.getNodesApiGetFilteredNodes();

        //拼接指定标签参数
        if (mac != null) {
            for (String i : mac) {
                url += "&mac[]=" + i;
            }
        }
        if (uid != null) {
            for (String i : uid) {
                url += "&uid[]=" + i;
            }
        }
        if (fields != null) {
            for (String i : fields) {
                url += "&fields[]=" + i;
            }
        }
        if (interval != null) {
            for (String i : interval) {
                url += "&interval[]=" + i;
            }
        }
        hp.setUrl(url);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.GET);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseNode.class);
    }

    /**
     * 创建子节点
     *
     * @param req
     * @return
     * @throws Exception
     */
    public NodesItem createNode(RequestNode req) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getNodesApiCreateNode());
        hp.setHeader(header);
        hp.setPostParam(JSONUtil.getObjectToString(req));
        hp.setMethod(HttpMethod.POST);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), NodesItem.class);
    }

    /**
     * 修改节点
     *
     * @param req
     * @param uid
     * @return
     * @throws Exception
     */
    public NodesItem editNode(RequestNode req, String uid) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getNodesApiEditNode() + uid);
        hp.setHeader(header);
        hp.setPostParam(JSONUtil.getObjectToString(req));
        hp.setMethod(HttpMethod.PUT);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), NodesItem.class);
    }

    /**
     * 删除节点
     *
     * @param uid
     * @return
     * @throws Exception
     */
    public String deleteNode(String uid) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getNodesApiDeleteNode() + uid);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.DELETE);
        String msg = HttpClient.ExecApi(hp);
        return msg;
    }

    /**
     * 将节点分配给指定的项目
     *
     * @param req
     * @return
     * @throws Exception
     */
    public NodesItem assignNodeToProject(RequestNodeToProject req) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getNodesApiAssignNodeToProject());
        hp.setHeader(header);
        hp.setPostParam(JSONUtil.getObjectToString(req));
        hp.setMethod(HttpMethod.POST);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), NodesItem.class);
    }

    /**
     * 从指定的项目中释放节点
     *
     * @param req
     * @return
     * @throws Exception
     */
    public NodesItem releaseNodeFromProject(RequestNodeToProject req) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getNodesApiReleaseNodeFromProject());
        hp.setHeader(header);
        hp.setPostParam(JSONUtil.getObjectToString(req));
        hp.setMethod(HttpMethod.POST);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), NodesItem.class);
    }
}
