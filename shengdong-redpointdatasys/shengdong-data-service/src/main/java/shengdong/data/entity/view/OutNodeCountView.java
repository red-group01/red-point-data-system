package shengdong.data.entity.view;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * 用于页面展示的标签数量统计列表实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OutNodeCountView {
    private String title;
    private List<NameAndValue> nvlist = new ArrayList<>();
}
