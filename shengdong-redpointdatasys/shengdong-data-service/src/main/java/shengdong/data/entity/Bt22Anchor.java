package shengdong.data.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 锚节点实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Bt22Anchor {

    //自增主键
    private Long id;

    //锚节点唯一标识
    private String anchorUid;

    //公司唯一标识
    private String companyUid;

    //项目唯一标识
    private String projectUid;

    //地点子区域唯一标识
    private String sublocationUid;

    //硬件设备节点mac地址
    private String macAddress;

    //锚节点名称
    private String name;

    //位置是否锁定
    private Boolean locked;

    //锚坐标x
    private Long positionX;

    //锚坐标y
    private Long positionY;

    //锚坐标z
    private Long positionZ;

    //类别（锚，桥）
    private String type;

    //硬件设备节点的服务器的唯一ID
    private Long nodeId;

    //创建时间（UTC）
    private Date createdAt;

    //更新时间（UTC）
    private Date updatedAt;

    //数据最后更新者
    private String creatorAt;

    //数据信息备注
    private String remark;
    /**
     * 项目所属用户id
     */
    private Long userId;
    /**
     * 已删除
     */
    private Boolean deleteMark;

}