package shengdong.redpoint.websockentity;

import java.util.List;
import lombok.Data;

@Data
public class ZoneGeometry{
	private List<PositionsItem> positions;
}