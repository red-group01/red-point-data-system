package shengdong.redpoint.api;

import shengdong.common.util.JSONUtil;
import shengdong.redpoint.httpentity.request.RequestProjects;
import shengdong.redpoint.httpentity.response.ResponseAllProjects;
import shengdong.redpoint.httpentity.response.ResponsetProjects;
import shengdong.redpoint.model.query.Header;
import shengdong.redpoint.model.query.HttpMethod;
import shengdong.redpoint.model.query.HttpParam;

public class IotProject extends IotApiBase {

    public IotProject(IotApiConfig cnf, Header httpheader) {
        super(cnf, httpheader);
    }

    /**
     * 读取项目列表
     *
     * @return
     * @throws Exception
     */
    public ResponseAllProjects getProjects() throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getProjectsApiGetProjects());
        hp.setHeader(header);
        hp.setMethod(HttpMethod.GET);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseAllProjects.class);
    }

    /**
     * 创建项目
     *
     * @param req
     * @return 项目创建结果
     * @throws Exception
     */
    public ResponsetProjects createProject(RequestProjects req) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getProjectsApiCreateProject());
        hp.setHeader(header);
        hp.setPostParam(JSONUtil.getObjectToString(req));
        hp.setMethod(HttpMethod.POST);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponsetProjects.class);
    }

    /**
     * 更新项目信息
     *
     * @param req
     * @param uid
     * @return
     * @throws Exception
     */
    public ResponsetProjects updateProject(RequestProjects req, String uid) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getProjectsApiUpdateProject() + uid);
        hp.setHeader(header);
        hp.setPostParam(JSONUtil.getObjectToString(req));
        hp.setMethod(HttpMethod.PUT);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponsetProjects.class);
    }

    /**
     * 删除项目
     *
     * @param uid
     * @return
     * @throws Exception
     */
    public String deleteProject(String uid) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getProjectsApiDeleteProject() + uid);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.DELETE);
        String msg = HttpClient.ExecApi(hp);
        return msg;
    }
}
