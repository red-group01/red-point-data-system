package shengdong.redpoint.httpentity.response;

import java.util.List;
import lombok.Data;

@Data
public class ResponseGroups{
	private List<GroupsItem> groups;
}