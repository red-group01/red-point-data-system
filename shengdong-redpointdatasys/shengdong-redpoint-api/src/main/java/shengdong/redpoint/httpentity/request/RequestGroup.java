package shengdong.redpoint.httpentity.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class RequestGroup {
    @JsonProperty("sensor_config")
    private SensorConfig sensorConfig;
    @JsonProperty("custom_info")
    private String customInfo;
    private String color;
    private String name;
    @JsonProperty("tag_config")
    private TagConfig tagConfig;
    @JsonProperty("user_params")
    private UserParams userParams;
}