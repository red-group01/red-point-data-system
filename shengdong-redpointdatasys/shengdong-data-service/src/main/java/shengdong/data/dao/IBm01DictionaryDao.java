package shengdong.data.dao;

import shengdong.data.entity.Bm01Dictionary;

/**
 * 数据字典Dao接口
 */
public interface IBm01DictionaryDao {
    /**
     * 插入数据字典数据
     * @param record 数据字典实体类
     * @return 插入的行数
     */
    public int insert(Bm01Dictionary record);
}
