package shengdong.redpoint.httpentity.request;

import lombok.Data;

@Data
public class RequestWinAnchor{
	private String name;
	private Position position;
	private String type;
}