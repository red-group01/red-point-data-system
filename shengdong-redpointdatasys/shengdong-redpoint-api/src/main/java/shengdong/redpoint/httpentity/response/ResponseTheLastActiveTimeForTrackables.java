package shengdong.redpoint.httpentity.response;

import lombok.Data;

import java.util.List;

@Data
public class ResponseTheLastActiveTimeForTrackables {
    private List<Position> positions;
}