package shengdong.data.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import shengdong.data.entity.Bt21Node;

import java.util.List;

/**
 * 硬件设备节点表Mapper映射方法接口
 */
@Mapper
@Component
public interface Bt21NodeMapper {

    /**
     * 根据id删除硬件设备节点表数据
     *
     * @param id 自增主键
     * @return 删除行数
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 新增硬件设备节点表数据
     *
     * @param record 硬件设备节点实体类
     * @return 新增行数
     */
    int insert(Bt21Node record);

    /**
     * 新增硬件设备节点表数据(根据一定规则)
     *
     * @param record 硬件设备节点实体类
     * @return 新增行数
     */
    int insertSelective(Bt21Node record);

    /**
     * 根据id查询硬件设备节点表数据
     *
     * @param id 硬件设备节点实体类
     * @return 硬件设备节点实体类
     */
    Bt21Node selectByPrimaryKey(Long id);

    /**
     * 更新硬件设备节点表数据(根据一定规则)
     *
     * @param record 硬件设备节点实体类
     * @return 更新行数
     */
    int updateByPrimaryKeySelective(Bt21Node record);

    /**
     * 更新硬件设备节点表数据
     *
     * @param record 硬件设备节点实体类
     * @return 更新行数
     */
    int updateByPrimaryKey(Bt21Node record);

    List<Bt21Node> selectByUserId(Long id);

    List<Bt21Node> selectAllNode();

    void truncateTable();
}