package shengdong.redpoint.httpentity.response;

import lombok.Data;

@Data
public class Addresses{
	private String phone;
	private String email;
}