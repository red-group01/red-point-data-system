package shengdong.redpoint.httpentity.request;

import lombok.Data;

@Data
public class RequestLocation{
	private int elevation;
	private String zip;
	private String streetAddress;
	private int orientation;
	private String city;
	private int latitude;
	private String description;
	private Object zip4;
	private String name;
	private Object streetAddress2;
	private String state;
	private UserParams userParams;
	private int longitude;
}