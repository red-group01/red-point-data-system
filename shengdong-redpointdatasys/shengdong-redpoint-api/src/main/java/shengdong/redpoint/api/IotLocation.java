package shengdong.redpoint.api;

import com.google.common.base.Strings;
import shengdong.common.util.JSONUtil;
import shengdong.redpoint.httpentity.request.RequestLocation;
import shengdong.redpoint.httpentity.response.ResponseLocation;
import shengdong.redpoint.httpentity.response.ResponsetAllLocations;
import shengdong.redpoint.model.query.Header;
import shengdong.redpoint.model.query.HttpMethod;
import shengdong.redpoint.model.query.HttpParam;

public class IotLocation extends IotApiBase {

    public IotLocation(IotApiConfig cnf, Header httpheader) {
        super(cnf, httpheader);
    }

    /**
     * 获取全部站点
     *
     * @return
     * @throws Exception
     */
    public ResponsetAllLocations getSites() throws Exception {
        return getSingleSite("");
    }

    /**
     * 获取指定站点
     *
     * @param uid 站点uid，为空则取全部
     * @return
     * @throws Exception
     */
    public ResponsetAllLocations getSingleSite(String uid) throws Exception {
        String url = config.getDomen();
        if (!Strings.isNullOrEmpty(uid)) {
            url += config.getLocationApiGetSingleSite() + uid;
        } else {
            url += config.getLocationApiGetSites();
        }
        HttpParam hp = new HttpParam();
        hp.setUrl(url);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.GET);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponsetAllLocations.class);
    }

    /**
     * 添加站点
     *
     * @param req
     * @return
     * @throws Exception
     */
    public ResponseLocation addSite(RequestLocation req) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getLocationApiAddSite());
        hp.setHeader(header);
        hp.setPostParam(JSONUtil.getObjectToString(req));
        hp.setMethod(HttpMethod.POST);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseLocation.class);
    }

    /**
     * 修改站点
     *
     * @param req
     * @param uid
     * @return
     * @throws Exception
     */
    public ResponseLocation updateSite(RequestLocation req, String uid) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getLocationApiUpdateSite() + uid);
        hp.setHeader(header);
        hp.setPostParam(JSONUtil.getObjectToString(req));
        hp.setMethod(HttpMethod.POST);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseLocation.class);
    }

    /**
     * 删除站点
     *
     * @param uid
     * @return
     * @throws Exception
     */
    public String deleteSite(String uid) throws Exception {

        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getLocationApiDeleteSite() + uid);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.DELETE);
        String msg = HttpClient.ExecApi(hp);
        return msg;
    }
}
