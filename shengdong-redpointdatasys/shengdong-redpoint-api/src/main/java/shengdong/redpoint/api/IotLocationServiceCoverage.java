package shengdong.redpoint.api;

import shengdong.common.util.JSONUtil;
import shengdong.redpoint.httpentity.response.ResponseLocationServiceCoverage;
import shengdong.redpoint.model.query.Header;
import shengdong.redpoint.model.query.HttpMethod;
import shengdong.redpoint.model.query.HttpParam;

public class IotLocationServiceCoverage extends IotApiBase {

    public IotLocationServiceCoverage(IotApiConfig cnf, Header httpheader) {
        super(cnf, httpheader);
    }

    /**
     * 获取项目中所有现有位置服务覆盖区域
     *
     * @return
     * @throws Exception
     */
    public ResponseLocationServiceCoverage getAllLocationServiceCoverage() throws Exception {
        String url = config.getDomen() + config.getGetAllLocationServiceCoverage();
        HttpParam hp = new HttpParam();
        hp.setUrl(url);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.GET);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseLocationServiceCoverage.class);
    }

    /**
     * 从特定子位置获取位置服务覆盖区域
     *
     * @param sublocation_uid
     * @return
     * @throws Exception
     */
    public ResponseLocationServiceCoverage getLocationServiceCoverageFromSublocations(String sublocation_uid)
        throws Exception {
        String url = config.getDomen() + config.getGetLocationServiceCoverageFromSublocations() + sublocation_uid;
        HttpParam hp = new HttpParam();
        hp.setUrl(url);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.GET);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseLocationServiceCoverage.class);
    }

    /**
     * 获取具有指定覆盖UID的单个覆盖区域
     *
     * @param sublocation_uid
     * @return
     * @throws Exception
     */
    public ResponseLocationServiceCoverage getSingleLocationServiceCoverage(String sublocation_uid) throws Exception {
        String url = config.getDomen() + config.getGetSingleLocationServiceCoverage() + sublocation_uid;
        HttpParam hp = new HttpParam();
        hp.setUrl(url);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.GET);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseLocationServiceCoverage.class);
    }

}
