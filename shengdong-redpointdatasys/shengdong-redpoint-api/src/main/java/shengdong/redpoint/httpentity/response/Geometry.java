package shengdong.redpoint.httpentity.response;

import java.util.List;
import lombok.Data;

@Data
public class Geometry{
	private String shape;
	private List<PositionsItem> positions;
	private int margin;
	private int radius;
}