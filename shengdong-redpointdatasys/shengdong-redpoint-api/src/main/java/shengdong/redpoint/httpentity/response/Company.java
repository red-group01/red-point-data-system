package shengdong.redpoint.httpentity.response;

import lombok.Data;

import java.io.Serializable;

@Data
public class Company implements Serializable {
    private String uid;
    private Role role;
    private String roleUid;
    private String scope;
    private String name;
    private long id;
    private int taggingsCount;
    private UserParams userParams;
}
