package shengdong.redpoint.websockentity;

import lombok.Data;

@Data
public class ResponsePositionMessage {
    private String method;
    private String jsonrpc;
    private ResponsePositionMessageParams params;
}