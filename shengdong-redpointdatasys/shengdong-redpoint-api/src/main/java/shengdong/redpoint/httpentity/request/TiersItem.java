package shengdong.redpoint.httpentity.request;

import lombok.Data;

@Data
public class TiersItem{
	private String shape;
	private int radius;
}