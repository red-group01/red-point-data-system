package shengdong.data.entity;

import lombok.Data;

import java.util.Date;

@Data
public class Bt50PoinsHisView {
    /**
     * email
     */
    private String email;

    /**
     * password
     */
    private String password;

    /**
     * project_uid
     */
    private String projectUid;
    private String subLocationUid;
    /**
     * 定位历史记录最后同步时间
     */
    private Date lastpointsynAt;
    /**
     * 警报历史记录最后同步时间
     */
    private Date lastalarmsynAt;
    /**
     * 区域事件最后同步时间
     */
    private Date lastalzonesynAt;
    private String serverIp;
    private long userId;
    private String token;
}
