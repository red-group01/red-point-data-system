package shengdong.data.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import shengdong.data.dao.IBt41PositionDao;
import shengdong.data.entity.Bt41Position;
import shengdong.data.mapper.Bt41PositionMapper;

import java.util.List;

/**
 * 位置历史信息Dao实现
 */
@Repository
public class Bt41PositionDaoImpl implements IBt41PositionDao {
    // 注入Mapper接口
    @Autowired
    private Bt41PositionMapper bt41PositionMapper;

    /**
     * 插入位置历史信息数据
     *
     * @param record 位置历史信息实体类
     * @return 插入的行数
     */

    @Override
    public int insert(Bt41Position record) {
        return bt41PositionMapper.insert(record);
    }

    @Override
    public void truncateTable() {
        bt41PositionMapper.truncateTable();
    }

    @Override
    public List<Bt41Position> getAllPostion(String projectUid) {
        return bt41PositionMapper.selectAllPostion(projectUid);
    }

    /**
     * 根据objUid查询位置历史信息表数据
     *
     * @param objUid 位置历史信息实体类
     * @return 位置历史信息实体类
     */
    @Override
    public Bt41Position selectByObjUid(String objUid) {
        return bt41PositionMapper.selectByObjUid(objUid);
    }

    /**
     * 根据objUid查询区间内的位置历史信息表数据
     *
     * @param objUid 位置历史信息实体类
     * @return 位置历史信息实体类
     */
    @Override
    public List<Bt41Position> selectByObjUidInTime(String objUid, String tsBegin, String tsEnd) {
        return bt41PositionMapper.selectByObjUidInTime(objUid, tsBegin, tsEnd);
    }

    @Override
    public List<Bt41Position> selectSublocationUidInTime(String sublocationUid, String tsBegin, String tsEnd) {
        return bt41PositionMapper.selectSublocationUidInTime(sublocationUid, tsBegin, tsEnd);
    }

    @Override
    public List<Bt41Position> selectSublocationUidInTime2(String sublocationUid, String tsBegin, String tsEnd) {
        return bt41PositionMapper.selectSublocationUidInTime2(sublocationUid, tsBegin, tsEnd);
    }

    @Override
    public List<Bt41Position> selectInObjUidInTime(String tsBegin, String tsEnd, List<String> objUids) {
        return bt41PositionMapper.selectInObjUidInTime(tsBegin, tsEnd, objUids);
    }

    @Override
    public Bt41Position selectNewDataTime() {
        return bt41PositionMapper.selectNewDataTime();
    }
}
