package shengdong.redpoint.httpentity.response;

import java.util.List;
import lombok.Data;

@Data
public class ResponseTrackableObjects{
	private List<TrackableObjectsItem> trackableObjects;
}