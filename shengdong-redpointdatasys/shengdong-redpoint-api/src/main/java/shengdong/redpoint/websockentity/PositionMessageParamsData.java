package shengdong.redpoint.websockentity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class PositionMessageParamsData {
    @JsonProperty("pe_dim")
    private int peDim;
    private int fom;
    private int ms;
    @JsonProperty("sl_id")
    private String slId;
    @JsonProperty("project_uid")
    private String projectUid;
    private String type;
    private String mac;
    @JsonProperty("dev_id")
    private String devId;
    private boolean inactive;
    @JsonProperty("edge_corr")
    private boolean edgeCorr;
    @JsonProperty("to_type_id")
    private int toTypeId;
    private int locationId;
    private int x;
    private int y;
    private int sl;
    @JsonProperty("company_uid")
    private String companyUid;
    private int z;
    private int id;
    private int sfsn;
    private int ts;
    @JsonProperty("slvr_mode")
    private String slvrMode;
}