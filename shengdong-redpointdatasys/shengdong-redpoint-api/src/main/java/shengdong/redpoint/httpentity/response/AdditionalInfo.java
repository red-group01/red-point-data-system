package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class AdditionalInfo {
    @JsonProperty("contact_name")
    private String contactName;
    @JsonProperty("contact_uid")
    private String contactSlUid;
    private int distance;
    @JsonProperty("contact_uid")
    private String contactUid;
    @JsonProperty("contact_y")
    private int contactY;
    @JsonProperty("contact_x")
    private int contactX;
    private String name;
    @JsonProperty("contact_category")
    private String contactCategory;
    private String category;
    @JsonProperty("zone_violation_type")
    private String zoneViolationType;
    @JsonProperty("zone_uid")
    private String zoneUid;
    private String text;
    @JsonProperty("zone_alarm_level")
    private int zoneAlarmLevel;
}