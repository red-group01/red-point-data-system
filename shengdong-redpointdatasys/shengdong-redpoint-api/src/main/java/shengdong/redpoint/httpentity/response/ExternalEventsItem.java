package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ExternalEventsItem {
    @JsonProperty("event_type")
    private String eventType;
    @JsonProperty("event_info")
    private String eventInfo;
    @JsonProperty("mac_address")
    private String macAddress;
    @JsonProperty("object_name")
    private String objectName;
    private long x;
    private long y;
    @JsonProperty("project_uid")
    private String projectUid;
    private long z;
    @JsonProperty("object_uid")
    private String objectUid;
    @JsonProperty("area_uid")
    private String areaUid;
    private String ts;
}