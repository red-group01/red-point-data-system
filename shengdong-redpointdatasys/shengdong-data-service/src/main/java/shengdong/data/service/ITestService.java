package shengdong.data.service;

import shengdong.data.entity.TestTable;

public interface ITestService {
    public TestTable listTestTableById(Integer id);
    public int insertTestTable(TestTable testTable);
}
