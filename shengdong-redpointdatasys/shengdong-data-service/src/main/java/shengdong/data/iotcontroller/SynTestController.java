package shengdong.data.iotcontroller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import shengdong.common.error.impl.BaseResponse;
import shengdong.common.util.CommonUtil;
import shengdong.data.dao.IBt23TrackableDao;
import shengdong.data.dao.IBt31ProjectDao;
import shengdong.data.dao.IBt41PositionDao;
import shengdong.data.entity.Bt14User;
import shengdong.data.entity.Bt23Trackable;
import shengdong.data.service.IBaseInfoService;
import shengdong.data.service.IDataService;
import shengdong.redpoint.api.IotApiConfig;
import shengdong.redpoint.model.query.Header;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/iot/testsyn")
public class SynTestController {

    @Autowired
    private IBaseInfoService bt14UserService;
    @Autowired
    private IDataService dataService;
    @Autowired
    private IBaseInfoService infoService;
    @Autowired
    IBt31ProjectDao daoproj;

    /**
     * 无基础数据 响应百、千、万条
     *
     * @return
     */
    @GetMapping("/ptest1")
    public BaseResponse<String> test1() throws Exception {
        t2001();
        t2002();
        t2003();
        return BaseResponse.success("ok");
    }

    /**
     * 100条基础数据 响应百、千、万条
     *
     * @return
     */
    @GetMapping("/ptest2")
    public BaseResponse<String> test2() throws Exception {
        t2004();
        t2005();
        t2006();
        return BaseResponse.success("ok");
    }

    /**
     * 1000条基础数据 响应百、千、万条
     *
     * @return
     */
    @GetMapping("/ptest3")
    public BaseResponse<String> test3() throws Exception {
        t2007();
        t2008();
        t2009();
        return BaseResponse.success("ok");
    }

    /**
     * 基础0条，执行T2001、T2010测试取100条
     *
     * @return
     */
    @GetMapping("/t2001")
    public BaseResponse<String> t2001() throws Exception {
        log.info("T2001、T2010");
        //daoproj.trancateTable();
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setProjectsApiGetProjects("shengdong/api/projects?id=a");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        infoService.synProject(cnf, h, uent);
        return BaseResponse.success("ok");
    }

    /**
     * 基础0条，执行T2002、T2011测试取1000条
     *
     * @return
     */
    @GetMapping("/t2002")
    public BaseResponse<String> t2002() throws Exception {
        log.info("T2002、T2011");
        //daoproj.trancateTable();
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setProjectsApiGetProjects("shengdong/api/projects?id=b");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        infoService.synProject(cnf, h, uent);
        return BaseResponse.success("ok");
    }

    /**
     * 基础0条，执行T2003、T2012测试取10000条
     *
     * @return
     */
    @GetMapping("/t2003")
    public BaseResponse<String> t2003() throws Exception {
        log.info("T2003、T2012");
        //daoproj.trancateTable();
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setProjectsApiGetProjects("shengdong/api/projects?id=c");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        infoService.synProject(cnf, h, uent);
        return BaseResponse.success("ok");
    }

    /**
     * 基础1000条 执行T2004、T2013测试请求100条
     *
     * @return
     */
    @GetMapping("/t2004")
    public BaseResponse<String> t2004() throws Exception {
        log.info("T2004、T2013");
        //daoproj.trancateTable();
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setProjectsApiGetProjects("shengdong/api/projects?id=a");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        infoService.synProject(cnf, h, uent);
        return BaseResponse.success("ok");
    }

    /**
     * 基础1000条 执行T2005、T2014测试请求1000条
     *
     * @return
     */
    @GetMapping("/t2005")
    public BaseResponse<String> t2005() throws Exception {
        log.info("T2005、T2014");
        //daoproj.trancateTable();
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setProjectsApiGetProjects("shengdong/api/projects?id=b");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        infoService.synProject(cnf, h, uent);
        return BaseResponse.success("ok");
    }

    /**
     * 基础1000条 执行T2006、T2015测试请求10000条
     *
     * @return
     */
    @GetMapping("/t2006")
    public BaseResponse<String> t2006() throws Exception {
        log.info("T2006、T2015");
        //daoproj.trancateTable();
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setProjectsApiGetProjects("shengdong/api/projects?id=c");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        infoService.synProject(cnf, h, uent);
        return BaseResponse.success("ok");
    }

    /**
     * 基础10000条 执行T2007、T2016测试请求100条
     *
     * @return
     */
    @GetMapping("/t2007")
    public BaseResponse<String> t2007() throws Exception {
        log.info("T2007、T2016");
        //daoproj.trancateTable();
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setProjectsApiGetProjects("shengdong/api/projects?id=a");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        infoService.synProject(cnf, h, uent);
        return BaseResponse.success("ok");
    }

    /**
     * 基础1000条 执行T2008、T2017测试请求1000条
     *
     * @return
     */
    @GetMapping("/t2008")
    public BaseResponse<String> t2008() throws Exception {
        log.info("T2008、T2017");
        //daoproj.trancateTable();
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setProjectsApiGetProjects("shengdong/api/projects?id=b");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        infoService.synProject(cnf, h, uent);
        return BaseResponse.success("ok");
    }

    /**
     * 基础10000条 执行T2009、T2018测试请求10000条
     *
     * @return
     */
    @GetMapping("/t2009")
    public BaseResponse<String> t2009() throws Exception {
        log.info("T2009、T2018");
        //daoproj.trancateTable();
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setProjectsApiGetProjects("shengdong/api/projects?id=c");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        infoService.synProject(cnf, h, uent);
        return BaseResponse.success("ok");
    }

    @Autowired
    IBt41PositionDao daoPosition;
    @Autowired
    IBt23TrackableDao daoTrackable;

    /**
     * 历史数据压缩文件处理 T2019 T2020 T2021
     *
     * @param path
     * @return
     * @throws Exception
     */
    @GetMapping("/t2019")
    public BaseResponse<String> t2019(String path) throws Exception {
        ApplicationHome home = new ApplicationHome(getClass());
        daoPosition.truncateTable();
        daoTrackable.truncateTable();
        Bt23Trackable ent = new Bt23Trackable();
        ent.setTrackableUid("_VmsjmCxSDysnNBNu5mB2w");
        daoTrackable.insert(ent);
        //        Path.of(path),Path.of(path) + ".zip", StandardCopyOption.REPLACE_EXISTING

        String src = home + "/download/" + UUID.randomUUID().toString() + ".zip";
        String des = home + "/download/" + UUID.randomUUID().toString();
        Files.copy(Path.of(path), Path.of(src), StandardCopyOption.REPLACE_EXISTING);
        StopWatch sw = new StopWatch();
        sw.start();
        dataService.extracted(src, des);
        sw.stop();
        String msg = "入库" + daoPosition.getAllPostion("").stream().count() + "条" + CommonUtil.msFormat(
            sw.getTotalTimeMillis());
        log.info(msg);
        return BaseResponse.success(msg);
    }

    /**
     * T2022 T2023 T2024
     *
     * @return
     * @throws Exception
     */
    @GetMapping("/t2022")
    public BaseResponse<String> t2022() throws Exception {
        return BaseResponse.success("ok 入库" + daoPosition.getAllPostion("").stream().count() + "条");
    }
}
