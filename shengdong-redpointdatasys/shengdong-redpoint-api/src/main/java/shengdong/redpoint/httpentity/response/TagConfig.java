package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class TagConfig {
    private Boolean stationary;
    @JsonProperty("t2t_no_alarm")
    private List<T2tNoAlarm> t2tNoAlarm;
    @JsonProperty("t2t_cfg")
    private T2tCfg t2tCfg;
}