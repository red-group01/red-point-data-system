package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Announce {
    private String flags;
    @JsonProperty("last_ts")
    private String lastTs;
    private long ts;
}