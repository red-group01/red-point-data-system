package shengdong.redpoint.httpentity.request;

import lombok.Data;

@Data
public class Geometry {
    private Integer radius;
}
