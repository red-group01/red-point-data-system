package shengdong.redpoint.websockentity;

import lombok.Data;

@Data
public class ResponsePositionMessageParams {
    private PositionMessageParamsData data;
    private String subscription;
}