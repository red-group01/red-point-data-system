package shengdong.common.util;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.logging.log4j.util.Strings;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.jdbc.datasource.init.ScriptUtils;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;

public class DbUtil {

    /**
     * // 待插入数据链表 List<TableDTO> needToInsert = ……； // 进行新增 Consumer<List<TableDTO>> consumer = o ->
     * mapper.insertTable(o); batchSplitInsert(needToInsert, consumer);
     */
    //    public <T> void batchSplitInsert(List<T> list, Consumer insertFunc) {
    //        List<List<T>> all = new ArrayList<>();
    //        if (list.size() > maxInsertItemNumPerTime) {
    //            int i = 0;
    //            while (i < list.size()) {
    //                List subList = list.subList(i, i + maxInsertItemNumPerTime);
    //                i = i + maxInsertItemNumPerTime;
    //                all.add(subList);
    //            }
    //            all.parallelStream().forEach(insertFunc);
    //        } else {
    //            insertFunc.accept(list);
    //        }
    //    }

    //    void batch(List<T> list, Consumer func) {
    //        SqlSession sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH, false);
    //        StudentMapper studentMapperNew = sqlSession.getMapper(StudentMapper.class);
    //        list.stream().forEach(func);
    //        sqlSession.commit();
    //        sqlSession.clearCache();
    //    }

    /**
     * 执行sql文件
     *
     * @param sqlFileName
     * @param dbConfig
     * @throws IOException
     */
    void execsql(String sqlFileName, String dbConfig) throws IOException {
        String resource = "mybatis-config.xml";
        if (!Strings.isNotEmpty(dbConfig)) {
            resource = dbConfig;
        }
        //获取配置文件流
        InputStream resourceAsStream = Resources.getResourceAsStream(resource);
        //创建SqlSessionFactory对象
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(resourceAsStream);
        SqlSession session = sqlSessionFactory.openSession();
        //创建数据库连接（测试用）
        Connection connection = session.getConnection();
        ClassPathResource rc = new ClassPathResource(sqlFileName);
        EncodedResource er = new EncodedResource(rc, "utf-8");
        ScriptUtils.executeSqlScript(connection, er);
    }
}
