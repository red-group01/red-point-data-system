package shengdong.data.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import shengdong.data.dao.IBt91LifeCycleDao;
import shengdong.data.entity.Bt91LifeCycle;
import shengdong.data.mapper.Bt91LifeCycleMapper;

/**
 * 数据生命周期Dao实现
 */
@Repository
public class Bt91LifeCycleDaoImpl implements IBt91LifeCycleDao {
    // 注入Mapper接口
    @Autowired
    private Bt91LifeCycleMapper bt91LifeCycleMapper;

    /**
     * 插入数据生命周期数据
     *
     * @param record 数据生命周期实体类
     * @return 插入的行数
     */
    @Override
    public int insert(Bt91LifeCycle record) {
        return bt91LifeCycleMapper.insert(record);
    }
}
