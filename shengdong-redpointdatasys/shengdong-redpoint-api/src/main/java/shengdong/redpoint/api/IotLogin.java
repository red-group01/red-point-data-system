package shengdong.redpoint.api;

import shengdong.common.util.JSONUtil;
import shengdong.redpoint.httpentity.response.ResponseLogin;
import shengdong.redpoint.httpentity.response.ResponseValidateToken;
import shengdong.redpoint.model.query.HttpMethod;
import shengdong.redpoint.model.query.HttpParam;

public class IotLogin extends IotApiBase {

    public IotLogin(IotApiConfig cnf) {
        super(cnf, null);
    }

    /**
     * 用户登录接口
     *
     * @return
     */
    public ResponseLogin login(String email, String password) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setMethod(HttpMethod.POST);
        hp.setUrl(config.getDomen() + config.getAuthApiLogin());
        hp.setPostParam("{\"email\":\"" + email + "\",\"password\":\"" + password + "\"}");
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseLogin.class);
    }

    /**
     * 用户token验证
     *
     * @return
     */
    public ResponseValidateToken validateToken(String email, String token) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setMethod(HttpMethod.POST);
        hp.setUrl(config.getDomen() + config.getAuthApiValidateToken());
        hp.setPostParam("{\"email\":\"" + email + "\",\"token\":\"" + token + "\",\"onBehalfToken\":\"\"}");
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseValidateToken.class);
    }

    /**
     * 刷新用户token
     *
     * @return
     */
    public ResponseValidateToken refreshToken(String email, String token) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setMethod(HttpMethod.POST);
        hp.setUrl(config.getDomen() + config.getAuthApiRefreshToken());
        hp.setPostParam("{\"email\":\"" + email + "\",\"token\":\"" + token + "\"}");
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseValidateToken.class);
    }

    /**
     * 用户注销
     *
     * @return
     */
    public ResponseLogin logOut(String email, String password) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setMethod(HttpMethod.POST);
        hp.setUrl(config.getDomen() + config.getAuthApiLogout());
        hp.setPostParam("{\"email\":\"" + email + "\",\"password\":\"" + password + "\"}");
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseLogin.class);
    }
}
