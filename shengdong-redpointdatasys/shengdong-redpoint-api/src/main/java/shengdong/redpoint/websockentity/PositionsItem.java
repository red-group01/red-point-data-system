package shengdong.redpoint.websockentity;

import lombok.Data;

@Data
public class PositionsItem{
	private String x;
	private String y;
}