package shengdong.data.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 硬件设备节点实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Bt21Node {

    //自增主键
    private Long id;

    //硬件设备节点唯一标识
    private String nodeUid;

    //公司唯一标识
    private String companyUid;

    //项目唯一标识
    private String projectUid;

    //硬件设备节点mac地址
    private String macAddress;

    //硬件设备节点名称
    private String name;

    //父节点mac地址
    private String parentMacAddress;

    //桥节点mac地址
    private String bridgeMacAddress;

    //节点软件版本
    private String swVersion;

    //节点硬件信息
    private String hwInfo;

    //节点最近与服务器的通讯时间戳
    private String lastHeard;

    //广播标志
    private String announceFlags;

    //服务器首次检测到节点时的UTC时间戳
    private Long announceTs;

    //服务器最后一次检测到节点时的UTC时间戳
    private String announceLastTs;

    //节点类型
    private String type;

    //无线电配置
    private String uwbTrxConfig;

    //请求时的UTC时间戳
    private Date currentSystemTimestamp;

    //是否连通外部电源
    private String statusExtPow;

    //指示电池电量是否不足
    private String statusLowBat;

    //指示节点是否已同步
    private String statusSync;

    //指示是否已收到计划
    private String statusTdoa;

    //电池电量，单位为毫伏
    private String batteryMv;

    //电池电量（百分比）
    private String batteryPr;

    //Unique ID of the SiteWise server
    private Long nodeId;

    //创建时间（UTC）
    private Date createdAt;

    //更新时间（UTC）
    private Date updatedAt;

    //数据最后更新者
    private String creatorAt;

    //数据信息备注
    private String remark;
    /**
     * 项目所属用户id
     */
    private Long userId;
    /**
     * 已删除
     */
    private Boolean deleteMark;

}