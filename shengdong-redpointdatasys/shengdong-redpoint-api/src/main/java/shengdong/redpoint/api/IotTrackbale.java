package shengdong.redpoint.api;

import com.google.common.base.Strings;
import shengdong.common.util.JSONUtil;
import shengdong.redpoint.httpentity.request.RequestTrackable;
import shengdong.redpoint.httpentity.response.*;
import shengdong.redpoint.model.query.Header;
import shengdong.redpoint.model.query.HttpMethod;
import shengdong.redpoint.model.query.HttpParam;

import java.text.MessageFormat;
import java.util.Date;

public class IotTrackbale extends IotApiBase {

    public IotTrackbale(IotApiConfig cnf, Header httpheader) {
        super(cnf, httpheader);
    }

    /**
     * 获取项目的所有可跟踪项
     *
     * @return
     * @throws Exception
     */
    public ResponseTrackableObjects getTrackables() throws Exception {
        return getSingleTrackable("");
    }

    /**
     * 获取具有指定可跟踪UID的可跟踪对象
     *
     * @param uid
     * @return
     * @throws Exception
     */
    public ResponseTrackableObjects getSingleTrackable(String uid) throws Exception {
        HttpParam hp = new HttpParam();
        String url = config.getDomen();
        if (!Strings.isNullOrEmpty(uid)) {
            url += config.getTrackablesApiGetSingleTrackable() + uid;
        } else {
            url += config.getTrackablesApiGetTrackables();
        }
        hp.setUrl(url);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.GET);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseTrackableObjects.class);
    }

    /**
     * 获取与特定节点的MAC地址关联的单个可跟踪对象。
     *
     * @param mac_address
     * @return
     * @throws Exception
     */
    public TrackableObjectsItem getTrackableByMacAddress(String mac_address) throws Exception {
        HttpParam hp = new HttpParam();
        String url = config.getTrackablesApiGetTrackableByMacAddress() + mac_address;
        hp.setUrl(url);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.GET);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), TrackableObjectsItem.class);
    }

    /**
     * 在指定的子地点和时间段内激活所有可跟踪对象。
     *
     * @param sublocation_uid
     * @param from
     * @param to
     * @return
     * @throws Exception
     */
    public ResponseTrackableObjects getTrackablesActiveForPeriod(String sublocation_uid, Date from, Date to)
        throws Exception {
        HttpParam hp = new HttpParam();
        String url =
            config.getTrackablesApiGetTrackablesActiveForPeriod() + "sublocation_uid=" + sublocation_uid + "&from=" + from.getTime() + "&to=" + to.getTime();
        hp.setUrl(url);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.GET);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseTrackableObjects.class);
    }

    /**
     * 获取已报告给服务器的所有项目可跟踪对象的最后报告信息。
     *
     * @return
     * @throws Exception
     */
    public ResponseTheLastActiveTimeForTrackables getTheLastActiveTimeForTrackables() throws Exception {
        HttpParam hp = new HttpParam();
        String url = config.getTrackablesApiGetTheLastActiveTimeForTrackables();
        hp.setUrl(url);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.GET);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseTheLastActiveTimeForTrackables.class);
    }

    /**
     * 创建一个新的可跟踪对象。
     *
     * @param req
     * @return
     * @throws Exception
     */
    public ResponseTrackable addTrackables(RequestTrackable req) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getTrackablesApiAddTrackables());
        hp.setHeader(header);
        hp.setPostParam(JSONUtil.getObjectToString(req));
        hp.setMethod(HttpMethod.POST);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseTrackable.class);
    }

    /**
     * 使用指定的可跟踪UID编辑可跟踪对象
     *
     * @param req
     * @return
     * @throws Exception
     */
    public ResponseTrackable updateTrackable(RequestTrackable req) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getTrackablesApiUpdateTrackable());
        hp.setHeader(header);
        hp.setPostParam(JSONUtil.getObjectToString(req));
        hp.setMethod(HttpMethod.PUT);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseTrackable.class);
    }

    /**
     * 为指定的可跟踪对象分配组
     *
     * @param trackable_uid 标签uid
     * @param group_uid     组uid
     * @return
     * @throws Exception
     */
    public String updateGroupsForTrackables(String trackable_uid, String[] group_uid) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + MessageFormat.format(config.getTrackablesApiUpdateGroupsForTrackables(),
            trackable_uid));
        hp.setHeader(header);
        hp.setPostParam(JSONUtil.getObjectToString(group_uid));
        hp.setMethod(HttpMethod.PUT);
        String msg = HttpClient.ExecApi(hp);
        return msg;
    }

    /**
     * 删除具有指定可跟踪UID的可跟踪对象
     *
     * @param trackable_uid
     * @return
     * @throws Exception
     */
    public String deleteTrackables(String trackable_uid) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getTrackablesApiDeleteTrackables() + trackable_uid);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.DELETE);
        String msg = HttpClient.ExecApi(hp);
        return msg;
    }

    /**
     * 获取到节点MAC地址的所有可跟踪关联的列表
     *
     * @return
     * @throws Exception
     */
    public ResponseTrackableObjectsAssociations getTrackableAssociations() throws Exception {

        return getSingleTrackableAssociation("");
    }

    /**
     * 根据指定的可跟踪关联UID获取单个可跟踪关联
     *
     * @param uid
     * @return
     * @throws Exception
     */
    public ResponseTrackableObjectsAssociations getSingleTrackableAssociation(String uid) throws Exception {
        HttpParam hp = new HttpParam();
        String url = config.getDomen() + config.getTrackablesApiGetSingleTrackableAssociation() + uid;
        hp.setUrl(url);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.GET);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseTrackableObjectsAssociations.class);
    }

    /**
     * 获取指定间隔内特定对象的可跟踪关联列表。
     *
     * @param start
     * @param end
     * @param uid
     * @return
     * @throws Exception
     */
    public ResponseTrackableObjectsAssociations getTrackableAssociationsHistory(Date start, Date end, String uid)
        throws Exception {
        HttpParam hp = new HttpParam();
        String url =
            config.getDomen() + config.getTrackablesApiGetTrackableAssociationsHistory() + "start=" + start.getTime() + "&end=" + end.getTime() + "&obj_uid=" + uid;
        hp.setUrl(url);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.GET);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseTrackableObjectsAssociations.class);
    }

    /**
     * 添加一个新的可跟踪关联
     *
     * @param mac_address
     * @param obj_uid
     * @return
     * @throws Exception
     */
    public ResponseTrackableObjectsAssociations addTrackableAssociation(String mac_address, String obj_uid)
        throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getTrackablesApiAddTrackableAssociation());
        hp.setHeader(header);
        hp.setPostParam("{\"mac_address\":\"" + mac_address + "\",\"obj_uid\":\"" + obj_uid + "\"}");
        hp.setMethod(HttpMethod.POST);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseTrackableObjectsAssociations.class);
    }

    /**
     * 编辑具有指定关联UID的可跟踪关联
     *
     * @param uid
     * @param mac_address
     * @param obj_uid
     * @return
     * @throws Exception
     */
    public ResponseTrackableObjectsAssociations updateTrackableAssociation(String uid, String mac_address,
        String obj_uid) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getTrackablesApiUpdateTrackableAssociation() + uid);
        hp.setHeader(header);
        hp.setPostParam("{\"mac_address\":\"" + mac_address + "\",\"obj_uid\":\"" + obj_uid + "\"}");
        hp.setMethod(HttpMethod.POST);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseTrackableObjectsAssociations.class);
    }

    /**
     * 关闭一个可跟踪的关联
     *
     * @param obj_uid
     * @return
     * @throws Exception
     */
    public ResponseTrackableObjectsAssociations closeTrackableAssociation(String obj_uid) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getTrackablesApiCloseTrackableAssociation());
        hp.setHeader(header);
        hp.setPostParam("{\"obj_uid\":\"" + obj_uid + "\"}");
        hp.setMethod(HttpMethod.PUT);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseTrackableObjectsAssociations.class);
    }

    /**
     * 删除一个可跟踪的关联。
     *
     * @param uid
     * @return
     * @throws Exception
     */
    public String deleteTrackableAssociation(String uid) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getTrackablesApiDeleteTrackableAssociation() + uid);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.DELETE);
        String msg = HttpClient.ExecApi(hp);
        return msg;
    }

    /**
     * 更新可跟踪对象的传感器配置
     *
     * @param uid
     * @param cnf
     * @return
     * @throws Exception
     */
    public String updateSensorConfigForTrackableObject(String uid, SensorConfig cnf) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(
            config.getDomen() + MessageFormat.format(config.getTrackablesApiUpdateSensorConfigForTrackableObject(),
                uid));
        hp.setHeader(header);
        hp.setPostParam(JSONUtil.getObjectToString(cnf));
        hp.setMethod(HttpMethod.PUT);
        String msg = HttpClient.ExecApi(hp);
        return msg;
    }

    /**
     * 更新可跟踪对象的传感器配置
     *
     * @param uid
     * @param cnf
     * @return
     * @throws Exception
     */
    public SensorConfig getSensorConfigForTrackableObject(String uid, SensorConfig cnf) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(
            config.getDomen() + MessageFormat.format(config.getTrackablesApiGetSensorConfigForTrackableObject(), uid));
        hp.setHeader(header);
        hp.setPostParam(JSONUtil.getObjectToString(cnf));
        hp.setMethod(HttpMethod.PUT);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), SensorConfig.class);
    }

    /**
     * 请求可跟踪对象的传感器数据 数据会由websocket响应
     *
     * @param uid
     * @param cnf
     * @return
     * @throws Exception
     */
    public String requestSensorDataForTrackableObject(String uid, SensorConfig cnf) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + MessageFormat.format(config.getTrackablesApiRequestSensorDataForTrackableObject(),
            uid));
        hp.setHeader(header);
        hp.setPostParam(JSONUtil.getObjectToString(cnf));
        hp.setMethod(HttpMethod.PUT);
        String msg = HttpClient.ExecApi(hp);
        return msg;
    }

    /**
     * 发送可跟踪对象的传感器数据
     *
     * @param uid
     * @param cnf
     * @return
     * @throws Exception
     */
    public String sendSensorDataForTrackableObject(String uid, SensorConfig cnf) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(
            config.getDomen() + MessageFormat.format(config.getTrackablesApiSendSensorDataForTrackableObject(), uid));
        hp.setHeader(header);
        hp.setPostParam(JSONUtil.getObjectToString(cnf));
        hp.setMethod(HttpMethod.PUT);
        String msg = HttpClient.ExecApi(hp);
        return msg;
    }

}
