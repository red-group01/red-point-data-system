package shengdong.redpoint.model.query;

import lombok.Data;

/**
 * 接口调用时固定携带的头部参数
 */
@Data
public class Header {
    /**
     * 用户账号
     */
    private String email = "";
    /**
     * 登录验证成功后的token
     */
    private String token = "";
    /**
     * 项目uid
     */
    private String project = "";
}
