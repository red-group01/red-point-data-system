package shengdong.data.dao;

import shengdong.data.entity.Bt32Location;

import java.util.List;

/**
 * 项目地点Dao接口
 */
public interface IBt32LocationDao {

    /**
     * 插入项目地点数据
     *
     * @param record 项目地点实体类
     * @return 插入的行数
     */
    public int insert(Bt32Location record);

    List<Bt32Location> getLocationByUserId(Long id);

    int updateByid(Bt32Location local);

    List<Bt32Location> getAllLocation();

    void truncateTable();
}
