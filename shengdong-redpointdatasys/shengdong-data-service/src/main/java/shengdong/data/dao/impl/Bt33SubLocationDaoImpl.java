package shengdong.data.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import shengdong.data.dao.IBt33SubLocationDao;
import shengdong.data.entity.Bt33SubLocation;
import shengdong.data.mapper.Bt33SubLocationMapper;

import java.util.List;

/**
 * 地点子区域Dao实现
 */
@Repository
public class Bt33SubLocationDaoImpl implements IBt33SubLocationDao {
    // 注入Mapper接口
    @Autowired
    private Bt33SubLocationMapper bt33SubLocationMapper;

    /**
     * 插入地点子区域数据
     *
     * @param record 地点子区域实体类
     * @return 插入的行数
     */
    @Override
    public int insert(Bt33SubLocation record) {
        return bt33SubLocationMapper.insert(record);
    }

    @Override
    public List<Bt33SubLocation> getUserSubLocation(Long id) {
        return bt33SubLocationMapper.selectByUserid(id);
    }

    @Override
    public int updateByid(Bt33SubLocation local) {
        return bt33SubLocationMapper.updateByPrimaryKey(local);
    }

    @Override
    public void truncateTable() {
        bt33SubLocationMapper.truncateTable();
    }

    @Override
    public List<Bt33SubLocation> getAllSublocation() {
        return bt33SubLocationMapper.selectAllSublocation();
    }

    @Override
    public List<Bt33SubLocation> selectByLocationUid(String locationUid) {
        return bt33SubLocationMapper.selectByLocationUid(locationUid);
    }
}
