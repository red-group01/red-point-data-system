package shengdong.redpoint.api;

import com.google.common.base.Strings;
import shengdong.common.util.JSONUtil;
import shengdong.redpoint.httpentity.response.ResponseEventsHistory;
import shengdong.redpoint.model.query.Header;
import shengdong.redpoint.model.query.HttpMethod;
import shengdong.redpoint.model.query.HttpParam;

import java.util.Date;

public class IotEventHistory extends IotApiBase {

    public IotEventHistory(IotApiConfig cnf, Header httpheader) {
        super(cnf, httpheader);
    }

    /**
     * 获取事件历史记录 uploadedFrom和uploadedTo字段不能与from、to字段一起使用。
     *
     * @param from
     * @param to
     * @param uploadedFrom
     * @param uploadedTo
     * @param event_type
     * @param track_uid
     * @param format
     * @param limit
     * @param archive
     * @param page
     * @return
     * @throws Exception
     */
    public ResponseEventsHistory getSingleFloorplan(Date from, Date to, Date uploadedFrom, Date uploadedTo,
        String event_type[], String track_uid[], String format, String limit, String archive, Integer page)
        throws Exception {
        HttpParam hp = new HttpParam();
        //format={json,csv}
        String url = config.getDomen() + config.getEventHistoryGetExternalEvents() + "format=json&event_type[]=pws";
        for (String i : track_uid) {
            url += "&track_uid[]={uid}";
        }
        if (Strings.isNullOrEmpty(archive)) {
            url += "&archive=" + archive;
        }
        if (Strings.isNullOrEmpty(limit)) {
            url += "&limit=" + limit;
        }
        if (page > 1) {
            url += "&page=" + page;
        }
        if (from != null) {
            url += "&from=" + from.getTime();
            if (to != null) {
                url += "&to=" + to.getTime();
            }
        } else if (uploadedFrom != null) {
            url += "&uploadedFrom=" + uploadedFrom.getTime();
            if (to != null) {
                url += "&uploadedTo=" + uploadedTo.getTime();
            }
        }

        hp.setHeader(header);
        hp.setMethod(HttpMethod.GET);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseEventsHistory.class);
    }
}
