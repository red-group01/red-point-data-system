package shengdong.redpoint.api;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import shengdong.common.util.JSONUtil;
import shengdong.redpoint.httpentity.response.ResponseWsHost;
import shengdong.redpoint.model.query.HttpMethod;
import shengdong.redpoint.model.query.HttpParam;

import java.net.URI;

public class IotWebSocketClient extends WebSocketClient {

    /**
     * Constructs a WebSocketClient instance and sets it to the connect to the specified URI. The channel does not
     * attampt to connect automatically. The connection will be established once you call <var>connect</var>.
     *
     * @param serverUri the server URI to connect to
     */
    public IotWebSocketClient(URI serverUri) {
        super(serverUri);
    }

    /**
     * Called after an opening handshake has been performed and the given websocket is ready to be written on.
     *
     * @param handshakedata The handshake of the websocket instance
     */
    @Override
    public void onOpen(ServerHandshake handshakedata) {

    }

    /**
     * Callback for string messages received from the remote host
     *
     * @param message The UTF-8 decoded message that was received.
     * @see #onMessage(ByteBuffer)
     **/
    @Override
    public void onMessage(String message) {

    }

    /**
     * Called after the websocket connection has been closed.
     *
     * @param code   The codes can be looked up here: {@link CloseFrame}
     * @param reason Additional information string
     * @param remote Returns whether or not the closing of the connection was initiated by the remote host.
     **/
    @Override
    public void onClose(int code, String reason, boolean remote) {

    }

    /**
     * Called when errors occurs. If an error causes the websocket connection to fail
     * {@link #onClose(int, String, boolean)} will be called additionally.<br> This method will be called primarily
     * because of IO or protocol errors.<br> If the given exception is an RuntimeException that probably means that you
     * encountered a bug.<br>
     *
     * @param ex The exception causing this error
     **/
    @Override
    public void onError(Exception ex) {

    }

    /**
     * 获取webSocket服务器地址
     *
     * @return
     * @throws Exception
     */
    public static URI getWsUrl(String ip) throws Exception {
        HttpParam hp = new HttpParam();
        String url = ip + "/api/near_miss/";
        hp.setUrl(url);
        hp.setMethod(HttpMethod.GET);
        ResponseWsHost rw = JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseWsHost.class);
        return new URI(ip + "/" + rw.getWsHost());
    }

    public void add() {

    }
}
