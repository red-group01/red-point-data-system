package shengdong.redpoint.httpentity.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Position {
    @JsonProperty("sublocation_uid")
    private String sublocationUid;
    @JsonProperty("sl_uid")
    private String slUid;
    @JsonProperty("sl_id")
    private String slId;
    private int x;
    private int y;
    private int z;
}