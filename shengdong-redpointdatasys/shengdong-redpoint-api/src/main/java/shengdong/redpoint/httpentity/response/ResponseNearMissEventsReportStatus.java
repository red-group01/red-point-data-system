package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ResponseNearMissEventsReportStatus {
    private Object output;
    private String request;
    private String uid;
    private Input input;
    @JsonProperty("id_txt")
    private String idTxt;
    private int id;
    private int status2;
    private Object error;
    private int status;
}