package shengdong.redpoint.api;

import com.google.common.base.Strings;
import shengdong.common.util.JSONUtil;
import shengdong.redpoint.httpentity.request.RequestFloorplan;
import shengdong.redpoint.httpentity.response.FloorplansItem;
import shengdong.redpoint.httpentity.response.ResponseFloorplan;
import shengdong.redpoint.model.query.Header;
import shengdong.redpoint.model.query.HttpMethod;
import shengdong.redpoint.model.query.HttpParam;

public class IotFloorPlan extends IotApiBase {

    public IotFloorPlan(IotApiConfig cnf, Header httpheader) {
        super(cnf, httpheader);
    }

    /**
     * 获取全部楼层规划
     *
     * @return
     * @throws Exception
     */
    public ResponseFloorplan getFloorplans() throws Exception {
        return getSingleFloorplan("");
    }

    /**
     * 获取指定楼层规划
     *
     * @param uid
     * @return
     * @throws Exception
     */
    public ResponseFloorplan getSingleFloorplan(String uid) throws Exception {
        HttpParam hp = new HttpParam();
        String url = config.getDomen();
        if (!Strings.isNullOrEmpty(uid)) {
            url += config.getFloorPlanApiGetSingleFloorplan() + uid;
        } else {
            url += config.getFloorPlanApiGetFloorplans();
        }
        hp.setUrl(url);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.GET);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseFloorplan.class);
    }

    /**
     * 创建子楼层规划
     *
     * @param req
     * @return
     * @throws Exception
     */
    public FloorplansItem createFloorplan(RequestFloorplan req) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getFloorPlanApiCreateFloorplan());
        hp.setHeader(header);
        hp.setPostParam(JSONUtil.getObjectToString(req));
        hp.setMethod(HttpMethod.POST);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), FloorplansItem.class);
    }

    /**
     * 修改子楼层规划
     *
     * @param req
     * @param uid
     * @return
     * @throws Exception
     */
    public FloorplansItem editFloorplan(RequestFloorplan req, String uid) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getFloorPlanApiEditFloorplan() + uid);
        hp.setHeader(header);
        hp.setPostParam(JSONUtil.getObjectToString(req));
        hp.setMethod(HttpMethod.PUT);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), FloorplansItem.class);
    }

    /**
     * 删除子楼层规划
     *
     * @param uid
     * @return
     * @throws Exception
     */
    public String deleteFloorplan(String uid) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getFloorPlanApiDeleteFloorplan() + uid);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.DELETE);
        String msg = HttpClient.ExecApi(hp);
        return msg;
    }
}
