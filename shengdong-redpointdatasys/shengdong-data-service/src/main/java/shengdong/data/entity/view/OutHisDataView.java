package shengdong.data.entity.view;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * 用于页面展示的历史数据实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OutHisDataView {
    private String title;
    private List<HisDataInfo> hisDataList = new ArrayList<>();
}
