package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class WinAnchorAssociationsItem {
    @JsonProperty("win_uid")
    private String winUid;
    private String uid;
    @JsonProperty("opened_at")
    private String openedAt;
    @JsonProperty("closed_at")
    private String closedAt;
    @JsonProperty("mac_address")
    private String macAddress;
    private Object status;
}