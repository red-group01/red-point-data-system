package shengdong.redpoint.api;

import shengdong.common.util.JSONUtil;
import shengdong.redpoint.httpentity.response.ResponseReports;
import shengdong.redpoint.model.query.Header;
import shengdong.redpoint.model.query.HttpMethod;
import shengdong.redpoint.model.query.HttpParam;

import java.util.Date;

public class IotReports extends IotApiBase {

    public IotReports(IotApiConfig cnf, Header httpheader) {
        super(cnf, httpheader);
    }

    /**
     * 获取报告列表
     *
     * @param site_uid
     * @param from
     * @param to
     * @param type     attendance_report、 alarms_report、 distance_report 、zone_activity_report
     * @return
     * @throws Exception
     */
    public ResponseReports getReportsList(String site_uid, Date from, Date to, String type) throws Exception {
        HttpParam hp = new HttpParam();
        String url =
            config.getDomen() + config.getReportApiGetReportsList() + "site_uid=" + site_uid + "&from=" + from.getTime() + "&to=" + to.getTime() + "type=" + type;
        hp.setUrl(url);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.GET);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseReports.class);
    }

    public void getReportAttendance() {
    }

    public void getReportAlarms() {
    }

    public void getReportDistance() {
    }

    public void getReportZoneActivity() {
    }
}
