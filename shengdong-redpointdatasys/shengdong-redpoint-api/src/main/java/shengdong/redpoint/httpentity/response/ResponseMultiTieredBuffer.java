package shengdong.redpoint.httpentity.response;

import java.util.List;
import lombok.Data;

@Data
public class ResponseMultiTieredBuffer{
	private List<ZonesItem> zones;
}