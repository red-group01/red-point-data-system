package shengdong.redpoint.httpentity.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Image {
    private String file;
    @JsonProperty("original_filename")
    private String originalFilename;
}