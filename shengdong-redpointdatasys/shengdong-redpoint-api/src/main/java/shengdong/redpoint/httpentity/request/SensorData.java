package shengdong.redpoint.httpentity.request;

import lombok.Data;

@Data
public class SensorData {
    private String text;
    private String raw;
}
