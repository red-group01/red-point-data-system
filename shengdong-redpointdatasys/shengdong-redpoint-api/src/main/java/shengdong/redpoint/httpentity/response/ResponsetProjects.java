package shengdong.redpoint.httpentity.response;

import lombok.Data;

@Data
public class ResponsetProjects {
    private String roleName;
    private Settings settings;
    private String uid;
    private String roleUid;
    private String scope;
    private String name;
    private String roleDisplayName;
    private int id;
    private int taggingsCount;
    private UserParams userParams;
}