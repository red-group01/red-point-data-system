package shengdong.redpoint.api;

import com.google.common.base.Strings;
import shengdong.common.util.JSONUtil;
import shengdong.redpoint.httpentity.request.RequestGroup;
import shengdong.redpoint.httpentity.request.RequestLocation;
import shengdong.redpoint.httpentity.response.GroupsItem;
import shengdong.redpoint.httpentity.response.ResponseGroups;
import shengdong.redpoint.model.query.Header;
import shengdong.redpoint.model.query.HttpMethod;
import shengdong.redpoint.model.query.HttpParam;

public class IotGroup extends IotApiBase {

    public IotGroup(IotApiConfig cnf, Header httpheader) {
        super(cnf, httpheader);
    }

    /**
     * 获取全部分组
     *
     * @return
     * @throws Exception
     */
    public ResponseGroups getGroups() throws Exception {
        return getSingleGroup("");
    }

    /**
     * 获取指定分组
     *
     * @param uid 分组uid，为空则取全部
     * @return
     * @throws Exception
     */
    public ResponseGroups getSingleGroup(String uid) throws Exception {
        String url = config.getDomen();
        if (!Strings.isNullOrEmpty(uid)) {
            url += config.getGroupsApiGetSingleGroup() + uid;
        } else {
            url += config.getGroupsApiGetGroups();
        }
        HttpParam hp = new HttpParam();
        hp.setUrl(url);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.GET);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseGroups.class);
    }

    /**
     * 添加分组
     *
     * @param req
     * @return
     * @throws Exception
     */
    public GroupsItem addGroup(RequestGroup req) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getGroupsApiAddGroup());
        hp.setHeader(header);
        hp.setPostParam(JSONUtil.getObjectToString(req));
        hp.setMethod(HttpMethod.POST);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), GroupsItem.class);
    }

    /**
     * 修改分组
     *
     * @param req
     * @param uid
     * @return
     * @throws Exception
     */
    public GroupsItem updateGroup(RequestLocation req, String uid) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getGroupsApiUpdateGroup() + uid);
        hp.setHeader(header);
        hp.setPostParam(JSONUtil.getObjectToString(req));
        hp.setMethod(HttpMethod.POST);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), GroupsItem.class);
    }

    /**
     * 删除分组
     *
     * @param uid
     * @return
     * @throws Exception
     */
    public String deleteGroup(String uid) throws Exception {

        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getGroupsApiDeleteGroup() + uid);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.DELETE);
        String msg = HttpClient.ExecApi(hp);
        return msg;
    }
}
