package shengdong.redpoint.httpentity.response;

import lombok.Data;

@Data
public class ReportsItem {
    private String uid;
    private ShiftItem shift;
    private String start;
    private String end;
}