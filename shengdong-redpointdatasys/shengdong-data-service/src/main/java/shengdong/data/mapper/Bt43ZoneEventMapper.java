package shengdong.data.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import shengdong.data.entity.Bt43ZoneEvent;

import java.util.Date;
import java.util.List;

/**
 * 区域事件表Mapper映射方法接口
 */
@Mapper
@Component
public interface Bt43ZoneEventMapper {

    /**
     * 根据id删除区域事件表数据
     *
     * @param id 自增主键
     * @return 删除行数
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 新增区域事件表数据
     *
     * @param record 区域事件实体类
     * @return 新增行数
     */
    int insert(Bt43ZoneEvent record);

    /**
     * 新增区域事件表数据(根据一定规则)
     *
     * @param record 区域事件点实体类
     * @return 新增行数
     */
    int insertSelective(Bt43ZoneEvent record);

    /**
     * 根据id查询区域事件表数据
     *
     * @param id 区域事件实体类
     * @return 区域事件实体类
     */
    Bt43ZoneEvent selectByPrimaryKey(Long id);

    /**
     * 更新区域事件表数据(根据一定规则)
     *
     * @param record 区域事件实体类
     * @return 更新行数
     */
    int updateByPrimaryKeySelective(Bt43ZoneEvent record);

    /**
     * 更新区域事件表数据
     *
     * @param record 区域事件实体类
     * @return 更新行数
     */
    int updateByPrimaryKey(Bt43ZoneEvent record);

    Date selectProjectLastTime(String puid);

    List<Bt43ZoneEvent> selectAllZoneEvent(String puid);

    void truncateTable();
}