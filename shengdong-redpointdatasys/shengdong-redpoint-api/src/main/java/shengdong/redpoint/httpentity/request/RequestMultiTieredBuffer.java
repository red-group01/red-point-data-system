package shengdong.redpoint.httpentity.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class RequestMultiTieredBuffer {
    private List<TiersItem> tiers;
    private String name;
    @JsonProperty("trackable_object_uid")
    private String trackableObjectUid;
}