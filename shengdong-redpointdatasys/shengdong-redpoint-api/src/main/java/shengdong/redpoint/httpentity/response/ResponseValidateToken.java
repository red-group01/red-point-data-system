package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ResponseValidateToken {
    private boolean success;

    @JsonProperty("data")
    private ValidateTokenData data;
}
