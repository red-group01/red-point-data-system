package shengdong.redpoint.websockentity;

import lombok.Data;

@Data
public class ResponseAlarmChannel {
    private String method;
    private String jsonrpc;
    private AlarmChannelParams params;
}