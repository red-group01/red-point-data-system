package shengdong.redpoint.api;

import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import shengdong.common.util.JSONUtil;
import shengdong.redpoint.httpentity.request.RequestAlarm;
import shengdong.redpoint.httpentity.response.AlarmsItem;
import shengdong.redpoint.httpentity.response.ResponseAlarms;
import shengdong.redpoint.model.query.Header;
import shengdong.redpoint.model.query.HttpMethod;
import shengdong.redpoint.model.query.HttpParam;

import java.util.Date;

@Slf4j
public class IotAlarmsApi extends IotApiBase {

    public IotAlarmsApi(IotApiConfig cnf, Header httpheader) {
        super(cnf, httpheader);
    }

    /**
     * 获取所有尚未清除的当前警报
     *
     * @return
     * @throws Exception
     */
    public ResponseAlarms getAlarms() throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getAlarmsApiGetAlarms());
        hp.setHeader(header);
        hp.setMethod(HttpMethod.GET);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseAlarms.class);
    }

    /**
     * 按项目获取警报历史记录
     *
     * @param from
     * @param to
     * @return
     * @throws Exception
     */
    public ResponseAlarms getAlarmHistoryJson(Date from, Date to) throws Exception {
        return getAlarmHistoryJson(from, to, null, null, null, null);
    }

    /**
     * 按子站点获取警报历史记录
     *
     * @param from
     * @param to
     * @param sublocation_uid
     * @return
     * @throws Exception
     */
    public ResponseAlarms getAlarmHistoryJson(Date from, Date to, String sublocation_uid) throws Exception {
        return getAlarmHistoryJson(from, to, null, sublocation_uid, null, null);
    }

    /**
     * @param from
     * @param to
     * @param obj_uid
     * @param sublocation_uid
     * @param location_uid
     * @param alarm_type_name
     * @return
     * @throws Exception
     */
    public ResponseAlarms getAlarmHistoryJson(Date from, Date to, String[] obj_uid, String sublocation_uid,
                                              String location_uid, String[] alarm_type_name) throws Exception {
        String format = "format=json";//csv
        HttpParam hp = new HttpParam();
        String url =
                config.getDomen() + config.getAlarmsApiGetAlarmHistory() + format + "&from=" + from.getTime() + "&to=" + to.getTime();
        if (obj_uid != null && obj_uid.length > 0) {
            for (String i : obj_uid) {
                url += "&obj_uid[]=" + i;
            }
        }
        if (!Strings.isNullOrEmpty(sublocation_uid)) {
            url += "&sublocation_uid=" + sublocation_uid;
        }
        if (!Strings.isNullOrEmpty(location_uid)) {
            url += "&location_uid=" + location_uid;
        }
        if (alarm_type_name != null && alarm_type_name.length > 0) {
            for (String i : alarm_type_name) {
                url += "&alarm_type_name[]=" + i;
            }
        }
        hp.setUrl(url);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.GET);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseAlarms.class);
    }

    /**
     * 获取所有活动警报
     *
     * @return
     * @throws Exception
     */
    public ResponseAlarms getActiveAlarms() throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getAlarmsApiGetActiveAlarms());
        hp.setHeader(header);
        hp.setMethod(HttpMethod.GET);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseAlarms.class);
    }

    /**
     * 创建一个告警
     *
     * @param req
     * @return
     * @throws Exception
     */
    public AlarmsItem createAlarm(RequestAlarm req) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getAlarmsApiCreateAlarm());
        hp.setHeader(header);
        hp.setPostParam(JSONUtil.getObjectToString(req));
        hp.setMethod(HttpMethod.POST);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), AlarmsItem.class);
    }

    /**
     * 根据指定的告警UID更新告警
     *
     * @param req
     * @param uid
     * @return
     * @throws Exception
     */
    public AlarmsItem updateAlarm(RequestAlarm req, String uid) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getAlarmsApiUpdateAlarm() + uid);
        hp.setHeader(header);
        hp.setPostParam(JSONUtil.getObjectToString(req));
        hp.setMethod(HttpMethod.POST);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), AlarmsItem.class);
    }

    /**
     * 删除指定告警UID的告警
     *
     * @param req
     * @param uid
     * @return
     * @throws Exception
     */
    public String deleteAlarm(RequestAlarm req, String uid) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getAlarmsApiDeleteAlarm() + uid);
        hp.setHeader(header);
        hp.setPostParam(JSONUtil.getObjectToString(req));
        hp.setMethod(HttpMethod.DELETE);
        String msg = HttpClient.ExecApi(hp);
        return msg;
    }

}
