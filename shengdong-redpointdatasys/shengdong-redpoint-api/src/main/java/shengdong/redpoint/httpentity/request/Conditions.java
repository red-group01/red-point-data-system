package shengdong.redpoint.httpentity.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Conditions {
    @JsonProperty("zone_severity_level")
    private int zoneSeverityLevel;
}