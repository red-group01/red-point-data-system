package shengdong.redpoint.model.query;

import lombok.Data;
import okhttp3.MediaType;
import okhttp3.RequestBody;

import java.util.HashMap;

/**
 * 调用红点接口时的参数
 */
@Data
public class HttpParam {
    private String url = "";
    private HttpMethod method;
    /**
     * 固定的header部分
     */
    private Header header = new Header();
    /**
     * 其他get附加参数
     */
    private HashMap<String, String> getParam;
    /**
     * 其他header附加参数
     */
    private HashMap<String, String> otherHeaderParam;
    /**
     * 其他post附加参数
     */
    private String postParam = "";

    /**
     * 构建post提交内容
     *
     * @return
     */
    public RequestBody getPostBody() {
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, getPostParam());
        return body;
    }
}
