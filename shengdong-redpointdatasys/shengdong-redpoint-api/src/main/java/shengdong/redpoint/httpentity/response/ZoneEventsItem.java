package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ZoneEventsItem {
    @JsonProperty("sublocation_uid")
    private String sublocationUid;
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("zone_uid")
    private String zoneUid;
    @JsonProperty("project_uid")
    private String projectUid;
    private String type;
    private int duration;
    private long x;
    @JsonProperty("obj_uid")
    private String objUid;
    @JsonProperty("zone_type_name")
    private String zoneTypeName;
    private String action;
    @JsonProperty("company_uid")
    private String companyUid;
    private long y;
    private long z;
}