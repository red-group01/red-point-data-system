package shengdong.data.entity.view;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用于页面展示的电子围栏列表实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OutZoneListView {
    private int serial;
    private String zoneName;
    private String type;
    private String isActive;
}
