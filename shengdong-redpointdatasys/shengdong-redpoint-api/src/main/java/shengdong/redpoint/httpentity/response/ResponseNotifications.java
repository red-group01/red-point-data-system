package shengdong.redpoint.httpentity.response;

import java.util.List;
import lombok.Data;

@Data
public class ResponseNotifications{
	private List<NotificationsItem> notifications;
}