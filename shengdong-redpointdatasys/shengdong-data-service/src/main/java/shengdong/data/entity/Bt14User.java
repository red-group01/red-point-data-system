package shengdong.data.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 用户实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Bt14User {

    //自增主键
    private Long id;

    //用户唯一标识
    private String userUid;

    //公司唯一标识
    private String companyUid;

    //项目唯一标识
    private String projectUid;

    //角色权限唯一标识
    private String roleUid;

    //用户名称
    private String name;

    //用户邮件
    private String email;

    //用户密码
    private String password;

    //人员组织结构唯一标识
    private String organizationUid;

    //创建时间(UTC)
    private Date createdAt;

    //更新时间(UTC)
    private Date updatedAt;

    //数据最后更新者
    private String creatorAt;

    //数据信息备注
    private String remark;
    /**
     * 接口用户的授权
     */
    private String token;
    /**
     * 接口配置
     */
    private Bm01Webserver serverCnf;
}