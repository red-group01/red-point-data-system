package shengdong.redpoint.model.query;

/**
 * http调用方式
 */
public enum HttpMethod {
    GET, POST, PUT, DELETE, PATCH
}
