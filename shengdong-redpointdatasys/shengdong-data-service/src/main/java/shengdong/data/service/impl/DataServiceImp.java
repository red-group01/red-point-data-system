package shengdong.data.service.impl;

import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;
import shengdong.common.constants.CommonConstants;
import shengdong.common.util.CommonUtil;
import shengdong.data.dao.*;
import shengdong.data.entity.*;
import shengdong.data.service.IDataService;
import shengdong.redpoint.api.*;
import shengdong.redpoint.httpentity.response.*;
import shengdong.redpoint.model.query.Header;

import java.io.File;
import java.nio.file.Files;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class DataServiceImp implements IDataService {
    @Autowired
    IBt50PoinsHisDao daoPoinsHis;
    @Autowired
    IBt31ProjectDao daoPorj;
    @Autowired
    IActivealarmDao activealarmDao;
    @Autowired
    IBt41PositionDao bt41PositionDao;
    @Value("${spring.jackson.time-zone}")
    private String timeZone;

    @Override
    public void synAll() throws Exception {
        List<Bt50PoinsHisView> cnflst = daoPorj.selectCnf();
        if (cnflst == null) {
            log.warn("cnflst is null,DataServiceImp:45");
            return;
        }
        cnflst.forEach(cnfitem -> {
            try {
                //用户验证
                IotApiConfig cnf = new IotApiConfig();
                cnf.setServerIp(cnfitem.getServerIp());
                IotLogin login = new IotLogin(cnf);
                boolean relogin = true;
                if (!Strings.isNullOrEmpty(cnfitem.getToken())) {
                    ResponseValidateToken rvt = login.refreshToken(cnfitem.getEmail(), cnfitem.getToken());
                    if (rvt.isSuccess() && !rvt.getData().getToken().equals(cnfitem.getToken())) {
                        Bt14User u = new Bt14User();
                        u.setToken(rvt.getData().getToken());
                        u.setId(cnfitem.getUserId());
                        daouser.updateUser(u);
                        relogin = false;
                    }
                }
                if (relogin) {
                    ResponseLogin rl = login.login(cnfitem.getEmail(), cnfitem.getPassword());
                    //登录失败则退出
                    if (!rl.isSuccess()) {
                        return;
                    } else {
                        Bt14User u = new Bt14User();
                        u.setToken(rl.getData().getToken());
                        u.setId(cnfitem.getUserId());
                        daouser.updateUser(u);
                    }
                }
                //数据请求
                Header h = new Header();
                h.setEmail(cnfitem.getEmail());
                h.setToken(cnfitem.getToken());
                h.setProject(cnfitem.getProjectUid());

            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
        synZoneEvents();
        synAlarmsHis();
        synPoinsHis();
    }

    @Autowired
    IBt43ZoneEventDao daoZoneEvent;

    /**
     * 同步区域事件
     */
    @Override
    public void synZoneEvents() {
        List<Bt50PoinsHisView> cnf = daoPoinsHis.selectCnf();
        if (cnf == null) {
            log.warn("cnf is null,DataServiceImp:101");
            return;
        }
        cnf.forEach(cnfitem -> {
            try {
                IotApiConfig cnf1 = new IotApiConfig();
                cnf1.setServerIp(cnfitem.getServerIp());
                synZoneEvents(cnfitem, cnf1);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    /**
     * 根据配置同步区域事件历史
     *
     * @param cnfitem
     * @param cnf
     * @throws Exception
     */
    @Override
    public void synZoneEvents(Bt50PoinsHisView cnfitem, IotApiConfig cnf) throws Exception {
        Header h = new Header();
        h.setEmail(cnfitem.getEmail());
        h.setToken(cnfitem.getToken());
        h.setProject(cnfitem.getProjectUid());
        Date start = getStart(cnfitem, cnfitem.getLastalzonesynAt(), -2);
        Date end = getEnd(start);
        end = verifyEnd(end);
        IotZoneEvent zoneEvent = new IotZoneEvent(cnf, h);
        ResponseZoneEvents events = zoneEvent.getZoneEvents(start, end, cnfitem.getSubLocationUid());
        if (events.getZoneEvents() == null) {
            log.warn("events.getZoneEvents() is null,DataServiceImp:133");
            return;
        }
        events.getZoneEvents().forEach(item -> {
            Bt43ZoneEvent ent = new Bt43ZoneEvent();
            ent.setAction(item.getAction());
            ent.setDuration(item.getDuration());
            ent.setCompanyUid(item.getCompanyUid());
            ent.setX(item.getX());
            ent.setY(item.getY());
            ent.setZ(item.getZ());
            ent.setCreatorAt(CommonConstants.AutomaticProgramUpdateFlag);
            ent.setCreatedAt(CommonUtil.fromUtcDate(item.getCreatedAt()));
            ent.setSublocationId(item.getSublocationUid());
            ent.setObjUid(item.getObjUid());
            ent.setZoneEventUid(item.getZoneUid());
            ent.setZoneTypeName(item.getZoneTypeName());
            ent.setZoneUid(item.getZoneUid());
            daoZoneEvent.insert(ent);
        });
        daoPoinsHis.flushZoneDate(end, cnfitem.getSubLocationUid());
    }

    @Autowired
    IBt44AlarmDao daoAlarm;

    @Override
    public void synAlarmsHis() {
        StopWatch watch = new StopWatch();
        watch.start();
        List<Bt50PoinsHisView> cnf = daoPoinsHis.selectCnf();
        if (cnf == null) {
            log.warn("cnf is null,DataServiceImp:165");
            return;
        }
        cnf.forEach(cnfitem -> {
            try {
                IotApiConfig cnf1 = new IotApiConfig();
                cnf1.setServerIp(cnfitem.getServerIp());
                synAlarmsHis(cnfitem, cnf1);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
        watch.stop();
        log.info("alarm history data set succeed,elapsed time:" + CommonUtil.msFormat(watch.getTotalTimeMillis()));
    }

    /**
     * 根据配置获取历史报警记录
     *
     * @param cnfitem
     * @param cnf
     * @return
     * @throws Exception
     */
    public void synAlarmsHis(Bt50PoinsHisView cnfitem, IotApiConfig cnf) throws Exception {
        Header h = new Header();
        h.setEmail(cnfitem.getEmail());
        h.setToken(cnfitem.getToken());
        h.setProject(cnfitem.getProjectUid());
        IotAlarmsApi alarm = new IotAlarmsApi(cnf, h);
        Date start = getStart(cnfitem, cnfitem.getLastalarmsynAt(), -2);
        Date end = getEnd(start);
        end = verifyEnd(end);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        ResponseAlarms res = alarm.getAlarmHistoryJson(start, end, cnfitem.getSubLocationUid());
        if (res.getAlarms() == null) {
            log.warn("res.getAlarms() is null,DataServiceImp:201");
            return;
        }
        res.getAlarms().forEach(item -> {
            Bt44Alarm ent = new Bt44Alarm();
            ent.setAlarmUid(item.getUid());
            ent.setProjectUid(h.getProject());
            ent.setZoneUid(item.getZoneUid());
            ent.setObjUid(item.getObjUid());
            int tz = 8;
            if (timeZone.equals("GMT+9")) {
                tz = 9;
            }
            ent.setCreatedAt(CommonUtil.dateAddHour(CommonUtil.fromUtcDate(item.getCreatedAt()), tz));
            ent.setCreatorAt(CommonConstants.AutomaticProgramUpdateFlag);
            ent.setAlarmUid(item.getUid());
            if (item.getAlarmTypeId() == 9) {
                ent.setAdditionalInfo(item.getAdditionalInfo().getName() + "draw near" + item.getAdditionalInfo().getContactName());
            } else {
                ent.setAdditionalInfo(item.getAdditionalInfo().getText());
            }
            ent.setHysteresisCount(item.getHysteresisCount());
            ent.setMacAddress(item.getMacAddress());
            ent.setState(item.getState());
            ent.setSublocationUid(item.getSublocationUid());
            ent.setTs(CommonUtil.fromUtcDate(item.getTs()));
            ent.setType(String.valueOf(item.getAlarmTypeId()));
            ent.setX(item.getX());
            ent.setY(item.getY());
            ent.setZ(item.getZ());
            Bt44Alarm bt44Alarm = new Bt44Alarm();
            bt44Alarm.setObjUid(ent.getObjUid());
            bt44Alarm.setCreatedAt(ent.getCreatedAt());
            List<Bt44Alarm> alarmList = daoAlarm.selectByObjAndTime(bt44Alarm);
            //根据告警设备uid和告警时间来去重，防止重复入库
            if (alarmList == null || alarmList.size() == 0) {
                daoAlarm.insert(ent);
            }
        });
        daoPoinsHis.flushAlarmDate(end, cnfitem.getSubLocationUid());
    }

    /**
     * 获取活动中的报警信息，实时信息不入库
     */
    @Override
    public void synAlarmsActive() {
        List<Bt50PoinsHisView> cnf = daoPoinsHis.selectCnf();
        try {
            if (cnf != null && cnf.size() > 0) {
                IotApiConfig cnf1 = new IotApiConfig();
                cnf1.setServerIp(cnf.get(0).getServerIp());
                synAlarmsActive(cnf.get(0), cnf1);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 根据配置获取活动中的报警信息，实时信息不入库
     *
     * @param cnfitem
     * @param cnf
     * @return
     * @throws Exception
     */
    public void synAlarmsActive(Bt50PoinsHisView cnfitem, IotApiConfig cnf) throws Exception {
        //清除实时告警库数据
        activealarmDao.deleteByUserId(cnfitem.getUserId());
        Header h = new Header();
        h.setEmail(cnfitem.getEmail());
        h.setToken(cnfitem.getToken());
        h.setProject(cnfitem.getProjectUid());
        IotAlarmsApi alarm = new IotAlarmsApi(cnf, h);
        ResponseAlarms res = alarm.getActiveAlarms();
        //List<Bt44Alarm> alarmList = new ArrayList<>();
        List<Activealarm> alarmList = new ArrayList<>();
        if (res.getAlarms() == null) {
            log.warn("res.getAlarms() is null,DataServiceImp:280");
            return;
        }
        res.getAlarms().forEach(item -> {
            //Bt44Alarm ent = new Bt44Alarm();
            Activealarm ent = new Activealarm();
            ent.setAlarmUid(item.getUid());
            ent.setProjectUid(h.getProject());
            ent.setZoneUid(item.getZoneUid());
            ent.setObjUid(item.getObjUid());
            int tz = 8;
            if (timeZone.equals("GMT+9")) {
                tz = 9;
            }
            ent.setCreatedAt(CommonUtil.dateAddHour(CommonUtil.fromUtcDate(item.getCreatedAt()), tz));
            ent.setCreatorAt(CommonConstants.AutomaticProgramUpdateFlag);
            ent.setAlarmUid(item.getUid());
            if (item.getAlarmTypeId() == 2 || item.getAlarmTypeId() == 8) {
                ent.setAdditionalInfo(item.getAdditionalInfo().getText());
            } else {
                ent.setAdditionalInfo(item.getAdditionalInfo().getContactSlUid());
            }
            ent.setHysteresisCount(item.getHysteresisCount());
            ent.setMacAddress(item.getMacAddress());
            ent.setState(item.getState());
            ent.setSublocationUid(item.getSublocationUid());
            ent.setTs(CommonUtil.fromUtcDate(item.getTs()));
            ent.setType(String.valueOf(item.getAlarmTypeId()));
            ent.setX(item.getX());
            ent.setY(item.getY());
            ent.setZ(item.getZ());
            ent.setUserId(cnfitem.getUserId());
            alarmList.add(ent);
            activealarmDao.insertSelective(ent);
        });
        // 如果存在实时报警数据,且改实时报警数据的触发时间大于同步时间，则主动同步历史报警数据
        for (Activealarm item : alarmList) {
            if (item.getCreatedAt().getTime() > cnfitem.getLastalarmsynAt().getTime()) {
                synAlarmsHis();
            }
        }
        //return alarmList;
    }

    @Autowired
    IBt14UserDao daouser;

    /**
     * 自动检查历史数据进度增量同步
     *
     * @return
     */
    @Override
    public void synPoinsHis() {
        StopWatch watch = new StopWatch();
        watch.start();
        List<Bt50PoinsHisView> cnf = daoPoinsHis.selectCnf();
        if (cnf == null) {
            log.warn("cnf is null,DataServiceImp:338");
            return;
        }
        cnf.forEach(cnfitem -> {
            try {
                IotApiConfig cnf1 = new IotApiConfig();
                cnf1.setServerIp(cnfitem.getServerIp());
                synProjHispoint(cnfitem, cnf1);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
        watch.stop();
        log.info("points history data set succeed,elapsed time:" + CommonUtil.msFormat(watch.getTotalTimeMillis()));
    }

    /**
     * 同步历史数据
     *
     * @param cnfitem
     */
    public void synProjHispoint(Bt50PoinsHisView cnfitem, IotApiConfig cnf) throws Exception {
        //数据下载
        Header h = new Header();
        h.setEmail(cnfitem.getEmail());
        h.setToken(cnfitem.getToken());
        h.setProject(cnfitem.getProjectUid());
        IotPositionHistory his = new IotPositionHistory(cnf, h);
        Date start = getStart(cnfitem, cnfitem.getLastpointsynAt(), -2);
        Date end = getEnd(start);
        end = verifyEnd(end);
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        log.info(cnfitem.getSubLocationUid() + "查询时段：" + sdf1.format(start) + "_" + sdf1.format(end));
        ApplicationHome home = new ApplicationHome(getClass());
        String cachepath = home.getDir().toString() + "\\download\\";
        Integer page = 0;
        while (true) {
            ResponsePositionHistory res = his.downLoad(cnfitem.getSubLocationUid(), start, end, cachepath, page);
            //返回null表示下载请求失败
            if (res != null) {
                String src = res.getFile().getAbsolutePath();
                String des = src + "tmp";
                long rows = extracted(src, des);
                //                log.info(cnfitem.getSubLocationUid() + "取到数据量：" + rows);
                //如果还有未下载数据则继续下载数据
                if (res.getPage() > 0) {
                    page += 1;
                } else {
                    break;
                }
            } else {
                break;
            }
        }
        //daoPoinsHis.flushPointDate(end, cnfitem.getSubLocationUid());
        //对于历史数据，查找最新的一条数据的时间作为下次更新时间
        Bt41Position position = bt41PositionDao.selectNewDataTime();
        daoPoinsHis.flushPointDate(position.getTs(), cnfitem.getSubLocationUid());
        //log.warn("sub up time:" + position.getTs());
    }

    /**
     * 如果未初始化 则返回当前时间倒退days天数
     *
     * @param cnfitem
     * @param start
     * @param days
     * @return
     * @throws ParseException
     */
    private Date getStart(Bt50PoinsHisView cnfitem, Date start, int days) throws ParseException {
        if (start == null) {
            //设置为程序启动前一天开始
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            start = CommonUtil.dateAddDay(sdf.parse(sdf.format(new Date())), days);
        }
        return start;
    }

    /**
     * 取指定日期结束时间
     *
     * @param start
     * @return
     * @throws ParseException
     */
    private Date getEnd(Date start) throws ParseException {
        Date end;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
        end = sdf1.parse(sdf.format(CommonUtil.dateAddDay(start, 1)) + " 23:59:59:999");
        return end;
    }

    /**
     * 加载下载的数据文件到数据库
     *
     * @param src 压缩包路径
     * @param des 解压路径
     * @return 文件行数
     * @throws Exception
     */
    @Override
    public long extracted(String src, String des) throws Exception {
        CommonUtil.unzip(src, des);
        File f = new File(des);
        File[] fs = f.listFiles(pathname -> pathname.getName().contains(".csv"));
        //如果压缩包包含多个文件则加载全部文件
        long datarows = 0;
        for (File i : fs) {
            //-1是文件头
            long rows = Files.lines(i.toPath()).count() - 1;
            datarows += Files.lines(i.toPath()).count() - 1;
            if (rows > 0) {
                daoPoinsHis.insertBatchByLoadData(i.getAbsolutePath());
            }
        }
        //删除压缩包及解压后的文件
        CommonUtil.deleteFile(src);
        CommonUtil.deleteDirectory(des);
        return datarows;
    }

    /**
     * 验证结束时间是否超过当前时间
     *
     * @param end
     * @return 如果超过当前时间则返回当前时间，否则返回原始值
     * @throws ParseException
     */
    private Date verifyEnd(Date end) throws ParseException {
        Date now = new Date();
        if (now.before(end)) {
            return now;
        }
        return end;
    }
}
