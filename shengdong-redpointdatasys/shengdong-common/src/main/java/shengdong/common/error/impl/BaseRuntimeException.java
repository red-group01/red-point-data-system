package shengdong.common.error.impl;

import shengdong.common.error.IErrorCode;

public class BaseRuntimeException extends RuntimeException{

    private String code;
    private String message;

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public BaseRuntimeException(IErrorCode commonErrorCode){
        super();
        this.code = commonErrorCode.getCode();
        this.message = commonErrorCode.getMessage();
    }

    public BaseRuntimeException(String code, String message){
        super();
        this.code = code;
        this.message = message;
    }
}
