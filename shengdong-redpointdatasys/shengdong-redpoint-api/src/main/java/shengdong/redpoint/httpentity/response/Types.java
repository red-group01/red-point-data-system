package shengdong.redpoint.httpentity.response;

import lombok.Data;

@Data
public class Types{
	private boolean phone;
	private boolean email;
}