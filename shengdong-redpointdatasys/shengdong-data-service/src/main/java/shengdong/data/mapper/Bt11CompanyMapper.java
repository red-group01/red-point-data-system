package shengdong.data.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import shengdong.data.entity.Bt11Company;

import java.util.List;

/**
 * 公司表Mapper映射方法接口
 */
@Mapper
@Component
public interface Bt11CompanyMapper {

    /**
     * 根据id删除公司表数据
     *
     * @param id 自增主键
     * @return 删除行数
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 新增公司表数据
     *
     * @param record 公司实体类
     * @return 新增行数
     */
    int insert(Bt11Company record);

    /**
     * 新增公司表数据(根据一定规则)
     *
     * @param record 公司实体类
     * @return 新增行数
     */
    int insertSelective(Bt11Company record);

    /**
     * 根据id查询公司表数据
     *
     * @param id 公司实体类
     * @return 公司实体类
     */
    Bt11Company selectByPrimaryKey(Long id);

    /**
     * 更新公司表数据(根据一定规则)
     *
     * @param record 公司实体类
     * @return 更新行数
     */
    int updateByPrimaryKeySelective(Bt11Company record);

    /**
     * 更新公司表数据
     *
     * @param record 公司实体类
     * @return 更新行数
     */
    int updateByPrimaryKey(Bt11Company record);

    Bt11Company selectByUid(String uid);

    void truncateTable();

    List<Bt11Company> selectAll();
}