package shengdong.data.entity;

import lombok.Data;

import java.util.Date;

@Data
public class BmColumn {
    String c1;
    String c2;
    String c3;
    Date dt1;
    Date dt2;
    Date dt3;
}
