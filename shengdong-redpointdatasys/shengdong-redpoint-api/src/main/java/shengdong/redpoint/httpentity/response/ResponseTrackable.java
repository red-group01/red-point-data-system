package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class ResponseTrackable {
    @JsonProperty("default_height")
    private int defaultHeight;
    @JsonProperty("position_update_interval")
    private int positionUpdateInterval;
    @JsonProperty("acc_config")
    private Object bt_config;
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("custom_info")
    private String customInfo;
    private Object groups;
    @JsonProperty("project_uid")
    private String projectUid;
    @JsonProperty("display_name")
    private String displayName;
    private String type;
    @JsonProperty("acc_config")
    private Object accConfig;
    private String uid;
    @JsonProperty("sensor_config")
    private SensorConfig sensorConfig;
    private Node node;
    @JsonProperty("pos_report_frequency")
    private int posReportFrequency;
    @JsonProperty("removed_at")
    private String removedAt;
    @JsonProperty("updated_at")
    private String updatedAt;
    private String name;
    @JsonProperty("company_uid")
    private String companyUid;
    @JsonProperty("self_zone_uid")
    private Object selfZoneUid;
    private TagConfig tagConfig;
    private String category;
    @JsonProperty("t2t_no_alarm_uid")
    private List<Object> t2tNoAlarmUid;
    private Object paired;
    private Position position;
}