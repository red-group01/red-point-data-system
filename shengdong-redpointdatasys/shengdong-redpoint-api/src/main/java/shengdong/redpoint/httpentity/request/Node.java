package shengdong.redpoint.httpentity.request;

import lombok.Data;

@Data
public class Node{
	private boolean idle;
	private String macAddress;
	private boolean responsive;
	private Object parentSublocationUid;
	private int nodeId;
}