package shengdong.data.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import shengdong.data.dao.IBt31ProjectDao;
import shengdong.data.entity.Bt31Project;
import shengdong.data.entity.Bt50PoinsHisView;
import shengdong.data.mapper.Bt31ProjectMapper;

import java.util.List;

/**
 * 项目Dao实现
 */
@Repository
public class Bt31ProjectDaoImpl implements IBt31ProjectDao {
    // 注入Mapper接口
    @Autowired
    private Bt31ProjectMapper bt31ProjectMapper;

    /**
     * 插入项目数据
     *
     * @param record 项目实体类
     * @return 插入的行数
     */
    @Override
    public int insert(Bt31Project record) {
        return bt31ProjectMapper.insert(record);
    }

    @Override
    public List<Bt31Project> getUserProj(Long userid) {
        return bt31ProjectMapper.selectByUserId(userid);
    }

    @Override
    public int updateByid(Bt31Project i) {
        return bt31ProjectMapper.updateByPrimaryKeySelective(i);
    }

    @Override
    public void trancateTable() {
        bt31ProjectMapper.truncateTable();
    }

    @Override
    public Bt31Project selectByUid(String uid) {
        return bt31ProjectMapper.selectByUid(uid);
    }

    @Override
    public List<Bt50PoinsHisView> selectCnf() {
        return bt31ProjectMapper.selectCnf();
    }
}
