package shengdong.data.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import shengdong.data.dao.IBt11CompanyDao;
import shengdong.data.entity.Bt11Company;
import shengdong.data.mapper.Bt11CompanyMapper;

import java.util.List;

/**
 * 公司Dao实现
 */
@Repository
public class Bt11CompanyDaoImpl implements IBt11CompanyDao {

    // 注入Mapper接口
    @Autowired
    private Bt11CompanyMapper bt11CompanyMapper;

    /**
     * 插入公司数据
     *
     * @param record 公司实体类
     * @return 插入的行数
     */
    @Override
    public int insert(Bt11Company record) {
        return bt11CompanyMapper.insert(record);
    }

    @Override
    public Bt11Company getCompanyByUid(String uid) {
        return bt11CompanyMapper.selectByUid(uid);
    }

    @Override
    public int update(Bt11Company entCompany) {
        return bt11CompanyMapper.updateByPrimaryKeySelective(entCompany);
    }

    @Override
    public void truncateTable() {
        bt11CompanyMapper.truncateTable();
    }

    @Override
    public List<Bt11Company> getAll() {
        return bt11CompanyMapper.selectAll();
    }
}
