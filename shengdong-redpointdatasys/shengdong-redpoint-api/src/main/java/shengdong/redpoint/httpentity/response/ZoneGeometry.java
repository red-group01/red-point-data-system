package shengdong.redpoint.httpentity.response;

import lombok.Data;

import java.util.List;

@Data
public class ZoneGeometry {
    private List<PositionsItem> positions;
    private int radius;
}