package shengdong.data.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import shengdong.common.config.ViewPageConfig;
import shengdong.common.error.impl.BaseResponse;
import shengdong.common.error.impl.BaseRuntimeException;
import shengdong.common.error.impl.CommonErrorCode;
import shengdong.common.log.IRedSysLog;
import shengdong.data.entity.TestTable;
import shengdong.data.service.ITestService;

@Slf4j
@RestController
@RequestMapping("/test")
public class TestController {

    //自动注入日志共通类
    @Autowired
    private IRedSysLog rs;
    @Autowired
    ViewPageConfig viewPageConfig;

    /**
     * 测试配置文件获取范例
     *
     * @return 测试文本
     */
    @GetMapping("/config")
    public BaseResponse<ViewPageConfig> testConfig() {
//        ViewPageConfig viewPageConfigOut = new ViewPageConfig();
//        BeanUtils.copyProperties(viewPageConfig, viewPageConfigOut);
//        return BaseResponse.success(viewPageConfigOut);
        return null;
    }

    /**
     * 日志输出范例
     *
     * @return 测试文本
     */
    @GetMapping("/log")
    public String testLog() {
        // 通过共通类输出日志
        rs.logError("logError", TestController.class);
        // 通过@Slf4j输出日志
        log.info("testinfo");
        log.error("testerror");

        return "Test logTest";
    }

    /**
     * 异常捕获输出范例
     *
     * @return 测试文本
     */
    @GetMapping("/ex")
    public BaseResponse<String> testEx(String no) {
        // 测试正常返回数据
        if ("1".equals(no)) {
            return BaseResponse.success("succ");
        }
        // 测试自定义异常
        if ("2".equals(no)) {
            throw new BaseRuntimeException(CommonErrorCode.REQUEST_PARAM_ERROR);
        }
        // 测试系统异常
        if ("3".equals(no)) {
            int a = 1 / 0;
        }

        return BaseResponse.success("def");
    }

    @Autowired
    private ITestService iTestService;

    /**
     * 测试获取测试表数据范例
     *
     * @param id
     * @return
     */
    @GetMapping("/testtable")
    public BaseResponse<TestTable> testGetTestTable(Integer id) {
        TestTable testTable = iTestService.listTestTableById(id);
        return BaseResponse.success(testTable);
    }

    /**
     * 测试往测试表插入数据范例
     *
     * @return
     */
    @GetMapping("/insert")
    public BaseResponse<TestTable> testInsertTestTable() {
        TestTable testTable = new TestTable(3, "王五");
        int result = iTestService.insertTestTable(testTable);
        TestTable testTableSelect = iTestService.listTestTableById(3);
        return BaseResponse.success(testTableSelect);
    }
}
