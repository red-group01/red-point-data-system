package shengdong.common.log;

public interface IRedSysLog {

    /**
     * 输出追踪日志
     * @param content 日志内容
     * @param logclass 日志发生的类
     */
    public void logError(String content,Class<?> logclass);

    /**
     * 格式化日志格式
     * @param code
     * @param msg
     * @return
     */
    public String toLog(String code, String msg);

}
