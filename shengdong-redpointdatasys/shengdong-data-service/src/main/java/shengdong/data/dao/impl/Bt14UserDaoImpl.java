package shengdong.data.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import shengdong.data.dao.IBt14UserDao;
import shengdong.data.entity.Bt14User;
import shengdong.data.mapper.Bt14UserMapper;

import java.util.List;

/**
 * 用户Dao实现
 */
@Repository
public class Bt14UserDaoImpl implements IBt14UserDao {

    // 注入Mapper接口
    @Autowired
    private Bt14UserMapper bt14UserMapper;

    /**
     * 插入用户数据
     *
     * @param record 用户实体类
     * @return 插入的行数
     */
    @Override
    public int insert(Bt14User record) {
        return bt14UserMapper.insert(record);
    }

    /**
     * 获取全部用户数据
     *
     * @return
     */
    @Override
    public List<Bt14User> getAll() {
        return bt14UserMapper.selectAllUser();
    }

    /**
     * 更新用户数据
     *
     * @param user
     * @return
     */
    @Override
    public int updateUser(Bt14User user) {
        return bt14UserMapper.updateByPrimaryKeySelective(user);
    }

    @Override
    public Bt14User getUserByEmail(String email) {
        return bt14UserMapper.selectByEmail(email);
    }

    @Override
    public void truncateTable() {

        bt14UserMapper.truncateTable();
    }
}
