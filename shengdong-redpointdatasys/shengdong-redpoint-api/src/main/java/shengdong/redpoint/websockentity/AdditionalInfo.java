package shengdong.redpoint.websockentity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class AdditionalInfo {
    @JsonProperty("zone_violation_type")
    private String zoneViolationType;
    @JsonProperty("zone_uid")
    private String zoneUid;
    private String text;
    @JsonProperty("zone_alarm_level")
    private int zoneAlarmLevel;
}