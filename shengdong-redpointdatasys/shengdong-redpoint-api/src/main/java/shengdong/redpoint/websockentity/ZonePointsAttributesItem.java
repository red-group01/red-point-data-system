package shengdong.redpoint.websockentity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ZonePointsAttributesItem {
    @JsonProperty("position_x")
    private int positionX;
    @JsonProperty("position_y")
    private int positionY;
    @JsonProperty("position_z")
    private Object positionZ;
}