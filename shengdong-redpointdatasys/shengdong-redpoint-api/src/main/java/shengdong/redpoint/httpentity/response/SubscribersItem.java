package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class SubscribersItem {
    private String uid;
    private Addresses addresses;
    @JsonProperty("removed_at")
    private String removedAt;
    @JsonProperty("updated_at")
    private String updatedAt;
    private String name;
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("project_uid")
    private String projectUid;
    private int id;
    private List<Object> notifications;
}