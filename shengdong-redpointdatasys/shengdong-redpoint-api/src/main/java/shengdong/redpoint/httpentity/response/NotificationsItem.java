package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class NotificationsItem {
    @JsonProperty("notification_type")
    private String notificationType;
    private String uid;
    private Types types;
    @JsonProperty("removed_at")
    private String removedAt;
    @JsonProperty("updated_at")
    private String updatedAt;
    private String name;
    private Boolean active;
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("project_uid")
    private String projectUid;
    private Long id;
    private Conditions conditions;
}