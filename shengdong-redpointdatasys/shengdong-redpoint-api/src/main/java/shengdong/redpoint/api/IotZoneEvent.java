package shengdong.redpoint.api;

import com.google.common.base.Strings;
import shengdong.common.util.JSONUtil;
import shengdong.redpoint.httpentity.response.ResponseZoneEvents;
import shengdong.redpoint.model.query.Header;
import shengdong.redpoint.model.query.HttpMethod;
import shengdong.redpoint.model.query.HttpParam;

import java.util.Date;

public class IotZoneEvent extends IotApiBase {

    public IotZoneEvent(IotApiConfig cnf, Header httpheader) {
        super(cnf, httpheader);
    }

    /**
     * 获取所有区域事件
     *
     * @param from
     * @param to
     * @param trackable_objects
     * @param sublocation_uid
     * @param zone_type_name
     * @return
     * @throws Exception
     */
    public ResponseZoneEvents getZoneEvents(Date from, Date to, String trackable_objects, String sublocation_uid,
        String zone_type_name) throws Exception {
        String url =
            config.getDomen() + config.getZoneEventsApiGetZoneEvents() + "&from=" + from.getTime() + "&to=" + to.getTime();
        if (!Strings.isNullOrEmpty(trackable_objects)) {
            url += "trackable_objects=" + trackable_objects;
        }
        if (!Strings.isNullOrEmpty(sublocation_uid)) {
            url += "&sublocation_uid=" + sublocation_uid;
        }
        HttpParam hp = new HttpParam();
        hp.setUrl(url);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.GET);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseZoneEvents.class);
    }

    public ResponseZoneEvents getZoneEvents(Date from, Date to, String sublocation_uid) throws Exception {
        return getZoneEvents(from, to, null, sublocation_uid, null);
    }
}
