package shengdong.data.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 可追踪对象节点实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Bt23Trackable {

    //自增主键
    private Long id;

    //可追踪对象节点唯一标识
    private String trackableUid;

    //公司唯一标识
    private String companyUid;

    //项目唯一标识
    private String projectUid;

    //硬件设备节点mac地址
    private String macAddress;

    //区域围栏唯一标识（如果存在）
    private String selfZoneUid;

    //定位对象群组标识列表
    private String grouplist;

    //地点子区域唯一标识
    private String sublocationUid;

    //可追踪对象节点名称
    private String name;

    //默认高度
    private Long defaultHeight;

    //显示的名称
    private String displayName;

    //蓝牙配置
    private String btConfig;

    //加速计配置
    private String accConfig;

    //删除时间（UTC）
    private Date removedAt;

    //传感器配置
    private String sensorConfig;

    //可追踪对象节点类别(NAV (default) ，ASSET，MOBILE)
    private String type;

    //设备是否应答
    private Boolean responsive;

    //设备是否闲置
    private Boolean idle;

    //位置更新的频率
    private String positionUpdateInterval;

    //可配置项目
    private String tagConfig;

    //可追踪对象节点类别(PERSON ，VEHICLE，UNSPECIFIED (default)，ASSET)
    private String category;

    //位置的 x 坐标
    private Long x;

    //位置的 y 坐标
    private Long y;

    //位置的 z 坐标
    private Long z;

    //硬件设备节点的服务器的唯一ID
    private Long nodeId;

    //创建时间（UTC）
    private Date createdAt;

    //更新时间（UTC）
    private Date updatedAt;

    //数据最后更新者
    private String creatorAt;

    //数据信息备注
    private String remark;
    /**
     * 项目所属用户id
     */
    private Long userId;
    /**
     * 已删除
     */
    private Boolean deleteMark;

    //自定义标签名称
    private String setName;
    //自定义标签类型
    private int setTypeid;
    //自定义标签显示图标
    private String setImageurl;
}