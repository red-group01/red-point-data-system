package shengdong.data.dao.impl;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import shengdong.data.dao.ITestDao;
import shengdong.data.entity.TestTable;
import shengdong.data.mapper.TestTableMapper;

import java.util.List;

@Repository
public class TestDaoImpl implements ITestDao {
    @Autowired
    private TestTableMapper testTableMapper;
    @Override
    public List<TestTable> listTestTable() {
        return null;
    }

    @Override
    public TestTable listTestTableById(Integer id) {
        return testTableMapper.selectByPrimaryKey(id);
    }

    @Override
    public int insertTestTable(TestTable testTable) {
        return testTableMapper.insert(testTable);
    }
}
