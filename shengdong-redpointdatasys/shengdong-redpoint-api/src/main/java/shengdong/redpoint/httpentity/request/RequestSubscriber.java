package shengdong.redpoint.httpentity.request;

import java.util.List;
import lombok.Data;

@Data
public class RequestSubscriber{
	private Addresses addresses;
	private String name;
	private String projectUid;
	private List<String> notifications;
}