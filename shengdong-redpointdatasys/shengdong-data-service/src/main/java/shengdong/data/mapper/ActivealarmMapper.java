package shengdong.data.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import shengdong.data.entity.Activealarm;

import java.util.List;

@Mapper
@Component
public interface ActivealarmMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Activealarm record);

    int insertSelective(Activealarm record);

    Activealarm selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Activealarm record);

    int updateByPrimaryKey(Activealarm record);

    int deleteByUserId(Long userId);

    List<Activealarm> selectBySubUid(String sublocationUid);
}