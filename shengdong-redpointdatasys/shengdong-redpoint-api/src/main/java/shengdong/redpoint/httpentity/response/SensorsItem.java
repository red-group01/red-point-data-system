package shengdong.redpoint.httpentity.response;

import lombok.Data;

@Data
public class SensorsItem{
	private boolean reliable;
	private String type;
	private boolean enabled;
}