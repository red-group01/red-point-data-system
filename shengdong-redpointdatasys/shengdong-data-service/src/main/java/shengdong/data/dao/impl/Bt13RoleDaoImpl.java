package shengdong.data.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import shengdong.data.dao.IBt13RoleDao;
import shengdong.data.entity.Bt13Role;
import shengdong.data.mapper.Bt13RoleMapper;

import java.util.List;

/**
 * 角色Dao实现
 */
@Repository
public class Bt13RoleDaoImpl implements IBt13RoleDao {

    // 注入Mapper接口
    @Autowired
    private Bt13RoleMapper bt13RoleMapper;

    /**
     * 插入角色数据
     *
     * @param record 角色实体类
     * @return 插入的行数
     */
    @Override
    public int insert(Bt13Role record) {
        return bt13RoleMapper.insert(record);
    }

    @Override
    public Bt13Role getRoleByUid(String uid) {
        return bt13RoleMapper.getRoleByUid(uid);
    }

    @Override
    public int update(Bt13Role entRole) {
        return bt13RoleMapper.updateByPrimaryKeySelective(entRole);
    }

    @Override
    public void truncateTable() {

        bt13RoleMapper.truncateTable();
    }

    @Override
    public List<Bt13Role> getAll() {
        return bt13RoleMapper.selectAll();
    }
}
