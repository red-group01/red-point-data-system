package shengdong.data;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("shengdong")
public class ShengdongDataServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShengdongDataServiceApplication.class, args);
    }

}
