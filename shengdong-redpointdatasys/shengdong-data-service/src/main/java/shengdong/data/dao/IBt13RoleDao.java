package shengdong.data.dao;

import shengdong.data.entity.Bt13Role;

import java.util.List;

/**
 * 角色Dao接口
 */
public interface IBt13RoleDao {
    /**
     * 插入角色数据
     *
     * @param record 角色实体类
     * @return 插入的行数
     */
    public int insert(Bt13Role record);

    Bt13Role getRoleByUid(String uid);

    int update(Bt13Role entRole);

    void truncateTable();

    List<Bt13Role> getAll();
}
