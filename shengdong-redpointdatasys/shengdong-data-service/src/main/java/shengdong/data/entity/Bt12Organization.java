package shengdong.data.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 人员组织结构实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Bt12Organization {
    //自增主键
    private Long id;

    //人员组织结构唯一标识
    private String organizationUid;

    //公司唯一标识
    private String companyUid;

    //组织结构编码
    private String organizationCode;

    //层级级别
    private String level;

    //组织结构名称
    private String name;

    //创建时间（UTC）
    private Date createdAt;

    //更新时间（UTC）
    private Date updatedAt;

    //数据最后更新者
    private String creatorAt;

    //数据信息备注
    private String remark;

}