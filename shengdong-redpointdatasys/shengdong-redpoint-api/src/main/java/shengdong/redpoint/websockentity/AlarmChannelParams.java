package shengdong.redpoint.websockentity;

import lombok.Data;

@Data
public class AlarmChannelParams {
    private AlarmChannelData data;
    private String subscription;
}