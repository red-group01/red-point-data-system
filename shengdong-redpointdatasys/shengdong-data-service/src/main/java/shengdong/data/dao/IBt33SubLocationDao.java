package shengdong.data.dao;

import shengdong.data.entity.Bt33SubLocation;

import java.util.List;

/**
 * 地点子区域Dao接口
 */
public interface IBt33SubLocationDao {
    /**
     * 插入地点子区域数据
     *
     * @param record 地点子区域实体类
     * @return 插入的行数
     */
    public int insert(Bt33SubLocation record);

    List<Bt33SubLocation> getUserSubLocation(Long id);

    int updateByid(Bt33SubLocation local);

    void truncateTable();

    List<Bt33SubLocation> getAllSublocation();

    List<Bt33SubLocation> selectByLocationUid(String locationUid);
}
