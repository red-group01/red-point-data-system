package shengdong.data.dao;

import shengdong.data.entity.Bt42Notifications;

import java.util.List;

/**
 * 系统通知Dao接口
 */
public interface IBt42NotificationsDao {
    /**
     * 插入系统通知数据
     *
     * @param record 系统通知实体类
     * @return 插入的行数
     */
    public int insert(Bt42Notifications record);

    void truncateTable();

    List<Bt42Notifications> getAllNotifications(String project);

    int updateByid(Bt42Notifications local);

    List<Bt42Notifications> getUserNotificationByUserId(Long id);
}
