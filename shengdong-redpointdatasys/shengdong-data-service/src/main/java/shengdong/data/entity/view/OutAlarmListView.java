package shengdong.data.entity.view;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 用于页面展示的告警列表实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OutAlarmListView {
    private int serial;
    private String nodeName;
    private String message;
    //@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date alarmTime;
    private String zoneName;
}
