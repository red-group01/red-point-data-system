package shengdong.data.entity.view;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用于页面展示的标签列表实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OutNodeListView {
    private int serial;
    private String nodeName;
    private String uid;
    private String type;
    private String isActive;
    private String coordinate;
}
