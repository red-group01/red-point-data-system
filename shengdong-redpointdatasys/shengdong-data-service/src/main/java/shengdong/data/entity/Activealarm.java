package shengdong.data.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Activealarm {
    private Long id;

    private String alarmUid;

    private String sublocationUid;

    private String zoneUid;

    private String macAddress;

    private String objUid;

    private String type;

    private String additionalInfo;

    private Integer hysteresisCount;

    private String state;

    private Date ts;

    private Long x;

    private Long y;

    private Long z;

    private Date createdAt;

    private Date updatedAt;

    private String creatorAt;

    private String remark;

    private String projectUid;

    private Long userId;

}