package shengdong.data.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import shengdong.data.entity.Bt42Notifications;

import java.util.List;

/**
 * 系统通知表Mapper映射方法接口
 */
@Mapper
@Component
public interface Bt42NotificationsMapper {

    /**
     * 根据id删除系统通知表数据
     *
     * @param id 自增主键
     * @return 删除行数
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 新增系统通知表数据
     *
     * @param record 系统通知实体类
     * @return 新增行数
     */
    int insert(Bt42Notifications record);

    /**
     * 新增系统通知表数据(根据一定规则)
     *
     * @param record 系统通知实体类
     * @return 新增行数
     */
    int insertSelective(Bt42Notifications record);

    /**
     * 根据id查询系统通知表数据
     *
     * @param id 系统通知实体类
     * @return 系统通知实体类
     */
    Bt42Notifications selectByPrimaryKey(Long id);

    /**
     * 更新系统通知表数据(根据一定规则)
     *
     * @param record 系统通知实体类
     * @return 更新行数
     */
    int updateByPrimaryKeySelective(Bt42Notifications record);

    /**
     * 更新系统通知表数据
     *
     * @param record 系统通知实体类
     * @return 更新行数
     */
    int updateByPrimaryKey(Bt42Notifications record);

    void truncateTable();

    List<Bt42Notifications> selectAllNotifications(String puid);

    List<Bt42Notifications> selectByUserId(Long id);
}