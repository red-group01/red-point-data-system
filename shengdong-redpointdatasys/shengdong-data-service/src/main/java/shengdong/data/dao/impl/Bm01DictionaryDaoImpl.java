package shengdong.data.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import shengdong.data.dao.IBm01DictionaryDao;
import shengdong.data.entity.Bm01Dictionary;
import shengdong.data.mapper.Bm01DictionaryMapper;

/**
 * 数据字典Dao实现
 */
@Repository
public class Bm01DictionaryDaoImpl implements IBm01DictionaryDao {

    // 注入Mapper接口
    @Autowired
    private Bm01DictionaryMapper bm01DictionaryMapper;

    /**
     * 插入数据字典数据
     *
     * @param record 数据字典实体类
     * @return 插入的行数
     */
    @Override
    public int insert(Bm01Dictionary record) {
        return bm01DictionaryMapper.insert(record);
    }
}
