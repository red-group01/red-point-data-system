package shengdong.data.service;

import shengdong.data.entity.Bt14User;
import shengdong.redpoint.api.IotApiConfig;
import shengdong.redpoint.model.query.Header;

public interface IBaseInfoService {
    /**
     * 同步全部基础配置数据
     *
     * @return
     */
    boolean synAll() throws Exception;

    boolean synLocation(IotApiConfig cnf, Header h, Bt14User user) throws Exception;

    boolean synSublocation(IotApiConfig cnf, Header h, Bt14User user) throws Exception;

    boolean synNode(IotApiConfig cnf, Header h, Bt14User user) throws Exception;

    boolean synAnchor(IotApiConfig cnf, Header h, Bt14User user) throws Exception;

    public boolean synNotifications(IotApiConfig cnf, Header h, Bt14User user) throws Exception;

    boolean synTrackable(IotApiConfig cnf, Header h, Bt14User user) throws Exception;

    boolean synUser(IotApiConfig cnf, Bt14User user) throws Exception;

    boolean synProject(IotApiConfig cnf, Header h, Bt14User user) throws Exception;

    boolean synZone(IotApiConfig cnf, Header h, Bt14User user) throws Exception;
}
