package shengdong.common.error;

public interface IErrorCode {

        /**
         * 获取错误码
         * @return
         */
        String getCode();

        /**
         * 获取错误信息
         * @return
         */
        String getMessage();

}
