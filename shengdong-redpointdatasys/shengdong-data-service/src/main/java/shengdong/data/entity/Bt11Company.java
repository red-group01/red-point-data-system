package shengdong.data.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 公司实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Bt11Company {

    //主键id
    private Long id;

    //公司唯一标识
    private String companyUid;

    //公司名称
    private String name;

    //服务器中公司的标识符
    private Long companyId;

    //创建时间（UTC）
    private Date createdAt;

    //更新时间（UTC）
    private Date updatedAt;

    //数据最后更新者
    private String creatorAt;

    //数据信息备注
    private String remark;

}