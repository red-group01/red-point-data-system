package shengdong.redpoint.httpentity.response;

import java.util.List;
import lombok.Data;

@Data
public class EventsItem{
	private int duration;
	private int distance;
	private int ms;
	private List<TrackablesItem> trackables;
	private String type;
	private int ts;
}