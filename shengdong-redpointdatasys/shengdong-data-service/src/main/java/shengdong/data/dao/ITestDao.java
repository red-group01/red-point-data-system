package shengdong.data.dao;

import shengdong.data.entity.TestTable;

import java.util.List;

public interface ITestDao {
    // 查询所有测试表数据
    public List<TestTable> listTestTable();
    // 根据id查询测试表数据
    public TestTable listTestTableById(Integer id);

    public int insertTestTable(TestTable testTable);
}
