package shengdong.common.config;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component
public class CameraConfig {
    private List<String> uid;
    private List<String> icoUid;
    private List<String> name;
    private List<String> type;
    private List<String> appKey;
    private List<String> appSecret;
    private List<String> url;
    private List<String> url2;
    private List<String> x;
    private List<String> y;
    private List<String> imageId;
    private List<String> mac;
}
