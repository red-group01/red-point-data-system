package shengdong.data.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import shengdong.data.dao.IBt44AlarmDao;
import shengdong.data.entity.Bt44Alarm;
import shengdong.data.mapper.Bt44AlarmMapper;

import java.util.Date;
import java.util.List;

/**
 * 设备告警Dao实现
 */
@Repository
public class Bt44AlarmDaoImpl implements IBt44AlarmDao {
    // 注入Mapper接口
    @Autowired
    private Bt44AlarmMapper bt44AlarmMapper;

    /**
     * 插入设备告警数据
     *
     * @param record 设备告警实体类
     * @return 插入的行数
     */
    @Override
    public int insert(Bt44Alarm record) {
        return bt44AlarmMapper.insert(record);
    }

    @Override
    public void truncateTable() {
        bt44AlarmMapper.truncateTable();
    }

    @Override
    public List<Bt44Alarm> getAllAlarmHis(String project) {
        return bt44AlarmMapper.selectAllAlarm(project);
    }

    @Override
    public List<Bt44Alarm> selectBySublocationUid(String sublocationUid, String createdAtBegin, String createdAtEnd) {
        return bt44AlarmMapper.selectBySublocationUid(sublocationUid, createdAtBegin, createdAtEnd);
    }

    @Override
    public List<Bt44Alarm> selectByObjAndTime(Bt44Alarm bt44Alarm) {
        return bt44AlarmMapper.selectByObjAndTime(bt44Alarm);
    }
}
