package shengdong.redpoint.httpentity.response;

import lombok.Data;

@Data
public class Settings {
    private long zoneVerticesLimit;
}