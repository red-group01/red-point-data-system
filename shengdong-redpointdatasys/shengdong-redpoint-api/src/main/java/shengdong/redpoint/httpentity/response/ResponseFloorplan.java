package shengdong.redpoint.httpentity.response;

import lombok.Data;

import java.util.List;

@Data
public class ResponseFloorplan {
    private List<FloorplansItem> floorplans;
}