package shengdong.data.dao;

import org.apache.ibatis.annotations.Param;
import shengdong.data.entity.Bt50PoinsHisView;

import java.util.Date;
import java.util.List;

public interface IBt50PoinsHisDao {

    /**
     * 历史数据文件导入
     *
     * @param absolutePath
     */
    void insertBatchByLoadData(@Param("path") String absolutePath);

    /**
     * 查询历史数据参数配置项
     *
     * @return
     */

    List<Bt50PoinsHisView> selectCnf();

    /**
     * 更新标签定位历史同步时间
     *
     * @param date
     * @param uid
     */

    void flushPointDate(Date date, String uid);

    /**
     * 更新报警历史同步时间
     *
     * @param date
     * @param uid
     */

    void flushAlarmDate(Date date, String uid);

    /**
     * 更新区域事件历史同步时间
     *
     * @param date
     * @param uid
     */

    void flushZoneDate(Date date, String uid);
}
