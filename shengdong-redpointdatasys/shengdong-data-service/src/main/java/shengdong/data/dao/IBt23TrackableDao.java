package shengdong.data.dao;

import shengdong.data.entity.BaseNodeType;
import shengdong.data.entity.Bt23Trackable;

import java.util.List;

/**
 * 可追踪对象节点Dao接口
 */
public interface IBt23TrackableDao {

    /**
     * 插入可追踪对象节点数据
     *
     * @param record 可追踪对象节点实体类
     * @return 插入的行数
     */
    public int insert(Bt23Trackable record);

    List<Bt23Trackable> getUserTrackbale(Long id);

    int updateByid(Bt23Trackable local);

    void truncateTable();

    List<Bt23Trackable> getAllTrackable();

    /**
     * 根据id查询可追踪对象节点表数据
     *
     * @param trackableUid 可追踪对象节点uid
     * @return 可追踪对象节点实体类
     */
    List<Bt23Trackable> selectByTrackableUid(String trackableUid);

    /**
     * 根据地点子区域uid查询可追踪对象节点表数据
     *
     * @param sublocationUid 地点子区域uid
     * @return 可追踪对象节点实体类
     */
    List<Bt23Trackable> selectBySublocationUid(String sublocationUid);

    /**
     * 根据地点子区域uid查询可追踪对象节点表数据的类别分组
     *
     * @param sublocationUid 地点子区域uid
     * @return 可追踪对象节点实体类
     */
    List<BaseNodeType> selectBySublocationUidGroupByCategory(String sublocationUid);
}
