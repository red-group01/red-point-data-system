package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class ResponseEventsHistory {
    private String nextPage;
    @JsonProperty("external_events")
    private List<ExternalEventsItem> externalEvents;
    private int totalCount;
}