package shengdong.data.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import shengdong.data.entity.Bt31Project;
import shengdong.data.entity.Bt50PoinsHisView;

import java.util.List;

/**
 * 项目表Mapper映射方法接口
 */
@Mapper
@Component
public interface Bt31ProjectMapper {

    /**
     * 根据id删除项目表数据
     *
     * @param id 自增主键
     * @return 删除行数
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 根据id查询项目表数据
     *
     * @param id 项目实体类
     * @return 项目实体类
     */
    Bt31Project selectByPrimaryKey(Long id);

    /**
     * 更新项目表数据(根据一定规则)
     *
     * @param record 项目实体类
     * @return 更新行数
     */
    int updateByPrimaryKeySelective(Bt31Project record);

    /**
     * 更新项目表数据
     *
     * @param record 项目实体类
     * @return 更新行数
     */
    int updateByPrimaryKey(Bt31Project record);

    /**
     * 更新项目表数据
     *
     * @param record 项目实体类
     * @return 更新行数
     */
    int insert(Bt31Project record);

    List<Bt31Project> selectByUserId(Long userid);

    void truncateTable();

    Bt31Project selectByUid(String uid);

    List<Bt50PoinsHisView> selectCnf();
}