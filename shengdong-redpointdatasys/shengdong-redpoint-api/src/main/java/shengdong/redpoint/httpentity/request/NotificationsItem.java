package shengdong.redpoint.httpentity.request;

import lombok.Data;

@Data
public class NotificationsItem{
	private String name;
	private String email;
}