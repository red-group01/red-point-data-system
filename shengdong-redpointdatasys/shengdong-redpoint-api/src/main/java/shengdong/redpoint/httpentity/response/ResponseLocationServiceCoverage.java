package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class ResponseLocationServiceCoverage {
    @JsonProperty("location_service_coverage")
    private List<LocationServiceCoverageItem> locationServiceCoverage;
}