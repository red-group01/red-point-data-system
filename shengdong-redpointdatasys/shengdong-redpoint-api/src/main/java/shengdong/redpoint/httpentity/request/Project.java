package shengdong.redpoint.httpentity.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Project {
    private String name;
    @JsonProperty("user_params")
    private UserParams userParams;
}