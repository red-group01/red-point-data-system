package shengdong.data.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 角色实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Bt13Role {

    //自增主键
    private Long id;

    //角色唯一标识
    private String roleUid;

    //角色名称
    private String name;

    //角色权限描述
    private String remark;

    //创建时间(UTC)
    private Date createdAt;

    //更新时间(UTC)
    private Date updatedAt;

    //数据最后更新者
    private String creatorAt;

}