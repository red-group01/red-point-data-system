package shengdong.redpoint.websockentity;

import lombok.Data;

@Data
public class RequestAdd {
    private String method;
    private int id;
    private String jsonrpc;
    private RequestAddParams params;
}