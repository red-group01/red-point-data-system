package shengdong.redpoint.httpentity.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class RequestAlarm {
    @JsonProperty("alarm_name")
    private String alarmName;
    @JsonProperty("sublocation_uid")
    private String sublocationUid;
    @JsonProperty("additional_info")
    private AdditionalInfo additionalInfo;
    @JsonProperty("obj_class")
    private String objClass;
    private int ms;
    private int x;
    @JsonProperty("obj_uid")
    private String objUid;
    private int y;
    private int z;
    private String state;
    private int ts;
}