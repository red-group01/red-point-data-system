package shengdong.data.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import shengdong.data.dao.IBt43ZoneEventDao;
import shengdong.data.entity.Bt43ZoneEvent;
import shengdong.data.mapper.Bt43ZoneEventMapper;

import java.util.Date;
import java.util.List;

/**
 * 区域事件Dao实现
 */
@Repository
public class Bt43ZoneEventDaoImpl implements IBt43ZoneEventDao {
    // 注入Mapper接口
    @Autowired
    private Bt43ZoneEventMapper bt43ZoneEventMapper;

    /**
     * 插入区域事件数据
     *
     * @param record 区域事件实体类
     * @return 插入的行数
     */
    @Override
    public int insert(Bt43ZoneEvent record) {
        return bt43ZoneEventMapper.insert(record);
    }

    @Override
    public Date getProjectLastTime(String puid) {
        return bt43ZoneEventMapper.selectProjectLastTime(puid);
    }

    @Override
    public void truncateTable() {
        bt43ZoneEventMapper.truncateTable();
    }

    @Override
    public List<Bt43ZoneEvent> getAllZoneEvent(String puid) {
        return bt43ZoneEventMapper.selectAllZoneEvent(puid);
    }
}
