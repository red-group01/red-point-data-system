package shengdong.redpoint.api;

import lombok.Getter;
import lombok.Setter;

public class IotApiConfig {
    @Getter
    @Setter
    private String serverIp = "18.181.137.245";

    //---------------------AUTH API
    @Getter
    @Setter
    private String authApiLogin = "api/auth/log_in";
    @Getter
    private String authApiValidateToken = "api/auth/validate_token";
    @Getter
    private String authApiRefreshToken = "api/auth/refresh_token";
    @Getter
    private String authApiLogout = "api/auth/log_out";
    //---------------------AUTH API

    //---------------------PROJECTS API
    @Getter
    @Setter
    private String projectsApiGetProjects = "api/projects";
    @Getter
    private String projectsApiCreateProject = "api/projects";
    @Getter
    private String projectsApiUpdateProject = "api/projects/";
    @Getter
    private String projectsApiDeleteProject = "api/projects/";
    //---------------------PROJECTS API

    //---------------------LOCATIONS API
    @Getter
    @Setter
    private String locationApiGetSites = "api/locations/";
    @Getter
    private String locationApiGetSingleSite = "api/locations/";
    @Getter
    private String locationApiAddSite = "api/locations/";
    @Getter
    private String locationApiUpdateSite = "api/locations/";
    @Getter
    private String locationApiDeleteSite = "api/locations/";

    //---------------------LOCATIONS API

    //---------------------POSITIONS HISTORY API
    @Getter
    @Setter
    private String positionHistoryApiPositionsHistory = "api/positions_history";
    //---------------------POSITIONS HISTORY API

    //---------------------sublocations API
    @Getter
    @Setter
    private String sublocationsApiGetArea = "api/sublocations";
    @Getter
    private String sublocationsApiGetSingleArea = "api/sublocations/";
    @Getter
    private String sublocationsApiCreateArea = "api/sublocations";
    @Getter
    private String sublocationsApiEditArea = "api/sublocations/";
    @Getter
    private String sublocationsApiDeleteArea = "api/sublocations/";

    //---------------------sublocations API

    //---------------------FLOOR PLANS API
    @Getter
    private String floorPlanApiGetFloorplans = "api/floorplans";
    @Getter
    private String floorPlanApiGetSingleFloorplan = "api/floorplans/";
    @Getter
    private String floorPlanApiCreateFloorplan = "api/floorplans";
    @Getter
    private String floorPlanApiEditFloorplan = "api/floorplans/";
    @Getter
    private String floorPlanApiDeleteFloorplan = "api/floorplans/";

    //---------------------FLOOR PLANS API

    //---------------------NODES API
    @Getter
    @Setter
    private String nodesApiGetNodes = "/api/nodes";
    @Getter
    private String nodesApiGetSingleNode = "/api/nodes/";
    @Getter
    private String nodesApiGetFilteredNodes = "/api/nodes";
    @Getter
    private String nodesApiCreateNode = "/api/nodes";
    @Getter
    private String nodesApiEditNode = "/api/nodes/";
    @Getter
    private String nodesApiDeleteNode = "/api/nodes/";
    @Getter
    private String nodesApiAssignNodeToProject = "/api/nodes_associations/assign";
    @Getter
    private String nodesApiReleaseNodeFromProject = "/api/nodes_associations/release";

    //---------------------NODES API

    //---------------------WIN ANCHORS API
    @Getter
    @Setter
    private String winAnchorsApiGetWinAnchors = "api/win_anchors";
    @Getter
    private String winAnchorsApiGetSingleWinAnchor = "api/win_anchors/";
    @Getter
    private String winAnchorsApiCreateWinAnchors = "api/win_anchors";
    @Getter
    private String winAnchorsApiEditWinAnchors = "api/win_anchors/";
    @Getter
    private String winAnchorsApiDeleteAnchor = "api/win_anchors/";
    @Getter
    private String winAnchorsApiGetActiveAssociation = "api/win_anchor_associations";
    @Getter
    private String winAnchorsApiGetAllAssociations = "api/win_anchor_associations.json?all=true";
    @Getter
    private String winAnchorsApiCreateAssociation = "api/win_anchor_associations";
    @Getter
    private String winAnchorsApiEditAssociation = "api/win_anchor_associations/";
    @Getter
    private String winAnchorsApiCloseAssociation = "api/win_anchor_associations/close";
    @Getter
    private String winAnchorsApiCeleteAssociation = "api/win_anchor_associations/";

    //---------------------WIN ANCHORS API

    //---------------------TRACKABLES API
    @Getter
    @Setter
    private String trackablesApiGetTrackables = "api/trackable_objects/";
    @Getter
    private String trackablesApiGetSingleTrackable = "api/trackable_objects/";
    @Getter
    private String trackablesApiGetTrackableByMacAddress = "api/trackable_objects?mac_address=";
    @Getter
    private String trackablesApiGetTrackablesActiveForPeriod = "api/trackable_objects/active_for_period?";
    @Getter
    private String trackablesApiGetTheLastActiveTimeForTrackables = "api/trackable_objects/lastActive";
    @Getter
    private String trackablesApiAddTrackables = "api/trackable_objects/";
    @Getter
    private String trackablesApiUpdateTrackable = "api/trackable_objects/";
    @Getter
    private String trackablesApiUpdateGroupsForTrackables = "api/trackable_objects/{0}/groups/";
    @Getter
    private String trackablesApiDeleteTrackables = "api/trackable_objects/";
    @Getter
    private String trackablesApiGetTrackableAssociations = "api/trackable_objects_associations/";
    @Getter
    private String trackablesApiGetSingleTrackableAssociation = "api/trackable_objects_associations/";
    @Getter
    private String trackablesApiGetTrackableAssociationsHistory = "api/trackable_objects_associations?";
    @Getter
    private String trackablesApiAddTrackableAssociation = "api/trackable_objects_associations/";
    @Getter
    private String trackablesApiUpdateTrackableAssociation = "api/trackable_objects_associations/";
    @Getter
    private String trackablesApiCloseTrackableAssociation = "api/trackable_objects_associations/close";
    @Getter
    private String trackablesApiDeleteTrackableAssociation = "api/trackable_objects_associations/";
    @Getter
    private String trackablesApiUpdateSensorConfigForTrackableObject = "api/trackable_objects/{0}/sensor_config";
    @Getter
    private String trackablesApiGetSensorConfigForTrackableObject = "api/trackable_objects/{0}/sensor_config";
    @Getter
    private String trackablesApiRequestSensorDataForTrackableObject = "api/trackable_objects/{0}/sensor_data/request";
    @Getter
    private String trackablesApiSendSensorDataForTrackableObject = "api/trackable_objects/{0}/sensor_data/send";

    //---------------------TRACKABLES API

    //---------------------GROUPS API
    @Getter
    private String groupsApiGetGroups = "api/groups";
    @Getter
    private String groupsApiGetSingleGroup = "api/groups/";
    @Getter
    private String groupsApiAddGroup = "api/groups";
    @Getter
    private String groupsApiUpdateGroup = "api/groups/";
    @Getter
    private String groupsApiDeleteGroup = "api/groups/";
    //---------------------GROUPS API

    //---------------------ZONES API
    @Getter
    @Setter
    private String zonesApiGetAllZonesAndBuffers = "api/zones";
    @Getter
    private String zonesApiGetZonesAndBuffersHistory = "api/zones/?";
    @Getter
    private String zonesApiGetSingleZoneOrBuffer = "api/zones/";
    @Getter
    private String zonesApiCreateZone = "api/zones";
    @Getter
    private String zonesApiCreateBuffer = "api/zones";
    @Getter
    private String zonesApiEditZoneOrBuffer = "api/zones/";
    @Getter
    private String zonesApiDeleteZoneOrBuffer = "api/zones/";
    @Getter
    private String zonesApiGetZoneTypes = "api/zonetypes";
    @Getter
    private String zonesApiCreateMultiTieredBuffer = "api/multi_tiered_zone";
    @Getter
    private String zonesApiDeleteMultiTieredBuffer = "api/multi_tiered_zone/";

    //---------------------ZONES API

    //---------------------Zone Events API
    @Getter
    @Setter
    private String zoneEventsApiGetZoneEvents = "api/zone_events?";

    //---------------------Zone Events API

    //---------------------Location Service Coverage API
    @Getter
    private String getAllLocationServiceCoverage = "api/location_service_coverage";
    @Getter
    private String getLocationServiceCoverageFromSublocations = "api/location_service_coverage/sublocation/";
    @Getter
    private String getSingleLocationServiceCoverage = "api/location_service_coverage/";

    //---------------------Location Service Coverage API

    //---------------------ALARMS API
    @Getter
    @Setter
    private String alarmsApiGetAlarms = "api/alarms";
    @Getter
    private String alarmsApiGetAlarmHistory = "api/alarms?";
    @Getter
    private String alarmsApiGetActiveAlarms = "api/alarms/active";
    @Getter
    private String alarmsApiCreateAlarm = "api/alarms";
    @Getter
    private String alarmsApiUpdateAlarm = "api/alarms/";
    @Getter
    private String alarmsApiDeleteAlarm = "api/alarms/";
    //---------------------ALARMS API

    //---------------------NOTIFICATIONS AND SUBSCRIBERS API
    @Getter
    @Setter
    private String notificationsApiGetNotifications = "api/notifications";
    @Getter
    private String notificationsApiGetSingleNotification = "api/notifications/";
    @Getter
    private String notificationsApiCreateNotification = "api/notifications";
    @Getter
    private String notificationsApiEditNotification = "api/notifications/";
    @Getter
    private String notificationsApiDeleteNotification = "api/notifications/";
    @Getter
    private String subscribersApiGetSubscribers = "api/subscribers";
    @Getter
    private String subscribersApiGetSingleSubscriber = "api/subscribers/";
    @Getter
    private String subscribersApiCreateSubscriber = "api/subscribers";
    @Getter
    private String subscribersApiEditSubscriber = "api/subscriber/";
    @Getter
    private String subscribersApiDeleteSubscriber = "api/subscriber/{uid}";
    //---------------------NOTIFICATIONS AND SUBSCRIBERS API

    //---------------------REPORTS API
    @Getter
    private String reportApiGetReportsList = "api/report?";
    @Getter
    private String reportApiGetReport = "api/report/{uid}";

    //---------------------REPORTS API

    //---------------------Event History API
    @Getter
    private String eventHistoryGetExternalEvents = "/api/external_events?";

    //---------------------Event History API

    //---------------------NEAR MISS EVENTS API
    @Getter
    private String nearMissEventsApiCreateNearMissEventsReport = "api/near_miss/";
    @Getter
    private String nearMissEventsApiGetNearMissEventsReportStatus = "api/near_miss/status/";
    @Getter
    private String nearMissEventsApiGetNearMissEventsReport = "api/near_miss/";

    //---------------------NEAR MISS EVENTS API

    /**
     * 获取api地址前缀
     *
     * @return
     */
    public String getDomen() {
        return (isSsl ? "https://" : "http://") + serverIp + "/";
    }

    @Getter
    @Setter
    boolean isSsl = true;
}
