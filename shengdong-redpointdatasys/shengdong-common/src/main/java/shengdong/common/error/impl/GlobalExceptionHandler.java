package shengdong.common.error.impl;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.slf4j.Logger;
import shengdong.common.log.IRedSysLog;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    //private Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);
    @Autowired
    private IRedSysLog iRedSysLog;

    /**
     * 自定义异常的处理
     * @param exception
     * @return
     */
    @ExceptionHandler
    @ResponseBody
    public BaseResponse handle(BaseRuntimeException exception) {

        //记录错误日志
        //logger.error(iRedSysLog.toLog(exception.getCode(), exception.getMessage()), exception);
        log.error(iRedSysLog.toLog(exception.getCode(), exception.getMessage()), exception);

        //进行统一格式的错误返回
        return BaseResponse.error(exception.getCode(), exception.getMessage());
    }

    /**
     * 其他异常的处理
     *
     * @param e
     * @return
     */
    @ExceptionHandler
    @ResponseBody
    public BaseResponse<Object> handle(Exception e) {
        //记录日志
        //logger.error(iRedSysLog.toLog("500", e.getMessage()), e);
        log.error(iRedSysLog.toLog("500", e.getMessage()), e);
        //进行统一格式的错误返回
        return BaseResponse.error("500",e.getMessage());
    }
}
