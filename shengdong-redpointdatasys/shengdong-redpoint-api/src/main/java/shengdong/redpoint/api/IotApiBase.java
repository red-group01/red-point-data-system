package shengdong.redpoint.api;

import shengdong.redpoint.model.query.Header;

public class IotApiBase {

    protected IotApiConfig config;
    protected Header header;

    public IotApiBase(IotApiConfig cnf, Header httpheader) {
        config = cnf;
        header = httpheader;
    }
}
