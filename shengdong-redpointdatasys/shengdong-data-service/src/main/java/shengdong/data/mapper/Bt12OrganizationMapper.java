package shengdong.data.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import shengdong.data.entity.Bt12Organization;

/**
 * 人员组织结构表Mapper映射方法接口
 */
@Mapper
@Component
public interface Bt12OrganizationMapper {

    /**
     * 根据id删除人员组织结构表数据
     *
     * @param id 自增主键
     * @return 删除行数
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 新增人员组织结构表数据
     *
     * @param record 人员组织结构实体类
     * @return 新增行数
     */
    int insert(Bt12Organization record);

    /**
     * 新增人员组织结构表数据(根据一定规则)
     *
     * @param record 人员组织结构实体类
     * @return 新增行数
     */
    int insertSelective(Bt12Organization record);

    /**
     * 根据id查询人员组织结构表数据
     *
     * @param id 人员组织结构实体类
     * @return 人员组织结构实体类
     */
    Bt12Organization selectByPrimaryKey(Long id);

    /**
     * 更新人员组织结构表数据(根据一定规则)
     *
     * @param record 人员组织结构实体类
     * @return 更新行数
     */
    int updateByPrimaryKeySelective(Bt12Organization record);

    /**
     * 更新人员组织结构表数据
     *
     * @param record 人员组织结构实体类
     * @return 更新行数
     */
    int updateByPrimaryKey(Bt12Organization record);
}