package shengdong.redpoint.httpentity.response;

import java.util.List;
import lombok.Data;

@Data
public class ResponsetAllLocations{
	private List<LocationsItem> locations;
}