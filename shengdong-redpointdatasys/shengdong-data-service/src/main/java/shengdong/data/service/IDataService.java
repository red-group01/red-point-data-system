package shengdong.data.service;

import shengdong.data.entity.Bt44Alarm;
import shengdong.data.entity.Bt50PoinsHisView;
import shengdong.redpoint.api.IotApiConfig;

import java.util.List;

public interface IDataService {
    void synAll() throws Exception;

    void synZoneEvents();

    void synZoneEvents(Bt50PoinsHisView cnfitem, IotApiConfig cnf) throws Exception;

    void synAlarmsHis();

    void synAlarmsHis(Bt50PoinsHisView cnfitem, IotApiConfig cnf) throws Exception;

    /**
     * 获取活动中的报警信息
     */
    void synAlarmsActive();

    void synPoinsHis();

    void synProjHispoint(Bt50PoinsHisView cnfitem, IotApiConfig cnf) throws Exception;

    long extracted(String src, String des) throws Exception;

}
