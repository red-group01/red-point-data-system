package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class NodeStatus {
    @JsonProperty("ext_pow")
    private Boolean extPow;
    @JsonProperty("low_bat")
    private Boolean lowBat;
    private Boolean sync;
    private Boolean tdoa;
}
