package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Node {
    private String cbid;
    private boolean idle;
    @JsonProperty("mac_address")
    private String macAddress;
    private boolean responsive;
    @JsonProperty("parent_sublocation_uid")
    private String parentSublocationUid;
    @JsonProperty("node_id")
    private long nodeId;
    private String fw;
    private String hw;
}