package shengdong.data.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import shengdong.data.dao.IBt21NodeDao;
import shengdong.data.entity.Bt21Node;
import shengdong.data.mapper.Bt21NodeMapper;

import java.util.List;

/**
 * 硬件设备节点Dao实现
 */
@Repository
public class Bt21NodeDaoImpl implements IBt21NodeDao {
    // 注入Mapper接口
    @Autowired
    private Bt21NodeMapper bt21NodeMapper;

    /**
     * 插入硬件设备节点数据
     *
     * @param record 硬件设备节点实体类
     * @return 插入的行数
     */

    @Override
    public int insert(Bt21Node record) {
        return bt21NodeMapper.insert(record);
    }

    @Override
    public List<Bt21Node> getUserNode(Long id) {
        return bt21NodeMapper.selectByUserId(id);
    }

    @Override
    public int updateByid(Bt21Node i) {
        return bt21NodeMapper.updateByPrimaryKeySelective(i);
    }

    @Override
    public void truncateTable() {
        bt21NodeMapper.truncateTable();
    }

    @Override
    public List<Bt21Node> getAllNode() {
        return bt21NodeMapper.selectAllNode();
    }
}
