package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class ProjectsItem {
    private String uid;
    private Settings settings;
    private String name;
    private int priv;
    private long id;
    @JsonProperty("user_params")
    private UserParams userParams;
    private List<Object> users;
}