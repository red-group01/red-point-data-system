package shengdong.redpoint.api;

import lombok.Data;

@Data
public class RequestNodeToProject{
	private Node node;
	private Project project;
}