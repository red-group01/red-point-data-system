package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ShiftItem {
    @JsonProperty("expected_others")
    private int expectedOthers;
    @JsonProperty("expected_vehicles")
    private int expectedVehicles;
    private String name;
    private String start;
    private boolean active;
    private String end;
    @JsonProperty("expected_person")
    private int expectedPerson;
}