package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class FloorplansItem {
    @JsonProperty("sublocation_uid")
    private String sublocationUid;
    @JsonProperty("image_scale")
    private int imageScale;
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("project_uid")
    private String projectUid;
    @JsonProperty("display_name")
    private String displayName;
    @JsonProperty("image_rotation")
    private int imageRotation;
    private String url;
    private String uid;
    @JsonProperty("image_offset_x")
    private int imageOffsetX;
    @JsonProperty("updated_at")
    private String updatedAt;
    private int size;
    @JsonProperty("sublocation_uid")
    private int sublocationId;
    @JsonProperty("image_offset_y")
    private int imageOffsetY;
    @JsonProperty("img_blob")
    private Object imgBlob;
    private int width;
    @JsonProperty("company_uid")
    private String companyUid;
    private int selected;
    private int height;
}