package shengdong.data.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import shengdong.data.dao.IBt23TrackableDao;
import shengdong.data.entity.BaseNodeType;
import shengdong.data.entity.Bt23Trackable;
import shengdong.data.mapper.Bt23TrackableMapper;

import java.util.List;

/**
 * 可追踪对象节点Dao实现
 */
@Repository
public class Bt23TrackableDaoImpl implements IBt23TrackableDao {
    // 注入Mapper接口
    @Autowired
    private Bt23TrackableMapper bt23TrackableMapper;

    /**
     * 插入可追踪对象节点数据
     *
     * @param record 可追踪对象节点实体类
     * @return 插入的行数
     */
    @Override
    public int insert(Bt23Trackable record) {
        return bt23TrackableMapper.insert(record);
    }

    @Override
    public List<Bt23Trackable> getUserTrackbale(Long id) {
        return bt23TrackableMapper.selectByUserId(id);
    }

    @Override
    public int updateByid(Bt23Trackable local) {
        return bt23TrackableMapper.updateByPrimaryKeySelective(local);
    }

    @Override
    public void truncateTable() {
        bt23TrackableMapper.truncateTable();
    }

    @Override
    public List<Bt23Trackable> getAllTrackable() {
        return bt23TrackableMapper.selectAllTrackable();
    }

    /**
     * 根据id查询可追踪对象节点表数据
     *
     * @param trackableUid 可追踪对象节点uid
     * @return 可追踪对象节点实体类
     */
    @Override
    public List<Bt23Trackable> selectByTrackableUid(String trackableUid) {
        return bt23TrackableMapper.selectByTrackableUid(trackableUid);
    }

    @Override
    public List<Bt23Trackable> selectBySublocationUid(String sublocationUid) {
        return bt23TrackableMapper.selectBySublocationUid(sublocationUid);
    }

    @Override
    public List<BaseNodeType> selectBySublocationUidGroupByCategory(String sublocationUid) {
        return bt23TrackableMapper.selectBySublocationUidGroupByCategory(sublocationUid);
    }
}
