package shengdong.data.entity.view;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import shengdong.data.entity.Bt34Zone;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Project {
    //自增主键
    private Long id;

    //项目唯一标识
    private String projectUid;

    //项目名称
    private String name;

    //允许创建的区域最大数量
    private Long zoneVerticesLimit;

    //项目地址
    private String address;

    //经理
    private String manager;

    //时区
    private String timezone;

    //项目的服务器标识id
    private Long projectId;

    //创建时间(UTC)
    private Date createdAt;

    //更新时间(UTC)
    private Date updatedAt;

    //数据最后更新者
    private String creatorAt;

    //数据信息备注
    private String remark;
    /**
     * 项目所属用户id
     */
    private Long userId;
    /**
     * 已删除
     */
    private Boolean deleteMark;
    private List<Location> locations = new ArrayList<>();
    private List<Bt34Zone> zones = new ArrayList<>();
}
