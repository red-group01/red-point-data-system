package shengdong.redpoint.api;

import com.google.common.base.Strings;
import shengdong.common.util.JSONUtil;
import shengdong.redpoint.httpentity.request.RequestMultiTieredBuffer;
import shengdong.redpoint.httpentity.request.RequestZones;
import shengdong.redpoint.httpentity.response.ResponseMultiTieredBuffer;
import shengdong.redpoint.httpentity.response.ResponseZones;
import shengdong.redpoint.httpentity.response.ResponseZonesTypes;
import shengdong.redpoint.httpentity.response.ZonesItem;
import shengdong.redpoint.model.query.Header;
import shengdong.redpoint.model.query.HttpMethod;
import shengdong.redpoint.model.query.HttpParam;

import java.util.Date;

public class IotZone extends IotApiBase {

    public IotZone(IotApiConfig cnf, Header httpheader) {
        super(cnf, httpheader);
    }

    /**
     * 获取所有的区域和缓冲区
     *
     * @return
     * @throws Exception
     */
    public ResponseZones getAllZonesAndBuffers() throws Exception {
        return getSingleZoneOrBuffer("");
    }

    /**
     * 获取指定子位置和时间段内存在的所有分区。
     *
     * @param include_removed
     * @param sublocation_uid
     * @param from
     * @param to
     * @return
     * @throws Exception
     */
    public ResponseZones getZonesAndBuffersHistory(Boolean include_removed, String sublocation_uid, Date from, Date to)
        throws Exception {
        String url =
            config.getDomen() + config.getZonesApiGetZonesAndBuffersHistory() + "include_removed=" + include_removed + "&sublocation_uid=" + sublocation_uid + "&from=" + from.getTime() + "&to=" + to.getTime();
        HttpParam hp = new HttpParam();
        hp.setUrl(url);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.GET);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseZones.class);
    }

    /**
     * 获取具有指定区域UID的单个区域
     *
     * @param uid
     * @return
     * @throws Exception
     */
    public ResponseZones getSingleZoneOrBuffer(String uid) throws Exception {
        String url = config.getDomen();
        if (!Strings.isNullOrEmpty(uid)) {
            url += config.getZonesApiGetSingleZoneOrBuffer() + uid;
        } else {
            url += config.getZonesApiGetAllZonesAndBuffers();
        }
        HttpParam hp = new HttpParam();
        hp.setUrl(url);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.GET);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseZones.class);
    }

    /**
     * 创建一个新的多边形区域。
     *
     * @param req
     * @return
     * @throws Exception
     */
    public ZonesItem createZone(RequestZones req) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getZonesApiCreateZone());
        hp.setHeader(header);
        hp.setPostParam(JSONUtil.getObjectToString(req));
        hp.setMethod(HttpMethod.POST);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ZonesItem.class);
    }

    /**
     * 创建一个新的缓冲区，或者一个附加到指定可跟踪对象并随其移动的圆形区域。
     *
     * @param req
     * @return
     * @throws Exception
     */
    public ZonesItem createBuffer(RequestZones req) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getZonesApiCreateBuffer());
        hp.setHeader(header);
        hp.setPostParam(JSONUtil.getObjectToString(req));
        hp.setMethod(HttpMethod.POST);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ZonesItem.class);
    }

    /**
     * 使用指定的区域UID编辑区域
     *
     * @param req
     * @param uid
     * @return
     * @throws Exception
     */
    public ZonesItem editZoneOrBuffer(RequestZones req, String uid) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getZonesApiEditZoneOrBuffer() + uid);
        hp.setHeader(header);
        hp.setPostParam(JSONUtil.getObjectToString(req));
        hp.setMethod(HttpMethod.PUT);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ZonesItem.class);
    }

    /**
     * 删除指定分区UID的分区
     *
     * @param uid
     * @return
     * @throws Exception
     */
    public String deleteZoneOrBuffer(String uid) throws Exception {

        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getZonesApiDeleteZoneOrBuffer() + uid);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.DELETE);
        String msg = HttpClient.ExecApi(hp);
        return msg;
    }

    /**
     * 获取现有区域类型的列表。
     *
     * @return
     * @throws Exception
     */
    public ResponseZonesTypes getZoneTypes() throws Exception {
        String url = config.getDomen() + config.getZonesApiGetZoneTypes();
        HttpParam hp = new HttpParam();
        hp.setUrl(url);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.GET);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseZonesTypes.class);
    }

    /**
     * 创建多层缓冲区，具有多个定义的半径，以创建不同的地理围栏层。
     *
     * @param req
     * @return
     * @throws Exception
     */
    public ResponseMultiTieredBuffer createMultiTieredBuffer(RequestMultiTieredBuffer req) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getZonesApiCreateMultiTieredBuffer());
        hp.setHeader(header);
        hp.setPostParam(JSONUtil.getObjectToString(req));
        hp.setMethod(HttpMethod.POST);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseMultiTieredBuffer.class);
    }

    /**
     * 使用与其关联的可跟踪对象的UID删除多层缓冲区。
     *
     * @param uid
     * @return
     * @throws Exception
     */
    public String deleteMultiTieredBuffer(String uid) throws Exception {

        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getZonesApiDeleteMultiTieredBuffer() + uid);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.DELETE);
        String msg = HttpClient.ExecApi(hp);
        return msg;
    }

}
