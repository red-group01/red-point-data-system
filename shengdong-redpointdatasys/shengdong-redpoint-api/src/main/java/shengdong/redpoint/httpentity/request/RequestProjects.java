package shengdong.redpoint.httpentity.request;

import lombok.Data;

@Data
public class RequestProjects{
	private Project project;
}