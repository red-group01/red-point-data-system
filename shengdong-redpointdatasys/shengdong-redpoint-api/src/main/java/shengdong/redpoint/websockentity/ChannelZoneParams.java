package shengdong.redpoint.websockentity;

import lombok.Data;

@Data
public class ChannelZoneParams {
    private ChannelZoneData data;
    private String subscription;
}