package shengdong.common.constants;

public final class CommonConstants {
    /**
     * db更新时 程序自动更新标志
     */
    public static String AutomaticProgramUpdateFlag = "auto";
    /**
     *
     */
    public static String TimeUnassigned = "9999-12-31T23:59:59.000Z";

}
