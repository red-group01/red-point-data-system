package shengdong.data.service.impl;

import com.google.common.base.Joiner;
import com.google.common.base.Predicate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;
import shengdong.common.constants.CommonConstants;
import shengdong.common.util.CommonUtil;
import shengdong.data.dao.*;
import shengdong.data.entity.*;
import shengdong.data.service.IBaseInfoService;
import shengdong.redpoint.api.*;
import shengdong.redpoint.httpentity.response.*;
import shengdong.redpoint.model.query.Header;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

@Slf4j
@Service
public class BaseInfoServiceImpl implements IBaseInfoService {
    @Autowired
    IBt14UserDao user;
    String email = "";
    String pwd = "";

    @Override
    public boolean synAll() throws Exception {
        List<Bt14User> ulist = user.getAll();
        for (Bt14User i : ulist) {
            IotApiConfig cnf = new IotApiConfig();
            cnf.setServerIp(i.getServerCnf().getServerIp());
            email = i.getEmail();// "yao.jianwen@as-bang.com";
            pwd = i.getPassword();// "Redp0int";
            //登录不成功则不执行其他同步操作
            if (synUser(cnf, i)) {
                Header h = new Header();
                h.setEmail(i.getEmail());
                h.setToken(i.getToken());
                //同步项目
                synProject(cnf, h, i);

                List<Bt31Project> proj_db = daoproj.getUserProj(i.getId());
                proj_db.forEach(new Consumer<Bt31Project>() {
                    @Override
                    public void accept(Bt31Project item) {
                        h.setProject(item.getProjectUid());
                        try {
                            //同步站点
                            boolean flag = synLocation(cnf, h, i);
                            if (flag) {
                                //同步子站点
                                flag = synSublocation(cnf, h, i);
                                if (flag) {
                                    //同步楼层
                                    //synFloor(cnf, h, i);
                                    //同步节点
                                    synNode(cnf, h, i);
                                    //同步锚
                                    synAnchor(cnf, h, i);
                                    //同步组
                                    //synGroup(cnf, h, i);
                                    //同步标签
                                    synTrackable(cnf, h, i);
                                    //同步区域和缓冲区
                                    synZone(cnf, h, i);
                                    //获取通知配置
                                    synNotifications(cnf, h, i);
                                }
                            }
                        } catch (Exception e) {
                            throw new RuntimeException(e);
                        }
                    }
                });
            } else {
                i.setToken("");
                daouser.updateUser(i);
            }
        }
        return true;
    }

    @Autowired
    IBt14UserDao daouser;
    @Autowired
    IBt11CompanyDao daocompany;
    @Autowired
    IBt13RoleDao daorole;

    /**
     * 同步所有账户、公司、角色数据
     *
     * @param cnf
     * @param user
     * @return
     */
    public boolean synUser(IotApiConfig cnf, Bt14User user) throws Exception {
        boolean result = false;
        IotLogin login = new IotLogin(cnf);
        ResponseLogin rl = login.login(email, pwd);
        //登录失败则退出
        if (!rl.isSuccess()) {
            return result;
        }
        user.setToken(rl.getData().getToken());
        //同步公司信息，没有则创建，有则更新&& !Strings.isNullOrEmpty(rl.getData().getCompany().getUid())
        if (rl.getData().getCompany() != null) {
            user.setCompanyUid(rl.getData().getCompany().getUid());
            Bt11Company ent_company = daocompany.getCompanyByUid(rl.getData().getCompany().getUid());
            if (ent_company == null) {
                ent_company = new Bt11Company();
                ent_company.setId(rl.getData().getCompany().getId());
                ent_company.setCompanyUid(rl.getData().getCompany().getUid());
                ent_company.setName(rl.getData().getCompany().getName());
                ent_company.setCreatedAt(new Date());
                ent_company.setCreatorAt(CommonConstants.AutomaticProgramUpdateFlag);
                user.setCompanyUid(ent_company.getCompanyUid());
                daocompany.insert(ent_company);
            } else {
                ent_company.setName(rl.getData().getCompany().getName());
                ent_company.setUpdatedAt(new Date());
                ent_company.setCreatorAt(CommonConstants.AutomaticProgramUpdateFlag);
                daocompany.update(ent_company);
            }

            //同步角色信息，没有则创建，有则更新
            if (rl.getData().getCompany().getRole() != null) {
                user.setCompanyUid(rl.getData().getCompany().getUid());
                Bt13Role ent_role = daorole.getRoleByUid(rl.getData().getCompany().getUid());
                if (ent_role == null) {
                    ent_role = new Bt13Role();
                    ent_role.setRoleUid(rl.getData().getCompany().getUid());
                    ent_role.setName(rl.getData().getCompany().getName());
                    ent_role.setCreatedAt(new Date());
                    user.setRoleUid(ent_role.getRoleUid());
                    ent_role.setCreatorAt(CommonConstants.AutomaticProgramUpdateFlag);
                    daorole.insert(ent_role);
                } else {
                    ent_role.setName(rl.getData().getCompany().getName());
                    ent_role.setUpdatedAt(new Date());
                    ent_role.setCreatorAt(CommonConstants.AutomaticProgramUpdateFlag);
                    daorole.update(ent_role);
                }
            }
        }
        user.setCreatorAt(CommonConstants.AutomaticProgramUpdateFlag);
        daouser.updateUser(user);
        result = true;
        return result;
    }

    @Autowired
    IBt21NodeDao daonode;

    /**
     * 同步所有节点
     *
     * @param cnf
     * @param h
     * @param user
     * @return
     */
    public boolean synNode(IotApiConfig cnf, Header h, Bt14User user) throws Exception {
        boolean result = false;
        IotNode node = new IotNode(cnf, h);
        StopWatch watch = new StopWatch();
        watch.start();
        List<NodesItem> node_web = node.getNodes().getNodes();
        watch.stop();
        log.info("node data accept:" + node_web.stream().count() + " piece,elapsed time:" + CommonUtil.msFormat(watch.getTotalTimeMillis()));
        if (node_web == null) {
            result = true;
            return result;
        }
        watch.start();
        List<Bt21Node> node_db = daonode.getUserNode(user.getId());
        //数据库中存在但是接口里没有则标记为已删除
        node_db.stream().forEach(new Consumer<Bt21Node>() {
            @Override
            public void accept(Bt21Node local) {
                NodesItem web = node_web.stream().filter(new Predicate<NodesItem>() {
                    @Override
                    public boolean apply(NodesItem input) {
                        return input.getUid().equals(local.getNodeUid());
                    }
                }).findFirst().orElse(null);
                //没在接口数据找到
                if (web == null) {
                    local.setDeleteMark(true);
                    local.setUpdatedAt(new Date());
                    daonode.updateByid(local);
                }
            }
        });
        //数据库已与接口返回的进行对比并修改到数据库
        node_db.stream().forEach(new Consumer<Bt21Node>() {
            @Override
            public void accept(Bt21Node local) {
                NodesItem web = node_web.stream().filter(new Predicate<NodesItem>() {
                    @Override
                    public boolean apply(NodesItem input) {
                        return input.getUid().equals(local.getNodeUid());
                    }
                }).findFirst().orElse(null);
                if (web != null) {
                    editNode(web, local);

                    daonode.updateByid(local);
                }
            }
        });
        //数据库不存在的进行插入
        node_web.stream().forEach(new Consumer<NodesItem>() {
            @Override
            public void accept(NodesItem web) {
                Bt21Node localent = node_db.stream().filter(new Predicate<Bt21Node>() {
                    @Override
                    public boolean apply(Bt21Node input) {
                        return web.getUid().equals(input.getNodeUid());
                    }
                }).findFirst().orElse(null);
                //本地没找到
                if (localent == null) {
                    Bt21Node local = new Bt21Node();

                    editNode(web, local);
                    local.setNodeId(web.getNodeId());

                    if (!Objects.isNull(user.getId())) {
                        local.setUserId(user.getId());
                    }
                    local.setCreatorAt(CommonConstants.AutomaticProgramUpdateFlag);
                    local.setCreatedAt(new Date());
                    //插入数据库
                    daonode.insert(local);
                }
            }
        });
        watch.stop();
        log.info("node data set:" + node_web.stream().count() + " piece,elapsed time:" + CommonUtil.msFormat(watch.getTotalTimeMillis()));
        result = true;
        return result;
    }

    /**
     * 编辑节点对象属性
     *
     * @param web
     * @param local
     */
    private void editNode(NodesItem web, Bt21Node local) {
        local.setHwInfo(web.getHwInfo().getHw());
        local.setAnnounceFlags(web.getAnnounce().getFlags());
        local.setAnnounceTs(web.getAnnounce().getTs());
        local.setAnnounceLastTs(web.getAnnounce().getLastTs());
        local.setNodeId(web.getNodeId());
        if (web.getBattery() != null) {
            local.setBatteryMv(web.getBattery().getMv());
            local.setBatteryPr(web.getBattery().getPr());
        }
        local.setMacAddress(web.getMacAddress());
        local.setBridgeMacAddress(web.getBridgeMacAddress());
        local.setCompanyUid(web.getCompanyUid());
        local.setLastHeard(web.getLastHeard());
        local.setParentMacAddress(web.getParentMacAddress());
        local.setProjectUid(web.getProjectUid());
        if (web.getStatus() != null) {
            local.setStatusExtPow(web.getStatus().getExtPow().toString());
            local.setStatusSync(web.getStatus().getSync().toString());
            local.setStatusTdoa(web.getStatus().getTdoa().toString());
            local.setStatusLowBat(web.getStatus().getLowBat().toString());
        }
        local.setSwVersion(web.getSwVersion().getGit());
        local.setType(web.getNodeType());
        if (web.getUwbTrxConfig() != null) {
            local.setUwbTrxConfig(web.getUwbTrxConfig().toString());
        }
        local.setNodeUid(web.getUid());
        local.setName(web.getName());
        local.setUpdatedAt(new Date());
        local.setDeleteMark(false);
    }

    @Autowired
    IBt22AnchorDao daoanchor;

    /**
     * 同步所有锚
     *
     * @param cnf
     * @param h
     * @param user
     * @return
     */
    public boolean synAnchor(IotApiConfig cnf, Header h, Bt14User user) throws Exception {
        boolean result = false;
        IotWinAnchors anchor = new IotWinAnchors(cnf, h);
        StopWatch watch = new StopWatch();
        watch.start();
        List<WinAnchorsItem> anchor_web = anchor.getWinAnchors().getWinAnchors();
        watch.stop();
        log.info("anchor data accept:" + anchor_web.stream().count() + " piece,elapsed time:" + CommonUtil.msFormat(watch.getTotalTimeMillis()));
        if (anchor_web == null) {
            result = true;
            return result;
        }
        watch.start();
        List<Bt22Anchor> anchor_db = daoanchor.getUserAnchor(user.getId());
        //数据库中存在但是接口里没有则标记为已删除
        anchor_db.stream().forEach(new Consumer<Bt22Anchor>() {
            @Override
            public void accept(Bt22Anchor local) {
                WinAnchorsItem web = anchor_web.stream().filter(new Predicate<WinAnchorsItem>() {
                    @Override
                    public boolean apply(WinAnchorsItem input) {
                        return input.getUid().equals(local.getAnchorUid());
                    }
                }).findFirst().orElse(null);
                //没在接口数据找到
                if (web == null) {
                    local.setDeleteMark(true);
                    local.setUpdatedAt(new Date());
                    daoanchor.updateByid(local);
                }
            }
        });
        //数据库已与接口返回的进行对比并修改到数据库
        anchor_db.stream().forEach(new Consumer<Bt22Anchor>() {
            @Override
            public void accept(Bt22Anchor local) {
                WinAnchorsItem web = anchor_web.stream().filter(new Predicate<WinAnchorsItem>() {
                    @Override
                    public boolean apply(WinAnchorsItem input) {
                        return input.getUid().equals(local.getAnchorUid());
                    }
                }).findFirst().orElse(null);
                if (web != null) {
                    editAnchor(web, local);
                    daoanchor.updateByid(local);
                }
            }
        });
        //数据库不存在的进行插入
        anchor_web.stream().forEach(new Consumer<WinAnchorsItem>() {
            @Override
            public void accept(WinAnchorsItem web) {
                Bt22Anchor localent = anchor_db.stream().filter(new Predicate<Bt22Anchor>() {
                    @Override
                    public boolean apply(Bt22Anchor input) {
                        return web.getUid().equals(input.getAnchorUid());
                    }
                }).findFirst().orElse(null);
                //本地没找到
                if (localent == null) {
                    Bt22Anchor local = new Bt22Anchor();
                    editAnchor(web, local);

                    local.setAnchorUid(web.getUid());

                    if (!Objects.isNull(user.getId())) {
                        local.setUserId(user.getId());
                    }
                    local.setCreatorAt(CommonConstants.AutomaticProgramUpdateFlag);
                    local.setCreatedAt(new Date());
                    //插入数据库
                    daoanchor.insert(local);
                }
            }
        });
        watch.stop();
        log.info("anchor data set:" + anchor_web.stream().count() + " piece,elapsed time:" + CommonUtil.msFormat(watch.getTotalTimeMillis()));
        result = true;
        return result;
    }

    /**
     * 锚属性编辑
     *
     * @param web
     * @param local
     */
    private void editAnchor(WinAnchorsItem web, Bt22Anchor local) {
        local.setLocked(web.isLocked());
        local.setType(web.getType());
        local.setCompanyUid(web.getCompanyUid());
        if (web.getNode() != null) {
            local.setNodeId(web.getNode().getNodeId());
            local.setMacAddress(web.getNode().getMacAddress());
        }
        if (web.getPosition() != null) {
            local.setPositionX(web.getPosition().getX());
            local.setPositionY(web.getPosition().getY());
            local.setPositionZ(web.getPosition().getZ());
        }
        local.setProjectUid(web.getProjectUid());
        local.setSublocationUid(web.getSublocationUid());
        local.setName(web.getName());
        local.setUpdatedAt(new Date());
        local.setDeleteMark(false);
    }

    @Autowired
    IBt42NotificationsDao daoNotification;

    public boolean synNotifications(IotApiConfig cnf, Header h, Bt14User user) throws Exception {
        boolean result = false;
        IotNotificationsAndSubscribersEndpoints notification = new IotNotificationsAndSubscribersEndpoints(cnf, h);
        List<NotificationsItem> notification_web = notification.getNotifications().getNotifications();
        if (notification_web == null) {
            result = true;
            return result;
        }
        List<Bt42Notifications> notification_db = daoNotification.getUserNotificationByUserId(user.getId());
        //数据库中存在但是接口里没有则标记为已删除
        notification_db.stream().forEach(local -> {
            NotificationsItem web = notification_web.stream().filter(new Predicate<>() {
                @Override
                public boolean apply(NotificationsItem input) {
                    return input.getUid().equals(local.getNotificationsUid());
                }
            }).findFirst().orElse(null);
            //没在接口数据找到
            if (web == null) {
                local.setDeleteMark(true);
                local.setUpdatedAt(new Date());
                daoNotification.updateByid(local);
            }
        });
        //数据库已与接口返回的进行对比并修改到数据库
        notification_db.stream().forEach(local -> {
            NotificationsItem web = notification_web.stream().filter(new Predicate<>() {
                @Override
                public boolean apply(NotificationsItem input) {
                    return input.getUid().equals(local.getNotificationsUid());
                }
            }).findFirst().orElse(null);
            if (web != null) {
                editNotification(web, local);
                daoNotification.updateByid(local);
            }
        });
        //数据库不存在的进行插入
        notification_web.stream().forEach(web -> {
            Bt42Notifications localent = notification_db.stream()
                    .filter((Predicate<Bt42Notifications>) input -> web.getUid().equals(input.getNotificationsUid()))
                    .findFirst().orElse(null);
            //本地没找到
            if (localent == null) {
                Bt42Notifications local = new Bt42Notifications();

                editNotification(web, local);

                local.setNotificationsUid(web.getUid());

                if (!Objects.isNull(user.getId())) {
                    local.setUserId(user.getId());
                }
                local.setCreatorAt(CommonConstants.AutomaticProgramUpdateFlag);
                local.setCreatedAt(new Date());
                //插入数据库
                daoNotification.insert(local);
            }
        });

        result = true;
        return result;
    }

    /**
     * 编辑通知对象属性
     *
     * @param web
     * @param local
     * @return
     */
    Bt42Notifications editNotification(NotificationsItem web, Bt42Notifications local) {
        local.setNotificationsId(web.getId());
        local.setActive(web.getActive());
        local.setCreatedAt(CommonUtil.fromUtcDate(web.getCreatedAt()));
        local.setCreatorAt(CommonConstants.AutomaticProgramUpdateFlag);
        local.setName(web.getName());
        local.setProjectUid(web.getProjectUid());
        //                    ent.setAlarmTypeList(item.getTypes());
        local.setTypeEmail(String.valueOf(web.getTypes().isEmail()));
        local.setTypePhone(String.valueOf(web.getTypes().isPhone()));
        local.setZoneSeverityLevel(web.getConditions().getZoneSeverityLevel());
        //        local.setNotificationsUid(web.getUid());
        local.setDeleteMark(false);
        return local;
    }

    @Autowired
    IBt23TrackableDao daotrackable;

    /**
     * 同步所有标签
     *
     * @param cnf
     * @param h
     * @param user
     * @return
     */
    public boolean synTrackable(IotApiConfig cnf, Header h, Bt14User user) throws Exception {
        boolean result = false;
        IotTrackbale trackbale = new IotTrackbale(cnf, h);
        StopWatch watch = new StopWatch();
        watch.start();
        List<TrackableObjectsItem> trackbale_web = trackbale.getTrackables().getTrackableObjects();
        watch.stop();
        log.info("trackbale data accept:" + trackbale_web.stream().count() + " piece,elapsed time:" + CommonUtil.msFormat(watch.getTotalTimeMillis()));
        if (trackbale_web == null) {
            result = true;
            return result;
        }
        watch.start();
        List<Bt23Trackable> trackbale_db = daotrackable.getUserTrackbale(user.getId());
        //数据库中存在但是接口里没有则标记为已删除
        trackbale_db.stream().forEach(new Consumer<Bt23Trackable>() {
            @Override
            public void accept(Bt23Trackable local) {
                TrackableObjectsItem web = trackbale_web.stream().filter(new Predicate<TrackableObjectsItem>() {
                    @Override
                    public boolean apply(TrackableObjectsItem input) {
                        return input.getUid().equals(local.getTrackableUid());
                    }
                }).findFirst().orElse(null);
                //没在接口数据找到
                if (web == null) {
                    local.setDeleteMark(true);
                    local.setUpdatedAt(new Date());
                    daotrackable.updateByid(local);
                }
            }
        });
        //数据库已与接口返回的进行对比并修改到数据库
        trackbale_db.stream().forEach(new Consumer<Bt23Trackable>() {
            @Override
            public void accept(Bt23Trackable local) {
                TrackableObjectsItem web = trackbale_web.stream().filter(new Predicate<TrackableObjectsItem>() {
                    @Override
                    public boolean apply(TrackableObjectsItem input) {
                        return input.getUid().equals(local.getTrackableUid());
                    }
                }).findFirst().orElse(null);
                if (web != null) {
                    editTrackable(web, local);
                    daotrackable.updateByid(local);
                }
            }
        });
        //数据库不存在的进行插入
        trackbale_web.stream().forEach(new Consumer<TrackableObjectsItem>() {
            @Override
            public void accept(TrackableObjectsItem web) {
                Bt23Trackable localent = trackbale_db.stream().filter(new Predicate<Bt23Trackable>() {
                    @Override
                    public boolean apply(Bt23Trackable input) {
                        return web.getUid().equals(input.getTrackableUid());
                    }
                }).findFirst().orElse(null);
                //本地没找到
                if (localent == null) {
                    Bt23Trackable local = new Bt23Trackable();

                    editTrackable(web, local);

                    local.setTrackableUid(web.getUid());

                    if (!Objects.isNull(user.getId())) {
                        local.setUserId(user.getId());
                    }
                    local.setCreatorAt(CommonConstants.AutomaticProgramUpdateFlag);
                    local.setCreatedAt(new Date());
                    //插入数据库
                    daotrackable.insert(local);
                }
            }
        });
        watch.stop();
        log.info("trackbale data set:" + trackbale_web.stream().count() + " piece,elapsed time:" + CommonUtil.msFormat(watch.getTotalTimeMillis()));
        result = true;
        return result;
    }

    /**
     * 标签属性编辑
     *
     * @param web
     * @param local
     */
    private void editTrackable(TrackableObjectsItem web, Bt23Trackable local) {
        local.setCategory(web.getCategory());
        local.setAccConfig(web.getAccConfig());
        local.setType(web.getType());
        local.setBtConfig(web.getBtConfig());
        local.setCompanyUid(web.getCompanyUid());
        local.setDefaultHeight(web.getDefaultHeight());
        local.setDisplayName(web.getDisplayName());
        //        if (web.getGroups() != null) {
        //            local.setGrouplist(Joiner.on(',').join(web.getGroups()));
        //        }
        local.setIdle(web.getNode().isIdle());
        local.setMacAddress(web.getNode().getMacAddress());
        local.setNodeId(web.getNode().getNodeId());
        local.setResponsive(web.getNode().isResponsive());
        local.setSublocationUid(web.getPosition().getSlUid());
        local.setPositionUpdateInterval(String.valueOf(web.getPositionUpdateInterval()));
        local.setProjectUid(web.getProjectUid());
        local.setRemovedAt(CommonUtil.fromUtcDate(web.getRemovedAt()));
        local.setSelfZoneUid(web.getSelfZoneUid());
        if (web.getSensorConfig() != null) {
            local.setSensorConfig(String.valueOf(web.getSensorConfig().getSensors().stream().count()));
        }
        local.setTagConfig(web.getTagConfig().getStationary().toString());
        local.setX(web.getPosition().getX());
        local.setY(web.getPosition().getY());
        local.setZ(web.getPosition().getZ());
        local.setName(web.getName());
        local.setUpdatedAt(new Date());
        local.setDeleteMark(false);
    }

    @Autowired
    IBt31ProjectDao daoproj;

    /**
     * 同步所有项目
     *
     * @return
     */

    public boolean synProject(IotApiConfig cnf, Header h, Bt14User user) throws Exception {
        boolean result = false;
        IotProject proj = new IotProject(cnf, h);
        StopWatch watch = new StopWatch();
        watch.start();
        List<ProjectsItem> proj_web = proj.getProjects().getProjects();
        watch.stop();
        log.info(
                "project data accept:" + proj_web.stream().count() + " piece,elapsed time:" + CommonUtil.msFormat(watch.getTotalTimeMillis()));
        if (proj_web == null) {
            result = true;
            return result;
        }
        watch.start();
        List<Bt31Project> proj_db = daoproj.getUserProj(user.getId());
        //数据库中存在但是接口里没有则标记为已删除
        proj_db.stream().forEach(new Consumer<Bt31Project>() {
            @Override
            public void accept(Bt31Project local) {
                ProjectsItem web = proj_web.stream().filter(new Predicate<ProjectsItem>() {
                    @Override
                    public boolean apply(ProjectsItem input) {
                        return input.getUid().equals(local.getProjectUid());
                    }
                }).findFirst().orElse(null);
                //没在接口数据找到
                if (web == null) {
                    local.setDeleteMark(true);
                    local.setUpdatedAt(new Date());
                    daoproj.updateByid(local);
                }
            }
        });
        //数据库已与接口返回的进行对比并修改到数据库
        proj_db.stream().forEach(new Consumer<Bt31Project>() {
            @Override
            public void accept(Bt31Project local) {
                ProjectsItem web = proj_web.stream().filter(new Predicate<ProjectsItem>() {
                    @Override
                    public boolean apply(ProjectsItem input) {
                        return input.getUid().equals(local.getProjectUid());
                    }
                }).findFirst().orElse(null);
                if (web != null) {
                    local.setManager(web.getUserParams().getManager());
                    local.setAddress(web.getUserParams().getAddress());
                    local.setTimezone(web.getUserParams().getTimezone());
                    local.setZoneVerticesLimit(web.getSettings().getZoneVerticesLimit());
                    local.setName(web.getName());
                    local.setUpdatedAt(new Date());
                    local.setDeleteMark(false);
                    daoproj.updateByid(local);
                }
            }
        });
        //数据库不存在的进行插入
        proj_web.stream().forEach(new Consumer<ProjectsItem>() {
            @Override
            public void accept(ProjectsItem web) {
                Bt31Project localent = proj_db.stream().filter(new Predicate<Bt31Project>() {
                    @Override
                    public boolean apply(Bt31Project input) {
                        return web.getUid().equals(input.getProjectUid());
                    }
                }).findFirst().orElse(null);
                //本地没找到
                if (localent == null) {
                    Bt31Project local = new Bt31Project();
                    local.setProjectUid(web.getUid());

                    if (!Objects.isNull(user.getId())) {
                        local.setUserId(user.getId());
                    }

                    local.setCreatorAt(CommonConstants.AutomaticProgramUpdateFlag);
                    local.setName(web.getName());
                    local.setManager(web.getUserParams().getManager());
                    local.setAddress(web.getUserParams().getAddress());
                    local.setTimezone(web.getUserParams().getTimezone());
                    local.setZoneVerticesLimit(web.getSettings().getZoneVerticesLimit());
                    local.setCreatedAt(new Date());
                    local.setProjectId(web.getId());
                    //插入数据库
                    daoproj.insert(local);
                }
            }
        });
        watch.stop();
        log.info(
                "project data set:" + proj_web.stream().count() + " piece,elapsed time:" + CommonUtil.msFormat(watch.getTotalTimeMillis()));
        result = true;
        return result;
    }

    @Autowired
    IBt32LocationDao daolocation;

    /**
     * 同步所有站点（基站）
     *
     * @return
     */
    public boolean synLocation(IotApiConfig cnf, Header h, Bt14User user) throws Exception {
        boolean result = false;
        IotLocation location = new IotLocation(cnf, h);
        StopWatch watch = new StopWatch();
        watch.start();
        //取得用户全部站点列表
        List<LocationsItem> location_web = location.getSites().getLocations();
        watch.stop();
        log.info("location data accept:" + location_web.stream().count() + " piece,elapsed time:" + CommonUtil.msFormat(watch.getTotalTimeMillis()));
        if (location_web == null) {
            result = false;
            return result;
        }
        watch.start();
        List<Bt32Location> location_db = daolocation.getLocationByUserId(user.getId());
        //数据库中存在但是接口里没有则标记为已删除
        location_db.stream().forEach(new Consumer<Bt32Location>() {
            @Override
            public void accept(Bt32Location local) {
                LocationsItem web = location_web.stream().filter(new Predicate<LocationsItem>() {
                    @Override
                    public boolean apply(LocationsItem input) {
                        return input.getUid().equals(local.getLocationUid());
                    }
                }).findFirst().orElse(null);
                //没在接口数据找到
                if (web == null) {
                    local.setDeleteMark(true);
                    local.setUpdatedAt(new Date());
                    daolocation.updateByid(local);
                }
            }
        });
        //数据库已与接口返回的进行对比并修改到数据库
        location_db.stream().forEach(new Consumer<Bt32Location>() {
            @Override
            public void accept(Bt32Location local) {
                LocationsItem web = location_web.stream().filter(new Predicate<LocationsItem>() {
                    @Override
                    public boolean apply(LocationsItem input) {
                        return input.getUid().equals(local.getLocationUid());
                    }
                }).findFirst().orElse(null);
                if (web != null) {
                    editLocation(web, local, user);
                    daolocation.updateByid(local);
                }
            }
        });
        //数据库不存在的进行插入
        location_web.stream().forEach(new Consumer<LocationsItem>() {
            @Override
            public void accept(LocationsItem web) {
                Bt32Location localent = location_db.stream().filter(new Predicate<Bt32Location>() {
                    @Override
                    public boolean apply(Bt32Location input) {
                        return web.getUid().equals(input.getLocationUid());
                    }
                }).findFirst().orElse(null);
                //本地没找到
                if (localent == null) {
                    Bt32Location local = new Bt32Location();
                    editLocation(web, local, user);
                    if (!Objects.isNull(user.getId())) {
                        local.setUserId(user.getId());
                    }
                    local.setCreatedAt(new Date());
                    local.setCreatorAt(CommonConstants.AutomaticProgramUpdateFlag);
                    //插入数据库
                    daolocation.insert(local);
                }
            }
        });
        watch.stop();
        log.info("location data set:" + location_web.stream().count() + " piece,elapsed time:" + CommonUtil.msFormat(watch.getTotalTimeMillis()));
        result = true;
        return result;
    }

    /**
     * 站点属性编辑
     *
     * @param web
     * @param local
     * @param user
     */
    private void editLocation(LocationsItem web, Bt32Location local, Bt14User user) {
        local.setAddress(web.getStreetAddress());
        local.setAddress2(web.getStreetAddress2());
        local.setCity(web.getCity());
        local.setCountry(web.getCountry());
        local.setDescription(web.getDescription());
        local.setCompanyUid(user.getCompanyUid());
        local.setElevation(web.getElevation());
        local.setLongitude(web.getLongitude());
        local.setLatitude(web.getLatitude());
        local.setName(web.getName());
        local.setOrientation(web.getOrientation());
        local.setProjectUid(web.getProjectUid());
        local.setState(web.getState());
        local.setUpdatedAt(new Date());
        local.setZip(web.getZip());
        local.setZip4(web.getZip4());
        local.setLocationUid(web.getUid());
        local.setDeleteMark(false);
    }

    @Autowired
    IBt33SubLocationDao daosublocation;

    /**
     * 同步子站点
     *
     * @return
     */

    public boolean synSublocation(IotApiConfig cnf, Header h, Bt14User user) throws Exception {
        boolean result = false;
        IotSubLocation sublocation = new IotSubLocation(cnf, h);
        StopWatch watch = new StopWatch();
        watch.start();
        List<SublocationsItem> sublocation_web = sublocation.getArea().getSublocations();
        watch.stop();
        log.info("sublocation data accept:" + sublocation_web.stream().count() + " piece,elapsed time:" + CommonUtil.msFormat(watch.getTotalTimeMillis()));
        if (sublocation_web == null) {
            result = false;
            return result;
        }
        watch.start();
        List<Bt33SubLocation> sublocation_db = daosublocation.getUserSubLocation(user.getId());
        //数据库中存在但是接口里没有则标记为已删除
        sublocation_db.stream().forEach(new Consumer<Bt33SubLocation>() {
            @Override
            public void accept(Bt33SubLocation local) {
                SublocationsItem web = sublocation_web.stream().filter(new Predicate<SublocationsItem>() {
                    @Override
                    public boolean apply(SublocationsItem input) {
                        return input.getUid().equals(local.getSublocationUid());
                    }
                }).findFirst().orElse(null);
                //没在接口数据找到
                if (web == null) {
                    local.setDeleteMark(true);
                    local.setUpdatedAt(new Date());
                    daosublocation.updateByid(local);
                }
            }
        });
        //数据库已与接口返回的进行对比并修改到数据库
        sublocation_db.stream().forEach(new Consumer<Bt33SubLocation>() {
            @Override
            public void accept(Bt33SubLocation local) {
                SublocationsItem web = sublocation_web.stream().filter(new Predicate<SublocationsItem>() {
                    @Override
                    public boolean apply(SublocationsItem input) {
                        return input.getUid().equals(local.getSublocationUid());
                    }
                }).findFirst().orElse(null);
                if (web != null) {
                    editSubLocation(web, local);
                    daosublocation.updateByid(local);
                }
            }
        });
        //数据库不存在的进行插入
        sublocation_web.stream().forEach(new Consumer<SublocationsItem>() {
            @Override
            public void accept(SublocationsItem web) {
                Bt33SubLocation localent = sublocation_db.stream().filter(new Predicate<Bt33SubLocation>() {
                    @Override
                    public boolean apply(Bt33SubLocation input) {
                        return web.getUid().equals(input.getSublocationUid());
                    }
                }).findFirst().orElse(null);
                //本地没找到
                if (localent == null) {
                    Bt33SubLocation local = new Bt33SubLocation();
                    editSubLocation(web, local);

                    local.setSublocationId(web.getId());
                    if (!Objects.isNull(user.getId())) {
                        local.setUserId(user.getId());
                    }

                    local.setCreatorAt(CommonConstants.AutomaticProgramUpdateFlag);
                    local.setName(web.getName());
                    local.setCreatedAt(new Date());
                    //插入数据库
                    daosublocation.insert(local);
                }
            }
        });
        watch.stop();
        log.info("sublocation data set:" + sublocation_web.stream().count() + " piece,elapsed time:" + CommonUtil.msFormat(watch.getTotalTimeMillis()));
        result = true;
        return result;
    }

    /**
     * 子站点属性设定
     *
     * @param web
     * @param local
     */
    private void editSubLocation(SublocationsItem web, Bt33SubLocation local) {
        local.setName(web.getName());
        local.setProjectUid(web.getProjectUid());
        local.setCompanyUid(web.getCompanyUid());
        local.setDescription(web.getDescription());
        local.setOffsetX(web.getOffsetX());
        local.setOffsetY(web.getOffsetY());
        local.setOffsetZ(web.getOffsetZ());
        local.setUpdatedAt(new Date());
        local.setSublocationUid(web.getUid());
        local.setLocationUid(web.getLocationUid());
        local.setDeleteMark(false);
    }

    @Autowired
    IBt34ZoneDao daozone;

    /**
     * 同步所有的电子围栏区域和缓冲区
     *
     * @param cnf
     * @param h
     * @param user
     * @return
     */
    public boolean synZone(IotApiConfig cnf, Header h, Bt14User user) throws Exception {
        boolean result = false;
        IotZone zone = new IotZone(cnf, h);
        StopWatch watch = new StopWatch();
        watch.start();
        List<ZonesItem> zone_web = zone.getAllZonesAndBuffers().getZones();
        watch.stop();
        log.info("zone data accept:" + zone_web.stream().count() + " piece,elapsed time:" + CommonUtil.msFormat(watch.getTotalTimeMillis()));
        if (zone_web == null) {
            result = true;
            return result;
        }
        watch.start();
        List<Bt34Zone> zone_db = daozone.getUserZone(user.getId());
        //数据库中存在但是接口里没有则标记为已删除
        zone_db.stream().forEach(new Consumer<Bt34Zone>() {
            @Override
            public void accept(Bt34Zone local) {
                ZonesItem web = zone_web.stream().filter(new Predicate<ZonesItem>() {
                    @Override
                    public boolean apply(ZonesItem input) {
                        return input.getUid().equals(local.getZoneUid());
                    }
                }).findFirst().orElse(null);
                //没在接口数据找到
                if (web == null) {
                    local.setDeleteMark(true);
                    local.setUpdatedAt(new Date());
                    daozone.updateByid(local);
                }
            }
        });
        //数据库已与接口返回的进行对比并修改到数据库
        zone_db.stream().forEach(new Consumer<Bt34Zone>() {
            @Override
            public void accept(Bt34Zone local) {
                ZonesItem web = zone_web.stream().filter(new Predicate<ZonesItem>() {
                    @Override
                    public boolean apply(ZonesItem input) {
                        return input.getUid().equals(local.getZoneUid());
                    }
                }).findFirst().orElse(null);
                if (web != null) {
                    editzone(web, local, user);

                    local.setUpdatedAt(new Date());
                    daozone.updateByid(local);
                }
            }
        });
        //数据库不存在的进行插入
        zone_web.stream().forEach(new Consumer<ZonesItem>() {
            @Override
            public void accept(ZonesItem web) {
                Bt34Zone localent = zone_db.stream().filter(new Predicate<Bt34Zone>() {
                    @Override
                    public boolean apply(Bt34Zone input) {
                        return web.getUid().equals(input.getZoneUid());
                    }
                }).findFirst().orElse(null);
                //本地没找到
                if (localent == null) {
                    Bt34Zone local = new Bt34Zone();

                    editzone(web, local, user);

                    local.setZoneId(web.getZoneId());
                    local.setZoneUid(web.getUid());

                    if (!Objects.isNull(user.getId())) {
                        local.setUserId(user.getId());
                    }
                    local.setCreatorAt(CommonConstants.AutomaticProgramUpdateFlag);
                    local.setCreatedAt(new Date());
                    //插入数据库
                    daozone.insert(local);
                }
            }
        });
        watch.stop();
        log.info("zone data set:" + zone_web.stream().count() + " piece,elapsed time:" + CommonUtil.msFormat(watch.getTotalTimeMillis()));
        result = true;
        return result;
    }

    /**
     * 编辑区域和缓冲区属性
     *
     * @param web
     * @param local
     * @param user
     */
    private void editzone(ZonesItem web, Bt34Zone local, Bt14User user) {
        local.setActive(web.isActive());
        local.setCompanyUid(user.getCompanyUid());
        local.setDefaultAllowed(web.isDefaultAllowed());
        local.setDynamic(web.isDynamic());
        local.setLocationUid(web.getLocationUid());
        local.setName(web.getName());
        local.setProjectUid(web.getProjectUid());
        local.setRemovedAt(CommonUtil.fromUtcDate(web.getRemovedAt()));
        local.setShape(web.getShape());
        local.setRadius(web.getZoneGeometry().getRadius());
        if (web.getZoneGeometry().getPositions() != null) {
            StringBuilder sb = new StringBuilder();
            int last = 0;
            for (PositionsItem item : web.getZoneGeometry().getPositions()) {
                last += 1;
                sb.append(item.getX());
                sb.append(".");
                sb.append(item.getY());
                if (last < web.getZoneGeometry().getPositions().size()) {
                    sb.append(",");
                }
            }
            local.setPositions(sb.toString());
        }
        local.setStartTime(web.getSchedule().getStartTime());
        local.setStopTime(web.getSchedule().getStopTime());
        local.setSublocationId(web.getSublocationId());
        local.setSublocationUid(web.getSublocationUid());
        local.setTrackableObjectUid(web.getTrackableObjectUid());
        local.setZoneAlarmLevel(web.getZoneAlarmLevel());
        if (web.getZoneExceptions() != null) {
            local.setZoneExceptions(Joiner.on(',').join(web.getZoneExceptions()));
        }
        if (web.getZoneExceptionsGroups() != null) {
            local.setZoneExceptionsGroups(Joiner.on(',').join(web.getZoneExceptionsGroups()));
        }
        local.setZoneMode(web.getZoneMode());
        local.setZoneTypeId(web.getZoneTypeId());
        local.setZoneTypeName(web.getZoneTypeName());
        local.setDeleteMark(false);
    }

}
