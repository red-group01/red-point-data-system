package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Position {
    private int ms;
    @JsonProperty("sl_id")
    private String slId;
    private String mac;
    @JsonProperty("dev_id")
    private String devId;
    private boolean inactive;
    @JsonProperty("edge_corr")
    private boolean edgeCorr;
    private int locationId;
    @JsonProperty("sl_uid")
    private String slUid;
    private long x;
    private long y;
    private long sl;
    private long z;
    private long ts;

    @JsonProperty("project_uid")
    private String projectUid;
    private String type;
    @JsonProperty("to_type_id")
    private int toTypeId;
    @JsonProperty("pe_dim")
    private int peDim;
    private int fom;
    @JsonProperty("company_uid")
    private String companyUid;
    private int sfsn;
    @JsonProperty("slvr_mode")
    private String slvrMode;
}