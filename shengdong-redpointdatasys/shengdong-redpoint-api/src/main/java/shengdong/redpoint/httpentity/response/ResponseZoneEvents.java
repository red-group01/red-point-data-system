package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class ResponseZoneEvents {
    @JsonProperty("zone_events")
    private List<ZoneEventsItem> zoneEvents;
}