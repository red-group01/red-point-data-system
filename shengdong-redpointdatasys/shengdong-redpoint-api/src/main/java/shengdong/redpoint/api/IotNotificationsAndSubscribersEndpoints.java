package shengdong.redpoint.api;

import com.google.common.base.Strings;
import shengdong.common.util.JSONUtil;
import shengdong.redpoint.httpentity.request.RequestNotifications;
import shengdong.redpoint.httpentity.request.RequestSubscriber;
import shengdong.redpoint.httpentity.response.NotificationsItem;
import shengdong.redpoint.httpentity.response.ResponseNotifications;
import shengdong.redpoint.httpentity.response.ResponseSubscribers;
import shengdong.redpoint.httpentity.response.SubscribersItem;
import shengdong.redpoint.model.query.Header;
import shengdong.redpoint.model.query.HttpMethod;
import shengdong.redpoint.model.query.HttpParam;

public class IotNotificationsAndSubscribersEndpoints extends IotApiBase {

    public IotNotificationsAndSubscribersEndpoints(IotApiConfig cnf, Header httpheader) {
        super(cnf, httpheader);
    }

    /**
     * 获取项目的所有通知配置文件
     *
     * @return
     * @throws Exception
     */
    public ResponseNotifications getNotifications() throws Exception {
        return getSingleNotification("");
    }

    /**
     * 获取具有指定通知UID的单个通知配置文件
     *
     * @param uid
     * @return
     * @throws Exception
     */
    public ResponseNotifications getSingleNotification(String uid) throws Exception {
        HttpParam hp = new HttpParam();
        String url = config.getDomen();
        if (!Strings.isNullOrEmpty(uid)) {
            url += config.getNotificationsApiGetSingleNotification() + uid;
        } else {
            url += config.getNotificationsApiGetNotifications();
        }
        hp.setUrl(url);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.GET);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseNotifications.class);
    }

    /**
     * 创建一个通知配置文件，用于生成电子邮件或短信通知。
     *
     * @param req
     * @return
     * @throws Exception
     */
    public NotificationsItem createNotification(RequestNotifications req) throws Exception {
        HttpParam hp = new HttpParam();
        String url = config.getDomen() + config.getNotificationsApiCreateNotification();
        hp.setUrl(url);
        hp.setHeader(header);
        hp.setPostParam(JSONUtil.getObjectToString(req));
        hp.setMethod(HttpMethod.POST);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), NotificationsItem.class);
    }

    /**
     * 使用指定的通知UID编辑通知配置文件
     *
     * @param req
     * @param uid
     * @return
     * @throws Exception
     */
    public NotificationsItem editNotification(RequestNotifications req, String uid) throws Exception {
        HttpParam hp = new HttpParam();
        String url = config.getDomen() + config.getNotificationsApiEditNotification() + uid;
        hp.setUrl(url);
        hp.setHeader(header);
        hp.setPostParam(JSONUtil.getObjectToString(req));
        hp.setMethod(HttpMethod.PUT);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), NotificationsItem.class);
    }

    /**
     * 删除具有指定通知UID的通知配置文件
     *
     * @param uid
     * @return
     */
    public String deleteNotification(String uid) throws Exception {
        HttpParam hp = new HttpParam();
        String url = config.getDomen() + config.getNotificationsApiDeleteNotification() + uid;
        hp.setUrl(url);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.DELETE);
        String msg = HttpClient.ExecApi(hp);
        return msg;
    }

    /**
     * 获取与项目关联的所有订阅者
     *
     * @return
     * @throws Exception
     */
    public ResponseSubscribers getSubscribers() throws Exception {
        return getSingleSubscriber("");
    }

    /**
     * 获取具有指定订阅者UID的单个订阅者
     *
     * @param uid
     * @return
     * @throws Exception
     */
    public ResponseSubscribers getSingleSubscriber(String uid) throws Exception {
        HttpParam hp = new HttpParam();
        String url = config.getDomen();
        if (!Strings.isNullOrEmpty(uid)) {
            url += config.getSubscribersApiGetSingleSubscriber() + uid;
        } else {
            url += config.getSubscribersApiGetSubscribers();
        }
        hp.setUrl(url);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.GET);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseSubscribers.class);
    }

    /**
     * 创建一个新的订阅者并将其分配给通知配置文件
     *
     * @param req
     * @return
     * @throws Exception
     */
    public SubscribersItem createSubscriber(RequestSubscriber req) throws Exception {
        HttpParam hp = new HttpParam();
        String url = config.getDomen() + config.getSubscribersApiCreateSubscriber();
        hp.setUrl(url);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.POST);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), SubscribersItem.class);
    }

    /**
     * 编辑具有指定订阅者UID的订阅者
     *
     * @param req
     * @param uid
     * @return
     * @throws Exception
     */
    public SubscribersItem editSubscriber(RequestSubscriber req, String uid) throws Exception {
        HttpParam hp = new HttpParam();
        String url = config.getDomen() + config.getSubscribersApiEditSubscriber() + uid;
        hp.setUrl(url);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.PUT);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), SubscribersItem.class);
    }

    /**
     * 删除指定用户UID的订阅者
     *
     * @param uid
     * @return
     * @throws Exception
     */
    public String deleteSubscriber(String uid) throws Exception {
        HttpParam hp = new HttpParam();
        String url = config.getDomen() + config.getSubscribersApiDeleteSubscriber() + uid;
        hp.setUrl(url);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.DELETE);
        String msg = HttpClient.ExecApi(hp);
        return msg;
    }

}
