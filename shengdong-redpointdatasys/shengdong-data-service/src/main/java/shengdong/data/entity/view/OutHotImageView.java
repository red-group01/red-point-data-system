package shengdong.data.entity.view;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * 用于页面展示的热力图数据实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OutHotImageView {
    private String title;
    private List<HotImageData> nvlist = new ArrayList<>();
}
