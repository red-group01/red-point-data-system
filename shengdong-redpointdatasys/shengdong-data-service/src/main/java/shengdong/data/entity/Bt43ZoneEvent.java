package shengdong.data.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 区域事件实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Bt43ZoneEvent {

    //自增主键
    private Long id;

    //区域事件唯一标识
    private String zoneEventUid;

    //区域围栏唯一标识
    private String zoneUid;

    //触发事件的其他对象uid
    private String objUid;

    //区域事件类别名称
    private String zoneTypeName;

    //区域事件状态（进入区域，离开区域）
    private String action;

    //区域事件发生的位置坐标x
    private Long x;

    //区域事件发生的位置坐标y
    private Long y;

    //区域事件发生的位置坐标z
    private Long z;

    //可跟踪对象在区域中花费的时间(以毫秒为单位)
    private Integer duration;

    //公司唯一标识
    private String companyUid;

    //项目唯一标识
    private String projectUid;

    //项目地点唯一标识
    private String locationUid;

    //The identifier of the area in the context of the server
    private String sublocationId;

    //创建时间（UTC）
    private Date createdAt;

    //更新时间（UTC）
    private Date updatedAt;

    //数据最后更新者
    private String creatorAt;

    //数据信息备注
    private String remark;

}