package shengdong.data.entity.view;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OutNodeStateListView {
    private int serial;
    private String nodeName;
    private String type;
    private String activeTime;
    private String distance;
    private double length;
}
