package shengdong.redpoint.httpentity.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class RequestAnchorBridge {
    @JsonProperty("win_uid")
    private String winUid;
    private String uid;
    @JsonProperty("mac_address")
    private String macAddress;
}