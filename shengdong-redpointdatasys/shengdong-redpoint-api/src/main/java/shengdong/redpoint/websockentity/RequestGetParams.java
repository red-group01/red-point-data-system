package shengdong.redpoint.websockentity;

import lombok.Data;

@Data
public class RequestGetParams {
    private String entity;
}