package shengdong.redpoint.httpentity.request;

import lombok.Data;

@Data
public class UserParams {
    private String color;
    private String address;
    private String timezone;
    private String manager;
}