package shengdong.data.dao;

import shengdong.data.entity.BaseNodeType;
import shengdong.data.entity.Bt22Anchor;

import java.util.List;

/**
 * 锚节点Dao接口
 */
public interface IBt22AnchorDao {

    /**
     * 插入锚节点数据
     *
     * @param record 锚节点实体类
     * @return 插入的行数
     */
    public int insert(Bt22Anchor record);

    int updateByid(Bt22Anchor local);

    List<Bt22Anchor> getUserAnchor(Long id);

    void truncateTable();

    List<Bt22Anchor> getAllAnchor();

    /**
     * 根据地点子区域uid查询锚节点表数据
     *
     * @param sublocationUid 地点子区域uid
     * @return 锚节点实体类
     */
    List<Bt22Anchor> selectBySublocationUid(String sublocationUid);

    /**
     * 根据地点子区域uid查询锚节点表数据的类别分组
     *
     * @param sublocationUid 地点子区域uid
     * @return 标签类别实体类
     */
    List<BaseNodeType> selectBySublocationUidGroupByType(String sublocationUid);
}
