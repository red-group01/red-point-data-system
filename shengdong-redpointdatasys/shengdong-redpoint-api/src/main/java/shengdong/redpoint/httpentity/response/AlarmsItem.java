package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class AlarmsItem {
    @JsonProperty("alarm_name")
    private String alarmName;
    private int acknowledged;
    @JsonProperty("sublocation_uid")
    private String sublocationUid;
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("zone_uid")
    private String zoneUid;
    @JsonProperty("hysteresis_count")
    private int hysteresisCount;
    private String uid;
    @JsonProperty("updated_at")
    private String updatedAt;
    @JsonProperty("additional_info")
    private AdditionalInfo additionalInfo;
    @JsonProperty("mac_address")
    private String macAddress;
    private long x;
    private long y;
    @JsonProperty("obj_uid")
    private String objUid;
    @JsonProperty("acknowledged_by")
    private Object acknowledgedBy;
    private long z;
    private String state;
    @JsonProperty("alarm_type_id")
    private int alarmTypeId;
    private String ts;
}