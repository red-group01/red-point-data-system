package shengdong.redpoint.httpentity.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class RequestZones {
    @JsonProperty("zone_mode")
    private String zoneMode;
    @JsonProperty("sublocation_uid")
    private String sublocationUid;
    private String shape;
    @JsonProperty("zone_geometry")
    private ZoneGeometry zoneGeometry;
    @JsonProperty("zone_points_attributes")
    private List<ZonePointsAttributesItem> zonePointsAttributes;
    private int priority;
    @JsonProperty("zone_alarm_level")
    private int zoneAlarmLevel;
    @JsonProperty("trackable_object_uid")
    private Object trackableObjectUid;
    @JsonProperty("default_allowed")
    private boolean defaultAllowed;
    @JsonProperty("zone_exceptions")
    private List<String> zoneExceptions;
    @JsonProperty("zone_exceptions_groups")
    private List<String> zoneExceptionsGroups;

    private Schedule schedule;
    @JsonProperty("custom_info")
    private String customInfo;
    private String name;
    @JsonProperty("zone_type_name")
    private String zoneTypeName;
    @JsonProperty("zone_type_id")
    private Object zoneTypeId;
    private String dynamic;
}