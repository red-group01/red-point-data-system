package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class ResponseWinAnchorAssociations {
    @JsonProperty("win_anchor_associations")
    private List<WinAnchorAssociationsItem> winAnchorAssociations;
}