package shengdong.redpoint.api;

import com.google.common.base.Strings;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import shengdong.common.util.CommonUtil;
import shengdong.redpoint.httpentity.request.Project;
import shengdong.redpoint.httpentity.request.RequestProjects;
import shengdong.redpoint.httpentity.request.UserParams;
import shengdong.redpoint.httpentity.response.*;
import shengdong.redpoint.model.query.Header;

import java.util.Date;

@SpringBootTest
class ShengdongRedpointApiApplicationTests {
    /**
     * 授权接口测试
     *
     * @throws Exception
     */
    @Test
    void logintest() throws Exception {
        IotApiConfig cnf = new IotApiConfig();
        String email = "yao.jianwen@as-bang.com";
        String pwd = "Redp0int3";
        IotLogin login = new IotLogin(cnf);
        ResponseLogin rl = login.login(email, pwd);

        logtxt("登录结果：" + rl.isSuccess());
        ResponseValidateToken rvt = login.validateToken(email, rl.getData().getToken());
        logtxt("token验证结果：" + rvt.isSuccess());
        logtxt("token刷新前：" + rl.getData().getToken());
        ResponseValidateToken rvt1 = login.refreshToken(email, rl.getData().getToken());
        logtxt("token刷新后：" + rvt1.getData().getToken());
    }

    /**
     * 项目管理接口测试
     *
     * @throws Exception
     */
    @Test
    void projecttest() throws Exception {
        IotApiConfig cnf = new IotApiConfig();
        Header h = gethttpheader(cnf, "");
        IotProject proj = new IotProject(cnf, h);
        ResponseAllProjects allproj = proj.getProjects();
        logtxt("项目数量：" + allproj.getProjects().stream().count());
        logtxt("项目清单：");
        for (ProjectsItem i : allproj.getProjects()) {
            logtxt(i.getName() + "_" + i.getUid());
        }
        RequestProjects rp = new RequestProjects();
        Project p = new Project();
        p.setName("testproj1");
        UserParams up = new UserParams();
        up.setAddress("projaddr");
        up.setTimezone("America/Los_Angeles");
        up.setManager("user_params");
        p.setUserParams(up);
        rp.setProject(p);
        ResponsetProjects rcp = proj.createProject(rp);
        logtxt("项目名称修改前：" + rp.getProject().getName());
        rp.getProject().setName("testproj1-alter");
        rcp = proj.updateProject(rp, rcp.getUid());
        logtxt("项目名称修改前：" + rcp.getName());
        String r = proj.deleteProject(rcp.getUid());
        logtxt("删除结果：" + (Strings.isNullOrEmpty(r) ? "成功" : r));
    }

    @Test
    void t1() throws Exception {
        IotApiConfig cnf = new IotApiConfig();
        String email = "yao.jianwen@as-bang.com";
        String pwd = "Redp0int";
        String token =
            "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHBpcmVzSW4iOjE4MDAsInVzZXIiOnsidWlkIjoiM0x6c2VNMWhSSmFSR2pWSURGcjJvQSIsInJvbGUiOiJjb21wYW55IGFkbWluIiwiZW1haWwiOiJ5YW8uamlhbndlbkBhcy1iYW5nLmNvbSIsInByb2plY3RzIjpbeyJwcm9qZWN0X3VpZCI6IlFqalBHUmNDUmJHekI4ZjhtYWU0UnciLCJwcm9qZWN0X25hbWUiOiJUYWlZdWFuT2ZmaWNlIiwicHJpdiI6MTN9XSwiYXV0aGVudGljYXRpb25fdG9rZW4iOiJleUpoYkdjaU9pSklVekkxTmlJc0luUjVjQ0k2SWtwWFZDSjkuZXlKbGVIQnBjbVZ6U1c0aU9qRXdNelk0TURBd0xDSnBaQ0k2SWpsT0xVSlhPUzF6VTFkWFkwdDBTRzgxY0hWa2FIY2lMQ0pwWVhRaU9qRTJOamszT1RRM016RjkuWlkxYUdyRUFrc3duTlZ5aGVaQXotQkUyMWlzLTVJYTItV2VlaDJZbVhadyIsImFkbWluIjoxLCJjb21wYW55X3VpZCI6IkxMc3k3a2RSVFZXOVFCV2pBdjlMM2ciLCJzdGF0dXMiOiJBQ1RJVkUifSwiaWF0IjoxNjc3NjM1MzIzfQ.XZJ7b1IggROwvcv2sm5rynHkY8BX1qEenZ1y738sAOM";
        //        IotLogin login = new IotLogin(cnf);
        //        ResponseLogin rl = login.login(email, pwd);
        Header h = gethttpheader(cnf, "QjjPGRcCRbGzB8f8mae4Rw");
        System.out.println(h.getToken());
        //        Header h = new Header();
        //        h.setProject("QjjPGRcCRbGzB8f8mae4Rw");
        //        h.setToken(token);
        //        h.setEmail(email);
        IotPositionHistory his = new IotPositionHistory(cnf, h);
        Date start = CommonUtil.fromUtcDate("2022-01-01T00:00:00.000Z");
        Date end = CommonUtil.fromUtcDate("2023-03-01T23:59:59.999Z");
        //        System.out.println(start.getTime() + "_" + end.getTime());
        int page = 0;
        while (true) {

            ResponsePositionHistory rh = his.downLoad("S4h-xxsaT5qfyEv-xiNW3w", start, end,
                "F:\\proj\\red-point-data-system\\red-point-data-system\\shengdong-redpointdatasys\\shengdong-data-service\\target\\classes\\download",
                page);
            if (rh.getPage() == 0) {
                break;
            }
            page += 1;
            System.out.println(rh.getFile().getName() + "_" + rh.getPage());
        }
    }

    @Test
    void t2() throws Exception {
        IotApiConfig cnf = new IotApiConfig();
        String email = "yao.jianwen@as-bang.com";
        String pwd = "Redp0int";
        String token =
            "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHBpcmVzSW4iOjE4MDAsInVzZXIiOnsidWlkIjoiM0x6c2VNMWhSSmFSR2pWSURGcjJvQSIsInJvbGUiOiJjb21wYW55IGFkbWluIiwiZW1haWwiOiJ5YW8uamlhbndlbkBhcy1iYW5nLmNvbSIsInByb2plY3RzIjpbeyJwcm9qZWN0X3VpZCI6IlFqalBHUmNDUmJHekI4ZjhtYWU0UnciLCJwcm9qZWN0X25hbWUiOiJUYWlZdWFuT2ZmaWNlIiwicHJpdiI6MTN9XSwiYXV0aGVudGljYXRpb25fdG9rZW4iOiJleUpoYkdjaU9pSklVekkxTmlJc0luUjVjQ0k2SWtwWFZDSjkuZXlKbGVIQnBjbVZ6U1c0aU9qRXdNelk0TURBd0xDSnBaQ0k2SWpsT0xVSlhPUzF6VTFkWFkwdDBTRzgxY0hWa2FIY2lMQ0pwWVhRaU9qRTJOamszT1RRM016RjkuWlkxYUdyRUFrc3duTlZ5aGVaQXotQkUyMWlzLTVJYTItV2VlaDJZbVhadyIsImFkbWluIjoxLCJjb21wYW55X3VpZCI6IkxMc3k3a2RSVFZXOVFCV2pBdjlMM2ciLCJzdGF0dXMiOiJBQ1RJVkUifSwiaWF0IjoxNjc3NjM1MzIzfQ.XZJ7b1IggROwvcv2sm5rynHkY8BX1qEenZ1y738sAOM";
        //        IotLogin login = new IotLogin(cnf);
        //        ResponseLogin rl = login.login(email, pwd);
        Header h = gethttpheader(cnf, "QjjPGRcCRbGzB8f8mae4Rw");
        System.out.println(h.getToken());
        //        Header h = new Header();
        //        h.setProject("QjjPGRcCRbGzB8f8mae4Rw");
        //        h.setToken(token);
        //        h.setEmail(email);
        IotAlarmsApi alrm = new IotAlarmsApi(cnf, h);
        Date start = CommonUtil.fromUtcDate("2022-02-01T00:00:00.000Z");
        Date end = CommonUtil.fromUtcDate("2022-02-03T00:00:00.000Z");
        ResponseAlarms res = alrm.getAlarmHistoryJson(start, end);
        logtxt(res.getAlarms().size());
    }

    @Test
    void t3() throws Exception {
        IotApiConfig cnf = new IotApiConfig();
        String email = "yao.jianwen@as-bang.com";
        String pwd = "Redp0int";
        String token =
            "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHBpcmVzSW4iOjE4MDAsInVzZXIiOnsidWlkIjoiM0x6c2VNMWhSSmFSR2pWSURGcjJvQSIsInJvbGUiOiJjb21wYW55IGFkbWluIiwiZW1haWwiOiJ5YW8uamlhbndlbkBhcy1iYW5nLmNvbSIsInByb2plY3RzIjpbeyJwcm9qZWN0X3VpZCI6IlFqalBHUmNDUmJHekI4ZjhtYWU0UnciLCJwcm9qZWN0X25hbWUiOiJUYWlZdWFuT2ZmaWNlIiwicHJpdiI6MTN9XSwiYXV0aGVudGljYXRpb25fdG9rZW4iOiJleUpoYkdjaU9pSklVekkxTmlJc0luUjVjQ0k2SWtwWFZDSjkuZXlKbGVIQnBjbVZ6U1c0aU9qRXdNelk0TURBd0xDSnBaQ0k2SWpsT0xVSlhPUzF6VTFkWFkwdDBTRzgxY0hWa2FIY2lMQ0pwWVhRaU9qRTJOamszT1RRM016RjkuWlkxYUdyRUFrc3duTlZ5aGVaQXotQkUyMWlzLTVJYTItV2VlaDJZbVhadyIsImFkbWluIjoxLCJjb21wYW55X3VpZCI6IkxMc3k3a2RSVFZXOVFCV2pBdjlMM2ciLCJzdGF0dXMiOiJBQ1RJVkUifSwiaWF0IjoxNjc3NjM1MzIzfQ.XZJ7b1IggROwvcv2sm5rynHkY8BX1qEenZ1y738sAOM";
        //        IotLogin login = new IotLogin(cnf);
        //        ResponseLogin rl = login.login(email, pwd);
        Header h = gethttpheader(cnf, "QjjPGRcCRbGzB8f8mae4Rw");
        System.out.println(h.getToken());
        //        Header h = new Header();
        //        h.setProject("QjjPGRcCRbGzB8f8mae4Rw");
        //        h.setToken(token);
        //        h.setEmail(email);
        IotAlarmsApi alrm = new IotAlarmsApi(cnf, h);
        IotNotificationsAndSubscribersEndpoints notification = new IotNotificationsAndSubscribersEndpoints(cnf, h);
        ResponseNotifications rs = notification.getNotifications();
        logtxt(rs.getNotifications().size());
        //        Date start = CommonUtil.fromUtcDate("2022-02-01T00:00:00.000Z");
        //        Date end = CommonUtil.fromUtcDate("2022-02-03T00:00:00.000Z");
        //        ResponseAlarms res = alrm.getAlarmHistoryJson(start, end);
        //        logtxt(res.getAlarms().size());
    }

    /**
     * 构建固定的http请求头
     *
     * @param projuid
     * @return
     * @throws Exception
     */
    Header gethttpheader(IotApiConfig cnf, String projuid) throws Exception {
        Header h = new Header();
        String email = "yao.jianwen@as-bang.com";
        String pwd = "Redp0int";
        IotLogin login = new IotLogin(cnf);
        ResponseLogin rl = login.login(email, pwd);
        h.setEmail(email);
        h.setToken(rl.getData().getToken());
        h.setProject(projuid);
        return h;
    }

    void logtxt(Object txt) {
        System.out.println(txt.toString());
    }
}
