package shengdong.redpoint.httpentity.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class RequestNotifications {
    @JsonProperty("notification_type")
    private String notificationType;
    private Types types;
    private String name;
    private Object active;
    @JsonProperty("project_uid")
    private String projectUid;
    private Conditions conditions;
}