package shengdong.data.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import shengdong.data.dao.IBt42NotificationsDao;
import shengdong.data.entity.Bt42Notifications;
import shengdong.data.mapper.Bt42NotificationsMapper;

import java.util.List;

/**
 * 系统通知Dao实现
 */
@Repository
public class Bt42NotificationsDaoImpl implements IBt42NotificationsDao {
    // 注入Mapper接口
    @Autowired
    private Bt42NotificationsMapper bt42NotificationsMapper;

    /**
     * 插入系统通知数据
     *
     * @param record 系统通知实体类
     * @return 插入的行数
     */
    @Override
    public int insert(Bt42Notifications record) {
        return bt42NotificationsMapper.insert(record);
    }

    @Override
    public void truncateTable() {
        bt42NotificationsMapper.truncateTable();
    }

    @Override
    public List<Bt42Notifications> getAllNotifications(String project) {
        return bt42NotificationsMapper.selectAllNotifications(project);
    }

    @Override
    public int updateByid(Bt42Notifications local) {
        return bt42NotificationsMapper.updateByPrimaryKeySelective(local);
    }

    @Override
    public List<Bt42Notifications> getUserNotificationByUserId(Long id) {
        return bt42NotificationsMapper.selectByUserId(id);
    }
}
