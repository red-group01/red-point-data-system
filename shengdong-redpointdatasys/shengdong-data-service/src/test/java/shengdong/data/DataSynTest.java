package shengdong.data;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import shengdong.data.entity.Bt14User;
import shengdong.data.service.IBaseInfoService;
import shengdong.data.service.IDataService;
import shengdong.redpoint.api.IotApiConfig;
import shengdong.redpoint.api.IotLogin;
import shengdong.redpoint.httpentity.response.ResponseLogin;
import shengdong.redpoint.model.query.Header;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class DataSynTest {
    @Autowired
    IDataService synData;
    @Autowired
    IBaseInfoService synBase;

    @Test
    public void t1() throws Exception {
        IotApiConfig cnf = new IotApiConfig();
        String email = "yao.jianwen@as-bang.com";
        String pwd = "Redp0int";
        String token =
            "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHBpcmVzSW4iOjE4MDAsInVzZXIiOnsidWlkIjoiM0x6c2VNMWhSSmFSR2pWSURGcjJvQSIsInJvbGUiOiJjb21wYW55IGFkbWluIiwiZW1haWwiOiJ5YW8uamlhbndlbkBhcy1iYW5nLmNvbSIsInByb2plY3RzIjpbeyJwcm9qZWN0X3VpZCI6IlFqalBHUmNDUmJHekI4ZjhtYWU0UnciLCJwcm9qZWN0X25hbWUiOiJUYWlZdWFuT2ZmaWNlIiwicHJpdiI6MTN9XSwiYXV0aGVudGljYXRpb25fdG9rZW4iOiJleUpoYkdjaU9pSklVekkxTmlJc0luUjVjQ0k2SWtwWFZDSjkuZXlKbGVIQnBjbVZ6U1c0aU9qRXdNelk0TURBd0xDSnBaQ0k2SWpsT0xVSlhPUzF6VTFkWFkwdDBTRzgxY0hWa2FIY2lMQ0pwWVhRaU9qRTJOamszT1RRM016RjkuWlkxYUdyRUFrc3duTlZ5aGVaQXotQkUyMWlzLTVJYTItV2VlaDJZbVhadyIsImFkbWluIjoxLCJjb21wYW55X3VpZCI6IkxMc3k3a2RSVFZXOVFCV2pBdjlMM2ciLCJzdGF0dXMiOiJBQ1RJVkUifSwiaWF0IjoxNjc3NjM1MzIzfQ.XZJ7b1IggROwvcv2sm5rynHkY8BX1qEenZ1y738sAOM";

        //        Header h = new Header();
        //        h.setProject("QjjPGRcCRbGzB8f8mae4Rw");
        //        h.setToken(token);
        //        h.setEmail(email);
        Header h = gethttpheader(cnf, "QjjPGRcCRbGzB8f8mae4Rw");
        Bt14User uent = new Bt14User();
        uent.setId(1l);
        synBase.synTrackable(cnf, h, uent);
    }

    @Test
    public void t2() throws Exception {
        IotApiConfig cnf = new IotApiConfig();
        //        String email = "yao.jianwen@as-bang.com";
        //        String pwd = "Redp0int";
        //        String token =
        //            "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHBpcmVzSW4iOjE4MDAsInVzZXIiOnsidWlkIjoiM0x6c2VNMWhSSmFSR2pWSURGcjJvQSIsInJvbGUiOiJjb21wYW55IGFkbWluIiwiZW1haWwiOiJ5YW8uamlhbndlbkBhcy1iYW5nLmNvbSIsInByb2plY3RzIjpbeyJwcm9qZWN0X3VpZCI6IlFqalBHUmNDUmJHekI4ZjhtYWU0UnciLCJwcm9qZWN0X25hbWUiOiJUYWlZdWFuT2ZmaWNlIiwicHJpdiI6MTN9XSwiYXV0aGVudGljYXRpb25fdG9rZW4iOiJleUpoYkdjaU9pSklVekkxTmlJc0luUjVjQ0k2SWtwWFZDSjkuZXlKbGVIQnBjbVZ6U1c0aU9qRXdNelk0TURBd0xDSnBaQ0k2SWpsT0xVSlhPUzF6VTFkWFkwdDBTRzgxY0hWa2FIY2lMQ0pwWVhRaU9qRTJOamszT1RRM016RjkuWlkxYUdyRUFrc3duTlZ5aGVaQXotQkUyMWlzLTVJYTItV2VlaDJZbVhadyIsImFkbWluIjoxLCJjb21wYW55X3VpZCI6IkxMc3k3a2RSVFZXOVFCV2pBdjlMM2ciLCJzdGF0dXMiOiJBQ1RJVkUifSwiaWF0IjoxNjc3NjM1MzIzfQ.XZJ7b1IggROwvcv2sm5rynHkY8BX1qEenZ1y738sAOM";
        //        Header h = new Header();
        //        h.setProject("QjjPGRcCRbGzB8f8mae4Rw");
        //        h.setToken(token);
        //        h.setEmail(email);
        Header h = gethttpheader(cnf, "QjjPGRcCRbGzB8f8mae4Rw");
        Bt14User uent = new Bt14User();
        uent.setId(1l);
        synBase.synNotifications(cnf, h, uent);
    }

    @Test
    public void t3() throws Exception {
        IotApiConfig cnf = new IotApiConfig();
        //        String email = "yao.jianwen@as-bang.com";
        //        String pwd = "Redp0int";
        //        String token =
        //            "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHBpcmVzSW4iOjE4MDAsInVzZXIiOnsidWlkIjoiM0x6c2VNMWhSSmFSR2pWSURGcjJvQSIsInJvbGUiOiJjb21wYW55IGFkbWluIiwiZW1haWwiOiJ5YW8uamlhbndlbkBhcy1iYW5nLmNvbSIsInByb2plY3RzIjpbeyJwcm9qZWN0X3VpZCI6IlFqalBHUmNDUmJHekI4ZjhtYWU0UnciLCJwcm9qZWN0X25hbWUiOiJUYWlZdWFuT2ZmaWNlIiwicHJpdiI6MTN9XSwiYXV0aGVudGljYXRpb25fdG9rZW4iOiJleUpoYkdjaU9pSklVekkxTmlJc0luUjVjQ0k2SWtwWFZDSjkuZXlKbGVIQnBjbVZ6U1c0aU9qRXdNelk0TURBd0xDSnBaQ0k2SWpsT0xVSlhPUzF6VTFkWFkwdDBTRzgxY0hWa2FIY2lMQ0pwWVhRaU9qRTJOamszT1RRM016RjkuWlkxYUdyRUFrc3duTlZ5aGVaQXotQkUyMWlzLTVJYTItV2VlaDJZbVhadyIsImFkbWluIjoxLCJjb21wYW55X3VpZCI6IkxMc3k3a2RSVFZXOVFCV2pBdjlMM2ciLCJzdGF0dXMiOiJBQ1RJVkUifSwiaWF0IjoxNjc3NjM1MzIzfQ.XZJ7b1IggROwvcv2sm5rynHkY8BX1qEenZ1y738sAOM";
        //        Header h = new Header();
        //        h.setProject("QjjPGRcCRbGzB8f8mae4Rw");
        //        h.setToken(token);
        //        h.setEmail(email);
        //        Header h = gethttpheader(cnf, "QjjPGRcCRbGzB8f8mae4Rw");
        //        System.out.println(h.getToken());
        synData.synAlarmsHis();
    }

    @Test
    public void t4() throws Exception {
        IotApiConfig cnf = new IotApiConfig();
        //        String email = "yao.jianwen@as-bang.com";
        //        String pwd = "Redp0int";
        //        String token =
        //            "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHBpcmVzSW4iOjE4MDAsInVzZXIiOnsidWlkIjoiM0x6c2VNMWhSSmFSR2pWSURGcjJvQSIsInJvbGUiOiJjb21wYW55IGFkbWluIiwiZW1haWwiOiJ5YW8uamlhbndlbkBhcy1iYW5nLmNvbSIsInByb2plY3RzIjpbeyJwcm9qZWN0X3VpZCI6IlFqalBHUmNDUmJHekI4ZjhtYWU0UnciLCJwcm9qZWN0X25hbWUiOiJUYWlZdWFuT2ZmaWNlIiwicHJpdiI6MTN9XSwiYXV0aGVudGljYXRpb25fdG9rZW4iOiJleUpoYkdjaU9pSklVekkxTmlJc0luUjVjQ0k2SWtwWFZDSjkuZXlKbGVIQnBjbVZ6U1c0aU9qRXdNelk0TURBd0xDSnBaQ0k2SWpsT0xVSlhPUzF6VTFkWFkwdDBTRzgxY0hWa2FIY2lMQ0pwWVhRaU9qRTJOamszT1RRM016RjkuWlkxYUdyRUFrc3duTlZ5aGVaQXotQkUyMWlzLTVJYTItV2VlaDJZbVhadyIsImFkbWluIjoxLCJjb21wYW55X3VpZCI6IkxMc3k3a2RSVFZXOVFCV2pBdjlMM2ciLCJzdGF0dXMiOiJBQ1RJVkUifSwiaWF0IjoxNjc3NjM1MzIzfQ.XZJ7b1IggROwvcv2sm5rynHkY8BX1qEenZ1y738sAOM";
        //        Header h = new Header();
        //        h.setProject("QjjPGRcCRbGzB8f8mae4Rw");
        //        h.setToken(token);
        //        h.setEmail(email);
        //        Header h = gethttpheader(cnf, "QjjPGRcCRbGzB8f8mae4Rw");
        //        System.out.println(h.getToken());
        synData.synZoneEvents();
    }

    @Test
    public void tData() throws Exception {
        //        IotApiConfig cnf = new IotApiConfig();
        //        Header h = gethttpheader(cnf, "QjjPGRcCRbGzB8f8mae4Rw");
        //        System.out.println(h.getToken());
        synData.synPoinsHis();
    }

    @Test
    public void tall() throws Exception {

        synBase.synAll();
    }

    /**
     * 构建固定的http请求头
     *
     * @param projuid
     * @return
     * @throws Exception
     */
    Header gethttpheader(IotApiConfig cnf, String projuid) throws Exception {
        Header h = new Header();
        String email = "yao.jianwen@as-bang.com";
        String pwd = "Redp0int";
        IotLogin login = new IotLogin(cnf);
        ResponseLogin rl = login.login(email, pwd);
        h.setEmail(email);
        h.setToken(rl.getData().getToken());
        h.setProject(projuid);
        return h;
    }
}
