package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ZoneType {
    private String name;
    private int id;
    @JsonProperty("display_name")
    private String displayName;
}