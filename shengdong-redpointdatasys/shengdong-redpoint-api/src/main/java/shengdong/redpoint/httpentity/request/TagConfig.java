package shengdong.redpoint.httpentity.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class TagConfig {
    @JsonProperty("t2t_cfg")
    private T2tCfg t2tCfg;
}