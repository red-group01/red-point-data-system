package shengdong.data.dao;

import shengdong.data.entity.Bt31Project;
import shengdong.data.entity.Bt50PoinsHisView;

import java.util.List;

/**
 * 项目Dao接口
 */
public interface IBt31ProjectDao {

    /**
     * 插入项目数据
     *
     * @param record 项目实体类
     * @return 插入的行数
     */
    public int insert(Bt31Project record);

    public List<Bt31Project> getUserProj(Long userid);

    int updateByid(Bt31Project i);

    void trancateTable();

    Bt31Project selectByUid(String uid);

    List<Bt50PoinsHisView> selectCnf();
}
