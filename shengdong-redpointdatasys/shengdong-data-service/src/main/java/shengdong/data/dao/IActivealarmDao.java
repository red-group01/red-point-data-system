package shengdong.data.dao;

import shengdong.data.entity.Activealarm;

import java.util.List;

/**
 * 实时告警信息Dao接口
 */
public interface IActivealarmDao {
    public int insertSelective(Activealarm record);

    public int deleteByUserId(Long userId);
    public List<Activealarm> selectBySubUid(String sublocationUid);
}
