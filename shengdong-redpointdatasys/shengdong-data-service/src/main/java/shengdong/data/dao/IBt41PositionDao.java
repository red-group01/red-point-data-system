package shengdong.data.dao;

import shengdong.data.entity.Bt41Position;

import java.util.List;

/**
 * 位置历史信息Dao接口
 */
public interface IBt41PositionDao {
    /**
     * 插入位置历史信息数据
     *
     * @param record 位置历史信息实体类
     * @return 插入的行数
     */
    public int insert(Bt41Position record);

    void truncateTable();

    List<Bt41Position> getAllPostion(String projectUid);

    /**
     * 根据objUid查询位置历史信息表数据
     *
     * @param objUid 位置历史信息实体类
     * @return 位置历史信息实体类
     */
    Bt41Position selectByObjUid(String objUid);

    /**
     * 根据objUid查询区间内的位置历史信息表数据
     *
     * @param objUid 位置历史信息实体类
     * @return 位置历史信息实体类
     */
    List<Bt41Position> selectByObjUidInTime(String objUid, String tsBegin, String tsEnd);

    /**
     * 根据sublocationUid查询区间内的位置历史信息表数据
     *
     * @param sublocationUid 位置历史信息实体类
     * @return 位置历史信息实体类
     */
    List<Bt41Position> selectSublocationUidInTime(String sublocationUid, String tsBegin, String tsEnd);

    /**
     * 根据sublocationUid查询区间内的位置历史信息表数据
     *
     * @param sublocationUid 位置历史信息实体类
     * @return 位置历史信息实体类
     */
    List<Bt41Position> selectSublocationUidInTime2(String sublocationUid, String tsBegin, String tsEnd);

    /**
     * 根据objUid集合查询区间内的位置历史信息表数据
     *
     * @param objUids 可追踪标签uid集合
     * @return 位置历史信息实体类
     */
    List<Bt41Position> selectInObjUidInTime(String tsBegin, String tsEnd, List<String> objUids);

    Bt41Position selectNewDataTime();
}
