package shengdong.redpoint.websockentity;

import lombok.Data;

@Data
public class ResponseNotifyChannelZone {
    private String method;
    private String jsonrpc;
    private ChannelZoneParams params;
}