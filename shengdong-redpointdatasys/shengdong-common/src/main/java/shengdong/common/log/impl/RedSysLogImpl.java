package shengdong.common.log.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import shengdong.common.log.IRedSysLog;

@Component
public class RedSysLogImpl implements IRedSysLog {

    //log门户对象
    Logger logger;

    /**
     * 输出追踪日志
     * @param content 日志内容
     * @param logclass 日志发生的类
     */
    @Override
    public void logError(String content, Class<?> logclass) {
        //实例化log门户对象
        logger = LoggerFactory.getLogger(logclass);
        //输出日志
        logger.error(content);
    }

    @Override
    public String toLog(String code, String msg) {
        return "code:" + code + "---msg:" + msg;
    }

}
