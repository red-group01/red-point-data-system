package shengdong.redpoint.httpentity.request;

import java.util.List;
import lombok.Data;

@Data
public class SensorConfig{
	private List<SensorsItem> sensors;
}