package shengdong.data.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 设备告警表实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Bt44Alarm {

    //自增主键
    private Long id;

    //设备告警唯一标识
    private String alarmUid;

    //地点子区域唯一标识
    private String sublocationUid;

    //区域围栏唯一标识
    private String zoneUid;

    //硬件设备节点mac地址
    private String macAddress;

    //触发报警的其他对象uid
    private String objUid;

    //报警类型
    private String type;

    //附加信息
    private String additionalInfo;

    //报警发布的数量
    private Integer hysteresisCount;

    //当前报警状态
    private String state;

    //时间戳
    private Date ts;

    //触发报警时可跟踪对象位置的x坐标
    private Long x;

    //触发报警时可跟踪对象位置的y坐标
    private Long y;

    //触发报警时可跟踪对象位置的z坐标
    private Long z;

    //创建时间（UTC）
    private Date createdAt;

    //更新时间（UTC）
    private Date updatedAt;

    //数据最后更新者
    private String creatorAt;

    //数据信息备注
    private String remark;
    /**
     * project_uid
     */
    private String projectUid;

}