package shengdong.data.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import shengdong.common.config.ViewPageConfig;
import shengdong.data.dao.*;
import shengdong.data.entity.*;
import shengdong.data.entity.view.*;
import shengdong.data.service.IDataService;
import shengdong.data.service.IViewService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ViewServiceImpl implements IViewService {
    @Autowired
    IBt44AlarmDao bt44AlarmDao;
    @Autowired
    IBt34ZoneDao bt34ZoneDao;
    @Autowired
    IBt23TrackableDao bt23TrackableDao;
    @Autowired
    IBt22AnchorDao bt22AnchorDao;
    @Autowired
    IBt41PositionDao bt41PositionDao;
    @Autowired
    IBt11CompanyDao bt11CompanyDao;
    @Autowired
    IBt14UserDao bt14UserDao;
    @Autowired
    IBt31ProjectDao bt31ProjectDao;
    @Autowired
    IBt32LocationDao bt32LocationDao;
    @Autowired
    IBt33SubLocationDao bt33SubLocationDao;
    @Autowired
    ViewPageConfig viewPageConfig;
    @Autowired
    IActivealarmDao activealarmDao;

    @Override
    public List<OutAlarmListView> getAlarmListView(String sublocationuid) {
        List<OutAlarmListView> alarmListViews = new ArrayList<>();
        Date now = new Date();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String begintime = df.format(now) + " 00:00:00";
        String endtime = df.format(now) + " 23:59:59";
//        begintime = "2023-03-20 00:00:00";
//        endtime = "2023-03-22 00:00:00";
        List<Bt44Alarm> alarmList = bt44AlarmDao.selectBySublocationUid(sublocationuid, begintime, endtime);
        List<Bt34Zone> zoneList = bt34ZoneDao.getAllZone();
        int serial = 0;
        for (Bt44Alarm item : alarmList) {
            serial++;
            OutAlarmListView outAlarmListView = new OutAlarmListView();
            for (Bt34Zone itemzone : zoneList) {
                if (item.getZoneUid() == null) {
                    outAlarmListView.setZoneName(item.getAdditionalInfo());
                } else {
                    if (item.getZoneUid().equals(itemzone.getZoneUid())) {
                        outAlarmListView.setZoneName(itemzone.getName());
                    }
                }
            }
            outAlarmListView.setSerial(serial);
            outAlarmListView.setMessage(item.getAdditionalInfo());
            outAlarmListView.setAlarmTime(item.getCreatedAt());
            List<Bt23Trackable> trackableList = bt23TrackableDao.selectByTrackableUid(item.getObjUid());
            if (trackableList != null && trackableList.size() > 0) {
                outAlarmListView.setNodeName(trackableList.get(0).getName());
            }
            alarmListViews.add(outAlarmListView);
        }
        return alarmListViews;
    }

    @Override
    public List<OutNodeListView> getNodeListView(String sublocationuid) {
        List<OutNodeListView> outNodeListViews = new ArrayList<>();
        List<Bt22Anchor> anchorList = bt22AnchorDao.selectBySublocationUid(sublocationuid);
        List<Bt23Trackable> trackableList = bt23TrackableDao.selectBySublocationUid(sublocationuid);
        int serial = 0;
        Date now = new Date();
        Long nowl = now.getTime();
        for (Bt22Anchor item : anchorList) {
            serial++;
            OutNodeListView outNodeListView = new OutNodeListView();
            outNodeListView.setSerial(serial);
            outNodeListView.setNodeName(item.getName());
            outNodeListView.setCoordinate(item.getPositionX().toString() + "." + item.getPositionY().toString());
            outNodeListView.setType(item.getType());
            outNodeListView.setUid(item.getAnchorUid());
            Date end = item.getUpdatedAt();
            setNodeActive(outNodeListViews, nowl, outNodeListView, end);
        }
        for (Bt23Trackable item : trackableList) {
            serial++;
            OutNodeListView outNodeListView = new OutNodeListView();
            outNodeListView.setSerial(serial);
            outNodeListView.setNodeName(item.getName());
            outNodeListView.setCoordinate(item.getX().toString() + "." + item.getY().toString());
            outNodeListView.setType(item.getCategory());
            outNodeListView.setUid(item.getTrackableUid());
            Bt41Position position = bt41PositionDao.selectByObjUid(item.getTrackableUid());
            if (position != null) {
                Date end = position.getTs();
                setNodeActive(outNodeListViews, nowl, outNodeListView, end);
            }
        }
        return outNodeListViews;
    }

    private void setNodeActive(List<OutNodeListView> outNodeListViews, Long nowl, OutNodeListView outNodeListView, Date end) {
        Long endl = end.getTime();
        Long timelag = nowl - endl;
        Long min = timelag / 60000;
        if (min.longValue() > 61) {
            outNodeListView.setIsActive("Lose");
        } else if (min.longValue() > 1 && min.longValue() < 61) {
            outNodeListView.setIsActive("> 1min");
        } else {
            outNodeListView.setIsActive("< 1min");
        }
        outNodeListViews.add(outNodeListView);
    }

    @Override
    public List<OutZoneListView> getZoneListView(String sublocationuid) {
        List<OutZoneListView> outZoneListViews = new ArrayList<>();
        List<Bt34Zone> zoneList = bt34ZoneDao.selectBySublocationUid(sublocationuid);
        int serial = 0;
        for (Bt34Zone item : zoneList) {
            serial++;
            OutZoneListView outZoneListView = new OutZoneListView();
            outZoneListView.setSerial(serial);
            outZoneListView.setZoneName(item.getName());
            outZoneListView.setType(item.getZoneTypeName());
            outZoneListView.setIsActive("No");
            if (item.getActive()) {
                outZoneListView.setIsActive("Yes");
            }
            outZoneListViews.add(outZoneListView);
        }
        List<Bt23Trackable> trackableList = bt23TrackableDao.selectBySublocationUid(sublocationuid);
        List<String> trackableUids = new ArrayList<>();
        for (Bt23Trackable item : trackableList) {
            trackableUids.add(item.getTrackableUid());
        }
        List<Bt34Zone> zoneList1 = bt34ZoneDao.selectInTrackableUid(trackableUids);
        for (Bt34Zone item : zoneList1) {
            serial++;
            OutZoneListView outZoneListView = new OutZoneListView();
            outZoneListView.setSerial(serial);
            outZoneListView.setZoneName(item.getName());
            outZoneListView.setType(item.getZoneTypeName());
            outZoneListView.setIsActive("No");
            if (item.getActive()) {
                outZoneListView.setIsActive("Yes");
            }
            outZoneListViews.add(outZoneListView);
        }
        return outZoneListViews;
    }

    @Override
    public List<OutNodeStateListView> getNodeStateListView(String sublocationuid) {
        List<OutNodeStateListView> outNodeStateListViews = new ArrayList<>();
        List<Bt23Trackable> trackableList = bt23TrackableDao.selectBySublocationUid(sublocationuid);
        int serial = 0;
        Date now = new Date();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String begintime = df.format(now) + " 00:00:00";
        String endtime = df.format(now) + " 23:59:59";
        List<String> objUids = new ArrayList<>();
        for (Bt23Trackable item : trackableList) {
            objUids.add(item.getTrackableUid());
        }
        List<Bt41Position> positionList = bt41PositionDao.selectInObjUidInTime(begintime, endtime, objUids);
        for (Bt23Trackable item : trackableList) {
            serial++;
            OutNodeStateListView outNodeStateListView = new OutNodeStateListView();
            outNodeStateListView.setSerial(serial);
            outNodeStateListView.setNodeName(item.getName());
            outNodeStateListView.setType(item.getCategory());
            outNodeStateListView.setActiveTime("0min");
            if (positionList != null && positionList.size() > 1) {
                double distancetotal = 0;
                Long axtiveTimeBegin = 0L;
                Long axtiveTimeEnd = 0L;
                double x1 = 0;
                double x2 = 0;
                double y1 = 0;
                double y2 = 0;
                int posflag = 0;
                for (int i = 0; i < positionList.size(); i++) {
                    if (positionList.get(i).getObjUid().equals(item.getTrackableUid())) {
                        //计算时长
                        if (axtiveTimeBegin.longValue() == 0) {
                            axtiveTimeBegin = positionList.get(i).getTs().getTime();
                            axtiveTimeEnd = positionList.get(i).getTs().getTime();
                        } else {
                            axtiveTimeEnd = positionList.get(i).getTs().getTime();
                        }
                        //计算距离
                        if (posflag == 0) {
                            x1 = positionList.get(i).getX();
                            y1 = positionList.get(i).getY();
                            posflag = 1;
                            continue;
                        }
                        if (posflag == 1) {
                            x2 = positionList.get(i).getX();
                            y2 = positionList.get(i).getY();
                            double distance = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
                            distancetotal = distancetotal + distance;
                            posflag = 0;
                        }
                    }
                }
                //计算时长
                Long activeTime = axtiveTimeEnd - axtiveTimeBegin;
                Long min = activeTime / 60000;
                if (min.longValue() > 60) {
                    outNodeStateListView.setActiveTime(min / 60 + "h");
                } else if (min.longValue() > 0) {
                    outNodeStateListView.setActiveTime(min + "min");
                }
                //计算距离
                distancetotal = Double.parseDouble(String.format("%.2f", distancetotal / 1000));
                outNodeStateListView.setDistance(String.valueOf(distancetotal) + "m");
                outNodeStateListView.setLength(distancetotal);
            }
            outNodeStateListViews.add(outNodeStateListView);
        }
        return outNodeStateListViews;
    }

    @Override
    public OutNodeCountView getNodeCountListView(String sublocationuid) {
        OutNodeCountView outNodeCountView = new OutNodeCountView();
        List<BaseNodeType> baseNodeTypes = new ArrayList<>();
        List<BaseNodeType> baseNodeTypes1 = bt22AnchorDao.selectBySublocationUidGroupByType(sublocationuid);
        List<BaseNodeType> baseNodeTypes2 = bt23TrackableDao.selectBySublocationUidGroupByCategory(sublocationuid);
        for (BaseNodeType item : baseNodeTypes1) {
            baseNodeTypes.add(item);
        }
        for (BaseNodeType item : baseNodeTypes2) {
            baseNodeTypes.add(item);
        }
        outNodeCountView.setTitle("タグの分類統計数");
        List<Bt22Anchor> anchorList = bt22AnchorDao.selectBySublocationUid(sublocationuid);
        List<Bt23Trackable> trackableList = bt23TrackableDao.selectBySublocationUid(sublocationuid);
        int anchorCount = 0;
        for (BaseNodeType item : baseNodeTypes) {
            NameAndValue nameAndValue = new NameAndValue();
            nameAndValue.setName(item.getNodeType());
            int count = 0;
            for (Bt22Anchor itemanchor : anchorList) {
                if (itemanchor.getType().equals(item.getNodeType())) {
                    count++;
                }
            }
            for (Bt23Trackable itemtra : trackableList) {
                if (itemtra.getCategory().equals(item.getNodeType())) {
                    count++;
                }
            }
            nameAndValue.setValue(count);
            outNodeCountView.getNvlist().add(nameAndValue);
        }
        return outNodeCountView;
    }

    @Override
    public OutHotImageView getHotImgView(String sublocationuid) {
        OutHotImageView outHotImageView = new OutHotImageView();
        OutHotImageView tempoutHotImageView = new OutHotImageView();
        outHotImageView.setTitle("タグのヒートマップ");
        List<Bt22Anchor> anchorList = bt22AnchorDao.selectBySublocationUid(sublocationuid);
        Long xmax = 0L;
        Long ymax = 0L;
        for (Bt22Anchor item : anchorList) {
            if (item.getPositionX().longValue() > xmax.longValue()) {
                xmax = item.getPositionX();
            }
            if (item.getPositionY().longValue() > ymax.longValue()) {
                ymax = item.getPositionY();
            }
        }
        for (int i = 0; i < xmax / 100; i++) {
            for (int j = 0; j < ymax / 100; j++) {
                HotImageData hid = new HotImageData();
                hid.setX((i + 1) * 100 - 50);
                hid.setY((j + 1) * 100 - 50);
                hid.setValue(0);
                outHotImageView.getNvlist().add(hid);
                HotImageData temphid = new HotImageData();
                temphid.setX((i + 1) * 100);
                temphid.setY((j + 1) * 100);
                temphid.setValue(0);
                tempoutHotImageView.getNvlist().add(temphid);
            }
        }
        Date now = new Date();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String begintime = df.format(now) + " 00:00:00";
        String endtime = df.format(now) + " 23:59:59";
        List<Bt41Position> positionList = bt41PositionDao.selectSublocationUidInTime(sublocationuid, begintime, endtime);
        for (Bt41Position item : positionList) {
            for (int i = 0; i < tempoutHotImageView.getNvlist().size(); i++) {
                if (item.getX() <= tempoutHotImageView.getNvlist().get(i).getX() && item.getY() <= tempoutHotImageView.getNvlist().get(i).getY()) {
                    outHotImageView.getNvlist().get(i).setValue(outHotImageView.getNvlist().get(i).getValue() + 1);
                    break;
                }
            }
        }
        for (int i = 0; i < outHotImageView.getNvlist().size(); i++) {
            if (outHotImageView.getNvlist().get(i).getValue() == 0) {
                outHotImageView.getNvlist().remove(i);
                i--;
            }
        }
        if (positionList.size() == 0) {
            return null;
        }
        return outHotImageView;
    }

    @Override
    public List<OutBaseInfoView> getBaseInfoView() {
        List<OutBaseInfoView> outBaseInfoViews = new ArrayList<>();
        List<Bt11Company> companyList = bt11CompanyDao.getAll();
        for (Bt11Company item : companyList) {
            OutBaseInfoView outBaseInfoView = new OutBaseInfoView();
            outBaseInfoView.setCompanyName(item.getName());
            outBaseInfoView.setCompanyUid(item.getCompanyUid());
            List<Bt14User> userList = bt14UserDao.getAll();
            List<OutCameraConfigView> cameraConfigViews = getCameraConfigView();
            outBaseInfoView.setCameraConfigViews(cameraConfigViews);
            for (Bt14User itemuser : userList) {
                outBaseInfoView.setServerIp(itemuser.getServerCnf().getServerIp());
                outBaseInfoView.setUserPwd(itemuser.getPassword());
                outBaseInfoView.setUserEmail(itemuser.getEmail());
                outBaseInfoView.setUserToken(itemuser.getToken());
                List<Bt31Project> projectList = bt31ProjectDao.getUserProj(itemuser.getId());
                List<Project> projects = new ArrayList<>();
                for (int i = 0; i < projectList.size(); i++) {
                    Project project = new Project();
                    BeanUtils.copyProperties(projectList.get(i), project);
                    projects.add(project);
                }
                outBaseInfoView.setProjects(projects);
                List<Bt34Zone> zones = bt34ZoneDao.getUserZone(itemuser.getId());
                outBaseInfoView.getProjects().get(0).setZones(zones);
                List<Bt32Location> locationList = bt32LocationDao.getLocationByUserId(itemuser.getId());
                List<Location> locations = new ArrayList<>();
                for (int i = 0; i < locationList.size(); i++) {
                    Location location = new Location();
                    BeanUtils.copyProperties(locationList.get(i), location);
                    locations.add(location);
                }
                outBaseInfoView.getProjects().get(0).setLocations(locations);
                for (Location itemlocation : outBaseInfoView.getProjects().get(0).getLocations()) {
                    List<Bt33SubLocation> subLocationList = bt33SubLocationDao.selectByLocationUid(itemlocation.getLocationUid());
                    List<SubLocation> subLocations = new ArrayList<>();
                    for (int i = 0; i < subLocationList.size(); i++) {
                        SubLocation subLocation = new SubLocation();
                        BeanUtils.copyProperties(subLocationList.get(i), subLocation);
                        subLocations.add(subLocation);
                    }
                    itemlocation.setSubLocations(subLocations);
                    for (SubLocation itemsub : itemlocation.getSubLocations()) {
                        List<Bt22Anchor> anchors = bt22AnchorDao.selectBySublocationUid(itemsub.getSublocationUid());
                        itemsub.setAnchors(anchors);
                        List<Bt23Trackable> trackables = bt23TrackableDao.selectBySublocationUid(itemsub.getSublocationUid());
                        itemsub.setTrackables(trackables);
                    }
                }
            }
            outBaseInfoViews.add(outBaseInfoView);
        }
        return outBaseInfoViews;
    }

    @Override
    public OutHisDataView getHisDataView(String sublocationuid, String begintime, String endtime, List<String> objUids) {
        OutHisDataView outHisDataView = new OutHisDataView();
        outHisDataView.setTitle("HisData");
        for (String item : objUids) {
            HisDataInfo hisDataInfo = new HisDataInfo();
            hisDataInfo.setUid(item);
            outHisDataView.getHisDataList().add(hisDataInfo);
        }
        List<Bt23Trackable> trackableList = bt23TrackableDao.selectBySublocationUid(sublocationuid);
        for (Bt23Trackable item : trackableList) {
            for (HisDataInfo hisDataInfo : outHisDataView.getHisDataList()) {
                if (hisDataInfo.getUid().equals(item.getTrackableUid())) {
                    hisDataInfo.setName(item.getName());
                }
            }
        }
        List<Bt41Position> positionList = bt41PositionDao.selectInObjUidInTime(begintime, endtime, objUids);
        for (Bt41Position item : positionList) {
            for (HisDataInfo hisDataInfo : outHisDataView.getHisDataList()) {
                if (hisDataInfo.getUid().equals(item.getObjUid())) {
                    if (hisDataInfo.getHisDataList().size() == 0) {
                        HisData hisData = new HisData();
                        if (item.getX().longValue() == 0) {
                            hisData.setX(1);
                        } else {
                            hisData.setX(Math.toIntExact(item.getX()));
                        }
                        if (item.getY().longValue() == 0) {
                            hisData.setY(1);
                        } else {
                            hisData.setY(Math.toIntExact(item.getY()));
                        }
                        hisData.setTs(item.getTs());
                        hisDataInfo.getHisDataList().add(hisData);
                    } else {
                        Long x1 = item.getX() - hisDataInfo.getHisDataList().get(hisDataInfo.getHisDataList().size() - 1).getX();
                        Long y1 = item.getY() - hisDataInfo.getHisDataList().get(hisDataInfo.getHisDataList().size() - 1).getY();
                        double length = Math.sqrt(x1 * x1 + y1 * y1);
                        if (length > 500) {
                            HisData hisData = new HisData();
                            if (item.getX().longValue() == 0) {
                                hisData.setX(1);
                            } else {
                                hisData.setX(Math.toIntExact(item.getX()));
                            }
                            if (item.getY().longValue() == 0) {
                                hisData.setY(1);
                            } else {
                                hisData.setY(Math.toIntExact(item.getY()));
                            }
                            hisData.setTs(item.getTs());
                            hisDataInfo.getHisDataList().add(hisData);
                        }
                    }
                }
            }
        }
        return outHisDataView;
    }

    @Override
    public OutHisDataView getHisDataView2(String begintime, String endtime, List<String> objUids) {
        OutHisDataView outHisDataView = new OutHisDataView();
//        outHisDataView.setTitle("HisData");
//        List<Bt41Position> positionList = bt41PositionDao.selectInObjUidInTime(begintime, endtime, objUids);
//        for (int i = 0; i < positionList.size(); i++) {
//            HisData hisData = new HisData();
//            hisData.setTs(positionList.get(i).getTs());
//            hisData.setValue(1);
//            hisData.setX(Math.toIntExact(positionList.get(i).getX()));
//            hisData.setY(Math.toIntExact(positionList.get(i).getY()));
//            if (i == 0) {
//                outHisDataView.getNvlist().add(hisData);
//            } else if (!positionList.get(i).getX().equals(positionList.get(i - 1).getX()) && !positionList.get(i).getY().equals(positionList.get(i - 1).getY())) {
//                Long x1 = positionList.get(i - 1).getX();
//                Long x2 = positionList.get(i).getX();
//                if (x1.equals(x2)) {
//                    Boolean flag = true;
//                }
//                Long y1 = positionList.get(i - 1).getY();
//                Long y2 = positionList.get(i).getY();
//                outHisDataView.getNvlist().add(hisData);
//            }
//        }
        return outHisDataView;
    }

    @Override
    public List<OutCameraConfigView> getCameraConfigView() {
        List<OutCameraConfigView> outCameraConfigViews = new ArrayList<>();
        int count = viewPageConfig.getCamera().getName().size();
        for (int i = 0; i < count; i++) {
            OutCameraConfigView outCameraConfigView = new OutCameraConfigView();
            outCameraConfigView.setName(viewPageConfig.getCamera().getName().get(i));
            outCameraConfigView.setType(viewPageConfig.getCamera().getType().get(i));
            outCameraConfigView.setMac(viewPageConfig.getCamera().getMac().get(i));
            outCameraConfigView.setUrl(viewPageConfig.getCamera().getUrl().get(i));
            outCameraConfigView.setUid(viewPageConfig.getCamera().getUid().get(i));
            outCameraConfigView.setAppKey(viewPageConfig.getCamera().getAppKey().get(i));
            outCameraConfigView.setAppSecret(viewPageConfig.getCamera().getAppSecret().get(i));
            outCameraConfigView.setIcoUid(viewPageConfig.getCamera().getIcoUid().get(i));
            outCameraConfigView.setImageId(viewPageConfig.getCamera().getImageId().get(i));
            outCameraConfigView.setUrl2(viewPageConfig.getCamera().getUrl2().get(i));
            LocationConfig locationConfig = new LocationConfig();
            locationConfig.setX(viewPageConfig.getCamera().getX().get(i));
            locationConfig.setY(viewPageConfig.getCamera().getY().get(i));
            outCameraConfigView.setLocation(locationConfig);
            outCameraConfigViews.add(outCameraConfigView);
        }
        return outCameraConfigViews;
    }

    @Override
    public List<Activealarm> getAlarmActiveView(String sublocationuid) {
        List<Activealarm> alarmList = activealarmDao.selectBySubUid(sublocationuid);

        return alarmList;
    }
}
