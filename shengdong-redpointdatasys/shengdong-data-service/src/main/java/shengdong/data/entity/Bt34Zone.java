package shengdong.data.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 区域围栏实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Bt34Zone {

    //自增主键
    private Long id;

    //区域围栏唯一标识
    private String zoneUid;

    //地点子区域唯一标识
    private String sublocationUid;

    //区域围栏名称
    private String name;

    //删除时间（UTC）
    private Date removedAt;

    //是否激活
    private Boolean active;

    //区域全局允许或禁止
    private Boolean defaultAllowed;

    //区域告警级别
    private Integer zoneAlarmLevel;

    //区域围栏类别名称
    private String zoneTypeName;

    //是否可以移动区域
    private Boolean dynamic;

    //围栏形状（可移动区域为圆形）
    private String shape;

    //区域半径
    private Integer radius;

    //区域边界点坐标
    private String positions;

    //可以忽略区域规则的“特殊情况”可跟踪项的唯一标识符数组
    private String zoneExceptions;

    //可以忽略区域规则的“特殊情况”组的唯一标识符数组
    private String zoneExceptionsGroups;

    //区域激活规则
    private String zoneMode;

    //区域计划激活时间
    private String startTime;

    //区域计划终止时间
    private String stopTime;

    //区域围栏类别(keep_in zone，keep_out zone，information zone)
    private Long zoneTypeId;

    //公司唯一标识
    private String companyUid;

    //项目唯一标识
    private String projectUid;

    //项目地点唯一标识
    private String locationUid;

    //The identifier of the area in the context of the server
    private Long sublocationId;

    //The unique identifier of the trackable around which the buffer is centered. If the geofence is a zone, returns null. Used whenthe geofence is a buffer.
    private String trackableObjectUid;

    //区域围栏的服务器标识id
    private Long zoneId;

    //创建时间（UTC）
    private Date createdAt;

    //更新时间（UTC）
    private Date updatedAt;

    //数据最后更新者
    private String creatorAt;

    //数据信息备注
    private String remark;
    /**
     * 项目所属用户id
     */
    private Long userId;
    /**
     * 已删除
     */
    private Boolean deleteMark;

}