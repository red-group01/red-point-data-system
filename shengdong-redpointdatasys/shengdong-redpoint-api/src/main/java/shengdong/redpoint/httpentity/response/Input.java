package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Input {
    @JsonProperty("sublocation_uid")
    private String sublocationUid;
    private int distance;
    @JsonProperty("project_uid")
    private String projectUid;
    private long from;
    private long to;
    private String reportUid;
}