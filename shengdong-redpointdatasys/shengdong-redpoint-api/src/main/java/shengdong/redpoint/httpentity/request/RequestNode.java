package shengdong.redpoint.httpentity.request;

import lombok.Data;

@Data
public class RequestNode{
	private String nodeType;
	private String macAddress;
	private String name;
}