package shengdong.redpoint.httpentity.request;

import lombok.Data;

@Data
public class RequestSublocations{
	private String name;
	private Object description;
	private int offsetX;
	private int offsetY;
	private String locationUid;
	private int offsetZ;
}