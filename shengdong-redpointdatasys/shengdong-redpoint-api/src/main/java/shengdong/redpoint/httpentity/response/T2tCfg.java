package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class T2tCfg {
    @JsonProperty("alarm_back")
    private boolean alarmBack;
    private String role;
    private int delay;
    private String pin;
    @JsonProperty("safe_distance")
    private int safeDistance;
    private int distance;
    @JsonProperty("alert_distance")
    private int alertDistance;
    private int angle;
    private boolean haptic;
    private boolean buzzer;
}