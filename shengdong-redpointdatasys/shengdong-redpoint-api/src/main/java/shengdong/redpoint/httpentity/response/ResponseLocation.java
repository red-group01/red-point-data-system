package shengdong.redpoint.httpentity.response;

import java.util.List;
import lombok.Data;

@Data
public class ResponseLocation{
	private List<LocationsItem> locations;
}