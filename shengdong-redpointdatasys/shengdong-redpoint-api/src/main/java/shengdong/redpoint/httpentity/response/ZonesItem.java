package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class ZonesItem {
    @JsonProperty("zone_geometry")
    private ZoneGeometry zoneGeometry;
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("location_uid")
    private String locationUid;
    @JsonProperty("zone_id")
    private long zoneId;
    @JsonProperty("default_allowed")
    private boolean defaultAllowed;
    private String uid;
    @JsonProperty("zone_exceptions")
    private List<String> zoneExceptions;
    private int crc16;
    @JsonProperty("updated_at")
    private String updatedAt;
    @JsonProperty("removed_at")
    private String removedAt;

    @JsonProperty("sublocation_id")
    private Long sublocationId;
    @JsonProperty("zone_type_name")
    private String zoneTypeName;
    private boolean dynamic;
    @JsonProperty("zone_type")
    private ZoneType zoneType;
    @JsonProperty("user_params")
    private UserParams userParams;
    @JsonProperty("zone_mode")
    private String zoneMode;
    @JsonProperty("sublocation_uid")
    private String sublocationUid;
    private String shape;
    @JsonProperty("trackables_in_zone")
    private List<TrackablesInZoneItem> trackablesInZone;
    private boolean active;
    @JsonProperty("project_uid")
    private String projectUid;
    private int priority;
    @JsonProperty("zone_alarm_level")
    private int zoneAlarmLevel;
    @JsonProperty("trackable_object_uid")
    private String trackableObjectUid;
    private Schedule schedule;
    private Object depth;
    private String name;
    @JsonProperty("zone_type_id")
    private long zoneTypeId;
    @JsonProperty("zone_exceptions_groups")
    private List<String> zoneExceptionsGroups;
}