package shengdong.data.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import shengdong.data.dao.IBt22AnchorDao;
import shengdong.data.entity.BaseNodeType;
import shengdong.data.entity.Bt22Anchor;
import shengdong.data.mapper.Bt22AnchorMapper;

import java.util.List;

/**
 * 锚节点Dao实现
 */
@Repository
public class Bt22AnchorDaoImpl implements IBt22AnchorDao {
    // 注入Mapper接口
    @Autowired
    private Bt22AnchorMapper bt22AnchorMapper;

    /**
     * 插入锚节点数据
     *
     * @param record 锚节点实体类
     * @return 插入的行数
     */
    @Override
    public int insert(Bt22Anchor record) {
        return bt22AnchorMapper.insert(record);
    }

    @Override
    public int updateByid(Bt22Anchor local) {
        return bt22AnchorMapper.updateByPrimaryKeySelective(local);
    }

    @Override
    public List<Bt22Anchor> getUserAnchor(Long id) {
        return bt22AnchorMapper.selectByUserId(id);
    }

    @Override
    public void truncateTable() {
        bt22AnchorMapper.truncateTable();
    }

    @Override
    public List<Bt22Anchor> getAllAnchor() {
        return bt22AnchorMapper.selectAllAnchor();
    }

    @Override
    public List<Bt22Anchor> selectBySublocationUid(String sublocationUid) {
        return bt22AnchorMapper.selectBySublocationUid(sublocationUid);
    }

    @Override
    public List<BaseNodeType> selectBySublocationUidGroupByType(String sublocationUid) {
        return bt22AnchorMapper.selectBySublocationUidGroupByType(sublocationUid);
    }

}
