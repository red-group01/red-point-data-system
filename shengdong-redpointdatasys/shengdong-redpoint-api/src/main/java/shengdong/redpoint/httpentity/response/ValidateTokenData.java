package shengdong.redpoint.httpentity.response;

import lombok.Data;

@Data
public class ValidateTokenData {
    private String email;
    private String token;
}
