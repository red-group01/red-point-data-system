package shengdong.redpoint.httpentity.request;

import lombok.Data;

@Data
public class Types{
	private boolean phone;
	private boolean email;
}