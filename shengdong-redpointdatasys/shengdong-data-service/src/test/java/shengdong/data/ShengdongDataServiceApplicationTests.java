package shengdong.data;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;
import shengdong.data.dao.*;
import shengdong.data.entity.*;
import shengdong.data.service.IBaseInfoService;
import shengdong.data.service.IDataService;
import shengdong.redpoint.api.IotApiConfig;
import shengdong.redpoint.model.query.Header;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
class ShengdongDataServiceApplicationTests {

    @Autowired
    IBt11CompanyDao dao_comp;
    @Autowired
    IBt13RoleDao dao_role;
    @Autowired
    IBt14UserDao dao_user;

    /**
     * 红点接口获取的入力数据为空
     */
    @Test
    void T1001() throws Exception {
        log.info("T1001");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setAuthApiLogin("shengdong/api/auth/log_in/t0001");
        dao_comp.truncateTable();
        dao_role.truncateTable();
        dao_user.truncateTable();

        long uc = dao_user.getAll().size();
        long cc = dao_comp.getAll().size();
        long rc = dao_role.getAll().size();

        Bt14User uent = new Bt14User();
        uent.setEmail("admin");
        uent.setPassword("admin");
        dao_user.insert(uent);
        uent = dao_user.getUserByEmail("admin");

        synservice.synUser(cnf, uent);
        long uc1 = dao_user.getAll().size();
        long cc1 = dao_comp.getAll().size();
        long rc1 = dao_role.getAll().size();
        Assert.isTrue(uc1 > uc && cc1 > cc && rc1 > rc, "用户同步失败");
    }

    /**
     * 红点接口获取的入力数据为数据库字段最大值 包含英文大小写及汉字
     */
    @Test
    void T1002() throws Exception {

        log.info("T1002");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setAuthApiLogin("shengdong/api/auth/log_in/t0002");
        dao_comp.truncateTable();
        dao_role.truncateTable();
        dao_user.truncateTable();

        long uc = dao_user.getAll().size();
        long cc = dao_comp.getAll().size();
        long rc = dao_role.getAll().size();

        Bt14User uent = new Bt14User();
        uent.setEmail("admin");
        uent.setPassword("admin");
        dao_user.insert(uent);
        uent = dao_user.getUserByEmail("admin");

        synservice.synUser(cnf, uent);
        long uc1 = dao_user.getAll().size();
        long cc1 = dao_comp.getAll().size();
        long rc1 = dao_role.getAll().size();
        Assert.isTrue(uc1 > uc && cc1 > cc && rc1 > rc, "用户同步失败");

    }

    /**
     * 红点接口获取的入力数据为数据库字段要求内的值
     */
    @Test
    void T1003() throws Exception {

        log.info("T1003");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setAuthApiLogin("shengdong/api/auth/log_in/t0003");
        dao_comp.truncateTable();
        dao_role.truncateTable();
        dao_user.truncateTable();

        long uc = dao_user.getAll().size();
        long cc = dao_comp.getAll().size();
        long rc = dao_role.getAll().size();

        Bt14User uent = new Bt14User();
        uent.setEmail("admin");
        uent.setPassword("admin");
        dao_user.insert(uent);
        uent = dao_user.getUserByEmail("admin");

        synservice.synUser(cnf, uent);
        long uc1 = dao_user.getAll().size();
        long cc1 = dao_comp.getAll().size();
        long rc1 = dao_role.getAll().size();
        Assert.isTrue(uc1 > uc && cc1 > cc && rc1 > rc, "用户同步失败");

    }

    /**
     * 红点接口获取的入力数据包含特殊字符
     */
    @Test
    void T1004() throws Exception {

        log.info("T1004");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setAuthApiLogin("shengdong/api/auth/log_in/t0004");
        dao_comp.truncateTable();
        dao_role.truncateTable();
        dao_user.truncateTable();

        long uc = dao_user.getAll().size();
        long cc = dao_comp.getAll().size();
        long rc = dao_role.getAll().size();

        Bt14User uent = new Bt14User();
        uent.setEmail("admin");
        uent.setPassword("admin");
        dao_user.insert(uent);
        uent = dao_user.getUserByEmail("admin");

        synservice.synUser(cnf, uent);
        long uc1 = dao_user.getAll().size();
        long cc1 = dao_comp.getAll().size();
        long rc1 = dao_role.getAll().size();
        Assert.isTrue(uc1 > uc && cc1 > cc && rc1 > rc, "用户同步失败");

    }

    /**
     * 红点api服务未启动，请求超时
     */
    @Test
    void T1005() {

        log.info("T1005");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8889");
        cnf.setAuthApiLogin("shengdong/api/auth/log_in/t0005");
        try {
            dao_comp.truncateTable();
            dao_role.truncateTable();
            dao_user.truncateTable();

            long uc = dao_user.getAll().size();
            long cc = dao_comp.getAll().size();
            long rc = dao_role.getAll().size();

            Bt14User uent = new Bt14User();
            uent.setEmail("admin");
            uent.setPassword("admin");
            dao_user.insert(uent);
            uent = dao_user.getUserByEmail("admin");

            synservice.synUser(cnf, uent);
            long uc1 = dao_user.getAll().size();
            long cc1 = dao_comp.getAll().size();
            long rc1 = dao_role.getAll().size();
            Assert.isTrue(uc1 > uc && cc1 > cc && rc1 > rc, "用户同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 红点api接口endpoint地址错误
     */
    @Test
    void T1006() {

        log.info("T1006");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setAuthApiLogin("shengdong/api/auth/log_in/t0006");
        try {
            dao_comp.truncateTable();
            dao_role.truncateTable();
            dao_user.truncateTable();

            long uc = dao_user.getAll().size();
            long cc = dao_comp.getAll().size();
            long rc = dao_role.getAll().size();

            Bt14User uent = new Bt14User();
            uent.setEmail("admin");
            uent.setPassword("admin");
            dao_user.insert(uent);
            uent = dao_user.getUserByEmail("admin");

            synservice.synUser(cnf, uent);
            long uc1 = dao_user.getAll().size();
            long cc1 = dao_comp.getAll().size();
            long rc1 = dao_role.getAll().size();
            Assert.isTrue(uc1 > uc && cc1 > cc && rc1 > rc, "用户同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 红点api接口返回错误404
     */
    @Test
    void T1007() {

        log.info("T1007");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setAuthApiLogin("shengdong/api/auth/log_in/t0007");
        try {
            dao_comp.truncateTable();
            dao_role.truncateTable();
            dao_user.truncateTable();

            long uc = dao_user.getAll().size();
            long cc = dao_comp.getAll().size();
            long rc = dao_role.getAll().size();

            Bt14User uent = new Bt14User();
            uent.setEmail("admin");
            uent.setPassword("admin");
            dao_user.insert(uent);
            uent = dao_user.getUserByEmail("admin");

            synservice.synUser(cnf, uent);
            long uc1 = dao_user.getAll().size();
            long cc1 = dao_comp.getAll().size();
            long rc1 = dao_role.getAll().size();
            Assert.isTrue(uc1 > uc && cc1 > cc && rc1 > rc, "用户同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 红点api接口返回错误422
     */
    @Test
    void T1008() {

        log.info("T1008");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setAuthApiLogin("shengdong/api/auth/log_in/t0008");
        try {
            dao_comp.truncateTable();
            dao_role.truncateTable();
            dao_user.truncateTable();

            long uc = dao_user.getAll().size();
            long cc = dao_comp.getAll().size();
            long rc = dao_role.getAll().size();

            Bt14User uent = new Bt14User();
            uent.setEmail("admin");
            uent.setPassword("admin");
            dao_user.insert(uent);
            uent = dao_user.getUserByEmail("admin");

            synservice.synUser(cnf, uent);
            long uc1 = dao_user.getAll().size();
            long cc1 = dao_comp.getAll().size();
            long rc1 = dao_role.getAll().size();
            Assert.isTrue(uc1 > uc && cc1 > cc && rc1 > rc, "用户同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Autowired
    IBt31ProjectDao dao_proj;

    /**
     * 红点接口获取的入力数据为空
     */
    @Test
    void T1009() throws Exception {

        log.info("T1009");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setProjectsApiGetProjects("shengdong/api/projects?id=1");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        dao_proj.trancateTable();
        int dbc = dao_proj.getUserProj(null).size();
        synservice.synProject(cnf, h, uent);
        int dbc1 = dao_proj.getUserProj(null).size();
        Assert.isTrue(dbc1 > dbc, "项目数据同步失败");
    }

    /**
     * 红点接口获取的入力数据为数据库字段最大值 包含英文大小写及汉字
     */
    @Test
    void T1010() throws Exception {

        log.info("T1010");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setProjectsApiGetProjects("shengdong/api/projects?id=2");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        dao_proj.trancateTable();
        int dbc = dao_proj.getUserProj(null).size();
        synservice.synProject(cnf, h, uent);
        int dbc1 = dao_proj.getUserProj(null).size();
        Assert.isTrue(dbc1 > dbc, "项目数据同步失败");
    }

    /**
     * 红点接口获取的入力数据为数据库字段要求内的值
     */
    @Test
    void T1011() throws Exception {

        log.info("T1011");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setProjectsApiGetProjects("shengdong/api/projects?id=3");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        dao_proj.trancateTable();
        int dbc = dao_proj.getUserProj(null).size();
        synservice.synProject(cnf, h, uent);
        int dbc1 = dao_proj.getUserProj(null).size();
        Assert.isTrue(dbc1 > dbc, "项目数据同步失败");
    }

    /**
     * 红点接口获取的入力数据包含特殊字符
     */
    @Test
    void T1012() throws Exception {

        log.info("T1012");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setProjectsApiGetProjects("shengdong/api/projects?id=4");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        dao_proj.trancateTable();
        int dbc = dao_proj.getUserProj(null).size();
        synservice.synProject(cnf, h, uent);
        int dbc1 = dao_proj.getUserProj(null).size();
        Assert.isTrue(dbc1 > dbc, "项目数据同步失败");
    }

    /**
     * 红点api服务未启动，请求超时
     */
    @Test
    void T1013() {

        log.info("T1013");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8889");
        cnf.setProjectsApiGetProjects("shengdong/api/projects?id=5");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        try {
            dao_proj.trancateTable();
            int dbc = dao_proj.getUserProj(null).size();

            synservice.synProject(cnf, h, uent);
            int dbc1 = dao_proj.getUserProj(null).size();
            Assert.isTrue(dbc1 > dbc, "项目数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 红点api接口endpoint地址错误
     */
    @Test
    void T1014() {

        log.info("T1014");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setProjectsApiGetProjects("shengdong/api/pro?id=6");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        try {
            dao_proj.trancateTable();
            int dbc = dao_proj.getUserProj(null).size();

            synservice.synProject(cnf, h, uent);
            int dbc1 = dao_proj.getUserProj(null).size();
            Assert.isTrue(dbc1 > dbc, "项目数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 红点api接口返回错误404
     */
    @Test
    void T1015() {

        log.info("T1015");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setProjectsApiGetProjects("shengdong/api/projects?id=7");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        try {
            dao_proj.trancateTable();
            int dbc = dao_proj.getUserProj(null).size();

            synservice.synProject(cnf, h, uent);
            int dbc1 = dao_proj.getUserProj(null).size();
            Assert.isTrue(dbc1 > dbc, "项目数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 红点api接口返回错误422
     */
    @Test
    void T1016() {

        log.info("T1016");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setProjectsApiGetProjects("shengdong/api/projects?id=8");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        try {
            dao_proj.trancateTable();
            int dbc = dao_proj.getUserProj(null).size();

            synservice.synProject(cnf, h, uent);
            int dbc1 = dao_proj.getUserProj(null).size();
            Assert.isTrue(dbc1 > dbc, "项目数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Autowired
    IBaseInfoService synservice;

    @Autowired
    IBt32LocationDao dao_location;

    /**
     * 红点接口获取的入力数据为空
     */
    @Test
    void T1017() throws Exception {
        log.info("T1017");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setLocationApiGetSites("shengdong/api/locations?id=1");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();

        dao_location.truncateTable();
        List<Bt32Location> db1 = dao_location.getAllLocation();
        synservice.synLocation(cnf, h, uent);
        List<Bt32Location> db2 = dao_location.getAllLocation();
        Assert.isTrue(db2.size() > db1.size(), "站点数据同步失败");
    }

    /**
     * 红点接口获取的入力数据为数据库字段最大值 包含英文大小写及汉字
     */
    @Test
    void T1018() throws Exception {
        log.info("T1018");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setLocationApiGetSites("shengdong/api/locations?id=2");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        dao_location.truncateTable();
        int dbc = dao_location.getAllLocation().size();
        synservice.synLocation(cnf, h, uent);
        int dbc1 = dao_location.getAllLocation().size();
        Assert.isTrue(dbc1 > dbc, "站点数据同步失败");
    }

    /**
     * 红点接口获取的入力数据为数据库字段要求内的值
     */
    @Test
    void T1019() throws Exception {
        log.info("T1019");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setLocationApiGetSites("shengdong/api/locations?id=3");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        dao_location.truncateTable();
        int dbc = dao_location.getAllLocation().size();
        synservice.synLocation(cnf, h, uent);
        int dbc1 = dao_location.getAllLocation().size();
        Assert.isTrue(dbc1 > dbc, "站点数据同步失败");
    }

    /**
     * 红点接口获取的入力数据包含特殊字符
     */
    @Test
    void T1020() throws Exception {
        log.info("T1020");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setLocationApiGetSites("shengdong/api/locations?id=4");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        dao_location.truncateTable();
        int dbc = dao_location.getAllLocation().size();
        synservice.synLocation(cnf, h, uent);
        int dbc1 = dao_location.getAllLocation().size();
        Assert.isTrue(dbc1 > dbc, "站点数据同步失败");
    }

    /**
     * 红点api服务未启动，请求超时
     */
    @Test
    void T1021() {
        log.info("T1021");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8889");
        cnf.setLocationApiGetSites("shengdong/api/locations?id=5");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        try {
            dao_location.truncateTable();
            int dbc = dao_location.getAllLocation().size();
            synservice.synLocation(cnf, h, uent);
            int dbc1 = dao_location.getAllLocation().size();
            Assert.isTrue(dbc1 > dbc, "站点数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 红点api接口endpoint地址错误
     */
    @Test
    void T1022() {
        log.info("T1022");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setLocationApiGetSites("shengdong/api/locations?id=6");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        try {
            dao_location.truncateTable();
            int dbc = dao_location.getAllLocation().size();
            synservice.synLocation(cnf, h, uent);
            int dbc1 = dao_location.getAllLocation().size();
            Assert.isTrue(dbc1 > dbc, "站点数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 红点api接口返回错误404
     */
    @Test
    void T1023() {
        log.info("T1023");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setLocationApiGetSites("shengdong/api/locations?id=7");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        try {
            dao_location.truncateTable();
            int dbc = dao_location.getAllLocation().size();
            synservice.synLocation(cnf, h, uent);
            int dbc1 = dao_location.getAllLocation().size();
            Assert.isTrue(dbc1 > dbc, "站点数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 红点api接口返回错误422
     */
    @Test
    void T1024() {
        log.info("T1024");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setLocationApiGetSites("shengdong/api/locations?id=8");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        try {
            dao_location.truncateTable();
            int dbc = dao_location.getAllLocation().size();
            synservice.synLocation(cnf, h, uent);
            int dbc1 = dao_location.getAllLocation().size();
            Assert.isTrue(dbc1 > dbc, "站点数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Autowired
    IBt33SubLocationDao daosublocation;

    /**
     * 红点接口获取的入力数据为空
     */
    @Test
    void T1025() throws Exception {
        log.info("T1025");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setSublocationsApiGetArea("shengdong/api/sublocations?id=1");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        daosublocation.truncateTable();
        int dbc = daosublocation.getAllSublocation().size();
        synservice.synSublocation(cnf, h, uent);
        int dbc1 = daosublocation.getAllSublocation().size();
        Assert.isTrue(dbc1 > dbc, "子站点数据同步失败");
    }

    /**
     * "红点接口获取的入力数据为数据库字段最大值 包含英文大小写及汉字"
     */
    @Test
    void T1026() throws Exception {
        log.info("T1026");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setSublocationsApiGetArea("shengdong/api/sublocations?id=2");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        daosublocation.truncateTable();
        int dbc = daosublocation.getAllSublocation().size();
        synservice.synSublocation(cnf, h, uent);
        int dbc1 = daosublocation.getAllSublocation().size();
        Assert.isTrue(dbc1 > dbc, "子站点数据同步失败");
    }

    /**
     * 红点接口获取的入力数据为数据库字段要求内的值
     */
    @Test
    void T1027() throws Exception {
        log.info("T1027");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setSublocationsApiGetArea("shengdong/api/sublocations?id=3");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        daosublocation.truncateTable();
        int dbc = daosublocation.getAllSublocation().size();
        synservice.synSublocation(cnf, h, uent);
        int dbc1 = daosublocation.getAllSublocation().size();
        Assert.isTrue(dbc1 > dbc, "子站点数据同步失败");
    }

    /**
     * 红点接口获取的入力数据包含特殊字符
     */
    @Test
    void T1028() throws Exception {
        log.info("T1028");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setSublocationsApiGetArea("shengdong/api/sublocations?id=4");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        daosublocation.truncateTable();
        int dbc = daosublocation.getAllSublocation().size();
        synservice.synSublocation(cnf, h, uent);
        int dbc1 = daosublocation.getAllSublocation().size();
        Assert.isTrue(dbc1 > dbc, "子站点数据同步失败");
    }

    /**
     * 红点api服务未启动，请求超时
     */
    @Test
    void T1029() {
        log.info("T1029");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8889");
        cnf.setSublocationsApiGetArea("shengdong/api/sublocations?id=5");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        try {
            daosublocation.truncateTable();
            int dbc = daosublocation.getAllSublocation().size();
            synservice.synSublocation(cnf, h, uent);
            int dbc1 = daosublocation.getAllSublocation().size();
            Assert.isTrue(dbc1 > dbc, "子站点数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 红点api接口endpoint地址错误
     */
    @Test
    void T1030() {
        log.info("T1030");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setSublocationsApiGetArea("shengdong/api/sub?id=6");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        try {
            daosublocation.truncateTable();
            int dbc = daosublocation.getAllSublocation().size();
            synservice.synSublocation(cnf, h, uent);
            int dbc1 = daosublocation.getAllSublocation().size();
            Assert.isTrue(dbc1 > dbc, "子站点数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 红点api接口返回错误404
     */
    @Test
    void T1031() {
        log.info("T1031");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setSublocationsApiGetArea("shengdong/api/sublocations?id=7");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        try {
            daosublocation.truncateTable();
            int dbc = daosublocation.getAllSublocation().size();
            synservice.synSublocation(cnf, h, uent);
            int dbc1 = daosublocation.getAllSublocation().size();
            Assert.isTrue(dbc1 > dbc, "子站点数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 红点api接口返回错误422
     */
    @Test
    void T1032() {
        log.info("T1032");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setSublocationsApiGetArea("shengdong/api/sublocations?id=8");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        try {
            daosublocation.truncateTable();
            int dbc = daosublocation.getAllSublocation().size();
            synservice.synSublocation(cnf, h, uent);
            int dbc1 = daosublocation.getAllSublocation().size();
            Assert.isTrue(dbc1 > dbc, "子站点数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Autowired
    IBt21NodeDao daonode;

    /**
     * 红点接口获取的入力数据为空
     */
    @Test
    void T1033() throws Exception {
        log.info("T1033");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setNodesApiGetNodes("shengdong/api/nodes?id=1");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        daonode.truncateTable();
        int dbc = daonode.getAllNode().size();
        synservice.synNode(cnf, h, uent);
        int dbc1 = daonode.getAllNode().size();
        Assert.isTrue(dbc1 > dbc, "节点数据同步失败");
    }

    /**
     * 红点接口获取的入力数据为数据库字段最大值 包含英文大小写及汉字
     */
    @Test
    void T1034() throws Exception {
        log.info("T1034");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setNodesApiGetNodes("shengdong/api/nodes?id=2");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        daonode.truncateTable();
        int dbc = daonode.getAllNode().size();
        synservice.synNode(cnf, h, uent);
        int dbc1 = daonode.getAllNode().size();
        Assert.isTrue(dbc1 > dbc, "节点数据同步失败");
    }

    /**
     * 红点接口获取的入力数据为数据库字段要求内的值
     */
    @Test
    void T1035() throws Exception {
        log.info("T1035");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setNodesApiGetNodes("shengdong/api/nodes?id=3");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        daonode.truncateTable();
        int dbc = daonode.getAllNode().size();
        synservice.synNode(cnf, h, uent);
        int dbc1 = daonode.getAllNode().size();
        Assert.isTrue(dbc1 > dbc, "节点数据同步失败");
    }

    /**
     * 红点接口获取的入力数据包含特殊字符
     */
    @Test
    void T1036() throws Exception {
        log.info("T1036");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setNodesApiGetNodes("shengdong/api/nodes?id=4");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        daonode.truncateTable();
        int dbc = daonode.getAllNode().size();
        synservice.synNode(cnf, h, uent);
        int dbc1 = daonode.getAllNode().size();
        Assert.isTrue(dbc1 > dbc, "节点数据同步失败");
    }

    /**
     * 红点api服务未启动，请求超时
     */
    @Test
    void T1037() {
        log.info("T1037");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8889");
        cnf.setNodesApiGetNodes("shengdong/api/nodes?id=5");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        try {
            daonode.truncateTable();
            int dbc = daonode.getAllNode().size();
            synservice.synNode(cnf, h, uent);
            int dbc1 = daonode.getAllNode().size();
            Assert.isTrue(dbc1 > dbc, "节点数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 红点api接口endpoint地址错误
     */
    @Test
    void T1038() {
        log.info("T1038");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setNodesApiGetNodes("shengdong/api/nod?id=6");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        try {
            daonode.truncateTable();
            int dbc = daonode.getAllNode().size();
            synservice.synNode(cnf, h, uent);
            int dbc1 = daonode.getAllNode().size();
            Assert.isTrue(dbc1 > dbc, "节点数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 红点api接口返回错误404
     */
    @Test
    void T1039() {
        log.info("T1039");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setNodesApiGetNodes("shengdong/api/nodes?id=7");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        try {
            daonode.truncateTable();
            int dbc = daonode.getAllNode().size();
            synservice.synNode(cnf, h, uent);
            int dbc1 = daonode.getAllNode().size();
            Assert.isTrue(dbc1 > dbc, "节点数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 红点api接口返回错误422
     */
    @Test
    void T1040() {
        log.info("T1040");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setNodesApiGetNodes("shengdong/api/nodes?id=8");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        try {
            daonode.truncateTable();
            int dbc = daonode.getAllNode().size();
            synservice.synNode(cnf, h, uent);
            int dbc1 = daonode.getAllNode().size();
            Assert.isTrue(dbc1 > dbc, "节点数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Autowired
    IBt22AnchorDao daoanchor;

    /**
     * 红点接口获取的入力数据为空
     */
    @Test
    void T1041() throws Exception {
        log.info("T1041");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setWinAnchorsApiGetWinAnchors("shengdong/api/win_anchors?id=1");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        daoanchor.truncateTable();
        int dbc = daoanchor.getAllAnchor().size();
        synservice.synAnchor(cnf, h, uent);
        int dbc1 = daoanchor.getAllAnchor().size();
        Assert.isTrue(dbc1 > dbc, "锚数据同步失败");
    }

    /**
     * 红点接口获取的入力数据为数据库字段最大值 包含英文大小写及汉字
     */
    @Test
    void T1042() throws Exception {
        log.info("T1042");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setWinAnchorsApiGetWinAnchors("shengdong/api/win_anchors?id=2");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        daoanchor.truncateTable();
        int dbc = daoanchor.getAllAnchor().size();
        synservice.synAnchor(cnf, h, uent);
        int dbc1 = daoanchor.getAllAnchor().size();
        Assert.isTrue(dbc1 > dbc, "锚数据同步失败");
    }

    /**
     * 红点接口获取的入力数据为数据库字段要求内的值
     */
    @Test
    void T1043() throws Exception {
        log.info("T1043");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setWinAnchorsApiGetWinAnchors("shengdong/api/win_anchors?id=3");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        daoanchor.truncateTable();
        int dbc = daoanchor.getAllAnchor().size();
        synservice.synAnchor(cnf, h, uent);
        int dbc1 = daoanchor.getAllAnchor().size();
        Assert.isTrue(dbc1 > dbc, "锚数据同步失败");
    }

    /**
     * 红点接口获取的入力数据包含特殊字符
     */
    @Test
    void T1044() throws Exception {
        log.info("T1044");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setWinAnchorsApiGetWinAnchors("shengdong/api/win_anchors?id=4");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        daoanchor.truncateTable();
        int dbc = daoanchor.getAllAnchor().size();
        synservice.synAnchor(cnf, h, uent);
        int dbc1 = daoanchor.getAllAnchor().size();
        Assert.isTrue(dbc1 > dbc, "锚数据同步失败");
    }

    /**
     * 红点api服务未启动，请求超时
     */
    @Test
    void T1045() {
        log.info("T1045");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8889");
        cnf.setWinAnchorsApiGetWinAnchors("shengdong/api/win_anchors?id=5");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        try {
            daoanchor.truncateTable();
            List<Bt22Anchor> db1 = daoanchor.getAllAnchor();
            synservice.synAnchor(cnf, h, uent);
            List<Bt22Anchor> db2 = daoanchor.getAllAnchor();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 红点api接口endpoint地址错误
     */
    @Test
    void T1046() {
        log.info("T1046");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setWinAnchorsApiGetWinAnchors("shengdong/api/win?id=6");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        try {
            daoanchor.truncateTable();
            List<Bt22Anchor> db1 = daoanchor.getAllAnchor();
            synservice.synAnchor(cnf, h, uent);
            List<Bt22Anchor> db2 = daoanchor.getAllAnchor();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 红点api接口返回错误404
     */
    @Test
    void T1047() {
        log.info("T1047");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setWinAnchorsApiGetWinAnchors("shengdong/api/win_anchors?id=7");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        try {
            daoanchor.truncateTable();
            List<Bt22Anchor> db1 = daoanchor.getAllAnchor();
            synservice.synAnchor(cnf, h, uent);
            List<Bt22Anchor> db2 = daoanchor.getAllAnchor();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 红点api接口返回错误422
     */
    @Test
    void T1048() {
        log.info("T1048");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setWinAnchorsApiGetWinAnchors("shengdong/api/win_anchors?id=8");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        try {
            daoanchor.truncateTable();
            List<Bt22Anchor> db1 = daoanchor.getAllAnchor();
            synservice.synAnchor(cnf, h, uent);
            List<Bt22Anchor> db2 = daoanchor.getAllAnchor();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Autowired
    IBt23TrackableDao daotrackable;

    /**
     * 红点接口获取的入力数据为空
     */
    @Test
    void T1049() throws Exception {
        log.info("T1049");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setTrackablesApiGetTrackables("shengdong/api/trackable_objects?id=1");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        daotrackable.truncateTable();
        int dbc = daotrackable.getAllTrackable().size();
        synservice.synTrackable(cnf, h, uent);
        int dbc1 = daotrackable.getAllTrackable().size();
        Assert.isTrue(dbc1 > dbc, "标签数据同步失败");
    }

    /**
     * 红点接口获取的入力数据为数据库字段最大值 包含英文大小写及汉字
     */
    @Test
    void T1050() throws Exception {
        log.info("T1050");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setTrackablesApiGetTrackables("shengdong/api/trackable_objects?id=2");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        daotrackable.truncateTable();
        int dbc = daotrackable.getAllTrackable().size();
        synservice.synTrackable(cnf, h, uent);
        int dbc1 = daotrackable.getAllTrackable().size();
        Assert.isTrue(dbc1 > dbc, "标签数据同步失败");
    }

    /**
     * 红点接口获取的入力数据为数据库字段要求内的值
     */
    @Test
    void T1051() throws Exception {
        log.info("T1051");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setTrackablesApiGetTrackables("shengdong/api/trackable_objects?id=3");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        daotrackable.truncateTable();
        int dbc = daotrackable.getAllTrackable().size();
        synservice.synTrackable(cnf, h, uent);
        int dbc1 = daotrackable.getAllTrackable().size();
        Assert.isTrue(dbc1 > dbc, "标签数据同步失败");
    }

    /**
     * 红点接口获取的入力数据包含特殊字符
     */
    @Test
    void T1052() throws Exception {
        log.info("T1052");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setTrackablesApiGetTrackables("shengdong/api/trackable_objects?id=4");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        daotrackable.truncateTable();
        int dbc = daotrackable.getAllTrackable().size();
        synservice.synTrackable(cnf, h, uent);
        int dbc1 = daotrackable.getAllTrackable().size();
        Assert.isTrue(dbc1 > dbc, "标签数据同步失败");
    }

    /**
     * 红点api服务未启动，请求超时
     */
    @Test
    void T1053() {
        log.info("T1053");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8889");
        cnf.setTrackablesApiGetTrackables("shengdong/api/trackable_objects?id=5");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        try {
            daotrackable.truncateTable();
            int dbc = daotrackable.getAllTrackable().size();
            synservice.synTrackable(cnf, h, uent);
            int dbc1 = daotrackable.getAllTrackable().size();
            Assert.isTrue(dbc1 > dbc, "标签数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 红点api接口endpoint地址错误
     */
    @Test
    void T1054() {
        log.info("T1054");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setTrackablesApiGetTrackables("shengdong/api/tra?id=6");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        try {
            daotrackable.truncateTable();
            int dbc = daotrackable.getAllTrackable().size();
            synservice.synTrackable(cnf, h, uent);
            int dbc1 = daotrackable.getAllTrackable().size();
            Assert.isTrue(dbc1 > dbc, "标签数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 红点api接口返回错误404
     */
    @Test
    void T1055() {
        log.info("T1055");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setTrackablesApiGetTrackables("shengdong/api/trackable_objects?id=7");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        try {
            daotrackable.truncateTable();
            int dbc = daotrackable.getAllTrackable().size();
            synservice.synTrackable(cnf, h, uent);
            int dbc1 = daotrackable.getAllTrackable().size();
            Assert.isTrue(dbc1 > dbc, "标签数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 红点api接口返回错误422
     */
    @Test
    void T1056() {
        log.info("T1056");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setTrackablesApiGetTrackables("shengdong/api/trackable_objects?id=8");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        try {
            daotrackable.truncateTable();
            int dbc = daotrackable.getAllTrackable().size();
            synservice.synTrackable(cnf, h, uent);
            int dbc1 = daotrackable.getAllTrackable().size();
            Assert.isTrue(dbc1 > dbc, "标签数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Autowired
    IBt34ZoneDao daozone;

    /**
     * 红点接口获取的入力数据为空
     */
    @Test
    void T1057() throws Exception {
        log.info("T1057");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setZonesApiGetAllZonesAndBuffers("shengdong/api/zones?id=1");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        daozone.truncateTable();
        int dbc = daozone.getAllZone().size();
        synservice.synZone(cnf, h, uent);
        int dbc1 = daozone.getAllZone().size();
        Assert.isTrue(dbc1 > dbc, "区域围栏数据同步失败");
    }

    /**
     * 红点接口获取的入力数据为数据库字段最大值 包含英文大小写及汉字
     */
    @Test
    void T1058() throws Exception {
        log.info("T1058");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setZonesApiGetAllZonesAndBuffers("shengdong/api/zones?id=2");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        daozone.truncateTable();
        int dbc = daozone.getAllZone().size();
        synservice.synZone(cnf, h, uent);
        int dbc1 = daozone.getAllZone().size();
        Assert.isTrue(dbc1 > dbc, "区域围栏数据同步失败");
    }

    /**
     * 红点接口获取的入力数据为数据库字段要求内的值
     */
    @Test
    void T1059() throws Exception {
        log.info("T1059");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setZonesApiGetAllZonesAndBuffers("shengdong/api/zones?id=3");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        daozone.truncateTable();
        int dbc = daozone.getAllZone().size();
        synservice.synZone(cnf, h, uent);
        int dbc1 = daozone.getAllZone().size();
        Assert.isTrue(dbc1 > dbc, "区域围栏数据同步失败");
    }

    /**
     * 红点接口获取的入力数据包含特殊字符
     */
    @Test
    void T1060() throws Exception {
        log.info("T1060");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setZonesApiGetAllZonesAndBuffers("shengdong/api/zones?id=4");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        daozone.truncateTable();
        int dbc = daozone.getAllZone().size();
        synservice.synZone(cnf, h, uent);
        int dbc1 = daozone.getAllZone().size();
        Assert.isTrue(dbc1 > dbc, "区域围栏数据同步失败");
    }

    /**
     * 红点api服务未启动，请求超时
     */
    @Test
    void T1061() {
        log.info("T1061");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8889");
        cnf.setZonesApiGetAllZonesAndBuffers("shengdong/api/zones?id=5");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        try {
            daozone.truncateTable();
            int dbc = daozone.getAllZone().size();
            synservice.synZone(cnf, h, uent);
            int dbc1 = daozone.getAllZone().size();
            Assert.isTrue(dbc1 > dbc, "区域围栏数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 红点api接口endpoint地址错误
     */
    @Test
    void T1062() {
        log.info("T1062");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setZonesApiGetAllZonesAndBuffers("shengdong/api/zon?id=6");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        try {
            daozone.truncateTable();
            int dbc = daozone.getAllZone().size();
            synservice.synZone(cnf, h, uent);
            int dbc1 = daozone.getAllZone().size();
            Assert.isTrue(dbc1 > dbc, "区域围栏数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 红点api接口返回错误404
     */
    @Test
    void T1063() {
        log.info("T1063");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setZonesApiGetAllZonesAndBuffers("shengdong/api/zones?id=7");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        try {
            daozone.truncateTable();
            int dbc = daozone.getAllZone().size();
            synservice.synZone(cnf, h, uent);
            int dbc1 = daozone.getAllZone().size();
            Assert.isTrue(dbc1 > dbc, "区域围栏数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 红点api接口返回错误422
     */
    @Test
    void T1064() {
        log.info("T1064");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setZonesApiGetAllZonesAndBuffers("shengdong/api/zones?id=8");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        Bt14User uent = new Bt14User();
        try {
            daozone.truncateTable();
            int dbc = daozone.getAllZone().size();
            synservice.synZone(cnf, h, uent);
            int dbc1 = daozone.getAllZone().size();
            Assert.isTrue(dbc1 > dbc, "区域围栏数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Autowired
    IDataService syndata;
    @Autowired
    IBt43ZoneEventDao daoZoneEvent;

    /**
     * 红点接口获取的入力数据为空
     */
    @Test
    void T1065() throws Exception {
        log.info("T1065");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setZoneEventsApiGetZoneEvents("shengdong/api/zone_events?id=1");
        Bt50PoinsHisView h = new Bt50PoinsHisView();
        daoZoneEvent.truncateTable();
        int dbc = daoZoneEvent.getAllZoneEvent(h.getProjectUid()).size();
        syndata.synZoneEvents(h, cnf);
        int dbc1 = daoZoneEvent.getAllZoneEvent(h.getProjectUid()).size();
        Assert.isTrue(dbc1 > dbc, "区域事件数据同步失败");
    }

    /**
     * "红点接口获取的入力数据为数据库字段最大值 包含英文大小写及汉字"
     */
    @Test
    void T1066() throws Exception {
        log.info("T1066");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setZoneEventsApiGetZoneEvents("shengdong/api/zone_events?id=2");
        Bt50PoinsHisView h = new Bt50PoinsHisView();
        daoZoneEvent.truncateTable();
        int dbc = daoZoneEvent.getAllZoneEvent(h.getProjectUid()).size();
        syndata.synZoneEvents(h, cnf);
        int dbc1 = daoZoneEvent.getAllZoneEvent(h.getProjectUid()).size();
        Assert.isTrue(dbc1 > dbc, "区域事件数据同步失败");
    }

    /**
     * 红点接口获取的入力数据为数据库字段要求内的值
     */
    @Test
    void T1067() throws Exception {
        log.info("T1067");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setZoneEventsApiGetZoneEvents("shengdong/api/zone_events?id=3");
        Bt50PoinsHisView h = new Bt50PoinsHisView();
        daoZoneEvent.truncateTable();
        int dbc = daoZoneEvent.getAllZoneEvent(h.getProjectUid()).size();
        syndata.synZoneEvents(h, cnf);
        int dbc1 = daoZoneEvent.getAllZoneEvent(h.getProjectUid()).size();
        Assert.isTrue(dbc1 > dbc, "区域事件数据同步失败");
    }

    /**
     * 红点接口获取的入力数据包含特殊字符
     */
    @Test
    void T1068() throws Exception {
        log.info("T1068");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setZoneEventsApiGetZoneEvents("shengdong/api/zone_events?id=4");
        Bt50PoinsHisView h = new Bt50PoinsHisView();
        daoZoneEvent.truncateTable();
        int dbc = daoZoneEvent.getAllZoneEvent(h.getProjectUid()).size();
        syndata.synZoneEvents(h, cnf);
        int dbc1 = daoZoneEvent.getAllZoneEvent(h.getProjectUid()).size();
        Assert.isTrue(dbc1 > dbc, "区域事件数据同步失败");
    }

    /**
     * 红点api服务未启动，请求超时
     */
    @Test
    void T1069() {
        log.info("T1069");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8889");
        cnf.setZoneEventsApiGetZoneEvents("shengdong/api/zone_events?id=5");
        try {
            Bt50PoinsHisView h = new Bt50PoinsHisView();
            daoZoneEvent.truncateTable();
            int dbc = daoZoneEvent.getAllZoneEvent(h.getProjectUid()).size();
            syndata.synZoneEvents(h, cnf);
            int dbc1 = daoZoneEvent.getAllZoneEvent(h.getProjectUid()).size();
            Assert.isTrue(dbc1 > dbc, "区域事件数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 红点api接口endpoint地址错误
     */
    @Test
    void T1070() {
        log.info("T1070");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setZoneEventsApiGetZoneEvents("shengdong/api/zone?id=6");
        try {
            Bt50PoinsHisView h = new Bt50PoinsHisView();
            daoZoneEvent.truncateTable();
            int dbc = daoZoneEvent.getAllZoneEvent(h.getProjectUid()).size();
            syndata.synZoneEvents(h, cnf);
            int dbc1 = daoZoneEvent.getAllZoneEvent(h.getProjectUid()).size();
            Assert.isTrue(dbc1 > dbc, "区域事件数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 红点api接口返回错误404
     */
    @Test
    void T1071() {
        log.info("T1071");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setZoneEventsApiGetZoneEvents("shengdong/api/zone?id=7");
        try {
            Bt50PoinsHisView h = new Bt50PoinsHisView();
            daoZoneEvent.truncateTable();
            int dbc = daoZoneEvent.getAllZoneEvent(h.getProjectUid()).size();
            syndata.synZoneEvents(h, cnf);
            int dbc1 = daoZoneEvent.getAllZoneEvent(h.getProjectUid()).size();
            Assert.isTrue(dbc1 > dbc, "区域事件数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 红点api接口返回错误422
     */
    @Test
    void T1072() {
        log.info("T1072");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setZoneEventsApiGetZoneEvents("shengdong/api/zone?id=8");
        try {
            Bt50PoinsHisView h = new Bt50PoinsHisView();
            daoZoneEvent.truncateTable();
            int dbc = daoZoneEvent.getAllZoneEvent(h.getProjectUid()).size();
            syndata.synZoneEvents(h, cnf);
            int dbc1 = daoZoneEvent.getAllZoneEvent(h.getProjectUid()).size();
            Assert.isTrue(dbc1 > dbc, "区域事件数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Autowired
    IBt44AlarmDao daoAlarm;

    /**
     * 红点接口获取的入力数据为空
     */
    @Test
    void T1073() throws Exception {
        log.info("T1073");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setAlarmsApiGetAlarms("shengdong/api/alarms?id=1");
        Bt50PoinsHisView h = new Bt50PoinsHisView();

        daoAlarm.truncateTable();
        int dbc = daoAlarm.getAllAlarmHis(h.getProjectUid()).size();
        syndata.synAlarmsHis(h, cnf);
        int dbc1 = daoAlarm.getAllAlarmHis(h.getProjectUid()).size();
        Assert.isTrue(dbc1 > dbc, "报警数据同步失败");
    }

    /**
     * 红点接口获取的入力数据为数据库字段最大值 包含英文大小写及汉字
     */
    @Test
    void T1074() throws Exception {
        log.info("T1074");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setAlarmsApiGetAlarms("shengdong/api/alarms?id=2");
        Bt50PoinsHisView h = new Bt50PoinsHisView();

        daoAlarm.truncateTable();
        int dbc = daoAlarm.getAllAlarmHis(h.getProjectUid()).size();
        syndata.synAlarmsHis(h, cnf);
        int dbc1 = daoAlarm.getAllAlarmHis(h.getProjectUid()).size();
        Assert.isTrue(dbc1 > dbc, "报警数据同步失败");
    }

    /**
     * 红点接口获取的入力数据为数据库字段要求内的值
     */
    @Test
    void T1075() throws Exception {
        log.info("T1075");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setAlarmsApiGetAlarms("shengdong/api/alarms?id=3");
        Bt50PoinsHisView h = new Bt50PoinsHisView();

        daoAlarm.truncateTable();
        int dbc = daoAlarm.getAllAlarmHis(h.getProjectUid()).size();
        syndata.synAlarmsHis(h, cnf);
        int dbc1 = daoAlarm.getAllAlarmHis(h.getProjectUid()).size();
        Assert.isTrue(dbc1 > dbc, "报警数据同步失败");
    }

    /**
     * 红点接口获取的入力数据包含特殊字符
     */
    @Test
    void T1076() throws Exception {
        log.info("T1076");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setAlarmsApiGetAlarms("shengdong/api/alarms?id=4");
        Bt50PoinsHisView h = new Bt50PoinsHisView();

        daoAlarm.truncateTable();
        int dbc = daoAlarm.getAllAlarmHis(h.getProjectUid()).size();
        syndata.synAlarmsHis(h, cnf);
        int dbc1 = daoAlarm.getAllAlarmHis(h.getProjectUid()).size();
        Assert.isTrue(dbc1 > dbc, "报警数据同步失败");
    }

    /**
     * 红点api服务未启动，请求超时
     */
    @Test
    void T1077() {
        log.info("T1077");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8889");
        cnf.setAlarmsApiGetAlarms("shengdong/api/alarms?id=5");
        Bt50PoinsHisView h = new Bt50PoinsHisView();

        try {
            daoAlarm.truncateTable();
            int dbc = daoAlarm.getAllAlarmHis(h.getProjectUid()).size();
            syndata.synAlarmsHis(h, cnf);
            int dbc1 = daoAlarm.getAllAlarmHis(h.getProjectUid()).size();
            Assert.isTrue(dbc1 > dbc, "报警数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 红点api接口endpoint地址错误
     */
    @Test
    void T1078() {
        log.info("T1078");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setAlarmsApiGetAlarms("shengdong/api/ala?id=6");
        Bt50PoinsHisView h = new Bt50PoinsHisView();

        try {
            daoAlarm.truncateTable();
            int dbc = daoAlarm.getAllAlarmHis(h.getProjectUid()).size();
            syndata.synAlarmsHis(h, cnf);
            int dbc1 = daoAlarm.getAllAlarmHis(h.getProjectUid()).size();
            Assert.isTrue(dbc1 > dbc, "报警数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 红点api接口返回错误404
     */
    @Test
    void T1079() {
        log.info("T1079");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setAlarmsApiGetAlarms("shengdong/api/alarms?id=7");
        Bt50PoinsHisView h = new Bt50PoinsHisView();

        try {
            daoAlarm.truncateTable();
            int dbc = daoAlarm.getAllAlarmHis(h.getProjectUid()).size();
            syndata.synAlarmsHis(h, cnf);
            int dbc1 = daoAlarm.getAllAlarmHis(h.getProjectUid()).size();
            Assert.isTrue(dbc1 > dbc, "报警数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 红点api接口返回错误422
     */
    @Test
    void T1080() {
        log.info("T1080");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setAlarmsApiGetAlarms("shengdong/api/alarms?id=8");
        Bt50PoinsHisView h = new Bt50PoinsHisView();

        try {
            daoAlarm.truncateTable();
            int dbc = daoAlarm.getAllAlarmHis(h.getProjectUid()).size();
            syndata.synAlarmsHis(h, cnf);
            int dbc1 = daoAlarm.getAllAlarmHis(h.getProjectUid()).size();
            Assert.isTrue(dbc1 > dbc, "报警数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Autowired
    IBt42NotificationsDao daoNotification;

    /**
     * 红点接口获取的入力数据为空
     */
    @Test
    void T1081() throws Exception {
        log.info("T1081");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setNotificationsApiGetNotifications("shengdong/api/notifications?id=1");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        daoNotification.truncateTable();
        int dbc = daoNotification.getAllNotifications(h.getProject()).size();
        Bt14User uent = new Bt14User();
        synservice.synNotifications(cnf, h, uent);
        int dbc1 = daoNotification.getAllNotifications(h.getProject()).size();
        Assert.isTrue(dbc1 > dbc, "通知数据同步失败");
    }

    /**
     * 红点接口获取的入力数据为数据库字段最大值 包含英文大小写及汉字
     */
    @Test
    void T1082() throws Exception {
        log.info("T1082");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setNotificationsApiGetNotifications("shengdong/api/notifications?id=2");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        daoNotification.truncateTable();
        int dbc = daoNotification.getAllNotifications(h.getProject()).size();
        Bt14User uent = new Bt14User();
        synservice.synNotifications(cnf, h, uent);
        int dbc1 = daoNotification.getAllNotifications(h.getProject()).size();
        Assert.isTrue(dbc1 > dbc, "通知数据同步失败");
    }

    /**
     * 红点接口获取的入力数据为数据库字段要求内的值
     */
    @Test
    void T1083() throws Exception {
        log.info("T1083");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setNotificationsApiGetNotifications("shengdong/api/notifications?id=3");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        daoNotification.truncateTable();
        int dbc = daoNotification.getAllNotifications(h.getProject()).size();
        Bt14User uent = new Bt14User();
        synservice.synNotifications(cnf, h, uent);
        int dbc1 = daoNotification.getAllNotifications(h.getProject()).size();
        Assert.isTrue(dbc1 > dbc, "通知数据同步失败");
    }

    /**
     * 红点接口获取的入力数据包含特殊字符
     */
    @Test
    void T1084() throws Exception {
        log.info("T1084");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setNotificationsApiGetNotifications("shengdong/api/notifications?id=4");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        daoNotification.truncateTable();
        int dbc = daoNotification.getAllNotifications(h.getProject()).size();
        Bt14User uent = new Bt14User();
        synservice.synNotifications(cnf, h, uent);
        int dbc1 = daoNotification.getAllNotifications(h.getProject()).size();
        Assert.isTrue(dbc1 > dbc, "通知数据同步失败");
    }

    /**
     * 红点api服务未启动，请求超时
     */
    @Test
    void T1085() {
        log.info("T1085");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8889");
        cnf.setNotificationsApiGetNotifications("shengdong/api/notifications?id=5");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        try {
            daoNotification.truncateTable();
            int dbc = daoNotification.getAllNotifications(h.getProject()).size();
            Bt14User uent = new Bt14User();
            synservice.synNotifications(cnf, h, uent);
            int dbc1 = daoNotification.getAllNotifications(h.getProject()).size();
            Assert.isTrue(dbc1 > dbc, "通知数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 红点api接口endpoint地址错误
     */
    @Test
    void T1086() {
        log.info("T1086");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setNotificationsApiGetNotifications("shengdong/api/not?id=6");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        try {
            daoNotification.truncateTable();
            int dbc = daoNotification.getAllNotifications(h.getProject()).size();
            Bt14User uent = new Bt14User();
            synservice.synNotifications(cnf, h, uent);
            int dbc1 = daoNotification.getAllNotifications(h.getProject()).size();
            Assert.isTrue(dbc1 > dbc, "通知数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 红点api接口返回错误404
     */
    @Test
    void T1087() {
        log.info("T1087");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setNotificationsApiGetNotifications("shengdong/api/notifications?id=7");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        try {
            daoNotification.truncateTable();
            int dbc = daoNotification.getAllNotifications(h.getProject()).size();
            Bt14User uent = new Bt14User();
            synservice.synNotifications(cnf, h, uent);
            int dbc1 = daoNotification.getAllNotifications(h.getProject()).size();
            Assert.isTrue(dbc1 > dbc, "通知数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 红点api接口返回错误422
     */
    @Test
    void T1088() {
        log.info("T1088");
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setNotificationsApiGetNotifications("shengdong/api/notifications?id=8");
        Header h = new Header();
        h.setProject("");
        h.setEmail("");
        h.setToken("");
        try {
            daoNotification.truncateTable();
            int dbc = daoNotification.getAllNotifications(h.getProject()).size();
            Bt14User uent = new Bt14User();
            synservice.synNotifications(cnf, h, uent);
            int dbc1 = daoNotification.getAllNotifications(h.getProject()).size();
            Assert.isTrue(dbc1 > dbc, "通知数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Autowired
    IBt41PositionDao daoPostion;

    /**
     * 红点接口获取的入力数据为空
     *
     * @throws Exception
     */
    @Test
    void T1089() throws Exception {
        log.info("T1089");
        Bt31Project ent31 = new Bt31Project();
        ent31.setProjectUid("");
        Bt32Location ent32 = new Bt32Location();
        ent32.setLocationUid("");
        ent32.setProjectUid("");
        Bt33SubLocation ent33 = new Bt33SubLocation();
        ent33.setLocationUid("");
        ent33.setSublocationUid("");
        Bt23Trackable ent23 = new Bt23Trackable();
        ent23.setTrackableUid("");
        ent23.setSublocationUid("");
        ent23.setProjectUid("");
        daotrackable.truncateTable();
        dao_proj.trancateTable();
        daosublocation.truncateTable();
        dao_location.truncateTable();
        daoPostion.truncateTable();

        daotrackable.insert(ent23);
        dao_proj.insert(ent31);
        daosublocation.insert(ent33);
        dao_location.insert(ent32);
        ApplicationHome home = new ApplicationHome(getClass());
        //ClassPathResource classPathResource = new ClassPathResource("testData/position_history_t0001.zip");
        String src1 = "src/main/resources/testData/position_history_t0001.zip";//classPathResource.getPath();
        String src = "src/main/resources/testData/position_history_t0001_tmp.zip";//classPathResource.getPath();
        Files.copy(Path.of(src1), Path.of(src), StandardCopyOption.REPLACE_EXISTING);
        String des = home + "/target/classes/download/" + UUID.randomUUID().toString();
        int dbc = daoPostion.getAllPostion("").size();
        syndata.extracted(src, des);
        int dbc1 = daoPostion.getAllPostion("").size();
        Assert.isTrue(dbc1 > dbc, "历史数据同步失败");
    }

    /**
     * 红点接口获取的入力数据为数据库字段最大值 包含英文大小写及汉字
     *
     * @throws Exception
     */
    @Test
    void T1090() throws Exception {
        log.info("T1090");
        String puid = "TdIGonlEFcNndxxcgUKyhY";
        String luid = puid;
        String sluid = puid;
        String tuid = puid;
        Bt31Project ent31 = new Bt31Project();
        ent31.setProjectUid(puid);
        Bt32Location ent32 = new Bt32Location();
        ent32.setLocationUid(luid);
        ent32.setProjectUid(puid);
        Bt33SubLocation ent33 = new Bt33SubLocation();
        ent33.setLocationUid(luid);
        ent33.setSublocationUid(sluid);
        Bt23Trackable ent23 = new Bt23Trackable();
        ent23.setTrackableUid(tuid);
        ent23.setSublocationUid(sluid);
        ent23.setProjectUid(puid);
        daotrackable.truncateTable();
        dao_proj.trancateTable();
        daosublocation.truncateTable();
        dao_location.truncateTable();
        daoPostion.truncateTable();

        daotrackable.insert(ent23);
        dao_proj.insert(ent31);
        daosublocation.insert(ent33);
        dao_location.insert(ent32);
        ApplicationHome home = new ApplicationHome(getClass());
        //        ClassPathResource classPathResource = new ClassPathResource("testData/position_history_t0001.zip");
        String src1 = "src/main/resources/testData/position_history_t0002.zip";//classPathResource.getPath();
        String src = "src/main/resources/testData/position_history_t0002_tmp.zip";//classPathResource.getPath();
        Files.copy(Path.of(src1), Path.of(src), StandardCopyOption.REPLACE_EXISTING);
        String des = home + "/target/classes/download/" + UUID.randomUUID().toString();
        int dbc = daoPostion.getAllPostion(puid).size();
        syndata.extracted(src, des);
        int dbc1 = daoPostion.getAllPostion(puid).size();
        Assert.isTrue(dbc1 > dbc, "历史数据同步失败");
    }

    /**
     * 红点接口获取的入力数据为数据库字段要求内的值
     *
     * @throws Exception
     */
    @Test
    void T1091() throws Exception {
        log.info("T1091");
        String puid = "_VmsjmCxSDysnNBNu5mB2w";
        String luid = puid;
        String sluid = puid;
        String tuid = puid;
        Bt31Project ent31 = new Bt31Project();
        ent31.setProjectUid(puid);
        Bt32Location ent32 = new Bt32Location();
        ent32.setLocationUid(luid);
        ent32.setProjectUid(puid);
        Bt33SubLocation ent33 = new Bt33SubLocation();
        ent33.setLocationUid(luid);
        ent33.setSublocationUid(sluid);
        Bt23Trackable ent23 = new Bt23Trackable();
        ent23.setTrackableUid(tuid);
        ent23.setSublocationUid(sluid);
        ent23.setProjectUid(puid);
        daotrackable.truncateTable();
        dao_proj.trancateTable();
        daosublocation.truncateTable();
        dao_location.truncateTable();
        daoPostion.truncateTable();

        daotrackable.insert(ent23);
        dao_proj.insert(ent31);
        daosublocation.insert(ent33);
        dao_location.insert(ent32);
        ApplicationHome home = new ApplicationHome(getClass());
        //        ClassPathResource classPathResource = new ClassPathResource("testData/position_history_t0001.zip");
        String src1 = "src/main/resources/testData/position_history_t0003.zip";//classPathResource.getPath();
        String src = "src/main/resources/testData/position_history_t0003_tmp.zip";//classPathResource.getPath();
        Files.copy(Path.of(src1), Path.of(src), StandardCopyOption.REPLACE_EXISTING);
        String des = home + "/target/classes/download/" + UUID.randomUUID().toString();
        int dbc = daoPostion.getAllPostion(puid).size();
        syndata.extracted(src, des);
        int dbc1 = daoPostion.getAllPostion(puid).size();
        Assert.isTrue(dbc1 > dbc, "历史数据同步失败");
    }

    /**
     * 红点接口获取的入力数据包含特殊字符
     *
     * @throws Exception
     */
    @Test
    void T1092() throws Exception {
        log.info("T1092");
        String puid = "<hr>";
        String luid = puid;
        String sluid = puid;
        String tuid = puid;
        Bt31Project ent31 = new Bt31Project();
        ent31.setProjectUid(puid);
        Bt32Location ent32 = new Bt32Location();
        ent32.setLocationUid(luid);
        ent32.setProjectUid(puid);
        Bt33SubLocation ent33 = new Bt33SubLocation();
        ent33.setLocationUid(luid);
        ent33.setSublocationUid(sluid);
        Bt23Trackable ent23 = new Bt23Trackable();
        ent23.setTrackableUid(tuid);
        ent23.setSublocationUid(sluid);
        ent23.setProjectUid(puid);
        daotrackable.truncateTable();
        dao_proj.trancateTable();
        daosublocation.truncateTable();
        dao_location.truncateTable();
        daoPostion.truncateTable();

        daotrackable.insert(ent23);
        dao_proj.insert(ent31);
        daosublocation.insert(ent33);
        dao_location.insert(ent32);
        ApplicationHome home = new ApplicationHome(getClass());
        //        ClassPathResource classPathResource = new ClassPathResource("testData/position_history_t0001.zip");
        String src1 = "src/main/resources/testData/position_history_t0004.zip";//classPathResource.getPath();
        String src = "src/main/resources/testData/position_history_t0004_tmp.zip";//classPathResource.getPath();
        Files.copy(Path.of(src1), Path.of(src), StandardCopyOption.REPLACE_EXISTING);
        String des = home + "/target/classes/download/" + UUID.randomUUID().toString();
        int dbc = daoPostion.getAllPostion(puid).size();
        syndata.extracted(src, des);
        int dbc1 = daoPostion.getAllPostion(puid).size();
        Assert.isTrue(dbc1 > dbc, "历史数据同步失败");
    }

    /**
     * 红点api服务未启动，请求超时
     *
     * @throws Exception
     */
    @Test
    void T1093() {
        log.info("T1093");
        Bt50PoinsHisView cnfitem = new Bt50PoinsHisView();
        cnfitem.setEmail("");
        cnfitem.setToken("");
        cnfitem.setProjectUid("");
        cnfitem.setLastpointsynAt(new Date());
        IotApiConfig cnf = new IotApiConfig();
        cnf.setServerIp("192.168.1.9:8889");
        cnf.setPositionHistoryApiPositionsHistory("shengdong/api/positions_history?id=5");
        try {
            daoPostion.truncateTable();
            int dbc = daoPostion.getAllPostion(cnfitem.getProjectUid()).size();
            syndata.synProjHispoint(cnfitem, cnf);
            int dbc1 = daoPostion.getAllPostion(cnfitem.getProjectUid()).size();
            Assert.isTrue(dbc1 > dbc, "历史数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 红点api接口endpoint地址错误
     */
    @Test
    void T1094() {
        log.info("T1094");
        Bt50PoinsHisView cnfitem = new Bt50PoinsHisView();
        cnfitem.setEmail("");
        cnfitem.setToken("");
        cnfitem.setProjectUid("");
        cnfitem.setLastpointsynAt(new Date());
        IotApiConfig cnf = new IotApiConfig();
        cnf.setSsl(false);
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setPositionHistoryApiPositionsHistory("shengdong/api/pos?id=6");
        try {
            daoPostion.truncateTable();
            int dbc = daoPostion.getAllPostion(cnfitem.getProjectUid()).size();
            syndata.synProjHispoint(cnfitem, cnf);
            int dbc1 = daoPostion.getAllPostion(cnfitem.getProjectUid()).size();
            Assert.isTrue(dbc1 > dbc, "历史数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 红点api接口返回错误404
     */
    @Test
    void T1095() {
        log.info("T1095");
        Bt50PoinsHisView cnfitem = new Bt50PoinsHisView();
        cnfitem.setEmail("");
        cnfitem.setToken("");
        cnfitem.setProjectUid("");
        cnfitem.setLastpointsynAt(new Date());
        IotApiConfig cnf = new IotApiConfig();
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setSsl(false);
        cnf.setPositionHistoryApiPositionsHistory("shengdong/api/positions_history?id=7");
        try {
            daoPostion.truncateTable();
            int dbc = daoPostion.getAllPostion(cnfitem.getProjectUid()).size();
            syndata.synProjHispoint(cnfitem, cnf);
            int dbc1 = daoPostion.getAllPostion(cnfitem.getProjectUid()).size();
            Assert.isTrue(dbc1 > dbc, "历史数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 红点api接口返回错误422
     */
    @Test
    void T1096() {
        log.info("T1096");
        Bt50PoinsHisView cnfitem = new Bt50PoinsHisView();
        cnfitem.setEmail("");
        cnfitem.setToken("");
        cnfitem.setProjectUid("");
        cnfitem.setLastpointsynAt(new Date());
        IotApiConfig cnf = new IotApiConfig();
        cnf.setServerIp("192.168.1.9:8888");
        cnf.setSsl(false);
        cnf.setPositionHistoryApiPositionsHistory("shengdong/api/positions_history?id=8");
        try {
            daoPostion.truncateTable();
            int dbc = daoPostion.getAllPostion(cnfitem.getProjectUid()).size();
            syndata.synProjHispoint(cnfitem, cnf);
            int dbc1 = daoPostion.getAllPostion(cnfitem.getProjectUid()).size();
            Assert.isTrue(dbc1 > dbc, "历史数据同步失败");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
