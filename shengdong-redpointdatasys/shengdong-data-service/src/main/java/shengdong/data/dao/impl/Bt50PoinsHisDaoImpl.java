package shengdong.data.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import shengdong.data.dao.IBt50PoinsHisDao;
import shengdong.data.entity.BmColumn;
import shengdong.data.entity.Bt50PoinsHisView;
import shengdong.data.mapper.Bt50PoinsHisMapper;

import java.util.Date;
import java.util.List;

@Repository
public class Bt50PoinsHisDaoImpl implements IBt50PoinsHisDao {
    @Autowired
    Bt50PoinsHisMapper mapper;

    /**
     * 历史数据文件导入
     *
     * @param absolutePath
     */
    @Override
    public void insertBatchByLoadData(String absolutePath) {
        mapper.insertBatchByLoadData(absolutePath);
        mapper.insertToPosition();
        mapper.truncateHistory();
    }

    /**
     * 查询历史数据参数配置项
     *
     * @return
     */
    @Override
    public List<Bt50PoinsHisView> selectCnf() {
        return mapper.selectCnf();
    }

    /**
     * 更新定位历史同步时间
     */
    @Override
    public void flushPointDate(Date date, String uid) {
        BmColumn i = new BmColumn();
        i.setC1(uid);
        i.setDt1(date);
        mapper.updateSynPostionDate(i);
    }

    /**
     * 更新报警历史同步时间
     *
     * @param date
     */
    @Override
    public void flushAlarmDate(Date date, String uid) {
        BmColumn i = new BmColumn();
        i.setC1(uid);
        i.setDt2(date);
        mapper.updateSynAlarmDate(i);
    }

    /**
     * 更新区域事件历史同步时间
     *
     * @param date
     */
    @Override
    public void flushZoneDate(Date date, String uid) {
        BmColumn i = new BmColumn();
        i.setC1(uid);
        i.setDt3(date);
        mapper.updateSynZoneDate(i);
    }
}
