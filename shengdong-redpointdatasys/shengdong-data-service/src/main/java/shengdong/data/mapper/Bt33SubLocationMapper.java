package shengdong.data.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import shengdong.data.entity.Bt33SubLocation;

import java.util.List;

/**
 * 地点子区域表Mapper映射方法接口
 */
@Mapper
@Component
public interface Bt33SubLocationMapper {

    /**
     * 根据id删除地点子区域表数据
     *
     * @param id 自增主键
     * @return 删除行数
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 新增地点子区域表数据
     *
     * @param record 地点子区域实体类
     * @return 新增行数
     */
    int insert(Bt33SubLocation record);

    /**
     * 新增地点子区域表数据(根据一定规则)
     *
     * @param record 地点子区域实体类
     * @return 新增行数
     */
    int insertSelective(Bt33SubLocation record);

    /**
     * 根据id查询地点子区域表数据
     *
     * @param id 地点子区域实体类
     * @return 地点子区域实体类
     */
    Bt33SubLocation selectByPrimaryKey(Long id);

    /**
     * 更新地点子区域表数据(根据一定规则)
     *
     * @param record 地点子区域实体类
     * @return 更新行数
     */
    int updateByPrimaryKeySelective(Bt33SubLocation record);

    /**
     * 更新地点子区域表数据
     *
     * @param record 地点子区域实体类
     * @return 更新行数
     */
    int updateByPrimaryKey(Bt33SubLocation record);

    List<Bt33SubLocation> selectByUserid(Long id);

    void truncateTable();

    List<Bt33SubLocation> selectAllSublocation();

    List<Bt33SubLocation> selectByLocationUid(String locationUid);
}