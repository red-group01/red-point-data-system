package shengdong.redpoint.api;

import shengdong.common.util.JSONUtil;
import shengdong.redpoint.httpentity.response.ResponseNearMissEventsReport;
import shengdong.redpoint.httpentity.response.ResponseNearMissEventsReportStatus;
import shengdong.redpoint.model.query.Header;
import shengdong.redpoint.model.query.HttpMethod;
import shengdong.redpoint.model.query.HttpParam;

import java.util.Date;

public class IotNearMissEvents extends IotApiBase {

    public IotNearMissEvents(IotApiConfig cnf, Header httpheader) {
        super(cnf, httpheader);
    }

    /**
     * 创建近距离事件报告
     *
     * @param to
     * @param from
     * @param distance
     * @param sublocation_uid
     * @return
     * @throws Exception
     */
    public String createNearMissEventsReport(Date to, Date from, String distance, String sublocation_uid)
        throws Exception {
        HttpParam hp = new HttpParam();
        String url = config.getDomen() + config.getNearMissEventsApiCreateNearMissEventsReport();
        hp.setUrl(url);
        hp.setHeader(header);
        hp.setPostParam(
            "{\"sublocation_uid\":\"" + sublocation_uid + "\",\"from\":" + from.getTime() + ",\"to\":" + to.getTime() + ",\"distance\":" + distance + "}");
        hp.setMethod(HttpMethod.POST);
        String msg = HttpClient.ExecApi(hp);
        return msg;
    }

    /**
     * 获取事件报告状态
     *
     * @param uid
     * @return
     * @throws Exception
     */
    public ResponseNearMissEventsReportStatus getNearMissEventsReportStatus(String uid) throws Exception {
        HttpParam hp = new HttpParam();
        String url = config.getDomen() + config.getNearMissEventsApiGetNearMissEventsReportStatus() + uid;
        hp.setUrl(url);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.GET);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseNearMissEventsReportStatus.class);
    }

    /**
     * 获取事件报告
     *
     * @param uid
     * @return
     * @throws Exception
     */
    public ResponseNearMissEventsReport getNearMissEventsReport(String uid) throws Exception {
        HttpParam hp = new HttpParam();
        String url = config.getDomen() + config.getNearMissEventsApiCreateNearMissEventsReport() + uid;
        hp.setUrl(url);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.GET);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseNearMissEventsReport.class);
    }

}
