package shengdong.data.dao;

import shengdong.data.entity.Bt44Alarm;

import java.util.Date;
import java.util.List;

/**
 * 设备告警Dao接口
 */
public interface IBt44AlarmDao {
    /**
     * 插入设备告警数据
     *
     * @param record 设备告警实体类
     * @return 插入的行数
     */
    public int insert(Bt44Alarm record);

    void truncateTable();

    List<Bt44Alarm> getAllAlarmHis(String project);

    /**
     * 根据地点子区域uid查询设备告警表数据
     *
     * @param sublocationUid 地点子区域uid
     * @return 设备告警实体类列表
     */
    List<Bt44Alarm> selectBySublocationUid(String sublocationUid, String createdAtBegin, String createdAtEnd);

    /**
     * 根据标签uid和报警时间查询设备告警表数据
     *
     * @param bt44Alarm
     * @return 设备告警实体类列表
     */
    List<Bt44Alarm> selectByObjAndTime(Bt44Alarm bt44Alarm);
}
