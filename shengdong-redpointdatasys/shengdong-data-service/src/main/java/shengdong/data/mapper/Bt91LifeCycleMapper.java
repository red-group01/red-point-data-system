package shengdong.data.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import shengdong.data.entity.Bt91LifeCycle;

/**
 * 数据生命周期表Mapper映射方法接口
 */
@Mapper
@Component
public interface Bt91LifeCycleMapper {

    /**
     * 根据id删除数据生命周期表数据
     *
     * @param id 自增主键
     * @return 删除行数
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 新增数据生命周期表数据
     *
     * @param record 数据生命周期实体类
     * @return 新增行数
     */
    int insert(Bt91LifeCycle record);

    /**
     * 新增数据生命周期表数据(根据一定规则)
     *
     * @param record 数据生命周期实体类
     * @return 新增行数
     */
    int insertSelective(Bt91LifeCycle record);

    /**
     * 根据id查询数据生命周期表数据
     *
     * @param id 数据生命周期实体类
     * @return 数据生命周期实体类
     */
    Bt91LifeCycle selectByPrimaryKey(Long id);

    /**
     * 更新数据生命周期表数据(根据一定规则)
     *
     * @param record 数据生命周期实体类
     * @return 更新行数
     */
    int updateByPrimaryKeySelective(Bt91LifeCycle record);

    /**
     * 更新数据生命周期表数据
     *
     * @param record 数据生命周期实体类
     * @return 更新行数
     */
    int updateByPrimaryKey(Bt91LifeCycle record);
}