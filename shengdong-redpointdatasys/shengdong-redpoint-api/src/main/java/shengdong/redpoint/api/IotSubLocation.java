package shengdong.redpoint.api;

import com.google.common.base.Strings;
import shengdong.common.util.JSONUtil;
import shengdong.redpoint.httpentity.request.RequestSublocations;
import shengdong.redpoint.httpentity.response.ResponseSublocations;
import shengdong.redpoint.httpentity.response.SublocationsItem;
import shengdong.redpoint.model.query.Header;
import shengdong.redpoint.model.query.HttpMethod;
import shengdong.redpoint.model.query.HttpParam;

public class IotSubLocation extends IotApiBase {

    public IotSubLocation(IotApiConfig cnf, Header httpheader) {
        super(cnf, httpheader);
    }

    /**
     * 获取全部区域
     *
     * @return
     * @throws Exception
     */
    public ResponseSublocations getArea() throws Exception {
        return getSingleArea("");
    }

    /**
     * 获取指定区域
     *
     * @param uid
     * @return
     * @throws Exception
     */
    public ResponseSublocations getSingleArea(String uid) throws Exception {
        HttpParam hp = new HttpParam();
        String url = config.getDomen();
        if (!Strings.isNullOrEmpty(uid)) {
            url += config.getSublocationsApiGetSingleArea() + uid;
        } else {
            url += config.getSublocationsApiGetArea();
        }
        hp.setUrl(url);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.GET);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), ResponseSublocations.class);
    }

    /**
     * 创建子区域
     *
     * @param req
     * @return
     * @throws Exception
     */
    public SublocationsItem createArea(RequestSublocations req) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getSublocationsApiCreateArea());
        hp.setHeader(header);
        hp.setPostParam(JSONUtil.getObjectToString(req));
        hp.setMethod(HttpMethod.POST);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), SublocationsItem.class);
    }

    /**
     * 修改子区域
     *
     * @param req
     * @param uid
     * @return
     * @throws Exception
     */
    public SublocationsItem editArea(RequestSublocations req, String uid) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getSublocationsApiEditArea() + uid);
        hp.setHeader(header);
        hp.setPostParam(JSONUtil.getObjectToString(req));
        hp.setMethod(HttpMethod.PUT);
        return JSONUtil.getJsonToBean(HttpClient.ExecApi(hp), SublocationsItem.class);
    }

    /**
     * 删除子区域
     *
     * @param uid
     * @return
     * @throws Exception
     */
    public String deleteArea(String uid) throws Exception {
        HttpParam hp = new HttpParam();
        hp.setUrl(config.getDomen() + config.getSublocationsApiDeleteArea() + uid);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.DELETE);
        String msg = HttpClient.ExecApi(hp);
        return msg;
    }
}
