package shengdong.data.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import shengdong.data.entity.Bt34Zone;

import java.util.List;

/**
 * 区域围栏表Mapper映射方法接口
 */
@Mapper
@Component
public interface Bt34ZoneMapper {

    /**
     * 根据id删除区域围栏表数据
     *
     * @param id 自增主键
     * @return 删除行数
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 新增区域围栏表数据
     *
     * @param record 区域围栏实体类
     * @return 新增行数
     */
    int insert(Bt34Zone record);

    /**
     * 新增区域围栏表数据(根据一定规则)
     *
     * @param record 区域围栏实体类
     * @return 新增行数
     */
    int insertSelective(Bt34Zone record);

    /**
     * 根据id查询区域围栏表数据
     *
     * @param id 区域围栏实体类
     * @return 区域围栏实体类
     */
    Bt34Zone selectByPrimaryKey(Long id);

    /**
     * 更新区域围栏表数据(根据一定规则)
     *
     * @param record 区域围栏实体类
     * @return 更新行数
     */
    int updateByPrimaryKeySelective(Bt34Zone record);

    /**
     * 更新区域围栏表数据
     *
     * @param record 区域围栏实体类
     * @return 更新行数
     */
    int updateByPrimaryKey(Bt34Zone record);

    List<Bt34Zone> selectByUserId(Long id);

    List<Bt34Zone> selectAllZone();

    void truncateTable();

    /**
     * 根据地点子区域uid查询区域围栏表数据
     *
     * @param sublocationUid 地点子区域uid
     * @return 区域围栏实体类列表
     */
    List<Bt34Zone> selectBySublocationUid(String sublocationUid);

    /**
     * 根据可追踪对象uid查询区域围栏表数据
     *
     * @param trackableUids 可追踪对象uid集合
     * @return 区域围栏实体类列表
     */
    List<Bt34Zone> selectInTrackableUid(List<String> trackableUids);
}