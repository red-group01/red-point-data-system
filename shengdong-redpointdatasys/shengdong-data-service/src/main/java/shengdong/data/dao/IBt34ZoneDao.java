package shengdong.data.dao;

import shengdong.data.entity.Bt34Zone;

import java.util.List;

/**
 * 区域围栏Dao接口
 */
public interface IBt34ZoneDao {
    /**
     * 插入区域围栏数据
     *
     * @param record 区域围栏实体类
     * @return 插入的行数
     */
    public int insert(Bt34Zone record);

    List<Bt34Zone> getUserZone(Long id);

    int updateByid(Bt34Zone local);

    List<Bt34Zone> getAllZone();

    void truncateTable();

    /**
     * 根据地点子区域uid查询区域围栏表数据
     *
     * @param sublocationUid 地点子区域uid
     * @return 区域围栏实体类列表
     */
    List<Bt34Zone> selectBySublocationUid(String sublocationUid);

    /**
     * 根据可追踪对象uid查询区域围栏表数据
     *
     * @param trackableUids 可追踪对象uid集合
     * @return 区域围栏实体类列表
     */
    List<Bt34Zone> selectInTrackableUid(List<String> trackableUids);

}
