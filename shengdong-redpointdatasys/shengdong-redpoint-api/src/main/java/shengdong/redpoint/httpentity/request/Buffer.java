package shengdong.redpoint.httpentity.request;

import lombok.Data;

@Data
public class Buffer {
    private boolean active;
    private String shape;
    private Geometry geometry;
}
