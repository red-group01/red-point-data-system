package shengdong.data.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import shengdong.data.entity.Bt32Location;

import java.util.List;

/**
 * 项目地点表Mapper映射方法接口
 */
@Mapper
@Component
public interface Bt32LocationMapper {

    /**
     * 根据id删除项目地点表数据
     *
     * @param id 自增主键
     * @return 删除行数
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 新增项目地点表数据
     *
     * @param record 项目地点实体类
     * @return 新增行数
     */
    int insert(Bt32Location record);

    /**
     * 新增项目地点表数据(根据一定规则)
     *
     * @param record 项目地点实体类
     * @return 新增行数
     */
    int insertSelective(Bt32Location record);

    /**
     * 根据id查询项目地点表数据
     *
     * @param id 项目地点实体类
     * @return 项目地点实体类
     */
    Bt32Location selectByPrimaryKey(Long id);

    /**
     * 更新项目地点表数据(根据一定规则)
     *
     * @param record 项目地点实体类
     * @return 更新行数
     */
    int updateByPrimaryKeySelective(Bt32Location record);

    /**
     * 更新项目地点表数据
     *
     * @param record 项目地点实体类
     * @return 更新行数
     */
    int updateByPrimaryKey(Bt32Location record);

    List<Bt32Location> selectByUserId(Long id);

    List<Bt32Location> selectAllLocation();

    void truncateTable();
}