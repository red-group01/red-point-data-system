package shengdong.data.entity.view;

import lombok.Data;

@Data
public class LocationConfig {
    private String x;
    private String y;
}
