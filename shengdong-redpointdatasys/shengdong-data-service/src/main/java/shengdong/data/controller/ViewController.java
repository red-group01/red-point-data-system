package shengdong.data.controller;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import shengdong.common.config.ViewPageConfig;
import shengdong.common.error.impl.BaseResponse;
import shengdong.data.dao.IBt34ZoneDao;
import shengdong.data.dao.IBt41PositionDao;
import shengdong.data.entity.Activealarm;
import shengdong.data.entity.Bt34Zone;
import shengdong.data.entity.Bt41Position;
import shengdong.data.entity.Bt44Alarm;
import shengdong.data.entity.view.*;
import shengdong.data.service.IViewService;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/view")
public class ViewController {
    @Autowired
    IViewService viewService;
    //    @Autowired
//    ViewPageConfig viewPageConfig;
    @Autowired
    IBt41PositionDao positionDao;

    @Autowired
    IBt34ZoneDao bt34ZoneDao;

    @GetMapping("/test")
    public BaseResponse<Bt34Zone> test() {
        List<Bt34Zone> zoneList = bt34ZoneDao.getAllZone();
        return BaseResponse.success(zoneList);
        //return null;
    }

    /**
     * 获取告警列表
     *
     * @param uid
     * @return
     */
    @GetMapping("/getalarmlist")
    @CrossOrigin(origins = "*")
    public BaseResponse<OutAlarmListView> getAlarmList(String uid) {
        List<OutAlarmListView> outAlarmListViews = viewService.getAlarmListView(uid);
        return BaseResponse.success(outAlarmListViews);
    }

    /**
     * 获取标签列表
     *
     * @param uid
     * @return
     */
    @GetMapping("/getnodelist")
    @CrossOrigin(origins = "*")
    public BaseResponse<OutNodeListView> getNodeList(String uid) {
        List<OutNodeListView> outNodeListViews = viewService.getNodeListView(uid);
        return BaseResponse.success(outNodeListViews);
    }

    /**
     * 获取围栏列表
     *
     * @param uid
     * @return
     */
    @GetMapping("/getzonelist")
    @CrossOrigin(origins = "*")
    public BaseResponse<OutZoneListView> getZoneList(String uid) {
        List<OutZoneListView> outZoneListViews = viewService.getZoneListView(uid);
        return BaseResponse.success(outZoneListViews);
    }

    /**
     * 获取设备状态列表
     *
     * @param uid
     * @return
     */
    @GetMapping("/getnodestatelist")
    @CrossOrigin(origins = "*")
    public BaseResponse<OutZoneListView> getNodeStateList(String uid) {
        List<OutNodeStateListView> outNodeStateListViews = viewService.getNodeStateListView(uid);
        return BaseResponse.success(outNodeStateListViews);
    }

    /**
     * 获取设备数量统计列表
     *
     * @param uid
     * @return
     */
    @GetMapping("/getnodecountlist")
    @CrossOrigin(origins = "*")
    public BaseResponse<OutNodeCountView> getNodeCountListView(String uid) {
        OutNodeCountView outNodeCountViews = viewService.getNodeCountListView(uid);
        return BaseResponse.success(outNodeCountViews);
    }

    /**
     * 获取热力图数据
     *
     * @param uid
     * @return
     */
    @GetMapping("/gethotimglist")
    @CrossOrigin(origins = "*")
    public BaseResponse<OutHotImageView> getHotImgView(String uid) {
        OutHotImageView outHotImageView = viewService.getHotImgView(uid);
        return BaseResponse.success(outHotImageView);
    }

    /**
     * 获取基础数据
     *
     * @param
     * @return
     */
    @GetMapping("/getbaseinfolist")
    @CrossOrigin(origins = "*")
    public BaseResponse<OutBaseInfoView> getBaseInfoView() {
        List<OutBaseInfoView> outBaseInfoViews = viewService.getBaseInfoView();
        return BaseResponse.success(outBaseInfoViews);
    }

    /**
     * 获取历史数据
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/gethisdatalist", method = RequestMethod.POST)
    @ResponseBody
    @CrossOrigin(origins = "*")
    public BaseResponse<OutHisDataView> getHisDataView(@RequestBody HisDataParam hisDataParam) {
        OutHisDataView outHisDataView = viewService.getHisDataView(hisDataParam.getSublocationuid(), hisDataParam.getBegintime(), hisDataParam.getEndtime(), hisDataParam.getUid());
        return BaseResponse.success(outHisDataView);
    }

    /**
     * 获取页面配置信息
     *
     * @return
     */
    @GetMapping("/getpageconfig")
    @CrossOrigin(origins = "*")
    public BaseResponse<OutCameraConfigView> getPageConfig() {
//        ViewPageConfig viewPageConfigOut = new ViewPageConfig();
//        BeanUtils.copyProperties(viewPageConfig, viewPageConfigOut);
//        return BaseResponse.success(viewPageConfigOut);
        List<OutCameraConfigView> outCameraConfigViews = viewService.getCameraConfigView();
        return BaseResponse.success(outCameraConfigViews);
    }

    /**
     * 获取活动的告警信息，如果有新的告警信息会主动同步历史告警信息
     *
     * @return
     */
    @GetMapping("/getalarmactive")
    @CrossOrigin(origins = "*")
    public BaseResponse<Activealarm> getAlarmActiveView(String uid) {
        List<Activealarm> alarmList = viewService.getAlarmActiveView(uid);
        return BaseResponse.success(alarmList);
    }
}
