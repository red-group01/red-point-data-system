package shengdong.redpoint.httpentity.request;

import lombok.Data;

@Data
public class Schedule{
	private String startTime;
	private String stopTime;
}