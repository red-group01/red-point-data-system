package shengdong.data.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 数据字典表实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Bm01Dictionary {

    //自增主键
    private Long id;

    //数据字典唯一标识
    private String dictionaryUid;

    // 字典项分类代码
    private String code;

    // 字典项分类代码值
    private String value;

    //字典项分类代码名称
    private String name;

    //创建时间（UTC)
    private Date createdAt;

    //更新时间（UTC）
    private Date updatedAt;

    //数据最后更新者
    private String creatorAt;

    //数据信息备注
    private String remark;

}