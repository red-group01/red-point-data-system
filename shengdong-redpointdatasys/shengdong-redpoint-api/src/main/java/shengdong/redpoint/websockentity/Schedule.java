package shengdong.redpoint.websockentity;

import lombok.Data;

@Data
public class Schedule{
	private String startTime;
	private String stopTime;
}