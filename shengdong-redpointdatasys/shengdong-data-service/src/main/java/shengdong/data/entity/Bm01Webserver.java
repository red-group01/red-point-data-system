package shengdong.data.entity;

import lombok.Data;

@Data
public class Bm01Webserver {
    /**
     * 自增主键
     */
    private Long id;

    /**
     * 服务器ip
     */
    private String serverIp;
}
