package shengdong.common.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Configuration//表明这是一个配置类
@ConfigurationProperties(prefix = "viewpage", ignoreInvalidFields = false)
//该注解用于绑定属性。prefix用来选择属性的前缀，也就是在remote.properties文件中的“remote”，ignoreUnknownFields是用来告诉SpringBoot在有属性不能匹配到声明的域时抛出异常。
@PropertySource(value = "classpath:config/viewpage.yml", factory = MyPropertySourceFactory.class)//配置文件路径，配置转换类
@Data//这个是一个lombok注解，用于生成getter&setter方法
@Component//标识为Bean
public class ViewPageConfig {
    CameraConfig camera;
}
