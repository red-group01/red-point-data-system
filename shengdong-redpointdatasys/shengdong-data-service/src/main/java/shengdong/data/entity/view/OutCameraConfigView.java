package shengdong.data.entity.view;

import lombok.Data;


@Data
public class OutCameraConfigView {
    private String uid;
    private String icoUid;
    private String name;
    private String type;
    private String appKey;
    private String appSecret;
    private String url;
    private LocationConfig location;
    private String url2;
    private String imageId;
    private String mac;
}
