package shengdong.redpoint.api;

import lombok.extern.slf4j.Slf4j;
import shengdong.redpoint.httpentity.response.ResponsePositionHistory;
import shengdong.redpoint.model.query.Header;
import shengdong.redpoint.model.query.HttpMethod;
import shengdong.redpoint.model.query.HttpParam;

import java.util.Date;

@Slf4j
public class IotPositionHistory extends IotApiBase {

    public IotPositionHistory(IotApiConfig cnf, Header httpheader) {
        super(cnf, httpheader);
    }

    /**
     * 下载文件
     *
     * @param sublocation_uid   子站点uid
     * @param from              开始时间
     * @param to                结束时间
     * @param trackable_objects 指定标签
     * @param page              页码
     * @return
     * @throws Exception
     */
    public ResponsePositionHistory downLoad(String sublocation_uid, Date from, Date to, String[] trackable_objects,
                                            Integer page, String savepath) throws Exception {
        String url = config.getDomen() + config.getPositionHistoryApiPositionsHistory();
        url +=
                "?sublocation_uid=" + sublocation_uid + "&from=" + from.getTime() + "&to=" + to.getTime() + "&page=" + page;
        //拼接指定标签参数
        if (trackable_objects != null) {
            for (String i : trackable_objects) {
                url += "&trackable_objects[]=" + i;
            }
        }
        HttpParam hp = new HttpParam();
        hp.setUrl(url);
        hp.setHeader(header);
        hp.setMethod(HttpMethod.GET);
        ResponsePositionHistory rph = HttpClient.downLoadFile(savepath, hp);
        return rph;
    }

    /**
     * 文件下载
     *
     * @param sublocation_uid 子站点uid
     * @param from            开始时间
     * @param to              结束时间
     * @param savepath        下载路径
     * @return
     */
    public ResponsePositionHistory downLoad(String sublocation_uid, Date from, Date to, String savepath)
            throws Exception {
        return downLoad(sublocation_uid, from, to, null, 0, savepath);
    }

    /**
     * 根据页码下载文件
     *
     * @param sublocation_uid
     * @param from
     * @param to
     * @param savepath
     * @param page
     * @return
     * @throws Exception
     */
    public ResponsePositionHistory downLoad(String sublocation_uid, Date from, Date to, String savepath, Integer page)
            throws Exception {
        return downLoad(sublocation_uid, from, to, null, page, savepath);
    }
}
