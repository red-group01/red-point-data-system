package shengdong.data.dao;

import shengdong.data.entity.Bt91LifeCycle;

/**
 * 数据生命周期Dao接口
 */
public interface IBt91LifeCycleDao {
    /**
     * 插入数据生命周期数据
     *
     * @param record 数据生命周期实体类
     * @return 插入的行数
     */
    public int insert(Bt91LifeCycle record);
}
