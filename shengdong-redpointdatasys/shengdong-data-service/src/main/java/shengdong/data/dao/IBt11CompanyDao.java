package shengdong.data.dao;

import shengdong.data.entity.Bt11Company;

import java.util.List;

/**
 * 公司Dao接口
 */
public interface IBt11CompanyDao {
    /**
     * 插入公司数据
     *
     * @param record 公司实体类
     * @return 插入的行数
     */
    public int insert(Bt11Company record);

    public Bt11Company getCompanyByUid(String uid);

    int update(Bt11Company entCompany);

    void truncateTable();

    List<Bt11Company> getAll();
}
