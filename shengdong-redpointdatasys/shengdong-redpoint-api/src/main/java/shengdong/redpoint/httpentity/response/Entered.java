package shengdong.redpoint.httpentity.response;

import lombok.Data;

@Data
public class Entered {
    private String time;
    private String x;
    private String y;
    private String z;
}
