package shengdong.data.dao;

import shengdong.data.entity.Bt14User;

import java.util.List;

/**
 * 用户Dao接口
 */
public interface IBt14UserDao {
    /**
     * 插入角色数据
     *
     * @param record 用户实体类
     * @return 插入的行数
     */
    public int insert(Bt14User record);

    /**
     * 获取全部用户数据
     *
     * @return
     */
    public List<Bt14User> getAll();

    /**
     * 更新用户数据
     *
     * @param user
     * @return
     */
    public int updateUser(Bt14User user);

    Bt14User getUserByEmail(String email);

    void truncateTable();
}
