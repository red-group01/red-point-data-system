package shengdong.redpoint.httpentity.response;

import java.util.List;
import lombok.Data;

@Data
public class ResponseNearMissEventsReport{
	private List<TrackableObjectsItem> trackableObjects;
	private List<EventsItem> events;
}