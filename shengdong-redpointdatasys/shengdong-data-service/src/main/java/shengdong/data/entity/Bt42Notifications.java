package shengdong.data.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 系统通知实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Bt42Notifications {

    //自增主键
    private Long id;

    //系统通知唯一标识
    private String notificationsUid;

    //项目唯一标识
    private String projectUid;

    //通知名称
    private String name;

    //通知的区域级别
    private Integer zoneSeverityLevel;

    //触发通知的报警类型列表
    private String alarmTypeList;

    //是否可用
    private Boolean active;

    //是否可以通过邮件收到通知
    private String typeEmail;

    //是否可以通过电话收到通知
    private String typePhone;

    //删除时间（UTC）
    private Date removedAt;

    //通知类别
    private String type;

    //系统通知的服务器标识id
    private Long notificationsId;

    //创建时间（UTC）
    private Date createdAt;

    //更新时间（UTC）
    private Date updatedAt;

    //数据最后更新者
    private String creatorAt;

    //数据信息备注
    private String remark;
    /**
     * 项目所属用户id
     */
    private Long userId;
    /**
     * 已删除
     */
    private Boolean deleteMark;

}