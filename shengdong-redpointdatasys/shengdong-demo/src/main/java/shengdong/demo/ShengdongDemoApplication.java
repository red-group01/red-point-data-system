package shengdong.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShengdongDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShengdongDemoApplication.class, args);
    }

}
