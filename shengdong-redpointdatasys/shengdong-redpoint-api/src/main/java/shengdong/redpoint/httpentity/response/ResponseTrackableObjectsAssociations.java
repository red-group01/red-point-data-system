package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ResponseTrackableObjectsAssociations {
    private String uid;
    @JsonProperty("removed_at")
    private String removedAt;
    @JsonProperty("mac_address")
    private String macAddress;
    @JsonProperty("trob_uid")
    private String trobUid;
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("obj_uid")
    private String objUid;

}
