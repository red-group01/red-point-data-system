package shengdong.data.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import shengdong.data.dao.ITestDao;
import shengdong.data.entity.TestTable;
import shengdong.data.service.ITestService;

@Service
public class TestServiceImpl implements ITestService {
    @Autowired
    private ITestDao iTestDao;
    @Override
    public TestTable listTestTableById(Integer id) {
        return iTestDao.listTestTableById(id);
    }

    @Override
    public int insertTestTable(TestTable testTable) {
        return iTestDao.insertTestTable(testTable);
    }
}
