package shengdong.data.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import shengdong.data.entity.BmColumn;
import shengdong.data.entity.Bt50PoinsHisView;

import java.util.List;

@Mapper
public interface Bt50PoinsHisMapper {
    /**
     * 历史数据文件导入
     *
     * @param absolutePath
     */
    void insertBatchByLoadData(@Param("path") String absolutePath);

    /**
     * 查询历史数据参数配置项
     *
     * @return
     */

    List<Bt50PoinsHisView> selectCnf();

    /**
     * 清空历史数据表
     */
    void truncateHistory();

    /**
     * 将文件数据整理到bt41position表
     */
    void insertToPosition();

    /**
     * 获取各子站点最后数据时间
     */

    List<BmColumn> selectLastDt();

    /**
     * 更新最后数据同步时间
     *
     * @param i
     */

    void updateSynPostionDate(BmColumn i);

    void updateSynAlarmDate(BmColumn i);

    void updateSynZoneDate(BmColumn i);
}
