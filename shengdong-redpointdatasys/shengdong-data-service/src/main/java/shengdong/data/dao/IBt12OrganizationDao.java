package shengdong.data.dao;

import shengdong.data.entity.Bt12Organization;

/**
 * 人员组织结构Dao接口
 */
public interface IBt12OrganizationDao {
    /**
     * 插入人员组织结构数据
     * @param record 人员组织结构实体类
     * @return 插入的行数
     */
    public int insert(Bt12Organization record);
}
