package shengdong.data.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 项目地点实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Bt32Location {

    //自增主键
    private Long id;

    //项目地点唯一标识
    private String locationUid;

    //项目唯一标识
    private String projectUid;

    //项目地点名称
    private String name;

    //描述
    private String description;

    //纬度
    private String latitude;

    //经度
    private String longitude;

    //高度
    private Integer elevation;

    //站点地址
    private String address;

    //站点地址2
    private String address2;

    //城市
    private String city;

    //国家
    private String state;

    //邮政编码
    private String zip;

    //邮政编码4
    private String zip4;

    //正北与场地坐标系之间的角度
    private Long orientation;

    //地区
    private String country;

    //公司唯一标识
    private String companyUid;

    //创建时间（UTC）
    private Date createdAt;

    //更新时间（UTC）
    private Date updatedAt;

    //数据最后更新者
    private String creatorAt;

    //数据信息备注
    private String remark;
    /**
     * 所属用户id
     */
    private long userId;
    /**
     * 已删除
     */
    private Boolean deleteMark;
}