package shengdong.redpoint.httpentity.response;

import lombok.Data;

@Data
public class HwInfo{
	private String fw;
	private int rev;
	private int ver;
	private String cbid;
	private String hw;
}