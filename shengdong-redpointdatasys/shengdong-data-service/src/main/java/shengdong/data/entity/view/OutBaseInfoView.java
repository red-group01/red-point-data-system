package shengdong.data.entity.view;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import shengdong.data.entity.Bt31Project;
import shengdong.data.entity.Bt32Location;
import shengdong.data.entity.Bt33SubLocation;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OutBaseInfoView {
    private String companyName;
    private String companyUid;
    private String userEmail;
    private String userPwd;
    private String userToken;
    private String serverIp;
    private List<Project> projects = new ArrayList<>();
    private List<OutCameraConfigView> cameraConfigViews = new ArrayList<>();
}
