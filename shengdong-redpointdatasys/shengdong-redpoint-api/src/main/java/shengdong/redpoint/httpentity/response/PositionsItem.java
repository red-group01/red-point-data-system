package shengdong.redpoint.httpentity.response;

import lombok.Data;

@Data
public class PositionsItem{
	private int x;
	private int y;
}