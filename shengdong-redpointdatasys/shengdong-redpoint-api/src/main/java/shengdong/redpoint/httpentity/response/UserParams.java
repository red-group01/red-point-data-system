package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class UserParams {
    private List<NotificationsItem> notifications;
    private String color;
    private String manager;
    private String address;
    private String timezone;
    @JsonProperty("custom_params")

    private String customParams;

}