package shengdong.redpoint.websockentity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class RequestDeleteParams {
    private List<String> data;
    private String authToken;
    private String userEmail;
    @JsonProperty("project_uid")
    private String projectUid;
    private String entity;
}