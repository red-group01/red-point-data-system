package shengdong.data.entity.view;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import shengdong.data.entity.Bt22Anchor;
import shengdong.data.entity.Bt23Trackable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SubLocation {
    //The identifier of the area in the context of the database.
    private Long id;

    //地点子区域唯一标识
    private String sublocationUid;

    //项目地点唯一标识
    private String locationUid;

    //地点子区域名称
    private String name;

    //描述
    private String description;

    //坐标偏移值x
    private Long offsetX;

    //坐标偏移值y
    private Long offsetY;

    //坐标偏移值z
    private Long offsetZ;

    //公司唯一标识
    private String companyUid;

    //项目唯一标识
    private String projectUid;

    //地点子区域的服务器标识id
    private Long sublocationId;

    //创建时间（UTC）
    private Date createdAt;

    //更新时间（UTC）
    private Date updatedAt;

    //数据最后更新者
    private String creatorAt;

    //数据信息备注
    private String remark;

    /**
     * 所属用户id
     */
    private long userId;
    /**
     * 已删除
     */
    private Boolean deleteMark;
    /**
     * 定位历史记录最后同步时间
     */
    private Date lastpointsynAt;
    /**
     * 警报历史记录最后同步时间
     */
    private Date lastalarmsynAt;
    /**
     * 区域事件最后同步时间
     */
    private Date lastalzonesynAt;
    private List<Bt22Anchor> anchors = new ArrayList<>();
    private List<Bt23Trackable> trackables = new ArrayList<>();
}
