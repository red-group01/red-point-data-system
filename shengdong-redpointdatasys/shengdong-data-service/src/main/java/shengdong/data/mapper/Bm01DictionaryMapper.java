package shengdong.data.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import shengdong.data.entity.Bm01Dictionary;

/**
 * 数据字典表Mapper映射方法接口
 */
@Mapper
@Component
public interface Bm01DictionaryMapper {
    /**
     * 根据id删除数据字典表数据
     *
     * @param id 自增主键
     * @return 删除行数
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 新增数据字典表数据
     *
     * @param record 数据字典实体类
     * @return 新增行数
     */
    int insert(Bm01Dictionary record);

    /**
     * 新增数据字典表数据(根据一定规则)
     *
     * @param record 数据字典实体类
     * @return 新增行数
     */
    int insertSelective(Bm01Dictionary record);

    /**
     * 根据id查询数据字典表数据
     *
     * @param id 数据字典实体类
     * @return 数据字典实体类
     */
    Bm01Dictionary selectByPrimaryKey(Long id);

    /**
     * 更新数据字典表数据(根据一定规则)
     *
     * @param record 数据字典实体类
     * @return 更新行数
     */
    int updateByPrimaryKeySelective(Bm01Dictionary record);

    /**
     * 更新数据字典表数据
     *
     * @param record 数据字典实体类
     * @return 更新行数
     */
    int updateByPrimaryKey(Bm01Dictionary record);
}