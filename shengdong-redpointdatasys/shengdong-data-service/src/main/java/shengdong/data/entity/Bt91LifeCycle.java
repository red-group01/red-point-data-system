package shengdong.data.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 数据生命周期表实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Bt91LifeCycle {

    //自增主键
    private Long id;

    //数据生命周期唯一标识，主键
    private String lifecycleUid;

    //公司唯一标识，外键
    private String companyUid;

    //项目唯一标识，外键
    private String projectUid;

    //对应数据表的类型
    private String type;

    //数据保存的生命周期具体数值
    private Integer cycle;

    //生命周期单位
    private String unit;

    //创建时间（UTC）
    private Date createdAt;

    //更新时间（UTC）
    private Date updatedAt;

    //数据最后更新者
    private String creatorAt;

    //数据信息备注
    private String remark;

}