package shengdong.data.dao;

import shengdong.data.entity.Bt21Node;

import java.util.List;

/**
 * 硬件设备节点Dao接口
 */
public interface IBt21NodeDao {

    /**
     * 插入硬件设备节点数据
     *
     * @param record 硬件设备节点实体类
     * @return 插入的行数
     */
    public int insert(Bt21Node record);

    List<Bt21Node> getUserNode(Long id);

    int updateByid(Bt21Node local);

    void truncateTable();

    List<Bt21Node> getAllNode();
}
