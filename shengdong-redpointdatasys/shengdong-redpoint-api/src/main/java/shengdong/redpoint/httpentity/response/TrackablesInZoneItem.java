package shengdong.redpoint.httpentity.response;

import lombok.Data;

@Data
public class TrackablesInZoneItem {
    private String uid;
    private Entered entered;
}
