package shengdong.redpoint.httpentity.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class NodesItem {
    @JsonProperty("last_heard")
    private String lastHeard;
    @JsonProperty("uwb_trx_config")
    private Object uwbTrxConfig;
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("project_uid")
    private String projectUid;
    @JsonProperty("current_system_timestamp")
    private String currentSystemTimestamp;
    private Battery battery;
    private Announce announce;
    @JsonProperty("sw_version")
    private SwVersion swVersion;
    @JsonProperty("parent_mac_address")
    private String parentMacAddress;
    private String uid;
    @JsonProperty("node_type")
    private String nodeType;
    @JsonProperty("updated_at")
    private String updatedAt;
    @JsonProperty("mac_address")
    private String macAddress;
    @JsonProperty("periferial_info")
    private Object periferialInfo;
    private String name;
    @JsonProperty("company_uid")
    private String companyUid;
    @JsonProperty("hw_info")
    private HwInfo hwInfo;
    @JsonProperty("bridge_mac_address")
    private String bridgeMacAddress;
    private NodeStatus status;
    @JsonProperty("node_id")
    private long nodeId;
}