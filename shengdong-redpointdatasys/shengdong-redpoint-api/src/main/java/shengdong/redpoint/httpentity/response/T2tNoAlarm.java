package shengdong.redpoint.httpentity.response;

import lombok.Data;

@Data
public class T2tNoAlarm {
    /**
     * 与可跟踪对象关联的MAC地址。(适用于stationary = false)
     */
    private String mac;
}
