package shengdong.common;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShengdongCommonApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShengdongCommonApplication.class, args);
    }

}
